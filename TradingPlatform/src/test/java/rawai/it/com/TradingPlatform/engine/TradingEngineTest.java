package rawai.it.com.TradingPlatform.engine;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;



public class TradingEngineTest {

	@Test
	public void testAgorithmStoping() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("0").withFilledMid("0", "0", "0", "0").build()).build());
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("5").withFilledMid("0", "0", "0", "16").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("6").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("7").withFilledMid("0", "0", "0", "50").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("8").withFilledMid("0", "0", "0", "52").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("9").withFilledMid("0", "0", "0", "50").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("10").withFilledMid("0", "0", "0", "50").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("11").withFilledMid("0", "0", "0", "54").build())
				.build();
		CandleDataPointInterface candle11 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("11").withFilledMid("0", "0", "0", "80").build())
				.build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);
		TradingSignal morethan20 = TradingSignalEnum.DELTA_MORE_THAN.getInstance(20);
		TradingSignal morethan1 = TradingSignalEnum.DELTA_MORE_THAN.getInstance(1);
		TradingSignal upwardsAny = TradingSignalEnum.UPWARD_ANY.getInstance();
		TradingSignal downwardsAny = TradingSignalEnum.DOWNWARD_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(morethan20);
		tradingSignals.add(morethan1);
		tradingSignals.add(upwardsAny);
		tradingSignals.add(downwardsAny);


		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);

		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		TradingInstrument instrument2 = new TradingInstrument();
		instrument2.setInstument("GER_TEE");
		instrument2.setUnits(400);
		instrument2.setTradingInterval(IntervalEnum.S5);

		tradingProfile.getTradingInstruments().add(instrument2);

		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile.fetchCandleList(instrument).addAll(candleList);

		TradingEngineContoller engine = new TradingEngineContoller();

		engine.processCandle(candle4, instrument);
		engine.processCandle(candle5, instrument);
		engine.processCandle(candle6, instrument);
		engine.processCandle(candle7, instrument);
		engine.processCandle(candle8, instrument);
		engine.processCandle(candle9, instrument);

		List<AlgorithmTrade> trades = engine.processCandle(candle10, instrument);
		engine.processCandle(candle11, instrument);

		assertThat(trades.get(0).isTrade()).isFalse();
	}

	@Test
	public void testAlgorithm1() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "40").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);
		TradingSignal downwardInversionAny = TradingSignalEnum.DOWNWARD_ANY.getInstance();
		TradingSignal downward5 = TradingSignalEnum.DOWNWARD_DELTA_MORE_THAN.getInstance(5);
		TradingSignal downwardAny = TradingSignalEnum.DOWNWARD_DELTA_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(downwardInversionAny);
		tradingSignals.add(downward5);
		tradingSignals.add(downwardAny);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);


		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile.fetchCandleList(instrument).addAll(candleList);

		TradingEngineContoller engine = new TradingEngineContoller();

		engine.processCandle(candle0, instrument);
		engine.processCandle(candle1, instrument);
		List<AlgorithmTrade> trades = engine.processCandle(candle2, instrument);

		assertThat(trades.get(0).isTrade()).isTrue();

	}

	@Test
	public void testAlgorithm2() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "40").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);
		TradingSignal downwardInversionAny = TradingSignalEnum.DOWNWARD_ANY.getInstance();
		TradingSignal downward5 = TradingSignalEnum.DOWNWARD_DELTA_MORE_THAN.getInstance(14);
		TradingSignal downwardAny = TradingSignalEnum.DOWNWARD_DELTA_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(downwardInversionAny);
		tradingSignals.add(downward5);
		tradingSignals.add(downwardAny);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile.fetchCandleList(instrument).addAll(candleList);

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		trades = engine.processCandle(candle9, instrument);
		trades = engine.processCandle(candle10, instrument);

		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(7);

	}

	@Test
	public void testAlgorithm4() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upGT10 = TradingSignalEnum.UPWARD_DELTA_MORE_THAN.getInstance(10);
		TradingSignal downAny = TradingSignalEnum.DOWNWARD_DELTA_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upGT10);
		tradingSignals.add(downAny);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();


		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);

		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(8);
	}

	@Test
	public void testAlgorithm3() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(5);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		trades = engine.processCandle(candle9, instrument);
		trades = engine.processCandle(candle10, instrument);
		assertThat(trades.get(0).isTrade()).isFalse();
	}

	@Test
	public void testAlgorithm3succsess() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3Multiuser3oneInstrument() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		TradingProfile tradingProfile1 = new TradingProfile();
		TradingProfile tradingProfile2 = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		Account account1 = new Account();
		account1.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		Account account2 = new Account();
		account2.setUserId("userTestId2");
		tradingProfile2.setAccount(account2);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();

		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);
		tradingProfile2.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny12 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny22 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny32 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT52 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		List<TradingSignal> tradingSignals2 = new ArrayList<>();
		tradingSignals2.add(upAny12);
		tradingSignals2.add(upAny22);
		tradingSignals2.add(upAny32);
		tradingSignals2.add(downLT52);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S5);
		algorithm1.setTradingInstrument(instrument);
		algorithm1.setBuy(false);

		TradingAlgorithm algorithm2 = new TradingAlgorithm(tradingSignals2, IntervalEnum.S5);
		algorithm2.setTradingInstrument(instrument);
		algorithm2.setBuy(true);

		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();
		tradingAlgorithms1.add(algorithm1);
		List<TradingAlgorithm> tradingAlgorithms2 = new ArrayList<>();
		tradingAlgorithms2.add(algorithm2);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);
		tradingProfile2.setTradingAlgorithms(tradingAlgorithms2);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);
		SessionUtils.getTradingProfiles().add(tradingProfile2);

		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile1.fetchCandleList(instrument).clear();
		tradingProfile2.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3Multiuser3moreInstrumentsSameInterval() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		TradingProfile tradingProfile1 = new TradingProfile();
		TradingProfile tradingProfile2 = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		Account account1 = new Account();
		account1.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		Account account2 = new Account();
		account2.setUserId("userTestId2");
		tradingProfile2.setAccount(account2);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(100);
		instrument1.setInstument("EUR_JPY");
		instrument1.setTradingInterval(IntervalEnum.S5);

		List<TradingInstrument> instruments = new ArrayList<>();

		instruments.add(instrument);
		instruments.add(instrument1);
		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);
		tradingProfile2.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny12 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny22 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny32 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT52 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		List<TradingSignal> tradingSignals2 = new ArrayList<>();
		tradingSignals2.add(upAny12);
		tradingSignals2.add(upAny22);
		tradingSignals2.add(upAny32);
		tradingSignals2.add(downLT52);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S5);
		algorithm1.setTradingInstrument(instrument1);
		algorithm1.setBuy(false);

		TradingAlgorithm algorithm2 = new TradingAlgorithm(tradingSignals2, IntervalEnum.S5);
		algorithm2.setTradingInstrument(instrument);
		algorithm2.setBuy(true);

		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();
		tradingAlgorithms1.add(algorithm1);
		List<TradingAlgorithm> tradingAlgorithms2 = new ArrayList<>();
		tradingAlgorithms2.add(algorithm2);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);
		tradingProfile2.setTradingAlgorithms(tradingAlgorithms2);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);
		SessionUtils.getTradingProfiles().add(tradingProfile2);

		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile1.fetchCandleList(instrument).clear();
		tradingProfile2.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3Multiuser3moreInstrumentsDifferentInterval() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		TradingProfile tradingProfile1 = new TradingProfile();
		TradingProfile tradingProfile2 = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		Account account1 = new Account();
		account1.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		Account account2 = new Account();
		account2.setUserId("userTestId2");
		tradingProfile2.setAccount(account2);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(100);
		instrument1.setInstument("EUR_JPY");
		instrument1.setTradingInterval(IntervalEnum.S10);

		List<TradingInstrument> instruments = new ArrayList<>();

		instruments.add(instrument);
		instruments.add(instrument1);
		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);
		tradingProfile2.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny12 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny22 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny32 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT52 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		List<TradingSignal> tradingSignals2 = new ArrayList<>();
		tradingSignals2.add(upAny12);
		tradingSignals2.add(upAny22);
		tradingSignals2.add(upAny32);
		tradingSignals2.add(downLT52);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S10);
		algorithm1.setTradingInstrument(instrument1);
		algorithm1.setBuy(false);

		TradingAlgorithm algorithm2 = new TradingAlgorithm(tradingSignals2, IntervalEnum.S5);
		algorithm2.setTradingInstrument(instrument);
		algorithm2.setBuy(true);

		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();
		tradingAlgorithms1.add(algorithm1);
		List<TradingAlgorithm> tradingAlgorithms2 = new ArrayList<>();
		tradingAlgorithms2.add(algorithm2);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);
		tradingProfile2.setTradingAlgorithms(tradingAlgorithms2);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);
		SessionUtils.getTradingProfiles().add(tradingProfile2);

		tradingProfile.fetchCandleList(instrument).clear();
		tradingProfile1.fetchCandleList(instrument).clear();
		tradingProfile2.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);

		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
		assertThat(trades.get(0).getTradingInterVal()).isEqualTo(IntervalEnum.S10);
	}

	@Test
	public void testAlgorithm3OneUserOneInstrumentDifferentIntervalSameAlgorithm() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(200);
		instrument1.setInstument("EUR_USD");
		instrument1.setTradingInterval(IntervalEnum.S10);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		instruments.add(instrument1);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals, IntervalEnum.S10);
		algorithm1.setTradingInstrument(instrument1);
		algorithm1.setBuy(true);

		tradingAlgorithms.add(algorithm);
		tradingAlgorithms.add(algorithm1);




		tradingProfile.setTradingAlgorithms(tradingAlgorithms);


		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3OneUserMoreInstrumentOneIntervalSameAlgorithm() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(200);
		instrument1.setInstument("EUR_JPY");
		instrument1.setTradingInterval(IntervalEnum.S5);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		instruments.add(instrument1);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm1.setTradingInstrument(instrument1);
		algorithm1.setBuy(true);

		tradingAlgorithms.add(algorithm);
		tradingAlgorithms.add(algorithm1);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3OneUserMoreInstrumentMoreIntervalSameAlgorithm() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(200);
		instrument1.setInstument("EUR_JPY");
		instrument1.setTradingInterval(IntervalEnum.S10);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		instruments.add(instrument1);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals, IntervalEnum.S10);
		algorithm1.setTradingInstrument(instrument1);
		algorithm1.setBuy(true);

		tradingAlgorithms.add(algorithm);
		tradingAlgorithms.add(algorithm1);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithm3MoreUserOneInstrumentOneIntervalSameAlgorithm() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingProfile tradingProfile1 = new TradingProfile();
		Account account1 = new Account();
		account.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);


		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);

		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S5);
		algorithm1.setTradingInstrument(instrument);
		algorithm1.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();


		tradingAlgorithms.add(algorithm);

		tradingAlgorithms1.add(algorithm1);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
		assertThat(trades.get(1).isTrade()).isTrue();
		assertThat(trades.get(1).getTradingCandleIndex()).isEqualTo(5);


	}

	@Test
	public void testAlgorithm3MoreUserOneInstrumentMoreIntervalSameAlgorithm() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "44").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingProfile tradingProfile1 = new TradingProfile();
		Account account1 = new Account();
		account.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(200);
		instrument1.setInstument("EUR_USD");
		instrument1.setTradingInterval(IntervalEnum.S10);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		instruments.add(instrument1);

		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S10);
		algorithm1.setTradingInstrument(instrument);
		algorithm1.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingAlgorithms1.add(algorithm1);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);

		trades = engine.processCandle(candle0, instrument1);
		trades = engine.processCandle(candle1, instrument1);
		trades = engine.processCandle(candle2, instrument1);
		trades = engine.processCandle(candle3, instrument1);
		trades = engine.processCandle(candle4, instrument1);
		trades = engine.processCandle(candle5, instrument1);
		trades = engine.processCandle(candle6, instrument1);
		trades = engine.processCandle(candle7, instrument1);
		trades = engine.processCandle(candle8, instrument1);
		assertThat(trades.get(0).getTradingInterVal()).isEqualTo(IntervalEnum.S10);
		assertThat(trades.get(0).isTrade()).isTrue();
		assertThat(trades.get(0).getTradingCandleIndex()).isEqualTo(5);
	}

	@Test
	public void testAlgorithmNullpointer() {

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "124.639").build()).build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "124.636").build()).build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "124.639").build()).build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "124.636").build()).build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "124.626").build()).build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "124.616").build()).build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "124.619").build()).build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "124.632").build()).build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "124.632").build()).build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "124.636").build()).build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "124.636").build()).build();
		CandleDataPointInterface candle11 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("L").withFilledMid("0", "0", "0", "124.629").build()).build();
		CandleDataPointInterface candle12 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("M").withFilledMid("0", "0", "0", "124.629").build()).build();
		CandleDataPointInterface candle13 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("N").withFilledMid("0", "0", "0", "124.636").build()).build();
		CandleDataPointInterface candle14 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("O").withFilledMid("0", "0", "0", "124.623").build()).build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingProfile tradingProfile1 = new TradingProfile();
		Account account1 = new Account();
		account.setUserId("userTestId1");
		tradingProfile1.setAccount(account1);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		TradingInstrument instrument1 = new TradingInstrument();
		instrument1.setUnits(200);
		instrument1.setInstument("EUR_USD");
		instrument1.setTradingInterval(IntervalEnum.S10);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		instruments.add(instrument1);

		tradingProfile.setTradingInstruments(instruments);
		tradingProfile1.setTradingInstruments(instruments);

		TradingSignal upAny1 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny2 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny3 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT5 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		TradingSignal upAny11 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny21 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal upAny31 = TradingSignalEnum.UPWARD_DELTA_ANY.getInstance();
		TradingSignal downLT51 = TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN.getInstance(35);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upAny1);
		tradingSignals.add(upAny2);
		tradingSignals.add(upAny3);
		tradingSignals.add(downLT5);

		List<TradingSignal> tradingSignals1 = new ArrayList<>();
		tradingSignals1.add(upAny11);
		tradingSignals1.add(upAny21);
		tradingSignals1.add(upAny31);
		tradingSignals1.add(downLT51);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		TradingAlgorithm algorithm1 = new TradingAlgorithm(tradingSignals1, IntervalEnum.S10);
		algorithm1.setTradingInstrument(instrument);
		algorithm1.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms1 = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingAlgorithms1.add(algorithm1);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile1.setTradingAlgorithms(tradingAlgorithms1);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);
		SessionUtils.getTradingProfiles().add(tradingProfile1);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		trades = engine.processCandle(candle8, instrument);
		trades = engine.processCandle(candle9, instrument);
		trades = engine.processCandle(candle10, instrument);
		trades = engine.processCandle(candle11, instrument);
		trades = engine.processCandle(candle12, instrument);
		trades = engine.processCandle(candle13, instrument);
		trades = engine.processCandle(candle14, instrument);
	}

	@Test
	public void testAlgorithm19() {

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "1.11102").build()).build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "1.11101").build()).build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "1.11099").build()).build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "1.11099").build()).build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "1.11101").build()).build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "1.11161").build()).build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal delta1 = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal delta2 = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal delta3 = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal delta4 = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal upward_delta_more_than_50 = TradingSignalEnum.UPWARD_DELTA_MORE_THAN.getInstance(50);

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(delta1);
		tradingSignals.add(delta2);
		tradingSignals.add(delta3);
		tradingSignals.add(delta4);
		tradingSignals.add(upward_delta_more_than_50);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
	}



	@Test
	public void testAlgorithm450UpwardInversionAny() {

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "1.11386").build()).build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "1.11377").build()).build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "1.11382").build()).build();

		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upwardAny = TradingSignalEnum.UPWARD_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upwardAny);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);

		assertThat(trades.get(0).isTrade()).isTrue();
	}

	@Test
	public void testAlgorithmAlgoithmReset() {

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-01T07:00:00.000000000Z").withFilledMid("0", "0", "0", "1.11386").build()).build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-01T07:01:00.000000000Z").withFilledMid("0", "0", "0", "1.11377").build()).build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-01T07:02:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-01T07:03:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-01T07:04:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();

		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-02T07:00:00.000000000Z").withFilledMid("0", "0", "0", "1.11386").build()).build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-02T07:01:00.000000000Z").withFilledMid("0", "0", "0", "1.11377").build()).build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-02T07:02:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-02T07:03:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-02T07:04:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();

		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-03T07:00:00.000000000Z").withFilledMid("0", "0", "0", "1.11386").build()).build();
		CandleDataPointInterface candle11 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-03T07:01:00.000000000Z").withFilledMid("0", "0", "0", "1.11377").build()).build();
		CandleDataPointInterface candle12 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-03T07:02:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle13 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-03T07:03:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();
		CandleDataPointInterface candle14 = CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-06-03T07:04:00.000000000Z").withFilledMid("0", "0", "0", "1.11382").build()).build();

		List<CandleDataPointInterface> candleList = new ArrayList<>();


		TradingProfile tradingProfile = new TradingProfile();
		Account account = new Account();
		account.setUserId("userTestId");
		tradingProfile.setAccount(account);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);

		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);

		tradingProfile.setTradingInstruments(instruments);

		TradingSignal upwardAny = TradingSignalEnum.UPWARD_ANY.getInstance();

		List<TradingSignal> tradingSignals = new ArrayList<>();
		tradingSignals.add(upwardAny);

		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setTradingInstrument(instrument);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

		tradingAlgorithms.add(algorithm);

		tradingProfile.setTradingAlgorithms(tradingAlgorithms);

		SessionUtils.getTradingProfiles().clear();
		SessionUtils.getTradingProfiles().add(tradingProfile);

		tradingProfile.fetchCandleList(instrument).clear();

		TradingEngineContoller engine = new TradingEngineContoller();

		List<AlgorithmTrade> trades = engine.processCandle(candle0, instrument);
		assertThat(candle0.isTheFirstCandleOfTheDay()).isTrue();
		trades = engine.processCandle(candle1, instrument);
		trades = engine.processCandle(candle2, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		trades = engine.processCandle(candle3, instrument);
		trades = engine.processCandle(candle4, instrument);
		trades = engine.processCandle(candle5, instrument);
		assertThat(candle0.isTheFirstCandleOfTheDay()).isTrue();
		trades = engine.processCandle(candle6, instrument);
		trades = engine.processCandle(candle7, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		trades = engine.processCandle(candle8, instrument);
		trades = engine.processCandle(candle9, instrument);
		trades = engine.processCandle(candle10, instrument);
		assertThat(candle0.isTheFirstCandleOfTheDay()).isTrue();
		trades = engine.processCandle(candle11, instrument);
		trades = engine.processCandle(candle12, instrument);
		assertThat(trades.get(0).isTrade()).isTrue();
		trades = engine.processCandle(candle13, instrument);
		trades = engine.processCandle(candle14, instrument);

		tradingProfile.fetchCandleList(instrument).clear();
		candle1.setTheFirstCandleOfTheDay(false);
		candle2.setTheFirstCandleOfTheDay(false);
		candle3.setTheFirstCandleOfTheDay(false);
		candle4.setTheFirstCandleOfTheDay(false);
		candle5.setTheFirstCandleOfTheDay(false);
		candle6.setTheFirstCandleOfTheDay(false);
		candle7.setTheFirstCandleOfTheDay(false);
		candle8.setTheFirstCandleOfTheDay(false);
		candle9.setTheFirstCandleOfTheDay(false);
		candle10.setTheFirstCandleOfTheDay(false);
		candle11.setTheFirstCandleOfTheDay(false);
		candle12.setTheFirstCandleOfTheDay(false);
		candle13.setTheFirstCandleOfTheDay(false);
		candle14.setTheFirstCandleOfTheDay(false);
		tradingProfile.addChandle(instrument, candle0);
		tradingProfile.addChandle(instrument, candle1);
		tradingProfile.addChandle(instrument, candle2);
		tradingProfile.addChandle(instrument, candle3);
		tradingProfile.addChandle(instrument, candle4);
		tradingProfile.addChandle(instrument, candle5);
		tradingProfile.addChandle(instrument, candle6);
		tradingProfile.addChandle(instrument, candle7);
		tradingProfile.addChandle(instrument, candle8);
		tradingProfile.addChandle(instrument, candle9);
		tradingProfile.addChandle(instrument, candle10);
		tradingProfile.addChandle(instrument, candle11);
		tradingProfile.addChandle(instrument, candle12);
		tradingProfile.addChandle(instrument, candle13);
		tradingProfile.addChandle(instrument, candle14);

		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle0, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle1, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle2, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle3, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle4, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle5, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle6, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle7, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle8, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle9, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle10, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle11, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle12, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle13, tradingProfile.fetchCandleList(instrument))).isTrue();
		assertThat(tradingProfile.candleWithThatDateAlreadyExists(candle14, tradingProfile.fetchCandleList(instrument))).isTrue();

		candleList = tradingProfile.fetchCandleList(instrument);
		for (CandleDataPointInterface candle : candleList) {
			System.out.println("Candle " + candle.getTime() + " " + candle.isTheFirstCandleOfTheDay());
		}
	}
}
