package rawai.it.com.TradingPlatform.signup;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Test;

import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;

import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class SignupControllerTest extends WebAppConfigurationAware {
    @Test
    public void displaysSignupForm() throws Exception {
		mockMvc.perform(get("signup"));
    }
}