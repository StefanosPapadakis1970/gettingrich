package rawai.it.com.TradingPlatform.tradingprofile;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import rawai.it.com.TradingPlatform.config.ApplicationConfig;
import rawai.it.com.TradingPlatform.config.EmbeddedDataSourceConfig;
import rawai.it.com.TradingPlatform.config.JpaConfig;
import rawai.it.com.TradingPlatform.config.NoCsrfSecurityConfig;
import rawai.it.com.TradingPlatform.config.WebMvcConfig;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@WebAppConfiguration
@ContextConfiguration(classes = { ApplicationConfig.class, EmbeddedDataSourceConfig.class, JpaConfig.class, NoCsrfSecurityConfig.class, WebMvcConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class dbunittest {

	@Autowired
	private TradingRepository tradingrepository;

	@Test
	@DatabaseSetup(connection = "testSchnuffi", type = DatabaseOperation.CLEAN_INSERT, value = "signals.xml")
	public void simpleTest() {
		TradingProfileEntity dbProfile = tradingrepository.findByUserID("4713");
		assertThat(dbProfile).isNotNull();
		assertThat(dbProfile.getUserID()).isEqualTo("4713");
		
	}

}
