package rawai.it.com.TradingPlatform.engine;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Test;

import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;
import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class TradingEngineControllerTest extends WebAppConfigurationAware {

	@Test
	@DatabaseSetup(value = { "/META-INF/signals.xml", "/META-INF/profile.xml" })
	public void startEngineTest() throws Exception {
		mockMvc.perform(get("/tradingengine/startEngine"));

		assertThat(SessionUtils.getTradingProfiles().size()).isEqualTo(2);
		assertThat(SessionUtils.getTradingProfiles().get(0).getTradingAlgorithms().size()).isEqualTo(1);
		assertThat(SessionUtils.getTradingProfiles().get(1).getTradingAlgorithms().size()).isEqualTo(1);

		assertThat(SessionUtils.getTradingProfiles().get(0).getTradingInstruments().size()).isBetween(1, 2);
		assertThat(SessionUtils.getTradingProfiles().get(1).getTradingInstruments().size()).isBetween(1, 2);

		ChartInstrument candle1 = SessionUtils.fetchLastCandle("101-011-5094777-001", "EUR_USD", "S5");
		ChartInstrument candle2 = SessionUtils.fetchLastCandle("101-011-5094777-001", "EUR_AUD", "S5");

		assertThat(candle1).isNotNull();
		assertThat(candle2).isNotNull();

	}
}
