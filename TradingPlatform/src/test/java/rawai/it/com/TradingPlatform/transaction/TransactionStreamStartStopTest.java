package rawai.it.com.TradingPlatform.transaction;

import static org.junit.Assert.assertThat;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.dashboard.TransactionStreamRunnable;
import rawai.it.com.TradingPlatform.databuilders.AccountBuilder;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionUtils.class, TransactionStreamRunnable.class, CloseableHttpClient.class, TransactionStreamService.class, HttpClientBuilder.class })
public class TransactionStreamStartStopTest {



	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testStartStream() throws Exception {
		
		CloseableHttpClient client = PowerMockito.mock(CloseableHttpClient.class);
		PowerMockito.whenNew(CloseableHttpClient.class).withAnyArguments().thenReturn(client);

		TransactionStreamRunnable mockRunner1 = PowerMockito.mock(TransactionStreamRunnable.class);
		TransactionStreamRunnable mockRunner2 = PowerMockito.mock(TransactionStreamRunnable.class);
		TransactionStreamRunnable mockRunner3 = PowerMockito.mock(TransactionStreamRunnable.class);
		TransactionStreamRunnable mockRunner4 = PowerMockito.mock(TransactionStreamRunnable.class);

		PowerMockito.when(mockRunner1.getUserID()).thenReturn("1");
		PowerMockito.when(mockRunner2.getUserID()).thenReturn("2");
		PowerMockito.when(mockRunner3.getUserID()).thenReturn("3");
		PowerMockito.when(mockRunner4.getUserID()).thenReturn("4");

		TransactionStreamService transactionStreamService = PowerMockito.mock(TransactionStreamService.class);

		PowerMockito.whenNew(TransactionStreamRunnable.class).withArguments(Mockito.eq("1"), Mockito.any(String.class), Mockito.any(TransactionStreamService.class))
				.thenReturn(mockRunner1);
		PowerMockito.whenNew(TransactionStreamRunnable.class).withArguments(Mockito.eq("2"), Mockito.any(String.class), Mockito.any(TransactionStreamService.class))
				.thenReturn(mockRunner2);
		PowerMockito.whenNew(TransactionStreamRunnable.class).withArguments(Mockito.eq("3"), Mockito.any(String.class), Mockito.any(TransactionStreamService.class))
				.thenReturn(mockRunner3);
		PowerMockito.whenNew(TransactionStreamRunnable.class).withArguments(Mockito.eq("4"), Mockito.any(String.class), Mockito.any(TransactionStreamService.class))
				.thenReturn(mockRunner4);

		TransactionStreamRunnable run1 = new TransactionStreamRunnable("1", "user1", transactionStreamService);

		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(true));


		Account account1 = AccountBuilder.aAccount().withUserID("1").withUserName("user1").build();
		Account account2 = AccountBuilder.aAccount().withUserID("2").withUserName("user2").build();
		Account account3 = AccountBuilder.aAccount().withUserID("3").withUserName("user3").build();
		Account account4 = AccountBuilder.aAccount().withUserID("4").withUserName("user4").build();

		SessionUtils.getAccounts().add(account1);
		SessionUtils.getAccounts().add(account2);
		SessionUtils.getAccounts().add(account3);
		SessionUtils.getAccounts().add(account4);


		SessionUtils.startUpTransactionStreams(transactionStreamService);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(true));

		SessionUtils.getTransactionRunners().remove(0);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(false));

		SessionUtils.startUpTransactionStreams(transactionStreamService);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(true));
		assertThat(SessionUtils.getTransactionRunners().size(), Matchers.is(4));


		SessionUtils.stopTransactionStreamRunners();
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(false));
		assertThat(SessionUtils.getTransactionRunners().size(), Matchers.is(0));

		SessionUtils.startUpTransactionStreams(transactionStreamService);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(true));
		assertThat(SessionUtils.getTransactionRunners().size(), Matchers.is(4));

		// Generate Exception
	}
}
