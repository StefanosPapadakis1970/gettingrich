package rawai.it.com.TradingPlatform.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;

public class TradingUtilsTest {

	@Test
	public void test() {
		int exponent = TradingUtils.calculateOnePipExponent(1.00321);
		long price = -110000;
		System.out.println("Exponent " + exponent + " Result" + TradingUtils.callculateDoubleValue(price, exponent));

		BigDecimal a = new BigDecimal(1.1442).setScale(5, RoundingMode.HALF_UP);
		BigDecimal b = new BigDecimal(1.14418).setScale(5, RoundingMode.HALF_UP);

		System.out.println(a.subtract(b));

	}

}
