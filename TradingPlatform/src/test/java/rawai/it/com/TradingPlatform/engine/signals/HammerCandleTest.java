package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class HammerCandleTest {

	@Test
	public void hammerCandleTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		HammerAny hammerAny = (HammerAny) TradingSignalEnum.HAMMER.getInstance(1);
        //                                                                                                         o    h    l      c  
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "100", "10", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("80", "100", "10", "100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("75", "100", "0", "100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("74", "100", "0", "100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.58032", "0.58036", "0.58026", "0.58035").build()).build());
		
		

		SignalResult result = hammerAny.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = hammerAny.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = hammerAny.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
		
		result = hammerAny.processCandles(candleList, 3);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(4));
		
		result = hammerAny.processCandles(candleList, 4);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(5));
	}
	
	@Test
	public void inverseHammerCandleTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		HammerAny hammerAny = (HammerAny) TradingSignalEnum.HAMMER.getInstance(0);
        //                                                                                                         o    h    l      c  
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "100", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "100", "0", "10").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "100", "0", "25").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "100", "0", "26").build()).build());
		

		SignalResult result = hammerAny.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(0));
		
		result = hammerAny.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = hammerAny.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
		
		result = hammerAny.processCandles(candleList, 3);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(4));
	}
}
