package rawai.it.com.TradingPlatform.tradingprofile;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithmForm;
import rawai.it.com.TradingPlatform.engine.TradingSignalForm;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;


@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class TradingAlgorithmControllerTest extends WebAppConfigurationAware {

	private static String SEC_CONTEXT_ATTR = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

	@Autowired
	private TradingRepository tradingRepository;

	@Test
	@DatabaseSetup("/META-INF/signals.xml")
	public void testCreateNewAlgorithm() throws Exception {
		mockMvc.perform(get("/tradingAlgorithm/editAlgorithm").param("name", "notexistentce")).andExpect(
				model().attribute("algorithmForm", hasProperty("algorithmName", org.hamcrest.CoreMatchers.is("notexistentce"))));
	}

	@Test
	@DatabaseSetup("/META-INF/signals.xml")
	public void testLoadAlgorithm() throws Exception {
		mockMvc.perform(get("/tradingAlgorithm/editAlgorithm").param("name", "testAlgorithm1"))
		.andExpect(model().attribute("algorithmForm", hasProperty("algorithmName", org.hamcrest.CoreMatchers.is("testAlgorithm1"))))
				.andExpect(model().attribute("algorithmForm", hasProperty("dbID", org.hamcrest.CoreMatchers.is("1"))))
				.andExpect(model().attribute("algorithmForm", hasProperty("signals", hasSize(4))));
	}

	@Test
	@DatabaseSetup("/META-INF/signals.xml")
	public void testUpdateAlgorithm() throws Exception {
		// TradingAlgorithmForm algorithm, Errors errors
		TradingAlgorithmEntity algorithm = tradingRepository.findByName("testAlgorithm1");
		TradingAlgorithmForm algorithmForm = TradingAlgorithmFormConverter.convertEntityToForm(algorithm);
		algorithmForm.setAlgorithmName("changedName");

		TradingSignalForm signalForm = algorithmForm.getSignals().get(0);
		algorithmForm.getSignals().remove(0);
		algorithmForm.getSignals().add(signalForm);

		mockMvc.perform(postForm("/update/tradingalgorithm", algorithmForm, "algorithmName", "dbID", "signals[0].description", "signals[0].dbID", "signals[1].description",
				"signals[1].dbID", "signals[2].description", "signals[2].dbID", "signals[3].description", "signals[3].dbID"));
		TradingAlgorithmEntity dbAlgorithm = tradingRepository.findByName("changedName");
		assertThat(dbAlgorithm, is(notNullValue()));
		assertThat(dbAlgorithm.getName(), is("changedName"));
		assertThat(dbAlgorithm.getTradingSignals().size(), is(4));
		assertThat(dbAlgorithm.getTradingSignals().get(3).getId(), is(1l));

		mockMvc.perform(postForm("/update/tradingalgorithm", algorithmForm, "algorithmName", "dbID", "signals[0].description", "signals[0].dbID", "signals[1].description",
				"signals[1].dbID", "signals[2].description", "signals[2].dbID", "signals[3].description", "signals[3].dbID"));

	}

	public MockHttpServletRequestBuilder postForm(String url, Object modelAttribute, String... propertyPaths) {

		try {
			MockHttpServletRequestBuilder form = post(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_FORM_URLENCODED);

			for (String path : propertyPaths) {
				form.param(path, BeanUtils.getProperty(modelAttribute, path));
			}

			return form;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
