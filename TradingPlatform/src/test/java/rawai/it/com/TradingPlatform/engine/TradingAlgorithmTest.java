package rawai.it.com.TradingPlatform.engine;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class TradingAlgorithmTest {

	@Test
	public void algorithmTestTrade() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("0").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("1").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("2").withFilledMid("0", "0", "0", "2").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("3").withFilledMid("0", "0", "0", "3").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("4").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("5").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("6").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("7").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("8").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("9").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("10").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("11").withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("12").withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("13").withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("14").withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("15").withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("16").withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("17").withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("18").withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("19").withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("20").withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("21").withFilledMid("0", "0", "0", "0.00067").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("22").withFilledMid("0", "0", "0", "0.00090").build()).build());

		TradingSignal signal = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal signal1 = TradingSignalEnum.DELTA_MORE_THAN.getInstance(20);

		List<TradingSignal> signals = new ArrayList<>();
		signals.add(signal);
		signals.add(signal1);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S15);
		TradingAlgorithm algorithm = new TradingAlgorithm(signals, IntervalEnum.S15);
		AlgorithmTrade trade = algorithm.generateTrade(candleList, false);

		while (!trade.isTrade() && trade.getTradingCandleIndex() < candleList.size() - 1) {
			trade = algorithm.generateTrade(candleList, false);
		}

		assertTrue(trade.isTrade() && trade.getTradingCandleIndex() == 22);

	}

	@Test
	public void algorithmTestNoTrade() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("0").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("1").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("2").withFilledMid("0", "0", "0", "2").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("3").withFilledMid("0", "0", "0", "3").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("4").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("5").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("6").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("7").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("10").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("11").withFilledMid("0", "0", "0", "1").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("12").withFilledMid("0", "0", "0", "0").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("13").withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("14").withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("15").withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("16").withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("17").withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("18").withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("19").withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("20").withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("21").withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("22").withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("23").withFilledMid("0", "0", "0", "0.00067").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("24").withFilledMid("0", "0", "0", "0.00076").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("25").withFilledMid("0", "0", "0", "0.00091").build()).build());

		TradingSignal signal = TradingSignalEnum.DELTA_LESS_THAN.getInstance(10);
		TradingSignal signal1 = TradingSignalEnum.DELTA_MORE_THAN.getInstance(20);

		List<TradingSignal> signals = new ArrayList<>();
		signals.add(signal);
		signals.add(signal1);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S15);
		TradingAlgorithm algorithm = new TradingAlgorithm(signals, IntervalEnum.S15);
		AlgorithmTrade trade = algorithm.generateTrade(candleList, false);

		assertTrue(!trade.isTrade());

	}

	@Test
	public void testKennAlgorithm1() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "40").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();

		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingSignal downwardInversionAny = TradingSignalEnum.DOWNWARD_ANY.getInstance();
		TradingSignal downward5 = TradingSignalEnum.DOWNWARD_DELTA_MORE_THAN.getInstance(5);
		TradingSignal downwardAny = TradingSignalEnum.DOWNWARD_DELTA_ANY.getInstance();

		List<TradingSignal> signals = new ArrayList<>();
		signals.add(downwardInversionAny);
		signals.add(downward5);
		signals.add(downwardAny);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S15);
		TradingAlgorithm algorithm = new TradingAlgorithm(signals, IntervalEnum.S15);
		algorithm.setBuy(true);
		AlgorithmTrade trade = algorithm.generateTrade(candleList, false);

		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		assertThat(trade.isTrade(), Matchers.is(true));
		assertThat(trade.getTradingCandleIndex(), Matchers.is(3));
	}

	@Test
	public void testKennAlgorithm2() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		CandleDataPointInterface candle0 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("A").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("B").withFilledMid("0", "0", "0", "35").build())
				.build();
		CandleDataPointInterface candle2 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("C").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle3 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("D").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle4 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("E").withFilledMid("0", "0", "0", "15").build())
				.build();
		CandleDataPointInterface candle5 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("F").withFilledMid("0", "0", "0", "20").build())
				.build();
		CandleDataPointInterface candle6 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("G").withFilledMid("0", "0", "0", "30").build())
				.build();
		CandleDataPointInterface candle7 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("H").withFilledMid("0", "0", "0", "40").build())
				.build();
		CandleDataPointInterface candle8 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("I").withFilledMid("0", "0", "0", "25").build())
				.build();
		CandleDataPointInterface candle9 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("J").withFilledMid("0", "0", "0", "10").build())
				.build();
		CandleDataPointInterface candle10 = CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withTime("K").withFilledMid("0", "0", "0", "5").build())
				.build();
		candleList.add(candle0);
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		candleList.add(candle5);
		candleList.add(candle6);
		candleList.add(candle7);
		candleList.add(candle8);
		candleList.add(candle9);
		candleList.add(candle10);

		TradingSignal downwardInversionAny = TradingSignalEnum.DOWNWARD_ANY.getInstance();
		TradingSignal downward15 = TradingSignalEnum.DOWNWARD_DELTA_MORE_THAN.getInstance(15);
		TradingSignal downwardAny = TradingSignalEnum.DOWNWARD_DELTA_ANY.getInstance();

		List<TradingSignal> signals = new ArrayList<>();
		signals.add(downwardInversionAny);
		signals.add(downward15);
		signals.add(downwardAny);

		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S15);
		TradingAlgorithm algorithm = new TradingAlgorithm(signals, IntervalEnum.S15);
		algorithm.setBuy(true);
		AlgorithmTrade trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);
		trade = algorithm.generateTrade(candleList, false);

	}

}
