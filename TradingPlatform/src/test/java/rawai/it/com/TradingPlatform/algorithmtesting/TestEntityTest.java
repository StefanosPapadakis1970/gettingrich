package rawai.it.com.TradingPlatform.algorithmtesting;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;
import rawai.it.com.TradingPlatform.databuilders.AlgorithmTradeEntityBuilder;
import rawai.it.com.TradingPlatform.databuilders.TestEntityBuilder;
import rawai.it.com.TradingPlatform.databuilders.TradingAlgorithmEntityBuilder;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.engine.db.TestRunEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.tradingprofile.RunType;

import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class TestEntityTest extends WebAppConfigurationAware {

	@Autowired
	private TestingRepository testingRepository;
	@Autowired
	private TradingRepository tradingRepository;
	@Autowired
	private AlgorithmTradeRepository algorithmTradeRepository;

	
	@Test
	public void saveLoadAlgorithmTrade() {
		AlgorithmTradeEntity trade = new AlgorithmTradeEntity();
		UUID uuid = UUID.randomUUID();
		trade.setUuid(uuid.toString());
		trade.setInstrument("Testinstrument");
		algorithmTradeRepository.save(trade);
		AlgorithmTradeEntity dbTrade = algorithmTradeRepository.findAlgorithmByUUID(uuid.toString());
		assertThat(dbTrade.getInstrument()).isEqualTo("Testinstrument");

	}

	@Test
	public void saveTestEntity() {
		TestEntity test = new TestEntity();
		TradingAlgorithmEntity algorithm = TradingAlgorithmEntityBuilder.aTradingAlgorithmEntityBuilder().withBuy(false).withName("TestAlg2").build();
		algorithm = tradingRepository.saveTradingAlgorithm(algorithm);
		algorithm = tradingRepository.findByName(algorithm.getName());


		AlgorithmTradeEntity trade1 = AlgorithmTradeEntityBuilder.aTrade().withBuySell(true).withInstrument("US1").build();
		AlgorithmTradeEntity trade2 = AlgorithmTradeEntityBuilder.aTrade().withBuySell(true).withInstrument("US2").build();

		List<TestRunEntity> testRuns = new ArrayList<>();

		test.setEndDate(new Date());
		test.setStartDate(new Date());
		test.setAlgorithm(algorithm);
		test.setGenerateTrades(true);
		test.setInstrument("TES_INS");
		test.setInterval(IntervalEnum.H12);
		test.setMaxNumberOfTransactions(99);
		test.setRunType(RunType.BACK_TESTING);
		test.setUnits(211);
		test.setUserID("4711");
		test.setStatus(TestStatusEnum.RUNNING);

		TestRunEntity testRun = new TestRunEntity();
		testRun.setExecutionDate(new Date());
		testRun.getTrades().add(trade1);
		testRun.getTrades().add(trade2);
		testRun.setTest(test);

		trade1.setTestRun(testRun);
		trade2.setTestRun(testRun);
		trade1.setAlgorithmID(algorithm.getId());
		trade2.setAlgorithmID(algorithm.getId());


		test.getTestRuns().add(testRun);

		test.setStatus(TestStatusEnum.SUCCESS);

		test = testingRepository.save(test);

		test = testingRepository.findByID(test.getId());

		test.setStatus(TestStatusEnum.ERROR);

		testingRepository.update(test);

		testingRepository.deleteTestEntity(test);
		tradingRepository.deleteTradingAlgorithmID(algorithm.getId());

	}

	@Test
	public void createNewTest() {
		TradingAlgorithmEntity algorithm = tradingRepository.findByName("TestAlg1");
		TestEntity testEntity = TestEntityBuilder.aTest().withAlgorithm(algorithm).build();
		testingRepository.update(testEntity);

	}
}
