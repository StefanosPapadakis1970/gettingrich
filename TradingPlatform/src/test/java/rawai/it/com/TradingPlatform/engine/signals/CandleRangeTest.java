package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class CandleRangeTest {

	@Test
	public void candleRangeLessTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleRangeLessThan rangeLess = (CandleRangeLessThan) TradingSignalEnum.CANDLERANGE_LESS.getInstance(200);
		SignalResult result = rangeLess.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "50", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "500", "0", "0.00100").build()).build());

		result = rangeLess.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(0));
		result = rangeLess.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeLess.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
	}

	@Test
	public void candleRangeMoreTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleRangeMoreThan rangeMore = (CandleRangeMoreThan) TradingSignalEnum.CANDLERANGE_MORE.getInstance(200);
		SignalResult result = rangeMore.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "50", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "500", "0", "0.00100").build()).build());

		result = rangeMore.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeMore.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(2));
		result = rangeMore.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
	}

	@Test
	public void candleRangeBetweenTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleRangeBetween rangeBetween = (CandleRangeBetween) TradingSignalEnum.CANDLERANGE_BETWEEN.getInstance(10, 20);
		SignalResult result = rangeBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "15", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "500", "0", "0.00100").build()).build());

		result = rangeBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeBetween.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeBetween.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
	}
}
