package rawai.it.com.TradingPlatform.transaction;

import java.io.IOException;

import org.junit.Test;

import rawai.it.com.TradingPlatform.dashboard.MarketOrderTransaction;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TransactionJSONTest {

	String data1 = "{\"type\":\"MARKET_ORDER\",\"instrument\":\"CN50_USD\",\"units\":\"20\",\"timeInForce\":\"FOK\",\"positionFill\":\"DEFAULT\",\"reason\":\"CLIENT_ORDER\",\"id\":\"1276\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1276\",\"requestID\":\"42285887279313765\",\"time\":\"2017-04-10T06:42:49.760284670Z\"}";
	String data2 = "{\"type\":\"ORDER_FILL\",\"orderID\":\"1276\",\"instrument\":\"CN50_USD\",\"units\":\"20\",\"price\":\"10462.9\",\"pl\":\"0.0000\",\"financing\":\"0.0000\",\"accountBalance\":\"109370.1313\",\"reason\":\"MARKET_ORDER\",\"tradeOpened\":{\"tradeID\":\"1277\",\"units\":\"20\"},\"id\":\"1277\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1276\",\"requestID\":\"42285887279313765\",\"time\":\"2017-04-10T06:42:49.760284670Z\"}";
	String data3 = "{\"type\":\"STOP_LOSS_ORDER\",\"tradeID\":\"1256\",\"timeInForce\":\"GTC\",\"triggerCondition\":\"TRIGGER_DEFAULT\",\"price\":\"1.00000\",\"reason\":\"CLIENT_ORDER\",\"id\":\"1278\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1278\",\"requestID\":\"42285889570442369\",\"time\":\"2017-04-10T06:51:55.466125364Z\"}";
	String data4 = "{\"type\":\"TAKE_PROFIT_ORDER\",\"tradeID\":\"1256\",\"timeInForce\":\"GTC\",\"triggerCondition\":\"TRIGGER_DEFAULT\",\"price\":\"2.00000\",\"reason\":\"CLIENT_ORDER\",\"id\":\"1279\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1279\",\"requestID\":\"42285889708915265\",\"time\":\"2017-04-10T06:52:28.093534962Z\"}";
	String data5 = "{\"type\":\"ORDER_CANCEL\",\"orderID\":\"1279\",\"reason\":\"CLIENT_REQUEST\",\"id\":\"1280\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1280\",\"requestID\":\"42285890057198199\",\"time\":\"2017-04-10T06:53:51.762066080Z\"}";
	String data6 = "{\"type\":\"ORDER_CANCEL\",\"orderID\":\"1278\",\"replacedByOrderID\":\"1282\",\"reason\":\"CLIENT_REQUEST_REPLACED\",\"id\":\"1281\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1281\",\"requestID\":\"42285890195671218\",\"time\":\"2017-04-10T06:54:24.778015947Z\"}";
	String data7 = "{\"type\":\"STOP_LOSS_ORDER\",\"tradeID\":\"1256\",\"timeInForce\":\"GTC\",\"triggerCondition\":\"TRIGGER_DEFAULT\",\"price\":\"1.01000\",\"reason\":\"REPLACEMENT\",\"replacesOrderID\":\"1278\",\"cancellingTransactionID\":\"1281\",\"id\":\"1282\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1281\",\"requestID\":\"42285890195671218\",\"time\":\"2017-04-10T06:54:24.778015947Z\"}";
	String data8 = "{\"type\":\"MARKET_ORDER\",\"instrument\":\"EUR_USD\",\"units\":\"-200\",\"timeInForce\":\"FOK\",\"positionFill\":\"REDUCE_ONLY\",\"reason\":\"TRADE_CLOSE\",\"tradeClose\":{\"units\":\"ALL\",\"tradeID\":\"1256\"},\"id\":\"1283\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1283\",\"requestID\":\"42285890376105335\",\"time\":\"2017-04-10T06:55:07.096280565Z\"}";
	String data9 = "{\"type\":\"ORDER_FILL\",\"orderID\":\"1283\",\"instrument\":\"EUR_USD\",\"units\":\"-200\",\"price\":\"1.05818\",\"pl\":\"-1.0920\",\"financing\":\"-0.0028\",\"accountBalance\":\"109369.0365\",\"reason\":\"MARKET_ORDER_TRADE_CLOSE\",\"tradesClosed\":[{\"tradeID\":\"1256\",\"units\":\"-200\",\"realizedPL\":\"-1.0920\",\"financing\":\"-0.0028\"}],\"id\":\"1284\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1283\",\"requestID\":\"42285890376105335\",\"time\":\"2017-04-10T06:55:07.096280565Z\"}";
	String data10 = "{\"type\":\"ORDER_CANCEL\",\"orderID\":\"1282\",\"reason\":\"LINKED_TRADE_CLOSED\",\"closedTradeID\":\"1256\",\"tradeCloseTransactionID\":\"1284\",\"id\":\"1285\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1283\",\"requestID\":\"42285890376105335\",\"time\":\"2017-04-10T06:55:07.096280565Z\"}";
	String data11 = "{\"type\":\"MARKET_ORDER\",\"instrument\":\"EUR_USD\",\"units\":\"-200\",\"timeInForce\":\"FOK\",\"positionFill\":\"REDUCE_ONLY\",\"reason\":\"TRADE_CLOSE\",\"tradeClose\":{\"units\":\"ALL\",\"tradeID\":\"1258\"},\"id\":\"1286\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1286\",\"requestID\":\"42285893200138207\",\"time\":\"2017-04-10T07:06:20.001136266Z\"}";
	String data12 = "{\"type\":\"MARKET_ORDER\",\"instrument\":\"CN50_USD\",\"units\":\"-20\",\"timeInForce\":\"FOK\",\"positionFill\":\"REDUCE_ONLY\",\"reason\":\"TRADE_CLOSE\",\"tradeClose\":{\"units\":\"ALL\",\"tradeID\":\"1277\"},\"id\":\"1288\",\"userID\":5094777,\"accountID\":\"101-011-5094777-001\",\"batchID\":\"1288\",\"requestID\":\"42285921877213949\",\"time\":\"2017-04-10T09:00:17.328178215Z\"}";

	@Test
	public void MarketOrderTransactionTest() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		mapper.readValue(data1, MarketOrderTransaction.class);
		mapper.readValue(data2, MarketOrderTransaction.class);
		mapper.readValue(data3, MarketOrderTransaction.class);
		mapper.readValue(data4, MarketOrderTransaction.class);
		mapper.readValue(data5, MarketOrderTransaction.class);
		mapper.readValue(data6, MarketOrderTransaction.class);
		mapper.readValue(data7, MarketOrderTransaction.class);
		mapper.readValue(data8, MarketOrderTransaction.class);
		mapper.readValue(data9, MarketOrderTransaction.class);
		mapper.readValue(data10, MarketOrderTransaction.class);
		mapper.readValue(data11, MarketOrderTransaction.class);
		mapper.readValue(data12, MarketOrderTransaction.class);
	}
}
