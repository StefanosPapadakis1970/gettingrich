package rawai.it.com.TradingPlatform.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Set;

import org.junit.Test;

import de.jollyday.Holiday;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameter;
import de.jollyday.ManagerParameters;

public class HoliDayTest {


	@Test
	public void holiDayTest(){
		HolidayManager newYorkManager;
		HolidayManager londonManager;
		ManagerParameter parameter = ManagerParameters.create("nyse");
		newYorkManager = HolidayManager.getInstance(parameter);
		parameter = ManagerParameters.create("gb");
		londonManager = HolidayManager.getInstance(parameter);
        
		Calendar gc = GregorianCalendar.getInstance();
		
		ArrayList<String> args = new ArrayList<>();
		ZoneId utcZone = ZoneId.of("UTC");

		for (int year = 2010; year < 2020; year++) {
			gc.set(Calendar.YEAR, year);


				Instant candleTime = Instant.ofEpochMilli(gc.getTimeInMillis());
				ZonedDateTime time = candleTime.atZone(utcZone);

				boolean newYorkHolyDay = newYorkManager.isHoliday(time.toLocalDate(), args.toArray(new String[] {}));
				boolean londonHoliday = londonManager.isHoliday(time.toLocalDate(), args.toArray(new String[] {}));

				Set<Holiday> holiDays = newYorkManager.getHolidays(year, args.toArray(new String[] {}));

				System.out.println("######################  YEAR NEW YORK " + year + "  #############################");

				for (Holiday holiDay : holiDays) {
					System.out.println(holiDay.getDate() + " " + holiDay.getDescription(Locale.ENGLISH));
				}

				holiDays = londonManager.getHolidays(year, args.toArray(new String[] {}));
				System.out.println("######################  YEAR LONDON " + year + "  #############################");
				for (Holiday holiDay : holiDays) {
					System.out.println(holiDay.getDate() + " " + holiDay.getDescription(Locale.ENGLISH));
				}


		}
	} 
}
