package rawai.it.com.TradingPlatform.engine;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import rawai.it.com.TradingPlatform.databuilders.TestDataProvider;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;

public class TradingProfileConverterTest {

	@Test
	public void converterToTradingProfileTest() {
		/*
		 * TradingProfileEntity profileEntity =
		 * TestDataProvider.buildTradingProfileEntity("4712"); TradingProfile
		 * profile =
		 * TradingProfileConverter.convertToTradingProfile(profileEntity);
		 * assertThat(profile.getAccount().getUserId()).isEqualTo("4712");
		 * assertThat
		 * (profile.getTradingAlgorithms().size()).isEqualTo(profileEntity
		 * .getTradingAlgorithms().size());
		 * assertThat(profile.getTradingInstruments
		 * ().size()).isEqualTo(profileEntity.getTradingInstruments().size());
		 * 
		 * TradingProfileEntity profileConvertedEntity =
		 * TradingProfileConverter.convertToTradingProfileEntity(profile);
		 * assertThat
		 * (profileConvertedEntity.getUserID()).isEqualTo(profile.getAccount
		 * ().getUserId());
		 * assertThat(profileConvertedEntity.getTradingAlgorithms
		 * ().size()).isEqualTo(profile.getTradingAlgorithms().size());
		 * assertThat
		 * (profileConvertedEntity.getTradingInstruments().size()).isEqualTo
		 * (profile.getTradingInstruments().size());
		 */
	}

	@Test
	public void convertProfileEntityToForm() {
		TradingProfileEntity profileEntity = TestDataProvider.buildTradingProfileEntity("4712");
		TradingProfileForm profile = TradingProfileConverter.convertToTradingProfileForm(profileEntity);
		assertThat(profile.getUserID()).isEqualTo("4712");
		assertThat(profile.getTradingAlgorithms().size()).isEqualTo(profileEntity.getTradingAlgorithms().size());
		assertThat(profile.getTradingInstruments().size()).isEqualTo(profileEntity.getTradingInstruments().size());
		assertThat(profile.getTradingAlgorithms().get(0).getAlgorithmName()).isEqualTo(profileEntity.getTradingAlgorithms().get(0).getName());

		TradingProfileEntity profileConvertedEntity = TradingProfileConverter.convertToTradingProfileEntity(profile.createTradingProfile());
		assertThat(profileConvertedEntity.getUserID()).isEqualTo(profile.getUserID());
		assertThat(profileConvertedEntity.getTradingAlgorithms().size()).isEqualTo(profile.getTradingAlgorithms().size());
		assertThat(profileConvertedEntity.getTradingInstruments().size()).isEqualTo(profile.getTradingInstruments().size());
		assertThat(profile.getTradingAlgorithms().get(0).getAlgorithmName()).isEqualTo(profileConvertedEntity.getTradingAlgorithms().get(0).getName());
	}
}
