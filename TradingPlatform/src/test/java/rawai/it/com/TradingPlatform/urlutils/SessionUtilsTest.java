package rawai.it.com.TradingPlatform.urlutils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SessionUtilsTest {

	@Test
	public void testFetchLastCandle() {
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit1", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit2", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit3", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit4", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit5", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit6", "interval1");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit7", "interval1");

		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit1", "interval2");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit2", "interval2");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit3", "interval2");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit4", "interval2");
		SessionUtils.setLastFechtedCandle("user1", "instrument1", "zeit5", "interval2");

		ChartInstrument candle = SessionUtils.fetchLastCandle("user1", "instrument1", "interval2");
		assertTrue(candle.getInstrument().equals("instrument1") && candle.getInterval().equals("interval2") && candle.getTime().equals("zeit5"));
		candle = SessionUtils.fetchLastCandle("user1", "instrument1", "interval1");
		assertTrue(candle.getInstrument().equals("instrument1") && candle.getInterval().equals("interval1") && candle.getTime().equals("zeit7"));
	}

}
