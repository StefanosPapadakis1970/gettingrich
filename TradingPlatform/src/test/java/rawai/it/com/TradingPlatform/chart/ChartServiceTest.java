package rawai.it.com.TradingPlatform.chart;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;


public class ChartServiceTest {


	@Test
	public void test() {

		ChartService chartService = new ChartService();
		assertThat(chartService.callculateOnePip(0.00001), Matchers.is(0.00001));
		assertThat(chartService.callculateOnePip(10.0001), Matchers.is(0.0001));
		assertThat(chartService.callculateOnePip(100.001), Matchers.is(0.001));
		assertThat(chartService.callculateOnePip(1000.01), Matchers.is(0.01));
		assertThat(chartService.callculateOnePip(10000.1), Matchers.is(0.1));

	}

}
