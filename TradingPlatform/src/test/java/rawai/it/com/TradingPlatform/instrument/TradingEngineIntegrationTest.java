package rawai.it.com.TradingPlatform.instrument;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.candle.CandleListResponse;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingEngineContoller;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ InstrumentController.class, RestTemplate.class, TradingEngineContoller.class })
public class TradingEngineIntegrationTest {

	@Mock
	private AccountRepository accountRepository;
	@Mock
	private SimpMessagingTemplate template;
	@Mock
	public HttpSession session;

	@InjectMocks
	private InstrumentController instrumentConroller;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private InstrumentController tradingInstrumentControllerMock;

	@InjectMocks
	private TradingEngineContoller tradeingEngine;

	private TradingEngineContoller tradeSpy;

	@Before
	public void setUp() throws Exception {
		// Initialize mocks created above
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testCase1() {

		List<CandleDataPointInterface> candleList = new ArrayList<>();
		candleList.add(CandleDataPointBuilder.aCandleDataPoint()
				.withCandle(CandleBuilder.aCandle().withTime("2017-02-28T04:40:00.000000000Z").withFilledMid("0", "0", "0", "0.00000").build()).build());

		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:05.000000000Z").withFilledMid("0", "0", "0", "0.00000").build();
		CandleDataPoint candleDataPoint1 = CandleDataPointBuilder.aCandleDataPoint().withCandle(candle1).build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:10.000000000Z").withFilledMid("0", "0", "0", "0.00000").build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:15.000000000Z").withFilledMid("0", "0", "0", "0.00000").build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:20.000000000Z").withFilledMid("0", "0", "0", "0.00003").build();
		List<Candle> candleEntityList = new ArrayList<>();
		candleEntityList.add(candle1);
		candleEntityList.add(candle2);
		candleEntityList.add(candle3);
		candleEntityList.add(candle4);

		CandleListResponse candleListResponse = new CandleListResponse();
		candleListResponse.setCandles(candleEntityList);

		ResponseEntity<CandleListResponse> candleListResponseEntity = new ResponseEntity<CandleListResponse>(candleListResponse, HttpStatus.OK);

		restTemplate = PowerMockito.mock(RestTemplate.class);
		try {
			PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);

		} catch (Exception e) {
			e.printStackTrace();
		}
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(
				candleListResponseEntity);

		TradingSignal morethan1 = TradingSignalEnum.DELTA_MORE_THAN.getInstance(1);
		List<TradingSignal> signals = new ArrayList<>();
		signals.add(morethan1);
		setUpSessionUtils(signals);

		Mockito.when(tradingInstrumentControllerMock.fetchCandles(Matchers.anyString(), Matchers.anyString(), Matchers.anyString())).thenReturn(candleList);


		tradeingEngine.startTradingEngine();
		AlgorithmTrade trade = tradeingEngine.getLastTrade();
		assertThat(trade.isTrade()).isTrue();
		assertThat(trade.isBuySell()).isTrue();
		assertThat(trade.getInstrument().getUnits()).isEqualTo(200);
		assertThat(trade.getInstrument().getTradingInterval()).isEqualTo(IntervalEnum.S5);
		assertThat(trade.getInstrument().getInstument()).isEqualTo("EUR_USD");


	}

	private void setUpSessionUtils(List<TradingSignal> tradingSignals) {
		Account account = new Account();
		account.setUserId("TestID");
		TradingProfile tradingProfile = new TradingProfile();
		tradingProfile.setAccount(account);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(200);
		instrument.setInstument("EUR_USD");
		instrument.setTradingInterval(IntervalEnum.S5);
		List<TradingInstrument> instruments = new ArrayList<>();
		instruments.add(instrument);
		tradingProfile.setTradingInstruments(instruments);
		TradingAlgorithm algorithm = new TradingAlgorithm(tradingSignals, IntervalEnum.S5);
		algorithm.setBuy(true);
		List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();
		tradingAlgorithms.add(algorithm);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		List<TradingProfile> profiles = new ArrayList<>();
		profiles.add(tradingProfile);

		SessionUtils.setTradingProfiles(profiles);
		SessionUtils.getAccounts().add(account);
	}
}
