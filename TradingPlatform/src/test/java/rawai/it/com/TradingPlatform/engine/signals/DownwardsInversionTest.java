package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class DownwardsInversionTest {

	@Test
	public void downwardInversionAny() {
		DownwardsInversionAny downwardsAny = (DownwardsInversionAny) TradingSignalEnum.DOWNWARD_ANY.getInstance();

		List<CandleDataPointInterface> candleList = new ArrayList<>();

		SignalResult result = downwardsAny.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());

		result = downwardsAny.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = downwardsAny.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
		
		result = downwardsAny.processCandles(candleList, 4);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(5));

	}

	@Test
	public void downwardInversionBetween() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		DownwardsInversionBetween downBetween = (DownwardsInversionBetween) TradingSignalEnum.DOWNWARD_BETWEEN.getInstance(4, 10);
		SignalResult result = downBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00010").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00039").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00010").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00035").build()).build());

		result = downBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));

		result = downBetween.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(2));

		result = downBetween.processCandles(candleList, 5);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(6));

		result = downBetween.processCandles(candleList, 8);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(9));
		
		candleList.clear();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67595","0.67595","0.67593","0.67593").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67590","0.67590","0.67590","0.67574").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67588","0.67592","0.67588","0.67595").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67590","0.67590","0.67580","0.67586").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67576","0.67576","0.67573","0.67573").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0.67570","0.67570","0.67570","0.67570").build()).build());
		
		result = downBetween.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));	

	}

	@Test
	public void downwardsInversionLessThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		DownwardsInversionLessThan lessthan = (DownwardsInversionLessThan) TradingSignalEnum.DOWNWARD_LESS.getInstance(5);
		SignalResult result = lessthan.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00104").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00080").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00102").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());

		result = lessthan.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));

		result = lessthan.processCandles(candleList, 5);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(6));

		result = lessthan.processCandles(candleList, 8);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(9));

	}

	@Test
	public void downwardInversionMoreThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		DownwardsInversionMoreThan morethan = (DownwardsInversionMoreThan) TradingSignalEnum.DOWNWARD_MORE.getInstance(5);

		SignalResult result = morethan.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00095").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00180").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00098").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00100").build()).build());

		result = morethan.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(2));

		result = morethan.processCandles(candleList, 5);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(6));

		result = morethan.processCandles(candleList, 8);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(9));

	}

}
