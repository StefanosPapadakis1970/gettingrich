package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;

public class DeltaTest {


	@Test
	public void testEnoughCandles() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());
		candleList.add(new CandleDataPoint());

		DeltaLessThan deltaLess = new DeltaLessThan(1);
		assertTrue(deltaLess.enoughCandles(candleList, 0));
		deltaLess.setMinimumNumberOfCandles(10);
		assertTrue(deltaLess.enoughCandles(candleList, 0));
		deltaLess.setMinimumNumberOfCandles(11);
		assertFalse(deltaLess.enoughCandles(candleList, 0));
		deltaLess.setMinimumNumberOfCandles(3);
		assertTrue(deltaLess.enoughCandles(candleList, 7));
		assertFalse(deltaLess.enoughCandles(candleList, 8));
	}

	@Test
	public void testDeltaLess() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00067").build()).build());

		DeltaLessThan deltaLess = new DeltaLessThan(10);
		int lastSignalCandleIndex = 0;
		SignalResult result = null;
		result = deltaLess.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = deltaLess.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = deltaLess.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = deltaLess.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);

	}

	@Test
	public void testDeltaMore() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00067").build()).build());

		DeltaMoreThan deltaMore = new DeltaMoreThan(10);

		int lastSingalCandle = 0;
		SignalResult result = null;
		result = deltaMore.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = deltaMore.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = deltaMore.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = deltaMore.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);

	}

	@Test
	public void testDeltaBetween() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00067").build()).build());

		DeltaBetween deltaBetween = new DeltaBetween(2, 10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = deltaBetween.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = deltaBetween.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = deltaBetween.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = deltaBetween.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = deltaBetween.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = deltaBetween.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == true);
	}

	@Test
	public void testDeltaAny() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		DeltaAny deltaAny = new DeltaAny();

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = deltaAny.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testUpwardDeltaAny() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00001").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		UpwardDeltaAny deltaAny = new UpwardDeltaAny();

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = deltaAny.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = deltaAny.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testUpwardDeltaBetween() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		UpwardDeltaBetween deltaAny = new UpwardDeltaBetween(10, 20);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = deltaAny.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = deltaAny.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = deltaAny.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = deltaAny.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = deltaAny.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = deltaAny.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testUpwardDeltaLessThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		UpwardDeltaLessThan upwardDeltaLessThan = new UpwardDeltaLessThan(10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = upwardDeltaLessThan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = upwardDeltaLessThan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = upwardDeltaLessThan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = upwardDeltaLessThan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = upwardDeltaLessThan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = upwardDeltaLessThan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testUpwardDeltaMoreThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		UpwardDeltaMoreThan morethan = new UpwardDeltaMoreThan(10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = morethan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = morethan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = morethan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testUpwardDeltaMoreThan50realTrade() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		// # id, c
		// '3283', '1.10975'
		// '3284', '1.10973'
		// '3285', '1.10975'
		// '3286', '1.10977'
		// '3287', '1.10980'
		// '3288', '1.10975'

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "1.10975").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "1.10973").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "1.10975").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "1.10977").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		UpwardDeltaMoreThan morethan = new UpwardDeltaMoreThan(10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = morethan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = morethan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = morethan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = morethan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testDownWardAny() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00005").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		DownwardDeltaAny downwardMorethan = new DownwardDeltaAny();

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = downwardMorethan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = downwardMorethan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = downwardMorethan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = downwardMorethan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = downwardMorethan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = downwardMorethan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testDownDeltaBetween() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00045").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		DownwardDeltaBetween downwardBetween = new DownwardDeltaBetween(10, 20);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = downwardBetween.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = downwardBetween.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = downwardBetween.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = downwardBetween.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = downwardBetween.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = downwardBetween.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testDownDeltaLessThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00035").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00049").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		DownwardDeltaLessThan downwardLessThan = new DownwardDeltaLessThan(10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = downwardLessThan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = downwardLessThan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = downwardLessThan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = downwardLessThan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = downwardLessThan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == false);
		result = downwardLessThan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

	@Test
	public void testDownDeltaMoreThan() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00035").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00049").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00040").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());

		DownwardDeltaMoreThan downwardMoreThan = new DownwardDeltaMoreThan(10);

		int lastCandleIndex = 0;
		SignalResult result = null;
		result = downwardMoreThan.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = downwardMoreThan.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = downwardMoreThan.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = downwardMoreThan.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = downwardMoreThan.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = downwardMoreThan.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}


	@Test
	public void testDeltaLessThan10() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37279").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37634").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37609").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37523").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37520").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37518").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37518").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37505").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37506").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37497").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "9.37487").build()).build());

		DeltaLessThan deltaLess = new DeltaLessThan(10);

		SignalResult result = null;
		result = deltaLess.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = deltaLess.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = deltaLess.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = deltaLess.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = deltaLess.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = deltaLess.processCandles(candleList, 9);
		assertTrue(result.getCandleIndex() == 10 && result.isSuccess() == false);
	}

}
