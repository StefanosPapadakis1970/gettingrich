package rawai.it.com.TradingPlatform.config;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@WebAppConfiguration
@ContextConfiguration(classes = {
        ApplicationConfig.class,
        //EmbeddedDataSourceConfig.class,
        JpaConfig.class,
        //NoCsrfSecurityConfig.class,
        WebMvcConfig.class
})

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
public abstract class WebAppConfigurationAware {

    @Inject
    protected WebApplicationContext wac;
	@Inject
	private FilterChainProxy springSecurityFilterChain;
    protected MockMvc mockMvc;
	//protected WebDriver driver;

    @Before
	public void before() {
		this.mockMvc = webAppContextSetup(this.wac).build();
    }

	@After
	public void destroy() {
		/*if (driver != null) {
			driver.close();
		}*/
	}
}
