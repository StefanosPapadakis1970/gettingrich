package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class CandleEnpointTest {

	@Test
	public void candleRangeTopHalfTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleEndpoint topHalf = (CandleEndpoint) TradingSignalEnum.CANDLE_ENDPOINT.getInstance(RangeEnum.TOP_HALF);
		SignalResult result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

 		//		                                                                                                   o     h      l      c
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "110").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "160").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("5.59","5.6","5.3","5.58").build()).build());

		result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = topHalf.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = topHalf.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
	}

	@Test
	public void candleRangeBottomHalfTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleEndpoint topHalf = (CandleEndpoint) TradingSignalEnum.CANDLE_ENDPOINT.getInstance(RangeEnum.BOTTOM_HALF);
		SignalResult result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "160").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "110").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("5.35","5.60","5.30","5.40").build()).build());		
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("5.40","5.60","5.30","5.35").build()).build());
		
		

		result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));

		result = topHalf.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = topHalf.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
		
		result = topHalf.processCandles(candleList, 3);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(3));
	}

	@Test
	public void candleRangeTopQuarterTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleEndpoint topHalf = (CandleEndpoint) TradingSignalEnum.CANDLE_ENDPOINT.getInstance(RangeEnum.TOP_QUARTER);
		SignalResult result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "170").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "180").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("5.58","5.6","5.3","5.59").build()).build());

		result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));

		result = topHalf.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		
		result = topHalf.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(2));
	}

	@Test
	public void candleRangeBottomQuarterTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		CandleEndpoint topHalf = (CandleEndpoint) TradingSignalEnum.CANDLE_ENDPOINT.getInstance(RangeEnum.BOTTOM_QUARTER);
		SignalResult result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "100", "170").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "200", "200", "120").build()).build());

		result = topHalf.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = topHalf.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
	}

}
