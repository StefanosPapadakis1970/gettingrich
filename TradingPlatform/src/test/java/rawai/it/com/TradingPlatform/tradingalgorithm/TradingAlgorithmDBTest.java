package rawai.it.com.TradingPlatform.tradingalgorithm;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;
import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;

import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class TradingAlgorithmDBTest extends WebAppConfigurationAware {

	@Autowired
	private TradingRepository tradingRepositiory;

	@Test
	public void algorithmTest() {

		TradingAlgorithmEntity algorithm = new TradingAlgorithmEntity();
		PostTradeSignalEntity postSignalEntity = new PostTradeSignalEntity();
		postSignalEntity.setValue(-1);
		postSignalEntity.setTradingSignal(PostTradingSignalEnum.STOPLOSS);
		algorithm.getPostTradingSignals().add(postSignalEntity);
		algorithm.setBuy(false);
		algorithm.setName("Alg1");
		tradingRepositiory.saveTradingAlgorithm(algorithm);

	}

}
