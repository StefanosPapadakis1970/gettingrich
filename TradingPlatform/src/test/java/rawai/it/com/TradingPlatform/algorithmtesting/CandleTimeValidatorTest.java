package rawai.it.com.TradingPlatform.algorithmtesting;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.candle.CandleTimeValidator;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;

public class CandleTimeValidatorTest {

	private static final Logger logger = LogManager.getLogger(CandleTimeValidatorTest.class);


	@Test
	public void testStartStop() throws ParseException {
		for (String timeZone : TimeZone.getAvailableIDs()) {
			logger.info("TimeZone: " + timeZone);
		}
		
		CandleTimeValidator candleValidator = new CandleTimeValidator();

		Calendar gc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		gc.setTime(new Date());
		gc.set(Calendar.DAY_OF_MONTH, 20);
		gc.set(Calendar.MONTH, 1);
		gc.set(Calendar.HOUR_OF_DAY, 7);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);

		DateTime time = new DateTime(gc.getTime(), DateTimeZone.UTC);
		Instant candleTime = Instant.ofEpochMilli(time.getMillis());

		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(false);

		gc.setTime(new Date());
		gc.set(Calendar.HOUR_OF_DAY, 9);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		time = new DateTime(gc.getTime(), DateTimeZone.UTC);

		candleTime = Instant.ofEpochMilli(time.getMillis());

		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(true);

		gc.setTime(new Date());
		gc.set(Calendar.HOUR_OF_DAY, 12);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		time = new DateTime(gc.getTime(), DateTimeZone.UTC);


		candleTime = Instant.ofEpochMilli(time.getMillis());
		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(true);

		gc.setTime(new Date());
		gc.set(Calendar.HOUR_OF_DAY, 21);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		time = new DateTime(gc.getTime(), DateTimeZone.UTC);

		candleTime = Instant.ofEpochMilli(time.getMillis());
		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(false);

		time = new DateTime("2017-07-07T10:45:10.000000000Z", DateTimeZone.UTC);
		candleTime = Instant.ofEpochMilli(time.getMillis());
		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(true);

		time = new DateTime("2017-07-07T07:00:00.000000000Z", DateTimeZone.UTC);
		candleTime = Instant.ofEpochMilli(time.getMillis());
		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(false);

		time = new DateTime("2017-07-07T21:00:00.000000000Z", DateTimeZone.UTC);
		candleTime = Instant.ofEpochMilli(time.getMillis());
		assertThat(candleValidator.isValidCandleTime(candleTime)).isEqualTo(false);


	}

	private ArrayList<CandleDataPointInterface> createCandleTimes(Date startDate, Date endDate) {
		ArrayList<CandleDataPointInterface> candles = new ArrayList<>();
		Calendar gc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		gc.setTime(startDate);
		SimpleDateFormat format = new SimpleDateFormat();


		while (startDate.before(endDate)) {
			Candle candle = new Candle();
			candle.setTime(format.format(startDate));
			Instant candleTime = Instant.ofEpochMilli(startDate.getTime());
//			if (CandleTimeValidator.isValidCandleTime(candleTime)) {
//				candles.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(candle).build());
//			}
			gc.set(Calendar.MINUTE, gc.get(Calendar.MINUTE) + 1);
			startDate = gc.getTime();
		}

		return candles;
	}

	@Test
	public void timeConversion() throws ParseException {

		DateTimeZone timeZoneBerlin = DateTimeZone.forID("Europe/Berlin");
		DateTimeZone timeZoneLondon = DateTimeZone.forID("Europe/London");
		DateTimeZone timeZoneKolkata = DateTimeZone.forID("Asia/Bangkok");
		DateTimeZone timeZoneNewYork = DateTimeZone.forID("America/New_York");

		Calendar gc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		gc.setTime(new Date());
		gc.set(Calendar.HOUR_OF_DAY, 15);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);

		DateTime time = new DateTime(gc.getTime(), DateTimeZone.UTC);
		logger.info("Berlin " + time);
		DateTime newYork = time.withZone(timeZoneNewYork);


		logger.info("New York " + newYork);


		DateTime nowLondon = DateTime.now(timeZoneLondon);
		DateTime nowKolkata = nowLondon.withZone(timeZoneKolkata);
		DateTime nowNewYork = nowLondon.withZone(timeZoneNewYork);
		DateTime nowUtc = nowLondon.withZone(DateTimeZone.UTC);

		logger.info("London " + timeZoneLondon);
		logger.info("Bangkok " + timeZoneKolkata);
		logger.info("UTC " + nowUtc);
		logger.info("New York " + nowNewYork);



		// Calendar gc = GregorianCalendar.getInstance();
		// gc.set(Calendar.HOUR_OF_DAY, 12);
		// gc.set(Calendar.MINUTE, 0);
		// gc.set(Calendar.SECOND, 0);
		//
		// Date convertDate = gc.getTime();
		//
		// Date convertedDate = convertDateFromTo(convertDate, "Europe/Berlin",
		// "America/New_York");
		// logger.info("############METHOD CALL#############################################");
		// logger.info(convertDate);
		// logger.info(convertedDate);
		// logger.info("#########################################################");
	}

	private Date convertDateFromTo(Date date, String fromTimeZoneID, String toTimeZoneID) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		TimeZone fromTimeZone = calendar.getTimeZone();
		TimeZone toTimeZone = TimeZone.getTimeZone(fromTimeZoneID);

		logger.info(calendar.getTime() + " " + calendar.getTimeZone().getID());

		calendar.setTimeZone(fromTimeZone);
		calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
		if (fromTimeZone.inDaylightTime(calendar.getTime())) {
			calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
		}

		calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
		if (toTimeZone.inDaylightTime(calendar.getTime())) {
			calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
		}

		logger.info(calendar.getTime() + " " + calendar.getTimeZone().getID());



		calendar.setTimeZone(TimeZone.getTimeZone(fromTimeZoneID));
		TimeZone fromTimeZone1 = calendar.getTimeZone();
		TimeZone toTimeZone1 = TimeZone.getTimeZone(toTimeZoneID);


		calendar.setTimeZone(fromTimeZone1);
		calendar.add(Calendar.MILLISECOND, fromTimeZone1.getRawOffset() * -1);
		if (fromTimeZone1.inDaylightTime(calendar.getTime())) {
			calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
		}

		calendar.add(Calendar.MILLISECOND, toTimeZone1.getRawOffset());
		if (toTimeZone1.inDaylightTime(calendar.getTime())) {
			calendar.add(Calendar.MILLISECOND, toTimeZone1.getDSTSavings());
		}


		return calendar.getTime();
	}
}
