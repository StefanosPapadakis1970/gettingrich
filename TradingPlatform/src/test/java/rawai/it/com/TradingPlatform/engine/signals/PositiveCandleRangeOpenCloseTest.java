package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class PositiveCandleRangeOpenCloseTest {

	@Test
	public void candleRangeLessTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		PositiveCandleRangeOpenCloseLessThan rangeLess = (PositiveCandleRangeOpenCloseLessThan) TradingSignalEnum.POSITIVE_CANDLERANGE_LESS_OC.getInstance(10);
		SignalResult result = rangeLess.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));
        //                                                                                                         o    h    l      c

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "15").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "29").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "5").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("5.35285", "5.35285", "5.35274", "5.35282").build()).build());

		result = rangeLess.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(0));
		result = rangeLess.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(2));
		result = rangeLess.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
		result = rangeLess.processCandles(candleList, 3);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(4));

	}

	@Test
	public void candleRangeMoreTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		PositiveCandleRangeOpenCloseMoreThan rangeMore = (PositiveCandleRangeOpenCloseMoreThan) TradingSignalEnum.POSITIVE_CANDLERANGE_MORE_OC.getInstance(5);
		SignalResult result = rangeMore.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));

        //                                                                                                         o    h    l      c
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "15").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "29").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "5").build()).build());

		result = rangeMore.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(0));
		result = rangeMore.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeMore.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
	}

	@Test
	public void candleRangeBetweenTest() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();
		PositiveCandleRangeOpenCloseBetween rangeBetween = (PositiveCandleRangeOpenCloseBetween) TradingSignalEnum.POSITIVE_CANDLERANGE_BETWEEN_OC.getInstance(10, 20);
		SignalResult result = rangeBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(0));
        //                                                                                                         o    h    l      c  
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "15").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "29").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("10", "100", "0", "5").build()).build());


		result = rangeBetween.processCandles(candleList, 0);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeBetween.processCandles(candleList, 1);
		assertThat(result.isSuccess(), Matchers.is(true));
		assertThat(result.getCandleIndex(), Matchers.is(1));
		result = rangeBetween.processCandles(candleList, 2);
		assertThat(result.isSuccess(), Matchers.is(false));
		assertThat(result.getCandleIndex(), Matchers.is(3));
	}
}
