package rawai.it.com.TradingPlatform.instrument;



import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleListResponse;
import rawai.it.com.TradingPlatform.config.ApplicationConfig;
import rawai.it.com.TradingPlatform.config.EmbeddedDataSourceConfig;
import rawai.it.com.TradingPlatform.config.JpaConfig;
import rawai.it.com.TradingPlatform.config.NoCsrfSecurityConfig;
import rawai.it.com.TradingPlatform.config.WebMvcConfig;
import rawai.it.com.TradingPlatform.config.WebSocketConfig;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.ChartInstrumentBuilder;
import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;
import rawai.it.com.TradingPlatform.urlutils.RegisterEnum;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

//@PowerMockIgnore({ "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest({ InstrumentController.class, InstrumentService.class})
@ActiveProfiles("test")
@WebAppConfiguration
@ContextConfiguration(classes = { ApplicationConfig.class, EmbeddedDataSourceConfig.class, JpaConfig.class, NoCsrfSecurityConfig.class, WebMvcConfig.class, WebSocketConfig.class })
public class InstrumentControllerTest {

	@Mock
	private AccountRepository accountRepository;
	@Mock
	private SimpMessagingTemplate template;
	@Mock
	public HttpSession session;

	@InjectMocks
	private InstrumentController instrumentConroller;

	@Inject
	protected WebApplicationContext wac;
	protected MockMvc mockMvc;

	@Autowired
	private InstrumentService instrumentService;

	@Test
	public void instrumentServiceTest() {
		Calendar gc = GregorianCalendar.getInstance();
		gc.set(Calendar.YEAR, 2017);
		gc.set(Calendar.MONTH, 5);
		gc.set(Calendar.DAY_OF_MONTH, 1);

		Date startDate = gc.getTime();
		gc.set(Calendar.DAY_OF_MONTH, 30);
		Date endDate = gc.getTime();

		List<TimeRange> timeRanges = instrumentService.createTimeRanges(new DateTime(startDate, DateTimeZone.UTC), new DateTime(endDate, DateTimeZone.UTC),
				IntervalEnum.valueOf("M1").toSeconds());
		for (TimeRange timeRange : timeRanges) {
			System.out.println(timeRange.getStartDate() + " " + timeRange.getEndDate());
		}

		instrumentService.fetchCandlesFromTo("101-011-5094777-001", "EUR_USD", "M5", startDate, endDate);
	}

	@Before
	public void setup() {

		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		// Setup Spring test in standalone mode
		this.mockMvc = MockMvcBuilders.standaloneSetup(instrumentConroller).build();
	//
	}

	@Test
	public void testCandleDateFetch() {
		Date startDate = null;
		Date endDate = new Date();

		Calendar gc = GregorianCalendar.getInstance();
		gc.set(Calendar.DAY_OF_YEAR, gc.get(Calendar.DAY_OF_YEAR) - 10);
		startDate = gc.getTime();

		List<TimeRange> timeRanges = instrumentService.createTimeRanges(new DateTime(startDate), new DateTime(endDate), IntervalEnum.S5.toSeconds());

		System.out.println("Calculating from to " + startDate + " " + endDate);
		for (TimeRange timeRange : timeRanges) {
			System.out.println(timeRange.getStartDate() + " " + timeRange.getEndDate());
		}

		System.out.println("Schnuffelich");
		List<Candle> candles = instrumentService.fetchCandlesFromTo("101-011-5094777-001", "EUR_USD", "S5", startDate, endDate);

		for (Candle candle : candles) {
			System.out.println(candle.getTime());
		}

		System.out.println("Number of Candles " + candles.size());

		// instrumentService.fetchCandlesFromTo("101-011-5094777-001",
		// "EUR_USD", "S5", startDate, endDate);
	}

	@Test
	public void testFetchCandleWithNoLAstCandle() throws Exception {
		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);
		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(null);
		SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "M1");
		String resultCandle = instrumentConroller.fetchCandleForChartWindow("101-011-5094777-001", "EUR_USD", "M5");
		assertTrue(resultCandle.indexOf("Candle") != -1);
		PowerMockito.verifyNew(RestTemplate.class).withNoArguments();
	}
	
	
	
	@Test
	public void testFindCandleUnixTime() throws Exception {
		// 2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("1489387195.000000000").build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("1489387200.000000000").build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("1489387205.000000000").build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("1489387210.000000000").build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);
		ChartInstrument chartInstrument = ChartInstrumentBuilder.aChartInstrument().withTime("1489387200.000000000").build();

		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);
		assertTrue(instrumentConroller.findLastCandleIndex(candleList, chartInstrument) == 1);
		chartInstrument.setTime("nonsens");
		assertTrue(instrumentConroller.findLastCandleIndex(candleList, chartInstrument) == -1);
	}

	@Test
	public void testFindCandle() throws Exception {
		//2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:00.000000000Z").build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:05.000000000Z").build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:10.000000000Z").build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:15.000000000Z").build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);		
		ChartInstrument chartInstrument = ChartInstrumentBuilder.aChartInstrument().withTime("2017-02-28T04:40:05.000000000Z").build();
		
		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);
		assertTrue(instrumentConroller.findLastCandleIndex(candleList, chartInstrument) == 1);
		chartInstrument.setTime("nonsens");
		assertTrue(instrumentConroller.findLastCandleIndex(candleList, chartInstrument) == -1);
	}
	
	@Test
	public void testFetchCandleUnixTime() throws Exception {
		// 2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("1489387195.000000000").withEmptyMid().build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("1489387200.000000000").withEmptyMid().build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("1489387205.000000000").withEmptyMid().build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("1489387210.000000000").withEmptyMid().build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);

		SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		SessionUtils.setLastFechtedCandle("101-011-5094777-001", "EUR_USD", "1489387195.000000000", "S5");

		CandleListResponse candleListResponse = new CandleListResponse();
		candleListResponse.setCandles(candleList);

		ResponseEntity<CandleListResponse> candleListResponseEntity = new ResponseEntity<CandleListResponse>(candleListResponse, HttpStatus.OK);

		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);

		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(
				candleListResponseEntity);

		String candle = instrumentConroller.fetchCandleForChartWindow("101-011-5094777-001", "EUR_USD", "S5");

		assertTrue(candle.indexOf("1489387200") != -1);

	}

	@Test
	public void testFetchCandle() throws Exception {
		//2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:00.000000000Z").withEmptyMid().build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:05.000000000Z").withEmptyMid().build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:10.000000000Z").withEmptyMid().build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:15.000000000Z").withEmptyMid().build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);		

		SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		SessionUtils.setLastFechtedCandle("101-011-5094777-001", "EUR_USD", "2017-02-28T04:40:00.000000000Z", "S5");
		
		CandleListResponse candleListResponse = new CandleListResponse();
		candleListResponse.setCandles(candleList);

		ResponseEntity<CandleListResponse> candleListResponseEntity = new ResponseEntity<CandleListResponse>(candleListResponse, HttpStatus.OK);

		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);
		
		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(
				candleListResponseEntity);

		String candle = instrumentConroller.fetchCandleForChartWindow("101-011-5094777-001", "EUR_USD", "S5");

		assertTrue(candle.indexOf("2017-02-28T04:40:05.000000000Z") != -1);
		
	}

	@Test
	public void testFetchCandleWithMissingCandleUnixTime() throws Exception {
		// 2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("1489392300.000000000").withEmptyMid().build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("1489392320.000000000").withEmptyMid().build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("1489392325.000000000").withEmptyMid().build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("1489392330.000000000").withEmptyMid().build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);

		SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		SessionUtils.setLastFechtedCandle("101-011-5094777-001", "EUR_USD", "1489392300.000000000", "S5");

		CandleListResponse candleListResponse = new CandleListResponse();
		candleListResponse.setCandles(candleList);

		ResponseEntity<CandleListResponse> candleListResponseEntity = new ResponseEntity<CandleListResponse>(candleListResponse, HttpStatus.OK);

		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);

		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(
				candleListResponseEntity);

		String candle = instrumentConroller.fetchCandleForChartWindow("101-011-5094777-001", "EUR_USD", "S5");

		assertTrue(candle.indexOf("missing") != -1);
	}

	@Test
	public void testFetchCandleWithMissingCandle() throws Exception {
		// 2017-02-28T04:40:10.000000Z"
		Candle candle1 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:00.000000000Z").withEmptyMid().build();
		Candle candle2 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:20.000000000Z").withEmptyMid().build();
		Candle candle3 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:25.000000000Z").withEmptyMid().build();
		Candle candle4 = CandleBuilder.aCandle().isComplete(true).withTime("2017-02-28T04:40:30.000000000Z").withEmptyMid().build();
		List<Candle> candleList = new ArrayList<>();
		candleList.add(candle1);
		candleList.add(candle2);
		candleList.add(candle3);
		candleList.add(candle4);

		SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		SessionUtils.setLastFechtedCandle("101-011-5094777-001", "EUR_USD", "2017-02-28T04:40:00.000000000Z", "S5");

		CandleListResponse candleListResponse = new CandleListResponse();
		candleListResponse.setCandles(candleList);

		ResponseEntity<CandleListResponse> candleListResponseEntity = new ResponseEntity<CandleListResponse>(candleListResponse, HttpStatus.OK);

		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);

		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		PowerMockito.when(restTemplate.exchange(Matchers.anyString(), Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>> any(), Matchers.any(Class.class))).thenReturn(
				candleListResponseEntity);

		String candle = instrumentConroller.fetchCandleForChartWindow("101-011-5094777-001", "EUR_USD", "S5");

		assertTrue(candle.indexOf("missing") != -1);
	}


	@Test
	public void testRegisterForChartInstrument() throws Exception {
		//PowerMockito.mockStatic(LogManagerFactory.class);
		//LogManager logger = PowerMockito.mock(LogManager.class);
		//PowerMockito.when(LogManagerFactory.getLogManager(Matchers.any(Class.class))).thenReturn(logger);
		SessionUtils.chartInstrumentMap = new HashMap<String, List<ChartInstrument>>();
		RestTemplate restTemplate = PowerMockito.mock(RestTemplate.class);
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);

		RegisterEnum result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		assertTrue(result == RegisterEnum.OK);
		result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		assertTrue(result == RegisterEnum.ALREADY_REGISTERED);
		result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		assertTrue(result == RegisterEnum.ALREADY_REGISTERED);
		result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S10");
		assertTrue(result == RegisterEnum.OK);
		result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S30");
		assertTrue(result == RegisterEnum.OK);
		result = SessionUtils.registerChartInstrument("101-011-5094777-001", "EUR_USD", "S30");
		assertTrue(result == RegisterEnum.ALREADY_REGISTERED);
		assertTrue(SessionUtils.getChartInstrumentMap().get("101-011-5094777-001").size() == 3);
		SessionUtils.unregisterChartInstrument("101-011-5094777-001", "EUR_USD", "S5");
		SessionUtils.unregisterChartInstrument("101-011-5094777-001", "EUR_USD", "S30");
		assertTrue(SessionUtils.getChartInstrumentMap().get("101-011-5094777-001").size() == 1);
	}

}
