package rawai.it.com.TradingPlatform.config;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.support.MessageBuilder;

import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SessionUtils.class)
public class PresenceChannelInterceptorTest {

	@Test
	public void startStopDashboard() {
		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();

		Map<String, Object> headers = new HashMap<String, Object>();
		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		headers.put("simpMessageType", StompCommand.CONNECT);
		headers.put("stompCommand", StompCommand.CONNECT);
		headers.put("simpSessionId", "urxjy068");
		headers.put("nativeHeaders", nativeHeaders);
		headers.put("simpSessionAttributes", simpSessionAttributes);

		token.add("DASHBOARD");
		nativeHeaders.put("token", token);

		Message<String> message1 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.CONNECT).setHeader("stompCommand", StompCommand.CONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message1, null, true);


		assertThat(SessionUtils.isDashBoardActive(), Matchers.is(true));

		Message<String> message2 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.DISCONNECT).setHeader("stompCommand", StompCommand.DISCONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message2, null, true);

		assertThat(SessionUtils.isDashBoardActive(), Matchers.is(false));
	}

	@Test
	public void startStopPosition() {
		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();

		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		token.add("ALL_POSITIONS");
		nativeHeaders.put("token", token);

		Message<String> message1 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.CONNECT).setHeader("stompCommand", StompCommand.CONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message1, null, true);

		assertThat(SessionUtils.isWatchPositions(), Matchers.is(true));

		Message<String> message2 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.DISCONNECT).setHeader("stompCommand", StompCommand.DISCONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message2, null, true);

		assertThat(SessionUtils.isWatchPositions(), Matchers.is(false));
	}

	@Test
	public void startStopSinglePosition() {
		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();

		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		token.add("SINGLE_POSITION");
		nativeHeaders.put("token", token);

		Message<String> message1 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.CONNECT).setHeader("stompCommand", StompCommand.CONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message1, null, true);

		assertThat(SessionUtils.isShowSingleInstrument(), Matchers.is(true));

		Message<String> message2 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.DISCONNECT).setHeader("stompCommand", StompCommand.DISCONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message2, null, true);

		assertThat(SessionUtils.isShowSingleInstrument(), Matchers.is(false));
	}

	@Test
	public void startStopAllOrders() {
		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();

		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		token.add("ALL_ORDERS");
		nativeHeaders.put("token", token);

		Message<String> message1 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.CONNECT).setHeader("stompCommand", StompCommand.CONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message1, null, true);

		assertThat(SessionUtils.isWatchOrders(), Matchers.is(true));

		Message<String> message2 = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.DISCONNECT).setHeader("stompCommand", StompCommand.DISCONNECT)
				.setHeader("simpSessionId", "1").setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		interceptor.postSend(message2, null, true);

		assertThat(SessionUtils.isWatchOrders(), Matchers.is(false));
	}

	@Test
	public void startTwoDashBoardsStopOne() {
		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();
		interceptor.postSend(connectMessage("DASHBOARD", "1"), null, true);
		interceptor.postSend(connectMessage("DASHBOARD", "2"), null, true);

		assertThat(SessionUtils.isDashBoardActive(), Matchers.is(true));

		interceptor.postSend(disconnectMessage("DASHBOARD", "1"), null, true);
		assertThat(SessionUtils.isDashBoardActive(), Matchers.is(true));
	}

	@Test
	public void startTwoTransactionsStopOne() {
		PowerMockito.mockStatic(SessionUtils.class);

		PresenceChannelInterceptor interceptor = new PresenceChannelInterceptor();
		interceptor.postSend(connectMessage("TRANSACTIONS", "1"), null, true);
		interceptor.postSend(connectMessage("TRANSACTIONS", "2"), null, true);

		PowerMockito.verifyStatic(Mockito.times(1));


		interceptor.postSend(disconnectMessage("TRANSACTIONS", "1"), null, true);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(true));

		interceptor.postSend(disconnectMessage("TRANSACTIONS", "2"), null, true);
		assertThat(SessionUtils.transactionStreamsRunning(), Matchers.is(false));
	}


	Message<String> connectMessage(String tokenItem, String sessionID) {
		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		simpSessionAttributes.put("Service", tokenItem);

		token.add(tokenItem);
		nativeHeaders.put("token", token);

		Message<String> message = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.CONNECT).setHeader("stompCommand", StompCommand.CONNECT)
				.setHeader("simpSessionId", sessionID).setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		return message;
	}

	Message<String> disconnectMessage(String tokenItem, String sessionID) {
		Map<String, Object> nativeHeaders = new HashMap<String, Object>();
		Map<String, Object> simpSessionAttributes = new HashMap<String, Object>();
		List<String> token = new ArrayList<>();

		simpSessionAttributes.put("Service", tokenItem);
		token.add(tokenItem);
		nativeHeaders.put("token", token);

		Message<String> message = MessageBuilder.withPayload("test").setHeader("simpMessageType", StompCommand.DISCONNECT).setHeader("stompCommand", StompCommand.DISCONNECT)
				.setHeader("simpSessionId", sessionID).setHeader("nativeHeaders", nativeHeaders).setHeader("simpSessionAttributes", simpSessionAttributes).build();

		return message;
	}

}
