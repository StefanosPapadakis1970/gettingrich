package rawai.it.com.TradingPlatform.engine.signals;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleDataPointBuilder;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class BeforeTest {

	@Test
	public void testGreaterThenBefore() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00000").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00004").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00002").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00006").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00039").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00055").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.00067").build()).build());

		GreaterThanBefore before1 = (GreaterThanBefore) TradingSignalEnum.GREATER_THAN_BEFORE.getInstance(1);
		SignalResult result = null;
		result = before1.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = before1.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = before1.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = before1.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = before1.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = before1.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = before1.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == true);

		GreaterThanBefore before2 = (GreaterThanBefore) TradingSignalEnum.GREATER_THAN_BEFORE.getInstance(2);
		result = before2.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = before2.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = before2.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = before2.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = before2.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = before2.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = before2.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == true);

		GreaterThanBefore before5 = (GreaterThanBefore) TradingSignalEnum.GREATER_THAN_BEFORE.getInstance(5);
		result = before5.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = before5.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = before5.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = before5.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = before5.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = before5.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = before5.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == true);
	}

	@Test
	public void testLessThenBefore() {
		List<CandleDataPointInterface> candleList = new ArrayList<>();

		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01100").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01090").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01080").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01070").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01060").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01050").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01080").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01030").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01020").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01010").build()).build());
		candleList.add(CandleDataPointBuilder.aCandleDataPoint().withCandle(CandleBuilder.aCandle().withFilledMid("0", "0", "0", "0.01000").build()).build());

		LessThanBefore lessThan1 = (LessThanBefore) TradingSignalEnum.LESS_THAN_BEFORE.getInstance(1);
		SignalResult result = null;
		result = lessThan1.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = lessThan1.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == true);
		result = lessThan1.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = lessThan1.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = lessThan1.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = lessThan1.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = lessThan1.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == false);

		LessThanBefore lessThan2 = (LessThanBefore) TradingSignalEnum.LESS_THAN_BEFORE.getInstance(2);
		result = lessThan2.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = lessThan2.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = lessThan2.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == true);
		result = lessThan2.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == true);
		result = lessThan2.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == true);
		result = lessThan2.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = lessThan2.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == false);

		LessThanBefore lessThan5 = (LessThanBefore) TradingSignalEnum.LESS_THAN_BEFORE.getInstance(5);
		result = lessThan5.processCandles(candleList, 0);
		assertTrue(result.getCandleIndex() == 0 && result.isSuccess() == false);
		result = lessThan5.processCandles(candleList, 1);
		assertTrue(result.getCandleIndex() == 1 && result.isSuccess() == false);
		result = lessThan5.processCandles(candleList, 2);
		assertTrue(result.getCandleIndex() == 2 && result.isSuccess() == false);
		result = lessThan5.processCandles(candleList, 3);
		assertTrue(result.getCandleIndex() == 3 && result.isSuccess() == false);
		result = lessThan5.processCandles(candleList, 4);
		assertTrue(result.getCandleIndex() == 4 && result.isSuccess() == false);
		result = lessThan5.processCandles(candleList, 5);
		assertTrue(result.getCandleIndex() == 5 && result.isSuccess() == true);
		result = lessThan5.processCandles(candleList, 6);
		assertTrue(result.getCandleIndex() == 6 && result.isSuccess() == true);

	}

}
