package rawai.it.com.TradingPlatform.tradingprofile;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.config.WebAppConfigurationAware;
import rawai.it.com.TradingPlatform.databuilders.TestDataProvider;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

import com.github.springtestdbunit.annotation.DbUnitConfiguration;


@DbUnitConfiguration(databaseConnection = { "testSchnuffi" })
public class TradingProfileDBTest extends WebAppConfigurationAware {


	@Autowired
	private TradingRepository tradingRepositiory;

	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Test
	public void insertUpdateDeleteTest() {
		TradingProfileEntity profile = TestDataProvider.buildTradingProfileEntity("101-011-5094777-001");
		TradingProfileEntity dbProfile;
		tradingRepositiory.saveTradingProfile(profile);
		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");
		assertThat(profile.getUserID()).isEqualTo(dbProfile.getUserID());

		tradingRepositiory.updateTradingProfile(dbProfile);
		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");

		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");

		assertThat(dbProfile.getTradingAlgorithms().get(0).getTradingSignals().get(0).getTradingSignal()).isEqualTo(TradingSignalEnum.UPWARD_ANY);
		assertThat(dbProfile.getTradingAlgorithms().get(0).getTradingSignals().get(1).getTradingSignal()).isEqualTo(TradingSignalEnum.UPWARD_LESS);
		assertThat(dbProfile.getTradingAlgorithms().get(0).getTradingSignals().get(2).getTradingSignal()).isEqualTo(TradingSignalEnum.CANDLE_ENDPOINT);

		tradingRepositiory.deleteTradingProfile(dbProfile);
		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");
		assertThat(dbProfile).isNull();
	}

	@Test
	public void updateTradingPfofileAndSaveAsEntityTest() {
		TradingProfileEntity profile = TestDataProvider.buildTradingProfileEntity("4711");
		TradingProfileEntity dbProfile;
		profile.setUserID("4711");
		tradingRepositiory.saveTradingProfile(profile);
		dbProfile = tradingRepositiory.findByUserID("4711");
		assertThat(profile.getUserID()).isEqualTo(dbProfile.getUserID());

		Account account = new Account();
		account.setUserId("4712");
		TradingProfile editProfile = TradingProfileConverter.convertToTradingProfile(dbProfile, account);
		editProfile.getAccount().setUserId("4713");
		TradingAlgorithm algorithm = new TradingAlgorithm();
		TradingInstrument instrument = new TradingInstrument();
		instrument.setInstument("AUD_USD");
		instrument.setUnits(300);
		instrument.setTradingInterval(IntervalEnum.M15);

		TradingSignal signal = TradingSignalEnum.DOWNWARD_BETWEEN.getInstance(5, 6);
		algorithm.getTradingSignals().add(signal);
		algorithm.setBuy(true);
		editProfile.getTradingAlgorithms().add(algorithm);
		editProfile.getTradingInstruments().add(instrument);
		editProfile.getTradingAlgorithms().get(0).setInterval(IntervalEnum.M15);

		TradingSignal firstSignal = editProfile.getTradingAlgorithms().get(0).getTradingSignals().get(0);
		editProfile.getTradingAlgorithms().get(0).getTradingSignals().remove(0);
		editProfile.getTradingAlgorithms().get(0).getTradingSignals().add(firstSignal);


		TradingProfileEntity editProfileEntity = TradingProfileConverter.convertToTradingProfileEntity(editProfile);
		// TradingProfileConverter.applyChanges(dbProfile, editProfileEntity);

		tradingRepositiory.updateTradingProfile(editProfileEntity);


		dbProfile = tradingRepositiory.findByUserID("4713");
		assertThat(dbProfile).isNotNull();
		assertThat(dbProfile.getTradingAlgorithms().get(1).getTradingSignals().get(0).getTradingSignal()).isEqualTo(TradingSignalEnum.DOWNWARD_BETWEEN);
		assertThat(dbProfile.getTradingAlgorithms().get(1).isBuy()).isEqualTo(true);
		assertThat(dbProfile.getTradingAlgorithms().get(1).getTradingSignals().get(0).getLow()).isEqualTo(5);
		assertThat(dbProfile.getTradingAlgorithms().get(1).getTradingSignals().get(0).getHigh()).isEqualTo(6);
		assertThat(dbProfile.getTradingAlgorithms().get(0).getTradingSignals().get(2).getTradingSignal()).isEqualTo(firstSignal.getTradingSignalEnum());
		tradingRepositiory.deleteTradingProfile(dbProfile);
	}

	@Test
	public void testRemoveInstruments() {
		TradingProfileEntity profile = TestDataProvider.buildTradingProfileEntity("101-011-5094777-001");
		TradingProfileEntity dbProfile;
		tradingRepositiory.saveTradingProfile(profile);
		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");
		dbProfile.getTradingInstruments().remove(0);
		dbProfile.getTradingAlgorithms().remove(0);

		tradingRepositiory.updateTradingProfile(dbProfile);
		dbProfile = tradingRepositiory.findByUserID("101-011-5094777-001");
		assertThat(dbProfile).isNotNull();
		assertThat(dbProfile.getTradingAlgorithms().size()).isEqualTo(0);
		assertThat(dbProfile.getTradingInstruments().size()).isEqualTo(0);
	}

	@After
	public void tearDown() {

		tradingRepositiory.deleteAllTradingProfiles();
	}
}
