function PriceConnectionPlain() {
	
	var priceMapBid = [];
	var priceMapAsk = [];
	var colorMapBid = [];
	var colorMapAsk = [];
	var priceClient = "";
		
	PriceConnectionPlain.prototype.getAskColor = function(instrument){
		return colorMapAsk[instrument];
	};
	
	PriceConnectionPlain.prototype.getBidColor = function(instrument){
		return colorMapBid[instrument];
	};
	
	PriceConnectionPlain.prototype.getBidPrice = function(instrument){
		return priceMapBid[instrument];
	};
	
	PriceConnectionPlain.prototype.getAskPrice = function(instrument){
		return priceMapAsk[instrument];
	};
	
	PriceConnectionPlain.prototype.close = function(){
		priceClient.disconnect();
	}
	PriceConnectionPlain.prototype.createConnection = function(token, bidID, askID, instrument){   	
		
    	var priceEndpoint = "prices";
    	var priceSocket = new SockJS(baseurl+priceEndpoint);
    	priceClient = Stomp.over(priceSocket);
    	priceClient.debug = null;
    	priceClient.connect({token: token}, function(frame) {
    		priceClient.subscribe("/topic/prices", function(price){  			
    			var displayPrice = JSON.parse(price.body);
    			var lastBidColor = "";
    			var lastAskColor = "";
    			
    			if (instrument != displayPrice.instrument){
    				return true;
    			}
    			
    			if (priceMapAsk[displayPrice.instrument] != displayPrice.closeoutAsk){
        			$("[id^='ask"+displayPrice.instrument+"']").removeClass();
    				
        			if (priceMapAsk[displayPrice.instrument]< displayPrice.closeoutAsk){            					
        				$("[id^='ask"+displayPrice.instrument+"']").css( "color", "green");
        				lastAskColor = "green";
        				
        			} 
        			if (priceMapAsk[displayPrice.instrument]> displayPrice.closeoutAsk){
        				$("[id^='ask"+displayPrice.instrument+"']").css( "color", "red");
        				lastAskColor = "red";
        			}
    			}
    			
    			if (priceMapBid[displayPrice.instrument] != displayPrice.closeoutBid){
        			if (priceMapBid[displayPrice.instrument]< displayPrice.closeoutBid){            					
        				lastBidColor = "green";
        			} 
        			if (priceMapAsk[displayPrice.instrument]> displayPrice.closeoutAsk){                				
        				lastBidColor = "red";
        			}            				
    			}

    			if (lastBidColor != ""){
    				colorMapBid[displayPrice.instrument] = lastBidColor;	
    			}
    			
    			if (lastAskColor != ""){
    				colorMapAsk[displayPrice.instrument] = lastAskColor;	
    			}
    			
    			priceMapBid[displayPrice.instrument] = [displayPrice.closeoutBid];
    			priceMapAsk[displayPrice.instrument] = [displayPrice.closeoutAsk];
    			
    			$(bidID).html(displayPrice.closeoutBid);
    			$(askID).html(displayPrice.closeoutAsk);
    			$(bidID).css("color",colorMapBid[displayPrice.instrument] );
    			$(askID).css("color",colorMapAsk[displayPrice.instrument] );
    			            			
    		});
    	});
	};
}