function PriceConnection() {
	
	var priceMapBid = [];
	var priceMapAsk = [];
	var colorMapBid = [];
	var colorMapAsk = [];
	var priceClient  = "";
		
	PriceConnection.prototype.getAskColor = function(instrument){
		return colorMapAsk[instrument];
	};
	
	PriceConnection.prototype.getBidColor = function(instrument){
		return colorMapBid[instrument];
	};
	
	PriceConnection.prototype.getBidPrice = function(instrument){
		return priceMapBid[instrument];
	};
	
	PriceConnection.prototype.getAskPrice = function(instrument){
		return priceMapAsk[instrument];
	};
	
	PriceConnection.prototype.close = function(){
		priceClient.disconnect();
	}
	
	
	PriceConnection.prototype.createConnection = function(token){
   	
		
    	var priceEndpoint = "prices";
    	var priceSocket = new SockJS(baseurl+priceEndpoint);
    	priceClient = Stomp.over(priceSocket);
    	priceClient.debug = null;
    	priceClient.connect({token: token}, function(frame) {
    		priceClient.subscribe("/topic/prices", function(price){
    			var displayPrice = JSON.parse(price.body);
    			var lastBidColor = "";
    			var lastAskColor = "";
    			if (priceMapAsk[displayPrice.instrument] != displayPrice.closeoutAsk){
        			$("[id^='ask"+displayPrice.instrument+"']").removeClass();
    				
        			if (priceMapAsk[displayPrice.instrument]< displayPrice.closeoutAsk){            					
        				$("[id^='ask"+displayPrice.instrument+"']").css( "color", "green");
        				lastAskColor = "green";
        				
        			} 
        			if (priceMapAsk[displayPrice.instrument]> displayPrice.closeoutAsk){
        				$("[id^='ask"+displayPrice.instrument+"']").css( "color", "red");
        				lastAskColor = "red";
        			}
    			}
    			
    			if (priceMapBid[displayPrice.instrument] != displayPrice.closeoutBid){
        			$("[id^='bid"+displayPrice.instrument+"']").removeClass();

        			if (priceMapBid[displayPrice.instrument]< displayPrice.closeoutBid){            					
        				$("[id^='bid"+displayPrice.instrument+"']").css( "color", "green");
        				lastBidColor = "green";
        				
        			} 
        			if (priceMapAsk[displayPrice.instrument]> displayPrice.closeoutAsk){                				
        				$("[id^='bid"+displayPrice.instrument+"']").css( "color", "red");
        				lastBidColor = "red";
        			}            				
    			}

    			$("[id^='bid"+displayPrice.instrument+"']").html(displayPrice.closeoutBid);
    			$("[id^='ask"+displayPrice.instrument+"']").html(displayPrice.closeoutAsk);

    			if (lastBidColor != ""){
    				colorMapBid[displayPrice.instrument] = lastBidColor;	
    			}
    			
    			if (lastAskColor != ""){
    				colorMapAsk[displayPrice.instrument] = lastAskColor;	
    			}
    			
    			
    			priceMapBid[displayPrice.instrument] = [displayPrice.closeoutBid];
    			priceMapAsk[displayPrice.instrument] = [displayPrice.closeoutAsk];
    			            			
    		});
    	});
	};
}