package rawai.it.com.TradingPlatform.candle;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandleBidAskDataPointListResponse {

	private List<CandleBidAskDataPoint> candles;

	public List<CandleBidAskDataPoint> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleBidAskDataPoint> candles) {
		this.candles = candles;
	}
}
