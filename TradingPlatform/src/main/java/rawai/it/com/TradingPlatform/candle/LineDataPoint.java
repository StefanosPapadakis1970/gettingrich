package rawai.it.com.TradingPlatform.candle;

import rawai.it.com.TradingPlatform.utils.FormatingUtils;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonRawValue;

public class LineDataPoint implements BasicDataPointInterface {

	private Long x;
	@JsonRawValue
	private String y;
	private String time;
	private String lineColor;
	private String markerType = "none";

	@Override
	public String getY() {
		return y;
	}

	@Override
	public Long getX() {
		return Instant.parse(time).toEpochMilli();
    }

	@Override
	public String getTime() {
		return time;
	}

	public void setX(Long x) {
		this.x = x;
	}

	public void setY(String y) {
		this.y = y;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLineColor() {
		return lineColor;
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

}
