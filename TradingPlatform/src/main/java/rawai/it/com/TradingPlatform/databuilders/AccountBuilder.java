package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.account.Account;

public class AccountBuilder {

	private Long id;
	private String email;
	private String password;
	private String role = "ROLE_USER";
	private String apiKey;
	private String userName;
	private String userId;

	private AccountBuilder() {

	}

	public static AccountBuilder aAccount() {
		return new AccountBuilder();
	}

	public AccountBuilder withUserID(String userID) {
		this.userId = userID;
		return this;
	}

	public AccountBuilder withID(Long id) {
		this.id = id;
		return this;
	}

	public AccountBuilder withUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public Account build() {
		Account account = new Account();
		account.setUserId(userId);
		account.setId(id);
		account.setUserName(userName);
		return account;
	}
}
