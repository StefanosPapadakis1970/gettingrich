package rawai.it.com.TradingPlatform.candle;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandleDataPointListResponse {

	@JsonProperty("candles")
	private List<CandleDataPoint> candles;

	public List<CandleDataPoint> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleDataPoint> candles) {
		this.candles = candles;
	}
}
