package rawai.it.com.TradingPlatform.engine.postsignals;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;
import rawai.it.com.TradingPlatform.utils.TradingUtils;


public class StopLossPostTradingSignal extends AbstractPostTradingSignal {

	public StopLossPostTradingSignal(int candlesBefore) {
		setValue(candlesBefore);
		setPostTradeSignalEnum(PostTradingSignalEnum.STOPLOSS);
	}

	@Override
	public void postProcessTrade(AlgorithmTrade trade) {
		CandleDataPointInterface stopLossCandle = trade.getCandleBeforeTradingCandle(getValue());		
		Double newBasePrice = Double.valueOf(stopLossCandle.getC());		
		Double newStopLossPrice = TradingUtils.calculateLossProfitPrice(newBasePrice, trade.getStopLossPips(), trade.isBuySell(), false);
		trade.setStopLossPrice(newStopLossPrice);
		trade.setStopLossMoved(true);
	}

	@Override
	public void dynamicPostProcessTrade(AlgorithmTrade trade, Double price) {
		// TODO Auto-generated method stub
	}
}
