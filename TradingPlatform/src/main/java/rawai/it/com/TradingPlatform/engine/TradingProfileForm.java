package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.databuilders.TradingProfileBuilder;

public class TradingProfileForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	@Valid
	private List<TradingInstrumentForm> tradingInstruments = new ArrayList<>();

	private List<TradingAlgorithmForm> tradingAlgorithms = new ArrayList<>();

	@NotBlank(message = TradingProfileForm.NOT_BLANK_MESSAGE)
	private String userID;

	private String userName;

	private String dbID;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public List<TradingInstrumentForm> getTradingInstruments() {
		return tradingInstruments;
	}

	public void setTradingInstruments(List<TradingInstrumentForm> tradingInstruments) {
		this.tradingInstruments = tradingInstruments;
	}

	public TradingProfile createTradingProfile() {
		Account account = new Account();
		account.setUserId(userID);

		TradingProfileBuilder builder = TradingProfileBuilder.aTradingProfileBuilder().withAccount(account)
				.withdbID((dbID == null || dbID.equals("") || dbID.equals("null")) ? null : Long.valueOf(dbID));
		for (TradingInstrumentForm form : tradingInstruments) {
			builder.withTradingInstrument(form.createTradingInstrument());
		}

		for (TradingAlgorithmForm algorithm : tradingAlgorithms) {
			builder.withTradingAlgorithm(algorithm.createAlgorithm());
		}

		return builder.build();
	}

	public String getDbID() {
		return dbID;
	}

	public void setDbID(String dbID) {
		this.dbID = dbID;
	}

	public List<TradingAlgorithmForm> getTradingAlgorithms() {
		return tradingAlgorithms;
	}

	public void setTradingAlgorithms(List<TradingAlgorithmForm> tradingAlgorithms) {
		this.tradingAlgorithms = tradingAlgorithms;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
