package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitOrder {

	private String id;
	private String time;
	private String userID;
	private String accountID;
	private String batchID;
	private String requestID;
	private TransactionType type;
	private String tradeID;
	private String clientTradeID;
	private String price;
	private String timeInForce;
	private String gtdTime;
	private OrderTriggerCondition triggerCondition;
	private TakeProfitOrderReason reason;
	private ClientExtensions clientExtensions;
	private String orderFillTransactionID;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getClientTradeID() {
		return clientTradeID;
	}

	public void setClientTradeID(String clientTradeID) {
		this.clientTradeID = clientTradeID;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getGtdTime() {
		return gtdTime;
	}

	public void setGtdTime(String gtdTime) {
		this.gtdTime = gtdTime;
	}

	public OrderTriggerCondition getTriggerCondition() {
		return triggerCondition;
	}

	public void setTriggerCondition(OrderTriggerCondition triggerCondition) {
		this.triggerCondition = triggerCondition;
	}

	public TakeProfitOrderReason getReason() {
		return reason;
	}

	public void setReason(TakeProfitOrderReason reason) {
		this.reason = reason;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

	public String getOrderFillTransactionID() {
		return orderFillTransactionID;
	}

	public void setOrderFillTransactionID(String orderFillTransactionID) {
		this.orderFillTransactionID = orderFillTransactionID;
	}

}
