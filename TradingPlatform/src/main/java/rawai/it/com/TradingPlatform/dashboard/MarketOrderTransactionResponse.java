package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketOrderTransactionResponse {

	private MarketOrderTransaction marketOrderTransaction;

	public MarketOrderTransaction getMarketOrderTransaction() {
		return marketOrderTransaction;
	}

	public void setMarketOrderTransaction(MarketOrderTransaction marketOrderTransaction) {
		this.marketOrderTransaction = marketOrderTransaction;
	}
}
