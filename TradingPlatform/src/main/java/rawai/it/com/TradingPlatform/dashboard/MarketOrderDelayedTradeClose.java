package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketOrderDelayedTradeClose {

	private String tradeID;
	private String clientTradeID;
	private String sourceTransactionID;

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getClientTradeID() {
		return clientTradeID;
	}

	public void setClientTradeID(String clientTradeID) {
		this.clientTradeID = clientTradeID;
	}

	public String getSourceTransactionID() {
		return sourceTransactionID;
	}

	public void setSourceTransactionID(String sourceTransactionID) {
		this.sourceTransactionID = sourceTransactionID;
	}
}
