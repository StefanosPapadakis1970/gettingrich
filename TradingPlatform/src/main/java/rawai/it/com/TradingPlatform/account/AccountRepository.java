package rawai.it.com.TradingPlatform.account;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Repository
@Transactional(readOnly = false)
public class AccountRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private PasswordEncoder passwordEncoder;

	@PostConstruct
	protected void initialize() {
		SessionUtils.setAccounts(findAll());
	}

	@Transactional
	public void updateAccount(Account account) {
		entityManager.persist(entityManager.merge(account));
	}

	@Transactional
	public Account save(Account account) {
		if (account.getPassword() != null) {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
		}
		entityManager.persist(account);
		return account;
	}

	public Account findByEmail(String email) {
		try {
			return entityManager.createNamedQuery(Account.FIND_BY_EMAIL, Account.class).setParameter("email", email).getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}

	public void deleteAccountByID(long id) {
		Query query = entityManager.createQuery("DELETE FROM Account AS a WHERE a.id=:id");
		query.setParameter("id", id);
		try {
			int result = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Account> findAll() {
		Query query = entityManager.createQuery("SELECT account FROM Account account where account.role ='ROLE_USER'");
		return (List<Account>) query.getResultList();

	}

	public Account findByUserId(String userid) {
		try {
			return entityManager.createNamedQuery(Account.FIND_BY_USERID, Account.class).setParameter("userId", userid).getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}

	public Account findByID(Long id) {
		try {
			return entityManager.createNamedQuery(Account.FIND_BY_ID, Account.class).setParameter("id", id).getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}
}
