package rawai.it.com.TradingPlatform.candle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Candle {

	private boolean complete;
	private Mid mid;
	private Ask ask;
	private Bid bid;
	private String time;
	private String volume;
	private int candleIndex;
	private boolean firstCandleOfTheDay = false;

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public Mid getMid() {
		return mid;
	}

	public void setMid(Mid mid) {
		this.mid = mid;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public int getCandleIndex() {
		return candleIndex;
	}

	public void setCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
	}

	public boolean isFirstCandleOfTheDay() {
		return firstCandleOfTheDay;
	}

	public void setFirstCandleOfTheDay(boolean firstCandleOfTheDay) {
		this.firstCandleOfTheDay = firstCandleOfTheDay;
	}

	public Ask getAsk() {
		return ask;
	}

	public void setAsk(Ask ask) {
		this.ask = ask;
	}

	public Bid getBid() {
		return bid;
	}

	public void setBid(Bid bid) {
		this.bid = bid;
	}
}
