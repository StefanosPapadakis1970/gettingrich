package rawai.it.com.TradingPlatform.engine.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;

@Entity
@Table(name = "posttradesignal")
@NamedQueries({ @NamedQuery(name = PostTradeSignalEntity.FIND_ALL, query = "select postTradingSignal from PostTradeSignalEntity postTradingSignal") })
public class PostTradeSignalEntity {

	public static final String FIND_ALL = "PostTradeSignalEntity.findAll";

	@Id
	@GeneratedValue
	private Long id;

	@Convert(converter = PostTradingSignalEnumConverter.class)
	private PostTradingSignalEnum tradingSignal;

	private int value;

	private boolean dynamic;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "algorithm_postsignal", joinColumns = { @JoinColumn(name = "signal_id") }, inverseJoinColumns = { @JoinColumn(name = "algorithm_id") })
	private List<TradingAlgorithmEntity> tradingalgorithm = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PostTradingSignalEnum getTradingSignal() {
		return tradingSignal;
	}

	public void setTradingSignal(PostTradingSignalEnum signal) {
		this.tradingSignal = signal;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public boolean isDynamic() {
		return dynamic;
	}

	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}

	public List<TradingAlgorithmEntity> getTradingalgorithm() {
		return tradingalgorithm;
	}

	public void setTradingalgorithm(List<TradingAlgorithmEntity> tradingalgorithm) {
		this.tradingalgorithm = tradingalgorithm;
	}
}
