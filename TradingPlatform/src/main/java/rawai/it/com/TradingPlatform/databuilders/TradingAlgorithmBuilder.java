package rawai.it.com.TradingPlatform.databuilders;

import java.util.List;

import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class TradingAlgorithmBuilder {
	
	private Long id;
	private List<TradingSignal> tradingSignals;
	private TradingProfile tradingprofile;
	private IntervalEnum tradingInterval;
	private TradingAlgorithm algorithm = new TradingAlgorithm();
	private String name;
	private int stopLossPips;
	private int takeProfitPips;


	private TradingAlgorithmBuilder() {

	}

	public static TradingAlgorithmBuilder aTradingAlgorithmEntityBuilder() {
		return new TradingAlgorithmBuilder();
	}

	public TradingAlgorithmBuilder withStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
		return this;
	}

	public TradingAlgorithmBuilder withTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
		return this;
	}

	public TradingAlgorithmBuilder withID(long id) {
		this.id = id;
		return this;
	}

	public TradingAlgorithmBuilder withTradingSignals(List<TradingSignal> tradingSignals) {
		this.tradingSignals = tradingSignals;
		return this;
	}

	public TradingAlgorithmBuilder withTradingProfile(TradingProfile tradingprofile) {
		this.tradingprofile = tradingprofile;
		return this;
	}

	public TradingAlgorithmBuilder withInterval(IntervalEnum tradingInterval) {
		this.tradingInterval = tradingInterval;
		return this;
	}

	public TradingAlgorithm build() {
		algorithm.setDbID(id);
		algorithm.setInterval(tradingInterval);
		algorithm.setTradingSignals(tradingSignals);
		algorithm.setName(name);
		algorithm.setStopLossPips(stopLossPips);
		algorithm.setTakeProfitPips(takeProfitPips);
		return algorithm;
	}

	public TradingAlgorithm buildClean() {
		TradingAlgorithm algorithm = new TradingAlgorithm();
		algorithm.setDbID(id);
		algorithm.setInterval(tradingInterval);
		algorithm.setTradingSignals(tradingSignals);
		algorithm.setName(name);
		algorithm.setStopLossPips(stopLossPips);
		algorithm.setTakeProfitPips(takeProfitPips);
		return algorithm;
	}

	public TradingAlgorithmBuilder withName(String algorithmName) {
		this.name = algorithmName;
		return this;
	}
}

