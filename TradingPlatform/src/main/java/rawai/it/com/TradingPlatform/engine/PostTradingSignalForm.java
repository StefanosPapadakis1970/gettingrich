package rawai.it.com.TradingPlatform.engine;

public class PostTradingSignalForm {

	private String dbID;

	private String description;

	public String getDbID() {
		return dbID;
	}

	public void setDbID(String dbID) {
		this.dbID = dbID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
