package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class DownwardDeltaLessThan extends AbstractTradingSignal {

	private int lessThanPips;

	public DownwardDeltaLessThan(int lessThanPips) {
		super(BUYSELL.BUY, TradingSignalEnum.DOWNWARD_DELTA_LESS_THAN, 2);
		this.lessThanPips = lessThanPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface compareCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface loopCandle = candles.get(lastSignalCandleIndex + 1);
		int loop = Integer.valueOf(loopCandle.getC().replace(".", ""));
		int compare = Integer.valueOf(compareCandle.getC().replace(".", ""));
		int diff = loop - compare;
		diff = diff * -1;
		return new SignalResult(diff > 0 && diff <= lessThanPips, lastSignalCandleIndex + 1, false);
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		return 0;
	}

	@Override
	public int getValue() {
		return lessThanPips;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
