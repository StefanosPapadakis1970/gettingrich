package rawai.it.com.TradingPlatform.home;

import java.security.Principal;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Controller
public class EvaluationController {

	// curl -H "Authorization: Bearer 12345678900987654321-abc34135acde13f13530"
	// https://api-fxpractice.oanda.com/

	@RequestMapping(value = "/eval/login", method = RequestMethod.GET)
	@ResponseBody
	public String index(Principal principal) {

		RestTemplate tmpl = new RestTemplate();
		ResponseEntity<String> resp = null;
		try {
			// v3
			resp = tmpl.exchange("https://api-fxpractice.oanda.com/v3/accounts/101-011-5094777-001", HttpMethod.GET, headers(),
					String.class);
			if (resp.getStatusCode() == HttpStatus.OK && resp.hasBody()) {
				return resp.getBody() + "schnulli";
			}
		} catch (RestClientException e) {
			e.printStackTrace();
		}

		return resp.toString();
	}

	private HttpEntity<Object> headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + "bad78168ee789660f40aeecdcbdd5609-c848370d2f9c1d61ec3123acd7d05784");
		headers.set("X-Accept-Datetime-Format", "UNIX");
		HttpEntity<Object> entity = new HttpEntity<>(headers);
		return entity;
	}

}
