package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class UpwardDeltaAny extends AbstractTradingSignal {


	public UpwardDeltaAny() {
		super(BUYSELL.BUY, TradingSignalEnum.UPWARD_DELTA_ANY, 2);
	}


	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}
		CandleDataPointInterface compareCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface loopCandle = candles.get(lastSignalCandleIndex + 1);
		int loop = Integer.valueOf(loopCandle.getC().replace(".", ""));
		int compare = Integer.valueOf(compareCandle.getC().replace(".", ""));
		int diff = loop - compare;

		return new SignalResult(diff > 0, lastSignalCandleIndex + 1, false);
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}

}
