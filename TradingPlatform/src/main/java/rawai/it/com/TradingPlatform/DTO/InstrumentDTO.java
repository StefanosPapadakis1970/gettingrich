package rawai.it.com.TradingPlatform.DTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class InstrumentDTO {

	private String userID;
	private String userName;
	private String instrument;
	@NotBlank(message = "Not Blank")
	@Pattern(regexp = "^[1-9][0-9]*", message = "Not a valid number (>0)")
	@NotNull(message = "Not Null")
	private String units;
	private boolean trade;
	private IntervalEnum interval;

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}



	public boolean isTrade() {
		return trade;
	}

	public void setTrade(boolean trade) {
		this.trade = trade;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public IntervalEnum getInterval() {
		return interval;
	}

	public void setInterval(IntervalEnum interval) {
		this.interval = interval;
	}
}
