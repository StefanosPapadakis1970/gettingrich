package rawai.it.com.TradingPlatform.engine;

import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;

public class TradingSignalToFormConverter {

	public static TradingSignalForm convertEntityToForm(TradingSignalEntity signal) {
		TradingSignalForm signalForm = new TradingSignalForm();
		signalForm.setDbID(String.valueOf(signal.getId()));
		signalForm.setDescription(signal.getTradingSignal().getDescription(signal));
		return signalForm;
	}

	public static PostTradingSignalForm convertPostSignalEntityToForm(PostTradeSignalEntity signal) {
		PostTradingSignalForm signalForm = new PostTradingSignalForm();
		signalForm.setDbID(String.valueOf(signal.getId()));
		signalForm.setDescription(signal.getTradingSignal().getDescription(signal));
		return signalForm;
	}
}
