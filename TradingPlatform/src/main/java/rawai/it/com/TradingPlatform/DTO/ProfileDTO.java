package rawai.it.com.TradingPlatform.DTO;

import java.util.ArrayList;
import java.util.List;

public class ProfileDTO {

	private String userID;

	private String userName;

	private List<InstrumentDTO> instruments = new ArrayList<>();

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<InstrumentDTO> getInstruments() {
		return instruments;
	}

	public void setInstruments(List<InstrumentDTO> instruments) {
		this.instruments = instruments;
	}
}
