package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class CandleRangeLessThan extends AbstractTradingSignal {

	private int lessThanPips;

	public CandleRangeLessThan(int lessThanPips) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.CANDLERANGE_LESS, 1);
		this.lessThanPips = lessThanPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface rangeCandle = candles.get(lastSignalCandleIndex);
		int high = Integer.valueOf(rangeCandle.getH().replace(".", ""));
		int low = Integer.valueOf(rangeCandle.getL().replace(".", ""));

		int diff = high - low;

		if (diff <= lessThanPips) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return lessThanPips;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
