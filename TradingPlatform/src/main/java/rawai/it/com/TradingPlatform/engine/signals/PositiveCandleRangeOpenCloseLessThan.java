package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class PositiveCandleRangeOpenCloseLessThan extends AbstractTradingSignal {

	private int lessThanPips;

	public PositiveCandleRangeOpenCloseLessThan(int lessThanPips) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.NEGATIVE_CANDLERANGE_LESS_OC, 1);
		this.lessThanPips = lessThanPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface rangeCandle = candles.get(lastSignalCandleIndex);
		int open = Integer.valueOf(rangeCandle.getO().replace(".", ""));
		int close = Integer.valueOf(rangeCandle.getC().replace(".", ""));

		int diff = close - open;

		if (diff <= lessThanPips && diff >= 0) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return lessThanPips;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
