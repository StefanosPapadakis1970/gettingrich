package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class DownwardsInversionMoreThan extends AbstractTradingSignal {

	private int moreThanPips;

	public DownwardsInversionMoreThan(int moreThanPips) {
		super(BUYSELL.SELL, TradingSignalEnum.DOWNWARD_MORE, 3);
		this.moreThanPips = moreThanPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface firstCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface middleCandle = candles.get(lastSignalCandleIndex + 1);
		CandleDataPointInterface lastCandle = candles.get(lastSignalCandleIndex + 2);
		int first = Integer.valueOf(firstCandle.getC().replace(".", ""));
		int middle = Integer.valueOf(middleCandle.getC().replace(".", ""));
		int last = Integer.valueOf(lastCandle.getC().replace(".", ""));

		int diff = middle - last;

		if (isDownwardsInversion(first, middle, last) && (diff > moreThanPips)) {
			return new SignalResult(true, lastSignalCandleIndex + 1, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		return 0;
	}

	@Override
	public int getLow() {
		return 0;
	}

	@Override
	public int getValue() {
		return moreThanPips;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
