package rawai.it.com.TradingPlatform.price;

import java.util.List;

public class PriceResponseList {

	private List<Price> prices;

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}
}
