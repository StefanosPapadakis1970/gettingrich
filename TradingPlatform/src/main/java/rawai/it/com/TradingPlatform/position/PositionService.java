package rawai.it.com.TradingPlatform.position;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountComplexCarrier;
import rawai.it.com.TradingPlatform.account.TradeSummary;
import rawai.it.com.TradingPlatform.order.Order;
import rawai.it.com.TradingPlatform.order.OrderListResponse;
import rawai.it.com.TradingPlatform.trade.Trade;
import rawai.it.com.TradingPlatform.trade.TradeListResponse;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import edu.emory.mathcs.backport.java.util.Collections;

@Service
public class PositionService {

	public static final Logger log = LogManager.getLogger(PositionService.class);
	@Autowired
	private CurlUtil curlUtil;

	public List<Position> getAllOpenPositions(String positionSelector) {
		log.info("#############START All Open Positions ########################");
		RestTemplate tmpl = new RestTemplate();
		List<Position> positions = new ArrayList<>();

		log.info("#######################   Start All Optimized ##########################");

		for (Account account : SessionUtils.getAccounts()) {
			String url = curlUtil.createAccountUrl() + "/" + account.getUserId();
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
			ResponseEntity<String> respString = tmpl.exchange(url, HttpMethod.GET, apiHeader, String.class);
			ResponseEntity<AccountComplexCarrier> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, AccountComplexCarrier.class);

			for (Position position : resp.getBody().getAccount().getPositions()) {
				List<String> tradeIDs = new ArrayList<>();
				if (position.getShort().getTradeIDs() != null) {
					tradeIDs.addAll(position.getShort().getTradeIDs());
				}

				if (position.getLong().getTradeIDs() != null) {
					tradeIDs.addAll(position.getLong().getTradeIDs());
				}

				for (String tradeId : tradeIDs) {
					Trade trade = getTradeById(resp.getBody().getAccount().getTrades(), tradeId);
					if (resp.getBody().getAccount().getOrders() != null) {
						List<Order> orders = getOrdersByTradeId(resp.getBody().getAccount().getOrders(), tradeId);
						for (Order order : orders) {
							if (order != null && order.getType().equals("TAKE_PROFIT")) {
								trade.setTakeProfitOrder(order);
							}

							if (order != null && order.getType().equals("STOP_LOSS")) {
								trade.setStopLossOrder(order);
							}

							if (order != null && order.getType().equals("LIMIT")) {
								trade.setStopLossOrder(order);
							}
						}
					}
					position.getTrades().add(trade);
				}
				// Figure out how to determine position is closed !
				position.setAccountName(account.getUserName());
				position.setUserID(account.getUserId());
				positions.add(position);
			}
		}

		// log.info("################  Starting Normal ########################");
		//
		// for (Account account : SessionUtils.getAccounts()) {
		// String url = curlUtil.createAccountUrl() + "/" + account.getUserId()
		// + "/" + positionSelector;
		// HttpEntity<Object> apiHeader =
		// curlUtil.createAPIHeader(account.getApiKey());
		// ResponseEntity<PositionListResponse> resp = tmpl.exchange(url,
		// HttpMethod.GET, apiHeader, PositionListResponse.class);
		// List<Trade> allTrades = getAllOpenTrades(account.getUserId());
		// List<Order> allPendingOrders =
		// getAllPendingOrders(account.getUserId());
		//
		// for (Position position : resp.getBody().getPositions()) {
		// List<String> tradeIDs = new ArrayList<>();
		// if (position.getShort().getTradeIDs() != null) {
		// tradeIDs.addAll(position.getShort().getTradeIDs());
		// }
		//
		// if (position.getLong().getTradeIDs() != null) {
		// tradeIDs.addAll(position.getLong().getTradeIDs());
		// }
		//
		// for (String tradeId : tradeIDs) {
		// Trade trade = getTradeById(allTrades, tradeId);
		// if (allPendingOrders != null) {
		// Order order = getOrderByTradeId(allPendingOrders, tradeId);
		// if (order != null && order.getTradeID() == "TAKE_PROFIT") {
		// trade.setTakeProfitOrder(order);
		// }
		//
		// if (order != null && order.getTradeID() == "STOP_LOSS") {
		// trade.setStopLossOrder(order);
		// }
		//
		// if (order != null && order.getTradeID() == "LIMIT") {
		// trade.setStopLossOrder(order);
		// }
		// }
		// position.getTrades().add(trade);
		// }
		//
		// position.setAccountName(account.getUserName());
		// position.setUserID(account.getUserId());
		// positions.add(position);
		// }
		// }
		// //
		// log.info("################  Finish Normal ########################");
		// //
		// log.info("#############END All Open Positions ########################");
		//
		// log.info("#######################   Finish All Optimized ##########################");
		return positions;
	}

	private TradeSummary getTradeSummaryById(List<TradeSummary> trades, String tradeId) {
		for (TradeSummary trade : trades) {
			if (trade.getId().equals(tradeId)) {
				return trade;
			}
		}
		return null;
	}

	public List<Position> getTopPositions(int numberOfPositions, final boolean winner) {
		RestTemplate tmpl = new RestTemplate();
		List<Position> positions = new ArrayList<>();

		for (Account account : SessionUtils.getAccounts()) {
			String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/openPositions";
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
			ResponseEntity<PositionListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PositionListResponse.class);

			for (Position position : resp.getBody().getPositions()) {
				if ((Double.valueOf(position.getUnrealizedPL()) >= 0 && winner) || (Double.valueOf(position.getUnrealizedPL()) <= 0 && !winner)) {
					position.setAccountName(account.getUserName());
					position.setUserID(account.getUserId());
					positions.add(position);
				}
			}
		}

		Collections.sort(positions, new Comparator<Position>() {
			@Override
			public int compare(Position o1, Position o2) {
				if (winner) {
					return Double.valueOf(o2.getUnrealizedPL()).compareTo(Double.valueOf(o1.getUnrealizedPL()));
				} else {
					return Double.valueOf(o1.getUnrealizedPL()).compareTo(Double.valueOf(o2.getUnrealizedPL()));
				}
			}
		});

		if (numberOfPositions <= positions.size()) {
			return positions.subList(0, numberOfPositions);
		} else {
			return positions.subList(0, positions.size());
		}
	}

	public List<Position> getSinglePosition(String instrument, String userID) {
		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userID);
		String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/positions/" + instrument;
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<PositionResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PositionResponse.class);
		ResponseEntity<String> respSingle = tmpl.exchange(url, HttpMethod.GET, apiHeader, String.class);
		List<Trade> allTrades = getAllOpenTrades(account.getUserId());
		List<Order> allPendingOrders = getAllPendingOrders(account.getUserId());
		Position position = resp.getBody().getPosition();

		List<String> tradeIDs = new ArrayList<>();
		tradeIDs.addAll(position.getTradesIDs());
		if (position.getShort() != null && position.getShort().getTradeIDs() != null) {
			tradeIDs.addAll(position.getShort().getTradeIDs());
		}

		if (position.getLong() != null && position.getLong().getTradeIDs() != null) {
			tradeIDs.addAll(position.getLong().getTradeIDs());
		}

		for (String tradeId : tradeIDs) {
			Trade trade = getTradeById(allTrades, tradeId);
			if (allPendingOrders != null) {
				List<Order> orders = getOrdersByTradeId(allPendingOrders, tradeId);
				for (Order order : orders) {
					if (order != null && order.getTradeID() == "TAKE_PROFIT") {
						trade.setTakeProfitOrder(order);
					}

					if (order != null && order.getTradeID() == "STOP_LOSS") {
						trade.setStopLossOrder(order);
					}
				}
			}
			position.getTrades().add(trade);
		}

		position.setAccountName(account.getUserName());
		position.setUserID(account.getUserId());
		List<Position> positions = new ArrayList<>();
		positions.add(position);
		return positions;
	}

	public List<Trade> getAllOpenTrades(String userID) {
		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userID);
		String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/openTrades";
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<TradeListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, TradeListResponse.class);
		return resp.getBody().getTrades();
	}

	public List<Order> getAllPendingOrders(String userID) {
		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userID);
		String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/pendingOrders";
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<OrderListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, OrderListResponse.class);
		return resp.getBody().getOrders();
	}

	private List<Order> getOrdersByTradeId(List<Order> orders, String tradeID) {
		List<Order> resultOrders = new ArrayList<>();
		for (Order order : orders) {
			if (order.getTradeID() != null && order.getTradeID().equals(tradeID)) {
				resultOrders.add(order);
			}
		}
		return resultOrders;
	}

	private Trade getTradeById(List<Trade> trades, String tradeID) {
		for (Trade trade : trades) {
			if (trade.getId().equals(tradeID)) {
				return trade;
			}
		}
		return null;
	}

	public List<Trade> getAllClosedTrades() {

		List<Trade> closedTrades = new ArrayList<>();

		for (Account account : SessionUtils.getAccounts()) {

			String userID = account.getUserId();

			RestTemplate tmpl = new RestTemplate();
			String apiKey = SessionUtils.findAccountByUserId(userID).getApiKey();
			String url = curlUtil.createAccountUrl() + "/" + userID + "/trades?state=CLOSED";
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);

			ResponseEntity<TradeListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, TradeListResponse.class);
			for (Trade trade : resp.getBody().getTrades()) {
				trade.setAccountName(account.getUserName());
			}

			closedTrades.addAll(resp.getBody().getTrades());
		}
		return closedTrades;
	}

}
