package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitDetails {

	private String price;
	private String timeInForce;// : (TimeInForce, default=GTC),
	private String gtdTime;// : (DateTime),
	private ClientExtensions clientExtensions;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getGtdTime() {
		return gtdTime;
	}

	public void setGtdTime(String gtdTime) {
		this.gtdTime = gtdTime;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

}
