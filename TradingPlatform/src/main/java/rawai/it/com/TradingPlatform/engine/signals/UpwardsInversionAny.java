package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class UpwardsInversionAny extends AbstractTradingSignal {

	public UpwardsInversionAny() {
		super(BUYSELL.BUY, TradingSignalEnum.UPWARD_ANY, 3);
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface firstCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface middleCandle = candles.get(lastSignalCandleIndex + 1);
		CandleDataPointInterface lastCandle = candles.get(lastSignalCandleIndex + 2);
		int first = Integer.valueOf(firstCandle.getC().replace(".", ""));
		int middle = Integer.valueOf(middleCandle.getC().replace(".", ""));
		int last = Integer.valueOf(lastCandle.getC().replace(".", ""));

		// TODO sameCandle
		if (isUpwardsInversion(first, middle, last)) {
			return new SignalResult(true, lastSignalCandleIndex + 1, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
