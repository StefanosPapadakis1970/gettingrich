package rawai.it.com.TradingPlatform.algorithmtesting;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.TestEntityConverter;
import rawai.it.com.TradingPlatform.engine.db.TestRunEntity;
import rawai.it.com.TradingPlatform.engine.db.TestRunRepository;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.instrument.Instrument;
import rawai.it.com.TradingPlatform.instrument.InstrumentService;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.messanger.Message;
import rawai.it.com.TradingPlatform.messanger.MessageService;
import rawai.it.com.TradingPlatform.messanger.MessageType;
import rawai.it.com.TradingPlatform.order.OrderService;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.tradingprofile.RunType;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.TradingUtils;

@Controller
public class TestingController {
	private static final Logger logger = LogManager.getLogger(TestingController.class);

	@Autowired
	private InstrumentService instrumentService;
	@Autowired
	private TradingRepository tradingrepository;
	@Autowired
	private TestingRepository testingRepository;
	@Autowired
	private TestRunRepository testrunRepository;
	@Autowired
	private OrderService orderService;
	@Autowired
	@Lazy
	private MessageService messageService;
	


	

	@RequestMapping(value = "testing", method = RequestMethod.GET)
	public ModelAndView showTestingView() {
		TestForm testForm = new TestForm();
		instrumentService.setupInstruments();
		List<Instrument> instruments = SessionUtils.getInstrumentMap().get(SessionUtils.getAccounts().get(0).getUserId());

		List<String> algorithms = tradingrepository.findAllTradingAlgorithmNames();
		ModelAndView resultView = new ModelAndView("/testing/testing");
		resultView.addObject("instruments", instruments);
		resultView.addObject("intervals", IntervalEnum.values());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("testForm", testForm);
		resultView.addObject("runTypes", RunType.values());
		resultView.addObject("algorithms", algorithms);
		return resultView;
	}

	@RequestMapping(value = "testing/startTest", method = RequestMethod.POST)
	public ModelAndView startTest(@Validated @ModelAttribute TestForm testForm, Errors errors) throws ParseException {
		ModelAndView resultView = new ModelAndView("/testing/testing");
		instrumentService.setupInstruments();
		List<Instrument> instruments = SessionUtils.getInstrumentMap().get(SessionUtils.getAccounts().get(0).getUserId());

		List<String> algorithms = tradingrepository.findAllTradingAlgorithmNames();
		resultView.addObject("instruments", instruments);
		resultView.addObject("intervals", IntervalEnum.values());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("testForm", testForm);
		resultView.addObject("runTypes", RunType.values());
		resultView.addObject("algorithms", algorithms);

		if (errors.hasErrors()) {
			return resultView;
		}

		if (testForm.getRunType().equals(RunType.BACK_TESTING)) {
			if (testForm.getStartDate().equals(testForm.getEndDate())) {
				errors.rejectValue("startDate", "NOT_BLANK", "Start Date and End Date must be different!");
				errors.rejectValue("endDate", "NOT_BLANK", "Start Date and End Date must be different!");
			}
		}

		if (testForm.getRunType().equals(RunType.LIVE_DATA)) {
			if (testForm.getMaxNumberOfTransactions().equals("")) {
				errors.rejectValue("maxNumberOfTransactions", "NOT_BLANK", "Please set Value !");
			}
		}

		if (errors.hasErrors()) {
			return resultView;
		}

		TradingAlgorithmEntity algorithm = tradingrepository.findByName(testForm.getAlgorithmName());
		TestEntity testEntity = TestEntityConverter.convertFormToEntity(testForm);
		testEntity.setAlgorithm(algorithm);
		testEntity.setStatus(TestStatusEnum.SUCCESS);
		testingRepository.update(testEntity);

		ModelAndView showAll = new ModelAndView("/testing/showAll");
		List<TestForm> tests = TestEntityConverter.convertEnityListToFormList(testingRepository.findAll());

		showAll.addObject("tests", tests);

		return showAll;
	}

	@RequestMapping(value = "testing/showAll", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView showAll() throws ParseException {
		ModelAndView showAll = new ModelAndView("/testing/showAll");
		List<TestForm> tests = TestEntityConverter.convertEnityListToFormList(testingRepository.findAll());
		// TODO 27.6 seedup possible -< testruns lazy
		showAll.addObject("tests", tests);
		return showAll;
	}

	@RequestMapping(value = "testing/startTest", method = RequestMethod.GET)
	@ResponseBody
	public String startTest(@RequestParam("testID") long testID) throws Throwable {
		List<AlgorithmTrade> trades = new ArrayList<>();
		List<AlgorithmTradeEntity> tradeEntities = new ArrayList<>();
		TestEntity testEntity = testingRepository.findByID(testID);
		Account account = SessionUtils.findAccountByUserId(testEntity.getUserID());

		TestRunEntity testRun = new TestRunEntity();
		testRun.setExecutionDate(new Date());
		testRun.setTest(testEntity);


		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(testEntity.getUnits());
		instrument.setInstument(testEntity.getInstrument());
		instrument.setMassTrade(false);
		instrument.setTradingInterval(testEntity.getInterval());

		TradingAlgorithm tradingAlgorithm = TradingProfileConverter.convertTradingAlgorithmEntity(testEntity.getAlgorithm());

		List<Candle> candles = null;
		try {
			candles = instrumentService.fetchCandlesFromTo(testEntity.getUserID(), testEntity.getInstrument(), testEntity.getGranularity().name(), testEntity.getStartDate(),
					testEntity.getEndDate());
		} catch (HttpServerErrorException e) {
			Message message = new Message();
			message.setHeader("ERROR ");
			message.setType(MessageType.TYPE_DANGER.getType());
			message.setMessage("Test could not be started! Check following link<br> http://developer.oanda.com/rest-live-v20/health/");
			messageService.sendGlobalMessage(message);
			return "ERROR";
		}

		testEntity.setStatus(TestStatusEnum.RUNNING);
		testingRepository.update(testEntity);
		List<CandleDataPointInterface> candleDataPoints = new ArrayList<>();

		int i = 0;
		logger.info("##################### Following Candles are involved in this TestCanse " + tradingAlgorithm.getName());
		for (Candle candle : candles) {
			logger.info("Candle time " + candle.getTime() + " first candle of the day " + candle.isFirstCandleOfTheDay());
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			candleDataPoint.setCandleIndex(i);
			candleDataPoints.add(candleDataPoint);
			i++;
		}

		int lastTradingIndex = 0;
		int tradeCounter = 0;

		for (i = 0; i < candleDataPoints.size(); i++) {

			AlgorithmTrade trade = tradingAlgorithm.generateTrade(candleDataPoints, true);
			if (trade.isTrade()) {

				if (testEntity.getMaxNumberOfTransactions() > 0 && testEntity.getMaxNumberOfTransactions() == tradeCounter) {
					break;
				}
				tradeCounter++;

				trade.setUnits(testEntity.getUnits());
				trade.setInstrument(instrument);
				trade.setAccount(account);
				trade.setTradingAlgorithm(tradingAlgorithm);

				// Setup Trade
				trade.setCreationDate(new Date());
				CandleDataPointInterface tradingCandle = candleDataPoints.get(trade.getTradingCandleIndex());
				

				
				trade.setPrice(Double.valueOf(tradingCandle.getC()));				
				trade.setTakeProfitPips(tradingAlgorithm.getTakeProfitPips());
				trade.setStopLossPips(tradingAlgorithm.getStopLossPips());
				trade.setCandles(candleDataPoints);

				Double takeProfitPrice = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getTakeProfitPips(), trade.isBuySell(), true);
				Double stopLossPrice = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getStopLossPips(), trade.isBuySell(), false);

				trade.setStopLossPrice(stopLossPrice);
				trade.setTakeProfitPrice(takeProfitPrice);

				tradingAlgorithm.postProcessTrade(trade);

				trade.setCandles(new ArrayList<CandleDataPointInterface>());


				if (trade.getTradingCandleIndex() + 10 < candleDataPoints.size()) {
					int index = 1;



					logger.info("StopLoss Price " + stopLossPrice + " TakeProfitPrice " + takeProfitPrice + " CandleSize " + candleDataPoints.size());

					while (true) {
						if ((trade.getTradingCandleIndex() + index) == candleDataPoints.size()) {
							// logger.info("END OF CANDLE ARRAY REACHED");
							trade.setFinished(false);
							break;

						}

						// Simulation of dynamic Postprocessing
						if (tradingAlgorithm.hasDynamicPostSignals()) {
							CandleDataPointInterface currentCandle = candleDataPoints.get(trade.getTradingCandleIndex() + index);
							Double pip = TradingUtils.calculateOnePip(Double.valueOf(currentCandle.getO()));
							logger.info("POSTPROCESSING CANDLE " + trade.getTradingCandleIndex() + index);
							// for (Double loopPrice =
							// Double.valueOf(currentCandle.getL()); loopPrice <
							// Double.valueOf(currentCandle.getH()); loopPrice
							// += pip) {
							// tradingAlgorithm.dynamicPostProcessTrades(trade,
							// loopPrice);
							// }
							tradingAlgorithm.dynamicPostProcessTrades(trade, Double.valueOf(currentCandle.getH()));
							if (stopLossPrice != trade.getStopLossPrice()) {
								index++;
								trade.setStopLossMoved(true);
							}
							logger.info("POSTPROCESSING CANDLE FINISHED " + trade.getTradingCandleIndex() + index);
						}

						takeProfitPrice = trade.getTakeProfitPrice();
						stopLossPrice = trade.getStopLossPrice();

						// if (!trade.isBuySell()) {
						// double tmp = takeProfitPrice;
						// takeProfitPrice = stopLossPrice;
						// stopLossPrice = tmp;
						// }

						
						CandleDataPoint candleDataPoint = (CandleDataPoint)candleDataPoints.get(trade.getTradingCandleIndex() + index);
						Candle currentCandle = candleDataPoint.getCandle();
						
						if ((trade.isBuySell() && Double.valueOf(currentCandle.getMid().getH()) >= takeProfitPrice)
								|| (!trade.isBuySell() && Double.valueOf(currentCandle.getMid().getL()) <= takeProfitPrice)) {
						
							trade.setSuccess(true);
							trade.setFinished(true);
							trade.setTakeProfitCandleIndex(trade.getTradingCandleIndex() + index);
							logger.info("TAKE PROFIT CANDLEINDEX " + trade.getTradingCandleIndex() + index);
						   break;	
						}							 
								
						if ((trade.isBuySell() && Double.valueOf(currentCandle.getMid().getL()) <= stopLossPrice)
								|| (!trade.isBuySell() && Double.valueOf(currentCandle.getMid().getH()) >= stopLossPrice)) {
							// logger.info("Stop Loss -> L" +
							// candleDataPoints.get(trade.getTradingCandleIndex()
							// + index).getL() + " stop " + stopLossPrice);
							// Success move buy
							// 0 0 0
							// 0 1 0
							// 1 0 1
							// 1 1 1

							boolean success = false;

							if (trade.isStopLossMoved()) {
								success = true;
							}

							trade.setSuccess(success);
							trade.setFinished(true);
							trade.setStopLossCandleIndex(trade.getTradingCandleIndex() + index);
							logger.info("STOP LOSS CANDLEINDEX " + trade.getTradingCandleIndex() + index);
							break;
						}
						// logger.info("HighLevel hit " +
						// Double.valueOf(candleDataPoints.get(trade.getTradingCandleIndex()
						// + index).getH()) + " take " + takeProfitPrice + " "
						// +
						// (Double.valueOf(candleDataPoints.get(trade.getTradingCandleIndex()
						// + index).getH()) >= takeProfitPrice));
						// logger.info("LowLevel  hit " +
						// candleDataPoints.get(trade.getTradingCandleIndex() +
						// index).getL() + "stop " + stopLossPrice + " "
						// +
						// (Double.valueOf(candleDataPoints.get(trade.getTradingCandleIndex()
						// + index).getL()) <= stopLossPrice));
						//
						// logger.info("H : " +
						// candleDataPoints.get(trade.getTradingCandleIndex() +
						// index).getH());
						// logger.info("L : " +
						// candleDataPoints.get(trade.getTradingCandleIndex() +
						// index).getL());
						index++;
					}

					logger.info("TRADE SUCCESS " + index + " " + (trade.getTradingCandleIndex() + index));
					if (trade.getTradingCandleIndex() + (index + 5) < candleDataPoints.size()) {
						trade.setCandles(candleDataPoints.subList(lastTradingIndex, trade.getTradingCandleIndex() + (index + 5)));
					} else {// TODO 22.06 Think about last tradeww.
						trade.setCandles(candleDataPoints.subList(lastTradingIndex, trade.getTradingCandleIndex()));
					}
				} else {// TODO 20.06 last Trade Fetch additional Candles
						// maybier
					trade.setCandles(candleDataPoints.subList(lastTradingIndex, trade.getTradingCandleIndex()));
				}


				// Adjust Candles for display
				int tradingIndex = 0;
				for (CandleDataPointInterface candle : trade.getCandles()) {
					if (candle.getCandleIndex() == trade.getTradingCandleIndex()) {
						break;
					}
					tradingIndex++;
				}

				if ((tradingIndex - tradingAlgorithm.getTotalNumberOfCandlesRequired()) > 0) {
					trade.setCandles(trade.getCandles().subList(tradingIndex - tradingAlgorithm.getTotalNumberOfCandlesRequired(), trade.getCandles().size()));
				} else {
					trade.setCandles(trade.getCandles().subList(0, trade.getCandles().size()));
				}

				// if (trade.getCandles().size() > 300) {
				// logger.info("------------------------------------------------------------------------------");
				// logger.info("----------------------Number of Candles > 100    ----------------------------");
				// logger.info("------------------" + (trade.getCandles().size()
				// - 290) + " bis " + trade.getCandles().size()
				// +
				// "------------------------------------------------------------");
				// trade.setCandles(trade.getCandles().subList(trade.getCandles().size()
				// - 290, trade.getCandles().size()));
				// }

				lastTradingIndex = trade.getTradingCandleIndex();

				AlgorithmTradeEntity tradeEntity = AlgorithmTradeConverter.convertToAlgorithmTradeEntity(trade);

				tradeEntity.setTestRun(testRun);

				testRun.getTrades().add(tradeEntity);

				trades.add(trade);

				i = trade.getTradingCandleIndex();
			}
			if (trade.isResetCandles()) {
				logger.info("TESTING RESETS ALGORITHM!!");
				tradingAlgorithm.resetAlgorithmForBackTesting();
			}
		}

		if (testEntity.isGenerateTrades()) {
			orderService.createTrades(trades);
		}

		int totalNumberOfTrades = trades.size();
		int sucessFullTrades = 0;
		int failedTrades = 0;
		// long profitTotalLong = 0;
		// /long lossTotalLong = 0;
		BigDecimal profitTotal = new BigDecimal(0);
		BigDecimal lossTotal = new BigDecimal(0);
		int openTrades = 0;
		logger.info("Number of Trades generated by Test: "+trades.size());
		for (AlgorithmTrade trade : trades) {

			if (trade.isFinished()) {
				int exponent = TradingUtils.calculateOnePipExponent(trade.getPrice());

				if (trade.isSuccess()) {
					sucessFullTrades++;
					if (trade.isStopLossMoved()) {
						logger.info("StopLoss -> moved");

						if (trade.isBuySell()) {

							profitTotal = profitTotal.add(new BigDecimal(trade.getStopLossPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
									new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP)));
							logger.info("profitTotal += trade.getStopLossPrice() - trade.getPrice();");
							logger.info(profitTotal + " " + trade.getStopLossPrice() + " " + trade.getPrice() + "--" + profitTotal);
						}

						if (!trade.isBuySell()) {
							profitTotal = profitTotal.add(new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
									new BigDecimal(trade.getStopLossPrice()).setScale(5, RoundingMode.HALF_UP)));
							// profitTotal += trade.getPrice() -
							// trade.getStopLossPrice();
							logger.info("profitTotal1 += trade.getPrice() - trade.getStopLossPrice();");
							logger.info(profitTotal + " " + trade.getPrice() + " " + trade.getStopLossPrice() + "--" + profitTotal);
						}
					} else {
						if (trade.isBuySell()) {
							profitTotal = profitTotal.add(new BigDecimal(trade.getTakeProfitPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
									new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP)));
							// profitTotal += trade.getTakeProfitPrice() -
							// trade.getPrice();
							logger.info("profitTotal += trade.getTakeProfitPrice() - trade.getPrice();");
							logger.info(profitTotal + " " + trade.getTakeProfitPrice() + " " + trade.getPrice() + "--" + profitTotal);
						}

						if (!trade.isBuySell()) {
							profitTotal = profitTotal.add(new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
									new BigDecimal(trade.getTakeProfitPrice()).setScale(5, RoundingMode.HALF_UP)));
							// profitTotal += trade.getPrice() -
							// trade.getTakeProfitPrice();
							logger.info("profitaTotal += trade.getPrice() - trade.getTakeProfitPrice()");
							logger.info(profitTotal + " " + trade.getPrice() + " " + trade.getTakeProfitPrice() + "--" + profitTotal);
						}

					}

				} else {

					if (trade.isBuySell()) {
						lossTotal = lossTotal.add(new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
								new BigDecimal(trade.getStopLossPrice()).setScale(5, RoundingMode.HALF_UP)));
						// lossTotal += trade.getPrice() -
						// trade.getStopLossPrice();
						logger.info("lossiTotal += trade.getPrice() - trade.getStopLossPrice();");
						logger.info(lossTotal + " " + trade.getPrice() + " " + trade.getStopLossPrice() + "--" + lossTotal);
					}

					if (!trade.isBuySell()) {
						lossTotal = lossTotal.add(new BigDecimal(trade.getStopLossPrice()).setScale(5, RoundingMode.HALF_UP).subtract(
								new BigDecimal(trade.getPrice()).setScale(5, RoundingMode.HALF_UP)));
						// lossTotal += trade.getStopLossPrice() -
						// trade.getPrice();
						logger.info("lossTotal += trade.getStopLossPrice() - trade.getPrice();");
						logger.info(lossTotal + " " + trade.getStopLossPrice() + " " + trade.getPrice() + "--" + lossTotal);
					}

					failedTrades++;
				}
			} else {
				openTrades++;
			}
		}

		BigDecimal balance;

		balance = profitTotal.subtract(lossTotal);

		if (testRun.getTrades().size() > 0) {
			int exponent = TradingUtils.calculateOnePipExponent(testRun.getTrades().get(0).getPrice());
			/*
			 * logger.info("Exponent " + exponent);
			 * logger.info("profitTotalLong " + profitTotalLong); profitTotal =
			 * TradingUtils.callculateDoubleValue(profitTotalLong, exponent);
			 * logger.info("lossTotalLong " + lossTotalLong); lossTotal =
			 * TradingUtils.callculateDoubleValue(lossTotalLong, exponent);
			 * logger.info("balance " + (profitTotalLong - lossTotalLong));
			 * balance = TradingUtils.callculateDoubleValue(profitTotalLong -
			 * lossTotalLong, exponent);
			 */
		}

		testEntity.getTestRuns().add(testRun);
		testRun.setTotalNumberOfTrades(totalNumberOfTrades);
		testRun.setSucessFullTrades(sucessFullTrades);
		testRun.setFailedTrades(failedTrades);
		testRun.setBalance(balance.doubleValue());
		testRun.setProfitTotal(profitTotal.doubleValue());
		testRun.setLossTotal(lossTotal.doubleValue());
		testRun.setTakeProfitPips(tradingAlgorithm.getTakeProfitPips());
		testRun.setStopLossPips(tradingAlgorithm.getStopLossPips());
		testRun.setOpenTrades(openTrades);
		testRun.setCandles(AlgorithmTradeConverter.convertCandles(candleDataPoints, testRun));
		testEntity.setStatus(TestStatusEnum.SUCCESS);

		testingRepository.update(testEntity);

		Message message = new Message();
		message.setHeader("Testrun finished");
		message.setMessage("Run of Algorithm " + tradingAlgorithm.getName() + " ready! Generated Trades: "+testRun.getTrades().size());
		messageService.sendGlobalMessage(message);

		return "OK";
	}

	@RequestMapping(value = "testing/deleteTest", method = RequestMethod.GET)
	public ModelAndView deleteTest(@RequestParam("testID") long testID) throws ParseException {
		TestEntity testEntity = testingRepository.findByID(testID);
		testingRepository.deleteTestEntity(testEntity);
		ModelAndView resultView = showAll();
		return resultView;
	}

	@RequestMapping(value = "testing/deleteTestRun", method = RequestMethod.GET)
	public ModelAndView deleteTestRun(@RequestParam("testRunID") long testRunID) throws ParseException {
		testrunRepository.deleteTestRunByID(testRunID);
		ModelAndView resultView = showAll();
		return resultView;
	}

	@RequestMapping(value = "testing/deleteTestRunData", method = RequestMethod.GET)
	public ModelAndView deleteTestRunData(@RequestParam("testRunID") long testRunID) throws ParseException {
		testrunRepository.deleteTestRunDataByID(testRunID);
		ModelAndView resultView = showAll();
		return resultView;
	}

}
