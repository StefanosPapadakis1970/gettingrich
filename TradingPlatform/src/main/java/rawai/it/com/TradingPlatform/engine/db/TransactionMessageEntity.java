package rawai.it.com.TradingPlatform.engine.db;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "transactionmessage")
@NamedQueries({
		@NamedQuery(name = TransactionMessageEntity.FIND_BY_TRADE_ID, query = "select message from TransactionMessageEntity message where message.tradeID = :tradeID order by message.id") })
public class TransactionMessageEntity {

	public static final String FIND_BY_TRADE_ID = "TransactionMessage.findByTradeID";

	@Id
	@GeneratedValue
	private Long id;

	private Long batchID;

	private Long tradeID;

	private Long orderID;

	private Date time;

	@Type(type = "text")
	private String message;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTradeID() {
		return tradeID;
	}

	public void setTradeID(Long tradeID) {
		this.tradeID = tradeID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getBatchID() {
		return batchID;
	}

	public void setBatchID(Long batchID) {
		this.batchID = batchID;
	}

	public Long getOrderID() {
		return orderID;
	}

	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
