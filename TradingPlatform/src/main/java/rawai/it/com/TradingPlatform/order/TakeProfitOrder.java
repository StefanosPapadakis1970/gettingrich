package rawai.it.com.TradingPlatform.order;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.dashboard.ClientExtensions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitOrder {

	private String timeInForce = "GTC";
	@NotBlank(message = "Enter Price")
	@Pattern(regexp = "[^-][0-9\\.]*", message = "Not a valid number (>0)")
	private String price;
	private String type = "TAKE_PROFIT";
	private String tradeID;
	private String id;
	private String userID;
	private String uuid;

	private ClientExtensions clientExtensions;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
