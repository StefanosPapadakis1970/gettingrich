package rawai.it.com.TradingPlatform.conditions;

import java.util.ArrayList;
import java.util.List;

public enum PageConditionEnum {
	DASHBOARD {
		@Override
		public List<String> getConditions() {
			List<String> conditions = new ArrayList<>();
			conditions.add(ConditionEnum.DASHBOARD.name());
			conditions.add(ConditionEnum.MESSAGE.name());
			conditions.add(ConditionEnum.TRANSACTION.name());
			return conditions;
		}
	},
	OPENPOSITIONS {
		@Override
		public List<String> getConditions() {
			List<String> conditions = new ArrayList<>();
			conditions.add(ConditionEnum.MESSAGE.name());
			conditions.add(ConditionEnum.PRICE.name());
			conditions.add(ConditionEnum.POSITION.name());
			return conditions;
		}
	},
	OPENORDERS {
		@Override
		public List<String> getConditions() {
			List<String> conditions = new ArrayList<>();
			conditions.add(ConditionEnum.MESSAGE.name());
			conditions.add(ConditionEnum.PRICE.name());
			conditions.add(ConditionEnum.POSITION.name());
			return conditions;
		}
	},
	TRADE {
		@Override
		public List<String> getConditions() {
			List<String> conditions = new ArrayList<>();
			conditions.add(ConditionEnum.MESSAGE.name());
			return conditions;
		}
	};

	public abstract List<String> getConditions();
}
