package rawai.it.com.TradingPlatform.tradingprofile;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.engine.PostTradingSignalForm;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithmForm;
import rawai.it.com.TradingPlatform.engine.TradingSignalForm;
import rawai.it.com.TradingPlatform.engine.TradingSignalToFormConverter;
import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;

@Controller
public class TradingAlgorithmController {

	@Autowired
	private TradingRepository tradingRepository;

	@RequestMapping(value = "tradingAlgorithm/delete", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView deleteAlgorithm(@RequestParam("ID") Long id) {
		ModelAndView resultView = new ModelAndView("fragments/response");

		if (tradingRepository.deleteTradingAlgorithmID(id)) {
			resultView.addObject("response", "Algorithm successfully deleted!");
		} else {
			resultView.addObject("response", "Could not delete Algorithm!");
		}
		return resultView;
	}

	@RequestMapping(value = "tradingAlgorithm/showAll", method = RequestMethod.GET)
	public ModelAndView editTradingProfile() {
		ModelAndView resultView = new ModelAndView("tradingalgorithm/showall");
		List<TradingAlgorithmForm> tradingAlgorithmForms = new ArrayList<>();
		List<TradingAlgorithmEntity> tradingAlgorithms = tradingRepository.findAllTradingAlgorithms();
		for (TradingAlgorithmEntity algorithmEntity : tradingAlgorithms) {
			tradingAlgorithmForms.add(TradingAlgorithmFormConverter.convertEntityToForm(algorithmEntity));
		}
		resultView.addObject("algorithms", tradingAlgorithmForms);
		return resultView;
	}

	@RequestMapping(value = "tradingAlgorithm/editAlgorithm", method = RequestMethod.GET)
	public ModelAndView editTradingAlgorithm(@RequestParam("name") String name) {
		ModelAndView resultView = new ModelAndView("tradingalgorithm/tradingAlgorithm");
		TradingAlgorithmForm algorithmForm = new TradingAlgorithmForm();
		TradingAlgorithmEntity algorithm = tradingRepository.findByName(name);
		String algorithmName = "";
		String dbID = null;
		if (algorithm == null) {
			algorithmName = name;
		} else {
			algorithmName = algorithm.getName();
			algorithmForm.setBuy(algorithm.isBuy());
			algorithmForm.setStopLossPips(algorithm.getStopLossPips());
			algorithmForm.setTakeProfitPips(algorithm.getTakeProfitPips());
			dbID = algorithm.getId().toString();
			for (TradingSignalEntity signal : algorithm.getTradingSignals()) {
				algorithmForm.getSignals().add(TradingSignalToFormConverter.convertEntityToForm(signal));
			}

			for (PostTradeSignalEntity signal : algorithm.getPostTradingSignals()) {
				algorithmForm.getPostTradngSignals().add(TradingSignalToFormConverter.convertPostSignalEntityToForm(signal));
			}
		}

		List<TradingSignalEntity> signals = tradingRepository.findAllTradingSignals();
		List<PostTradeSignalEntity> postTradingSignals = tradingRepository.findAllPostTradingSignals();
		List<TradingSignalForm> signalForms = new ArrayList<>();
		List<PostTradingSignalForm> postTradingSignalForms = new ArrayList<>();

		algorithmForm.setAlgorithmName(algorithmName);
		algorithmForm.setDbID(dbID);


		for (TradingSignalEntity signal : signals) {

			signalForms.add(TradingSignalToFormConverter.convertEntityToForm(signal));
		}

		for (PostTradeSignalEntity signal : postTradingSignals) {

			postTradingSignalForms.add(TradingSignalToFormConverter.convertPostSignalEntityToForm(signal));
		}

		resultView.addObject("algorithmForm", algorithmForm);
		resultView.addObject("allSignals", signalForms);
		resultView.addObject("allPostSignals", postTradingSignalForms);
		return resultView;
	}

	@RequestMapping(value = "update/tradingalgorithm", method = RequestMethod.POST)
	public ModelAndView updateTradingAlgorithm(@Valid @ModelAttribute("algorithm") TradingAlgorithmForm algorithm, Errors errors) {
		ModelAndView resultView = new ModelAndView("tradingalgorithm/tradingAlgorithm");

		List<TradingSignalEntity> signals = tradingRepository.findAllTradingSignals();
		List<PostTradeSignalEntity> postTradingsignals = tradingRepository.findAllPostTradingSignals();

		TradingAlgorithmEntity algorithmEntity = TradingAlgorithmFormConverter.convertFormToEntity(algorithm, signals, postTradingsignals);
		TradingAlgorithmEntity dbAlgorithm = tradingRepository.findByName(algorithmEntity.getName());

		if (dbAlgorithm != null) {
			algorithmEntity.setTradingprofiles(dbAlgorithm.getTradingprofiles());
		}

		tradingRepository.saveTradingAlgorithm(algorithmEntity);

		List<TradingSignalForm> signalForms = new ArrayList<>();
		for (TradingSignalEntity signal : signals) {
			signalForms.add(TradingSignalToFormConverter.convertEntityToForm(signal));
		}

		List<PostTradingSignalForm> postTrading = new ArrayList<>();
		for (PostTradeSignalEntity signal : postTradingsignals) {
			postTrading.add(TradingSignalToFormConverter.convertPostSignalEntityToForm(signal));
		}
		resultView.addObject("algorithmForm", algorithm);
		resultView.addObject("allSignals", signalForms);
		resultView.addObject("allPostSignals", postTrading);
		resultView.addObject("success", true);
		return resultView;
	}

}
