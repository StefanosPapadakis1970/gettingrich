package rawai.it.com.TradingPlatform.candle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandleBidAsk {

	private String time;
	private String openBid;
	private String openAsk;
	private String highBid;
	private String highAsk;
	private String lowBid;
	private String lowAsk;
	private String closeBid;
	private String closeAsk;
	private String volume;
	private boolean complete;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getOpenBid() {
		return openBid;
	}

	public void setOpenBid(String openBid) {
		this.openBid = openBid;
	}

	public String getOpenAsk() {
		return openAsk;
	}

	public void setOpenAsk(String openAsk) {
		this.openAsk = openAsk;
	}

	public String getHighBid() {
		return highBid;
	}

	public void setHighBid(String highBid) {
		this.highBid = highBid;
	}

	public String getHighAsk() {
		return highAsk;
	}

	public void setHighAsk(String highAsk) {
		this.highAsk = highAsk;
	}

	public String getLowBid() {
		return lowBid;
	}

	public void setLowBid(String lowBid) {
		this.lowBid = lowBid;
	}

	public String getLowAsk() {
		return lowAsk;
	}

	public void setLowAsk(String lowAsk) {
		this.lowAsk = lowAsk;
	}

	public String getCloseBid() {
		return closeBid;
	}

	public void setCloseBid(String closeBid) {
		this.closeBid = closeBid;
	}

	public String getCloseAsk() {
		return closeAsk;
	}

	public void setCloseAsk(String closeAsk) {
		this.closeAsk = closeAsk;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}
}
