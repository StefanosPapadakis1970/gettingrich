package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseOrderCancelTransaction {

	private String id;
	private String time;
	private String userID;
	private String accountID;
	private String batchID;
	private String requestID;
	private TransactionType type;
	private String orderID;
	private String clientOrderID;
	private OrderCancelReason reason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getClientOrderID() {
		return clientOrderID;
	}

	public void setClientOrderID(String clientOrderID) {
		this.clientOrderID = clientOrderID;
	}

	public OrderCancelReason getReason() {
		return reason;
	}

	public void setReason(OrderCancelReason reason) {
		this.reason = reason;
	}

}
