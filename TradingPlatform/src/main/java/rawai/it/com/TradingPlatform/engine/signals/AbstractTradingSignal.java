package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;


public abstract class AbstractTradingSignal implements TradingSignal {

	private Long dbID;

	protected BUYSELL buySell;

	public TradingSignalEnum tradingSignal = null;

	private int minimumNumberOfCandles = 0;

	private TradingAlgorithm tradingalgorithm;

	private boolean success = false;

	private int startCandleIndex;
	private int endCandleIndex;
	private int successCandleIndex;

	public AbstractTradingSignal(BUYSELL buySell, TradingSignalEnum tradingSignal, int minimumNumberOfCandles) {
		super();
		this.buySell = buySell;
		this.tradingSignal = tradingSignal;
		this.minimumNumberOfCandles = minimumNumberOfCandles;

	}

	public boolean enoughCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		setStartCandleIndex(lastSignalCandleIndex);
		return candles.subList(lastSignalCandleIndex, candles.size()).size() >= minimumNumberOfCandles;
	}

	public List<CandleDataPointInterface> prepareCandles(List<CandleDataPointInterface> candles, CandleDataPointInterface lastCandle) {
		int candleIndex = 0;
		if (lastCandle == null) {
			return candles;
		}
		for (CandleDataPointInterface candle : candles) {
			if (candle.getTime().equals(lastCandle.getTime())) {
				candleIndex = candles.indexOf(candle);
				break;
			}
		}
		return candles.subList(candleIndex, candles.size() - 1);
	}

	public boolean isDownwardsInversion(int first, int middle, int last) {
		return middle > first && middle > last;
	}

	public boolean isUpwardsInversion(int first, int middle, int last) {
		return middle < first && middle < last;
	}

	public BUYSELL getBuySell() {
		return buySell;
	}

	public void setBuySell(BUYSELL buySell) {
		this.buySell = buySell;
	}

	public TradingSignalEnum getTradingSignal() {
		return tradingSignal;
	}

	public TradingSignalEnum getTradingSignalEnum() {
		return tradingSignal;
	}

	public void setTradingSignal(TradingSignalEnum tradingSignal) {
		this.tradingSignal = tradingSignal;
	}

	public int getMinimumNumberOfCandles() {
		return minimumNumberOfCandles;
	}

	public void setMinimumNumberOfCandles(int minimumNumberOfCandles) {
		this.minimumNumberOfCandles = minimumNumberOfCandles;
	}

	public Long getDBID() {
		return dbID;
	}

	public void setDBID(Long dbID) {
		this.dbID = dbID;
	}

	@Override
	public void setTradingalgorithm(TradingAlgorithm tradingalgorithm) {
		this.tradingalgorithm = tradingalgorithm;

	}

	@Override
	public boolean isSuccess() {
		return success;
	}

	@Override
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getStartCandleIndex() {
		return startCandleIndex;
	}

	public void setStartCandleIndex(int startCandleIndex) {
		this.startCandleIndex = startCandleIndex;
	}

	public int getEndCandleIndex() {
		return endCandleIndex;
	}

	public void setEndCandleIndex(int endCandleIndex) {
		this.endCandleIndex = endCandleIndex;
	}

	public int getSuccessCandleIndex() {
		return successCandleIndex;
	}

	public void setSuccessCandleIndex(int successCandleIndex) {
		this.successCandleIndex = successCandleIndex;
	}

}
