package rawai.it.com.TradingPlatform.account;

public enum CurrencyEnum {
	EUR {
		@Override
		public String getCurrencySymbol() {
			return "€";
		}
	},
	USD {
		@Override
		public String getCurrencySymbol() {
			return "$";
		}
	};

	public abstract String getCurrencySymbol();
}
