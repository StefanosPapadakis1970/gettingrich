package rawai.it.com.TradingPlatform.account;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountIDListResponse {

	private List<AccountIdResponse> accounts;

	public List<AccountIdResponse> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountIdResponse> accounts) {
		this.accounts = accounts;
	}
}
