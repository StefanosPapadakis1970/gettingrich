package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.db.CandleEntity;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class AlgorithmTrade {

	private Long dbID;

	private TradingInstrument instrument;

	private boolean buySell;

	private int tradingCandleIndex = 0;

	private int stopLossCandleIndex = 0;

	private int takeProfitCandleIndex = 0;

	private boolean resetCandles = false;

	private TradingAlgorithm tradingAlgorithm;

	private List<CandleDataPointInterface> candles = new ArrayList<>();

	private Account account;

	private Date creationDate;

	// TODO i think not necessary
	private IntervalEnum tradingInterVal;

	private boolean trade;

	private Long testID;

	private Double price;

	private Double stopLossPrice;

	private Double takeProfitPrice;

	private int stopLossPips;

	private int takeProfitPips;

	private boolean success;

	private int units;

	private boolean finished;

	private boolean stopLossMoved;

	private String uuid;

	private Long stopLossTradeID;

	private Long takeProfitID;

	private boolean realTrade;

	private Double pl;

	private String tradeID;


	public AlgorithmTrade(TradingInstrument instrument, boolean buySell) {
		super();
		this.instrument = instrument;
		this.buySell = buySell;
	}

	public AlgorithmTrade() {
		// TODO Auto-generated constructor stub
	}

	public TradingInstrument getInstrument() {
		return instrument;
	}

	public void setInstrument(TradingInstrument instrument) {
		this.instrument = instrument;
	}

	public boolean isBuySell() {
		return buySell;
	}

	public void setBuySell(boolean buySell) {
		this.buySell = buySell;
	}

	public int getTradingCandleIndex() {
		return tradingCandleIndex;
	}

	public void setTradingCandleIndex(int tradingCandleIndex) {
		this.tradingCandleIndex = tradingCandleIndex;
	}

	public boolean isTrade() {
		return trade;
	}

	public void setTrade(boolean trade) {
		this.trade = trade;
	}

	public IntervalEnum getTradingInterVal() {
		return tradingInterVal;
	}

	public void setTradingInterVal(IntervalEnum tradingInterVal) {
		this.tradingInterVal = tradingInterVal;
	}

	public boolean isResetCandles() {
		return resetCandles;
	}

	public void setResetCandles(boolean resetCandles) {
		this.resetCandles = resetCandles;
	}

	public TradingAlgorithm getTradingAlgorithm() {
		return tradingAlgorithm;
	}

	public void setTradingAlgorithm(TradingAlgorithm tradingAlgorithm) {
		this.tradingAlgorithm = tradingAlgorithm;
	}

	public List<CandleDataPointInterface> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleDataPointInterface> candles) {
		this.candles = candles;
	}

	public void copyCandles(List<CandleDataPointInterface> candles) {
		this.candles.clear();
		this.candles.addAll(candles);
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long dbID) {
		this.dbID = dbID;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getTestID() {
		return testID;
	}

	public void setTestID(Long testID) {
		this.testID = testID;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public CandleDataPointInterface getCandleBeforeTradingCandle(int steps) {
		int index = 0;
		for (CandleDataPointInterface candle : getCandles()) {
			if (candle.getCandleIndex() == getTradingCandleIndex()) {
				break;
			}
			index++;
		}
		return getCandles().get(index - steps);
	}

	public Double getTakeProfitPrice() {
		return takeProfitPrice;
	}

	public void setTakeProfitPrice(Double takeProfitPrice) {
		this.takeProfitPrice = takeProfitPrice;
	}

	public Double getStopLossPrice() {
		return stopLossPrice;
	}

	public void setStopLossPrice(Double stopLossPrice) {
		this.stopLossPrice = stopLossPrice;
	}

	public int getStopLossCandleIndex() {
		return stopLossCandleIndex;
	}

	public void setStopLossCandleIndex(int stopLossCandleIndex) {
		this.stopLossCandleIndex = stopLossCandleIndex;
	}

	public int getTakeProfitCandleIndex() {
		return takeProfitCandleIndex;
	}

	public void setTakeProfitCandleIndex(int takeProfitCandleIndex) {
		this.takeProfitCandleIndex = takeProfitCandleIndex;
	}

	public boolean isStopLossMoved() {
		return stopLossMoved;
	}

	public void setStopLossMoved(boolean stopLossMoved) {
		this.stopLossMoved = stopLossMoved;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Long getStopLossTradeID() {
		return stopLossTradeID;
	}

	public void setStopLossTradeID(Long stopLossTradeID) {
		this.stopLossTradeID = stopLossTradeID;
	}

	public Long getTakeProfitID() {
		return takeProfitID;
	}

	public void setTakeProfitID(Long takeProfitID) {
		this.takeProfitID = takeProfitID;
	}

	public boolean isRealTrade() {
		return realTrade;
	}

	public void setRealTrade(boolean realTrade) {
		this.realTrade = realTrade;
	}

	public Double getPl() {
		return pl;
	}

	public void setPl(Double pl) {
		this.pl = pl;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}
	
	public Long getCloseOutTradeID() {
		if (pl < 0) {
			return stopLossTradeID;
		} 
		return takeProfitID;
	}
	
	public CandleDataPointInterface getTradingCandle() {
		for(CandleDataPointInterface candle : candles) {
			if (candle.getCandleIndex() == tradingCandleIndex) {
				return candle;
			}
		} 
		return null;
	}
}
