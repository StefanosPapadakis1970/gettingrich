package rawai.it.com.TradingPlatform.urlutils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.chart.ChartRunner;
import rawai.it.com.TradingPlatform.dashboard.DashBoardRunner;
import rawai.it.com.TradingPlatform.dashboard.DashboardController;
import rawai.it.com.TradingPlatform.dashboard.TransactionStreamRunnable;
import rawai.it.com.TradingPlatform.engine.CandleListener;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.instrument.CandleRunner;
import rawai.it.com.TradingPlatform.instrument.Instrument;
import rawai.it.com.TradingPlatform.instrument.InstrumentController;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.order.OpenOrdersRunner;
import rawai.it.com.TradingPlatform.order.OrderController;
import rawai.it.com.TradingPlatform.position.PositionController;
import rawai.it.com.TradingPlatform.position.PositionRunner;
import rawai.it.com.TradingPlatform.position.SinglePositionRunner;
import rawai.it.com.TradingPlatform.price.PriceRunner;
import rawai.it.com.TradingPlatform.price.PriceStreamRunnable;
import rawai.it.com.TradingPlatform.price.PriceStreamService;
import rawai.it.com.TradingPlatform.transaction.TransactionStreamService;

public class SessionUtils {

	private static final Logger log = LogManager.getLogger(SessionUtils.class);

	private static final Logger transactionLog = LogManager.getLogger("transaction-log");

	public static List<Account> accounts = new ArrayList<>();

	public static Map<String, String> userInstrumentMap = new HashMap<>();

	public static HashMap<String, List<Instrument>> instrumentMap = new HashMap<String, List<Instrument>>();

	public static HashMap<String, List<ChartInstrument>> chartInstrumentMap = new HashMap<String, List<ChartInstrument>>();

	public static HashMap<String, List<ChartInstrument>> lastFetchedCandleMap = new HashMap<String, List<ChartInstrument>>();

	public static List<CandleListener> candleListeners = new ArrayList<>();

	public static List<TradingProfile> tradingProfiles = new ArrayList<>();

	private static List<TransactionStreamRunnable> transactionRunners = new ArrayList<>();

	private static List<PriceStreamRunnable> priceRunners = new ArrayList<>();

	private static List<ChartRunner> chartRunners = new ArrayList<>();

	private static List<DashBoardRunner> dashBoardRunners = new ArrayList<>();

	private static List<PositionRunner> positionRunners = new ArrayList<>();

	private static List<SinglePositionRunner> singlePositionRunners = new ArrayList<>();

	private static List<OpenOrdersRunner> orderRunners = new ArrayList<>();

	private static List<CandleRunner> candleRunners = new ArrayList<>();

	private static SingleInstrument singleInstrument;

	private static boolean dashBoardActive = false;

	private static boolean watchPositions = false;

	private static boolean watchOrders = false;

	private static boolean showSingleInstrument;

	private static int clientExtensionIDCounter = 0;
	
	private static int tradeCounter = 0;

	public static int getExtensionID() {
		return clientExtensionIDCounter++;
	}

	public static Account findAccountByUserId(String userId) {
		for (Account account : accounts) {
			if (account.getUserId().equals(userId)) {
				return account;
			}
		}
		return null;
	}

	public static Account findAccountByAccountID(Long accountID) {
		for (Account account : accounts) {
			if (account.getId().equals(accountID)) {
				return account;
			}
		}
		return null;
	}

	public static void registerUserInstrument(String userID, String instrument) {
		if (userInstrumentMap.get(userID) != null) {
			userInstrumentMap.remove(userID);
		}
		userInstrumentMap.put(userID, instrument);
	}

	public static List<Account> getAccounts() {
		return accounts;
	}

	public static void setAccounts(List<Account> accounts) {
		SessionUtils.accounts = accounts;
	}

	public static HashMap<String, List<Instrument>> getInstrumentMap() {
		return instrumentMap;
	}

	public static Map<String, String> getUserInstrumentMap() {
		return userInstrumentMap;
	}

	public static RegisterEnum registerChartInstrument(String userID, String instrument, String interval) {
		if (chartInstrumentMap.get(userID) == null) {
			chartInstrumentMap.put(userID, new ArrayList<ChartInstrument>());
		}
		boolean alreadyRegistered = false;
		for (ChartInstrument chartInstrument : chartInstrumentMap.get(userID)) {
			if (chartInstrument.getInstrument().equals(instrument) && chartInstrument.getInterval().equals(interval)) {
				alreadyRegistered = true;
				break;
			}
		}
		if (!alreadyRegistered) {
			chartInstrumentMap.get(userID).add(new ChartInstrument(instrument, interval));
			return RegisterEnum.OK;
		}
		return RegisterEnum.ALREADY_REGISTERED;
	}

	public static HashMap<String, List<ChartInstrument>> getChartInstrumentMap() {
		return chartInstrumentMap;
	}

	public static void setChartInstrumentMap(HashMap<String, List<ChartInstrument>> chartInstrumentMap) {
		SessionUtils.chartInstrumentMap = chartInstrumentMap;
	}

	public static void unregisterChartInstrument(String userID, String instrument, String interval) {
		ChartInstrument instrumentToDelete = null;
		for (ChartInstrument chartInstrument : chartInstrumentMap.get(userID)) {
			if (chartInstrument.getInstrument().equals(instrument) && chartInstrument.getInterval().equals(interval)) {
				instrumentToDelete = chartInstrument;
			}
		}
		chartInstrumentMap.get(userID).remove(instrumentToDelete);
	}

	public static void setLastFechtedCandle(String userID, String instrument, String time, String interval) {
		ChartInstrument lastFetchedInstrument = new ChartInstrument(instrument, interval, time);
		List<ChartInstrument> candleList = lastFetchedCandleMap.get(userID);

		if (candleList == null) {
			candleList = new ArrayList<>();
			lastFetchedCandleMap.put(userID, candleList);
		}

		// only in chain if if the most actual
		if (!candleList.contains(lastFetchedInstrument)) {
			candleList.add(lastFetchedInstrument);
		} else {
			// Remove last Candle
			List<ChartInstrument> delCandle = new ArrayList<>();

			for (ChartInstrument candle : candleList) {
				if (candle.getInstrument().equals(instrument) && candle.getInterval().equals(interval)) {
					delCandle.add(candle);
				}
			}
			candleList.removeAll(delCandle);
			candleList.add(lastFetchedInstrument);
		}
	}

	public static ChartInstrument fetchLastCandle(String userID, String instrument, String interval) {
		ChartInstrument lastFetchedInstrument = new ChartInstrument(userID, instrument);
		lastFetchedInstrument.setInterval(interval);
		if (lastFetchedCandleMap == null || lastFetchedCandleMap.get(userID) == null) {
			return null;
		}

		for (ChartInstrument chartInstrument : lastFetchedCandleMap.get(userID)) {
			if (chartInstrument.getInstrument().equals(instrument) && chartInstrument.getInterval().equals(interval)) {
				return chartInstrument;
			}
		}
		return null;
	}

	public static void addCandleListener(CandleListener candleListener) {
		if (!candleListeners.contains(candleListener)) {
			candleListeners.add(candleListener);
		}
	}

	public static List<TradingProfile> getTradingProfiles() {
		return tradingProfiles;
	}

	public static void setTradingProfiles(List<TradingProfile> tradingProfiles) {
		SessionUtils.tradingProfiles = tradingProfiles;
	}

	public static boolean isDashBoardActive() {
		return dashBoardActive;
	}

	public static void setDashBoardActive(boolean dashBoardActive) {
		SessionUtils.dashBoardActive = dashBoardActive;
	}

	public static SingleInstrument getSingleInstrument() {
		return singleInstrument;
	}

	public static void setSingleInstrument(SingleInstrument singleInstrument) {
		SessionUtils.singleInstrument = singleInstrument;
	}

	public static boolean isWatchPositions() {
		return watchPositions;
	}

	public static void setWatchPositions(boolean watchPositions) {
		SessionUtils.watchPositions = watchPositions;
	}

	public static void setShowSingleInstrument(boolean showSingleInstrument) {
		SessionUtils.showSingleInstrument = showSingleInstrument;
	}

	public static boolean isShowSingleInstrument() {
		return showSingleInstrument;
	}

	public static void addTransactionStreamRunner(TransactionStreamRunnable transactionStreamRunner) {
		SessionUtils.transactionRunners.add(transactionStreamRunner);
	}

	public static void stopTransactionStreamRunners() {
		for (TransactionStreamRunnable runner : SessionUtils.transactionRunners) {
			runner.closeStream();// TODO think of error
		}
		SessionUtils.transactionRunners.clear();
	}

	private static void removeRunner(TransactionStreamRunnable runner) {
		int index = 0;
		for (TransactionStreamRunnable loopRunner : SessionUtils.transactionRunners) {
			if (runner.getUserID().equals(loopRunner.getUserID())) {
				index++;
				break;
			}
		}
		SessionUtils.transactionRunners.remove(index);
	}

	public static boolean transactionStreamsRunning() {
		if (SessionUtils.transactionRunners.size() == SessionUtils.accounts.size()) {
			return true;
		}

		for (Account account : SessionUtils.getAccounts()) {
			boolean found = false;
			for (TransactionStreamRunnable runner : SessionUtils.transactionRunners) {
				if (runner.getUserID().equals(account.getUserId().toString())) {
					found = true;
				}
			}
			if (!found) {
				return false;
			}
		}
		return false;
	}

	public static void startUpTransactionStreams(TransactionStreamService sevice) {
		stopTransactionStreamRunners();
		for (Account account : SessionUtils.getAccounts()) {
			transactionLog.info("Constructing Thread " + account.getUserId());
			TransactionStreamRunnable transactionStreamRunner = new TransactionStreamRunnable(account.getUserId(), account.getUserName(), sevice);
			transactionLog.info("Thread constructed for " + account.getUserId());
			Thread runner = new Thread(transactionStreamRunner);
			transactionLog.info("Starting Thread " + account.getUserId());
			runner.start();
			SessionUtils.addTransactionStreamRunner(transactionStreamRunner);
		}
	}

	public static void startUpTransactionStream(String userID, TransactionStreamService service) {
		transactionLog.info("Constructing Thread " + userID);
		TransactionStreamRunnable transactionStreamRunner = new TransactionStreamRunnable(userID, userID, service);
		transactionLog.info("Thread constructed for " + userID);
		Thread runner = new Thread(transactionStreamRunner);
		transactionLog.info("Starting Thread " + userID);
		runner.start();
		SessionUtils.addTransactionStreamRunner(transactionStreamRunner);
	}

	private static boolean alreadyRunning(String userId) {
		for (TransactionStreamRunnable runner : transactionRunners) {
			if (runner.getUserID().equals(userId)) {
				return true;
			}
		}
		return false;
	}

	public static List<TransactionStreamRunnable> getTransactionRunners() {
		return transactionRunners;
	}

	public static boolean isWatchOrders() {
		return watchOrders;
	}

	public static void setWatchOrders(boolean watchOrders) {
		SessionUtils.watchOrders = watchOrders;
	}

	public static List<PriceStreamRunnable> getPriceRunners() {
		return priceRunners;
	}

	public static void setPriceRunners(List<PriceStreamRunnable> priceRunners) {
		SessionUtils.priceRunners = priceRunners;
	}

	public static void stopPriceRunners(PriceRunner runner, String sessionID) {
		PriceStreamRunnable delRunner = null;
		for (PriceStreamRunnable priceRunner : priceRunners) {
			if (priceRunner.getSessionID().equals(sessionID)) {
				try {
					delRunner = priceRunner;
					priceRunner.getHttpClient().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		priceRunners.remove(delRunner);
	}

	// Maybe open one Stream for every user...
	public static void startPriceRunners(PriceRunner runner, PriceStreamService priceStreamService, List<String> instruments, String sessionID) {
		Account account = SessionUtils.getAccounts().get(0); // Wich user to use
		PriceStreamRunnable priceStreamrunnable = new PriceStreamRunnable(account.getUserId(), account.getUserName(), priceStreamService, runner, instruments, sessionID);
		SessionUtils.getPriceRunners().add(priceStreamrunnable);
		Thread thread = new Thread(priceStreamrunnable);
		thread.start();
	}

	public static void stopAllPriceRunners() {
		for (PriceStreamRunnable priceRunner : priceRunners) {
			try {
				priceRunner.getHttpClient().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		SessionUtils.getPriceRunners().clear();
	}

	public static void stopChartService(String sessionID) {
		ChartRunner delRunner = null;
		for (ChartRunner chartRunner : chartRunners) {
			if (chartRunner.getSessionID().equals(sessionID)) {
				chartRunner.setStop(true);
				delRunner = chartRunner;
			}
		}
		chartRunners.remove(delRunner);
	}

	// Why dis it somtime still uns
	public static void stopChartServices() {
		for (ChartRunner chartRunner : chartRunners) {
			chartRunner.setStop(true);
		}
	}

	// instruments.get(0), interval.get(0), userID.get(0), sessionID
	public static void startUpChartStream(String instrument, String interval, String userID, InstrumentController instrumentController, String sessionID) {
		ChartRunner chartRunner = new ChartRunner(instrument, interval, userID, instrumentController, sessionID);
		chartRunners.add(chartRunner);
		Thread thread = new Thread(chartRunner);
		thread.start();
	}

	public static void startDashBoardStream(DashboardController dashBoardController, String sessionID) {
		DashBoardRunner dashboardRunner = new DashBoardRunner(dashBoardController, sessionID);
		dashBoardRunners.add(dashboardRunner);
		Thread thread = new Thread(dashboardRunner);
		thread.start();
	}

	public static void stopDashboardRunner(String sessionID) {
		DashBoardRunner delBoard = null;
		for (DashBoardRunner dashBoardRunner : dashBoardRunners) {
			if (dashBoardRunner.getSessionID().equals(sessionID)) {
				dashBoardRunner.setStop(true);
				delBoard = dashBoardRunner;
			}
		}
		dashBoardRunners.remove(delBoard);
	}

	public static void startPositionRunner(PositionController positionController, String sessionID) {
		PositionRunner positionRunner = new PositionRunner(positionController, sessionID);
		positionRunners.add(positionRunner);
		Thread thread = new Thread(positionRunner);
		thread.start();
	}

	public static void stopPositionRunner(String sessionID) {
		PositionRunner delPos = null;
		for (PositionRunner positionRunner : positionRunners) {
			if (positionRunner.getSessionID().equals(sessionID)) {
				positionRunner.setStop(true);
				delPos = positionRunner;
			}
		}
		positionRunners.remove(delPos);
	}

	public static void startSinglePositionRunner(PositionController positionController, String sessionID) {
		SinglePositionRunner singlePositionRunner = new SinglePositionRunner(positionController, sessionID);
		Thread thread = new Thread(singlePositionRunner);
		thread.start();
		singlePositionRunners.add(singlePositionRunner);
	}

	public static void stopSinglePositionRunner(String sessionID) {
		SinglePositionRunner delRunner = null;
		for (SinglePositionRunner singlePositionRunner : singlePositionRunners) {
			if (singlePositionRunner.getSessionID().equals(sessionID)) {
				singlePositionRunner.setStop(true);
				delRunner = singlePositionRunner;
			}
		}
		singlePositionRunners.remove(delRunner);
	}

	public static void stopOpenOrderRunner(String sessionID) {
		for (OpenOrdersRunner runner : orderRunners) {
			if (runner.getSessionID().equals(sessionID)) {
				runner.setStop(true);
				orderRunners.remove(runner);
				return;
			}
		}
	}

	public static void startOpenOrderRunner(OrderController orderController, String sessionID) {
		OpenOrdersRunner runner = new OpenOrdersRunner(orderController, sessionID);
		Thread thread = new Thread(runner);
		thread.start();
		orderRunners.add(runner);
	}

	public static void startCandleRunner(InstrumentController instrumentController, IntervalEnum interval) {
		CandleRunner runner = new CandleRunner(instrumentController, interval);
		candleRunners.add(runner);
		Thread thread = new Thread(runner);
		thread.start();
	}

	public static void stopCandleRunners() {
		for (CandleRunner runner : candleRunners) {
			runner.setStop(true);
		}
	}

	public static void stopTransactionRunner(String userID, TransactionStreamService service) {
		for (TransactionStreamRunnable runner : transactionRunners) {
			if (runner != null && runner.getUserID() != null && runner.getUserID().equals(userID)) {
				runner.closeStream();
				transactionRunners.remove(runner);
				break;
			}
		}
	}

	public static Account findAccountByUserName(String username) {
		for (Account account : SessionUtils.getAccounts()) {
			if (account.getUserName().equals(username)) {
				return account;
			}
		}
		return null;
	}

	public static void restartTransactionRunner(String userID, TransactionStreamService service) {
		transactionLog.info("############# Restarting Transaction Stream #####################");
		stopTransactionRunner(userID, service);
		startUpTransactionStream(userID, service);
	}

	public static int getTradeCounter() {
		return tradeCounter;
	}

	public static void setTradeCounter(int tradeCounter) {
		SessionUtils.tradeCounter = tradeCounter;
	}
}
