package rawai.it.com.TradingPlatform.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.DTO.InstrumentDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Massorder {
	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	// {"orders":[{"id":"588","createTime":"2017-02-02T09:29:07.556536465Z","type":"TAKE_PROFIT","tradeID":"513","price":"1.00160","timeInForce":"GTC","triggerCondition":"TRIGGER_DEFAULT","state":"PENDING"}],"lastTransactionID":"588"}
	@JsonIgnore
	private String userID;
	private String units;
	@NotBlank(message = Massorder.NOT_BLANK_MESSAGE)
	private String instrument;
	private String timeInForce;
	private String type;
	private String positionFill;
	private String tradeID;
	private String price;
	private String id;
	@JsonIgnore
	private boolean buy = true;

	@Valid
	private List<InstrumentDTO> instruments = new ArrayList<>();


	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPositionFill() {
		return positionFill;
	}

	public void setPositionFill(String positionFill) {
		this.positionFill = positionFill;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public boolean isBuy() {
		return buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<InstrumentDTO> getInstruments() {
		return instruments;
	}

	public void setInstruments(List<InstrumentDTO> instruments) {
		this.instruments = instruments;
	}
}
