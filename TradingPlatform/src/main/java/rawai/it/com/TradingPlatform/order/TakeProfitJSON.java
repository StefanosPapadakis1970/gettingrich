package rawai.it.com.TradingPlatform.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitJSON {

	private TakeProfitOrder order;

	public TakeProfitOrder getOrder() {
		return order;
	}

	public void setOrder(TakeProfitOrder order) {
		this.order = order;
	}
}
