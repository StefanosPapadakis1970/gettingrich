package rawai.it.com.TradingPlatform.urlutils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class CurlUtil {

	@Value("${oanda.url}")
	public String PRAXIS_URL = null;
	@Value("${oanda.url}")
	public String BASE_URL = null;
	@Value("${oanda.streaming.url}")
	public String PRAXIS_STREAMING_URL = null;
	@Value("${oanda.streaming.url}")
	public String STREAMING_URL = null;
	@Value("${accounts.endpoint}")
	public String ACCOUNTS_ENDPOINT = null;


	public HttpEntity<Object> createAPIHeader(String apiKey) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + apiKey);
		// headers.set("X-Accept-Datetime-Format", "UNIX");
		headers.set("X-Accept-Datetime-Format", "RFC3339");
		headers.set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		HttpEntity<Object> entity = new HttpEntity<>(headers);
		return entity;
	}

	public HttpEntity<?> createAPIHeaderWithBody(String apiKey, String body) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", "Bearer " + apiKey);
		headers.set("X-Accept-Datetime-Format", "UNIX");
		headers.set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		HttpEntity<?> entity = new HttpEntity<>(body, headers);
		return entity;
	}

	public String createAccountUrl() {
		return PRAXIS_URL + ACCOUNTS_ENDPOINT;
	}

	public String createBaseUrl() {
		return PRAXIS_URL + "v3/";
	}

	public String createStreamingAccountUrl() {
		return PRAXIS_STREAMING_URL + ACCOUNTS_ENDPOINT;
	}
}
