package rawai.it.com.TradingPlatform.signup;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountIDListResponse;
import rawai.it.com.TradingPlatform.account.AccountIdResponse;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.account.UserService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class SignupController {

	private static final String SIGNUP_VIEW_NAME = "signup/signup";

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private UserService userService;
	@Autowired
	private CurlUtil curlUtil;

	@RequestMapping(value = "signup")
	public String signup(Model model) {
		model.addAttribute(new SignupForm());
		return SIGNUP_VIEW_NAME;
	}

	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra, HttpServletRequest request) {
		if (errors.hasErrors()) {
			return SIGNUP_VIEW_NAME;
		}
		Account account = signupForm.createAccount();
		RestTemplate tmpl = new RestTemplate();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<AccountIDListResponse> resp = null;
		try {
			resp = tmpl.exchange(curlUtil.createAccountUrl(), HttpMethod.GET, apiHeader, AccountIDListResponse.class);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				errors.rejectValue("apiKey", "NOT_BLANK", "API-Key ist not Authorized!");
				return SIGNUP_VIEW_NAME;
			}
		}

		boolean idFound = false;

		for (AccountIdResponse accountID : resp.getBody().getAccounts()) {
			if (accountID.getId().equals(account.getUserId())) {
				idFound = true;
			}
		}

		if (!idFound) {
			errors.rejectValue("userId", "NOT_BLANK", "UserId is not supported by given API-Key!");
			return SIGNUP_VIEW_NAME;
		}

		if (SessionUtils.findAccountByUserId(signupForm.getUserId()) != null) {
			errors.rejectValue("userId", "NOT_BLANK", "UserId is alread registered!");
			return SIGNUP_VIEW_NAME;
		}

		accountRepository.save(account);

		SessionUtils.getAccounts().add(account);
		return "redirect:/account/showall";
	}

}
