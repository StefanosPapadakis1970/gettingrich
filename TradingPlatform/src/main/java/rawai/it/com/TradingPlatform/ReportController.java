package rawai.it.com.TradingPlatform;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.emory.mathcs.backport.java.util.Collections;
import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.algorithmtesting.TestingRepository;
import rawai.it.com.TradingPlatform.chart.ChartService;
import rawai.it.com.TradingPlatform.dashboard.MarketOrderTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderCancelRejectTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderCancelTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderFillTransaction;
import rawai.it.com.TradingPlatform.dashboard.TakeProfitOrderRejectTransaction;
import rawai.it.com.TradingPlatform.dashboard.TakeProfitOrderTransaction;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.engine.db.TransactionMessageEntity;
import rawai.it.com.TradingPlatform.trade.Trade;
import rawai.it.com.TradingPlatform.trade.TradeResponse;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class ReportController {

	private static final Logger log = LogManager.getLogger(ReportController.class);

	@Autowired
	private AlgorithmTradeRepository algorithmTradeRepository;
	@Autowired
	private TradingRepository tradingRepositiory;
	@Autowired
	private TestingRepository testingRepository;
	@Autowired
	private ChartService chartService;
	@Autowired
	private CurlUtil curlUtil;

	//
	@RequestMapping(value = "/reports/showAllTrades", method = RequestMethod.GET)
	public ModelAndView showAllTrades() {
		List<AlgorithmTradeEntity> tradeEntities = algorithmTradeRepository.findAllTrades();
		List<AlgorithmTrade> trades = new ArrayList<>();
		List<TradingAlgorithmEntity> tradingAlgorithmTradeEntities = tradingRepositiory.findAllTradingAlgorithms();

		for (AlgorithmTradeEntity tradeEntity : tradeEntities) {
			AlgorithmTrade trade = AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);
			for (TradingAlgorithmEntity agorithmentity : tradingAlgorithmTradeEntities) {
				if (agorithmentity.getId().equals(tradeEntity.getAlgorithmID())) {
					TradingAlgorithm algorithm = TradingProfileConverter.convertTradingAlgorithmEntity(agorithmentity);
					trade.setTradingAlgorithm(algorithm);
				}
			}
			trades.add(trade);
		}

		ModelAndView resultView = new ModelAndView("/reports/showAll");
		resultView.addObject("trades", trades);
		return resultView;
	}

	@RequestMapping(value = "/reports/showAllTradesForTest", method = RequestMethod.GET)
	public ModelAndView showAllTradesForTest(@RequestParam("testID") Long testID) {
		ModelAndView resultView = new ModelAndView("/reports/showAll");
		resultView.addObject("testRuns", testingRepository.findByID(testID).getTestRuns());
		return resultView;
	}

	// ((public ModelAndView showChartForTade(Long tradeID) {
	@RequestMapping(value = "/reports/showTradeChart", method = RequestMethod.GET)
	public ModelAndView showAllTradeChart(@RequestParam("tradeID") Long tradeID, @RequestParam("zoomTradeStart") boolean zoomTradeStart) {
		AlgorithmTradeEntity tradeEntity = algorithmTradeRepository.findAlgorithmByID(tradeID);
		if (tradeEntity.isRealTrade()) {
			return chartService.showChartForRealTade(tradeID, zoomTradeStart);
		}
		return chartService.showChartForTade(tradeID, zoomTradeStart);
	}

	@RequestMapping(value = "/reports/showTestRunChart", method = RequestMethod.GET)
	public ModelAndView showTestRunChart(@RequestParam("testRunID") Long testRunID) {
		return chartService.showChartForTestRun(testRunID);
	}

	/*
	 * 
	 * Total Win:</td> <td /> </tr> <tr> <td>Total Loss:</td> <td /> </tr> <tr>
	 * <td>Total Profit:</td> <td /> </tr>
	 */

	@RequestMapping(value = "/reports/showAllEngineTrades", method = RequestMethod.GET)
	public ModelAndView showAllengineTrades() {
		ModelAndView resultView = new ModelAndView("/reports/showAllEngineTrades");
		List<AlgorithmTradeEntity> trades = algorithmTradeRepository.findAllEngineTrades();
		for (AlgorithmTradeEntity trade : trades) {
			log.info(trade.getTradeID());
		}
		updateEngineTrades(trades);
		resultView.addObject("algorithms", createAlgorithms(trades));
		return resultView;
	}

	@RequestMapping(value = "/reports/showTradeProtocol", method = RequestMethod.GET)
	public ModelAndView showTradeProtocol(@RequestParam("tradeID") Long tradeID) throws JsonParseException, JsonMappingException, IOException {
		ModelAndView resultView = new ModelAndView("/reports/showTradeProtocol");
		List<TransactionMessageEntity> messages = tradingRepositiory.findMessageByTradeID(tradeID);
		List<String> messageList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		for (TransactionMessageEntity message : messages) {
			MarketOrderTransaction transaction = mapper.readValue(message.getMessage(), MarketOrderTransaction.class);
			String messageString = createMessageString(transaction, message.getMessage());
			messageList.add(messageString);
		}

		resultView.addObject("messages", messageList);

		return resultView;
	}

	private String createMessageString(MarketOrderTransaction transaction, String line) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String message = "";


		switch (transaction.getType()) {
		case MARKET_ORDER:
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transaction);
			break;
		case ORDER_CANCEL:
			OrderCancelTransaction orderCancel = mapper.readValue(line, OrderCancelTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(orderCancel);
			break;
		case ORDER_CANCEL_REJECT:
			OrderCancelRejectTransaction orderCancelReject = mapper.readValue(line, OrderCancelRejectTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(orderCancelReject);
			break;
		case ORDER_FILL:
			OrderFillTransaction fillTransaction = mapper.readValue(line, OrderFillTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fillTransaction);
			break;
		case STOP_LOSS_ORDER:
			TakeProfitOrderTransaction stopLoss = mapper.readValue(line, TakeProfitOrderTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stopLoss);
			break;
		case STOP_LOSS_ORDER_REJECT:
			TakeProfitOrderRejectTransaction stopLossReject = mapper.readValue(line, TakeProfitOrderRejectTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stopLossReject);
			break;
		case TAKE_PROFIT_ORDER:
			TakeProfitOrderTransaction takeProfit = mapper.readValue(line, TakeProfitOrderTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(takeProfit);
			break;
		case TAKE_PROFIT_ORDER_REJECT:
			TakeProfitOrderRejectTransaction takeProfitReject = mapper.readValue(line, TakeProfitOrderRejectTransaction.class);
			message += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(takeProfitReject);
			break;
		default:
			break;
		}

		return message;
	}

	private void updateEngineTrades(List<AlgorithmTradeEntity> trades) {
		for (AlgorithmTradeEntity trade : trades) {
			if (!trade.isFinished()) {
				Trade tradeData = getTradeData(trade);
				if (tradeData != null && tradeData.getState() != null && tradeData.getState().equals("CLOSED")) {
					trade.setFinished(true);
					trade.setPl(Double.valueOf(tradeData.getRealizedPL()));
					if (tradeData.getStopLossOrder() != null) {
						trade.setStopLossPrice(Double.valueOf(tradeData.getStopLossOrder().getPrice()));
					}
					if (tradeData.getTakeProfitOrder() != null) {
						trade.setTakeProfitPrice(Double.valueOf(tradeData.getTakeProfitOrder().getPrice()));
					}

					algorithmTradeRepository.update(trade);
				}
			}
		}
	}

	public Trade getTradeData(AlgorithmTradeEntity trade) {
		RestTemplate tmpl = new RestTemplate();

		Account account = SessionUtils.findAccountByAccountID(trade.getAccountID());

		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());

		String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/trades/" + trade.getTradeID();

		ResponseEntity<TradeResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, TradeResponse.class);

		return resp.getBody().getTrade();
	}

	private BigDecimal calculateTotalProfit(List<AlgorithmTrade> trades) {
		BigDecimal price = new BigDecimal(0).setScale(5, RoundingMode.HALF_UP);
		for (AlgorithmTrade trade : trades) {
			if (trade.isFinished() && trade.getPl() != null) {
				price = price.add(new BigDecimal(trade.getPl()).setScale(5, RoundingMode.HALF_UP));
			}
		}
		return price;
	}

	private BigDecimal calculateTotalLoss(List<AlgorithmTrade> trades) {
		BigDecimal price = new BigDecimal(0).setScale(5, RoundingMode.HALF_UP);
		for (AlgorithmTrade trade : trades) {
			if (!trade.isSuccess() && trade.getPl() != null) {
				price = price.add(new BigDecimal(trade.getPl()).setScale(5, RoundingMode.HALF_UP));
			}
		}
		return price;
	}

	private BigDecimal calculateTotalWin(List<AlgorithmTrade> trades) {
		BigDecimal price = new BigDecimal(0).setScale(5, RoundingMode.HALF_UP);
		for (AlgorithmTrade trade : trades) {
			if (trade.isSuccess() && trade.getPl() != null) {
				price = price.add(new BigDecimal(trade.getPl()).setScale(5, RoundingMode.HALF_UP));
			}
		}
		return price;
	}

	private int calculateTakeProfitTrades(List<AlgorithmTrade> trades) {
		int takeProfitTrades = 0;
		for (AlgorithmTrade trade : trades) {
			if (trade.isFinished() && trade.isSuccess()) {
				takeProfitTrades++;
			}
		}
		return takeProfitTrades;
	}

	private int calculateStopLossTrades(List<AlgorithmTrade> trades) {
		int stopLossTrades = 0;
		for (AlgorithmTrade trade : trades) {
			if (trade.isFinished() && !trade.isSuccess()) {
				stopLossTrades++;
			}
		}
		return stopLossTrades;
	}

	private int calculateOpenedPositions(List<AlgorithmTrade> tradeList) {
		int openPositions = 0;
		for (AlgorithmTrade trade : tradeList) {
			if (!trade.isFinished()) {
				openPositions++;
			}
		}
		return openPositions;
	}

	private List<TradingAlgorithm> createAlgorithms(List<AlgorithmTradeEntity> trades) {
		List<TradingAlgorithm> resultList = new ArrayList<>();
		TradingAlgorithm algorithm = null;
		for (AlgorithmTradeEntity tradeEntity : trades) {
			AlgorithmTrade trade = AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);
			if (!algorithmListContainsID(resultList, tradeEntity.getAlgorithmID())) {
				TradingAlgorithmEntity algorithmEntity = tradingRepositiory.findTradingAlgorithmByID(tradeEntity.getAlgorithmID());
				algorithm = TradingProfileConverter.convertTradingAlgorithmEntity(algorithmEntity);
				resultList.add(algorithm);
			}

			algorithm = fetchAlgorithmByID(resultList, tradeEntity.getAlgorithmID());

			algorithm.getTrades().add(trade);
		}
		// Sort by date
		for (TradingAlgorithm iterAlgorithm : resultList) {
			Collections.sort(iterAlgorithm.getTrades(), new Comparator<AlgorithmTrade>() {

				@Override
				public int compare(AlgorithmTrade o1, AlgorithmTrade o2) {
					return o1.getCreationDate().compareTo(o2.getCreationDate());
				}
			});
			
			List<AlgorithmTrade> tradeList = iterAlgorithm.getTrades();
			iterAlgorithm.setOpenedPositions(tradeList.size());
			iterAlgorithm.setStillOpenPositions(calculateOpenedPositions(tradeList));
			iterAlgorithm.setNumberOfTakeProfitTrades(calculateTakeProfitTrades(tradeList));
			iterAlgorithm.setNumberOfStopLossTrades(calculateStopLossTrades(tradeList));
			iterAlgorithm.setTotalWin(calculateTotalWin(tradeList).setScale(6));
			iterAlgorithm.setTotalLoss(calculateTotalLoss(tradeList).setScale(6));
			iterAlgorithm.setTotalProfit(calculateTotalProfit(tradeList).setScale(6));
		}

		return resultList;
	}

	private TradingAlgorithm fetchAlgorithmByID(List<TradingAlgorithm> resultList, Long algorithmID) {
		for (TradingAlgorithm algorithm : resultList) {
			if (algorithm.getDbID().equals(algorithmID)) {
				return algorithm;
			}
		}
		return null;
	}

	private boolean algorithmListContainsID(List<TradingAlgorithm> algorithms, Long id) {
		for (TradingAlgorithm algorithm : algorithms) {
			if (algorithm.getDbID().equals(id)) {
				return true;
			}
		}
		return false;
	}
}
