package rawai.it.com.TradingPlatform.engine.postsignals;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;
import rawai.it.com.TradingPlatform.utils.TradingUtils;

public class TakeProfitPostTradingSignal extends AbstractPostTradingSignal {

	public TakeProfitPostTradingSignal(int candlesBefore) {
		setValue(candlesBefore);
		setPostTradeSignalEnum(PostTradingSignalEnum.TAKEPROFIT);
	}

	@Override
	public void postProcessTrade(AlgorithmTrade trade) {
		CandleDataPointInterface takeProfitCandle = trade.getCandleBeforeTradingCandle(getValue());		
		Double newBasePrice = Double.valueOf(takeProfitCandle.getC());		
		Double newTakeProfitPrice = TradingUtils.calculateLossProfitPrice(newBasePrice, trade.getTakeProfitPips(), trade.isBuySell(), true);
		trade.setTakeProfitPrice(newTakeProfitPrice);
	}

	@Override
	public void dynamicPostProcessTrade(AlgorithmTrade trade, Double price) {
		// TODO Auto-generated method stub

	}
}
