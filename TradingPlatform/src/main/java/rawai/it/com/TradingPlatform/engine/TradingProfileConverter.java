package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.DTO.InstrumentDTO;
import rawai.it.com.TradingPlatform.DTO.ProfileDTO;
import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;
import rawai.it.com.TradingPlatform.engine.postsignals.PostTradingSignal;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.tradingprofile.TradingAlgorithmFormConverter;

public class TradingProfileConverter {

	public static TradingProfile convertToTradingProfile(TradingProfileEntity profileEntity, Account account) {
		TradingProfile resultProfile = new TradingProfile();
		resultProfile.setAccount(account);
		resultProfile.setTradingAlgorithms(convertTradingAlgorithmList(profileEntity.getTradingAlgorithms()));
		resultProfile.setTradingInstruments(convertTradingInstrumentList(profileEntity.getTradingInstruments()));
		resultProfile.setDbID(profileEntity.getId());
		return resultProfile;
	}

	public static TradingProfileEntity convertToTradingProfileEntity(TradingProfile profile) {
		TradingProfileEntity profileEntity = new TradingProfileEntity();
		profileEntity.setUserID(profile.getAccount().getUserId());
		profileEntity.setTradingAlgorithms(convertToTradingAlgorithmEntityList(profile.getTradingAlgorithms(), profileEntity));
		profileEntity.setTradingInstruments(convertToTradingInstrumentEntityList(profile.getTradingInstruments(), profileEntity));
		profileEntity.setId(profile.getDbID());
		return profileEntity;
	}

	private static List<TradingInstrumentEntity> convertToTradingInstrumentEntityList(List<TradingInstrument> tradingInstruments, TradingProfileEntity profileEntity) {
		List<TradingInstrumentEntity> resultList = new ArrayList<>();
		for (TradingInstrument instrument : tradingInstruments) {
			resultList.add(convertToTradingInstrumentEntity(instrument, profileEntity));
		}

		return resultList;
	}

	public static TradingInstrumentEntity convertToTradingInstrumentEntity(TradingInstrument instrument, TradingProfileEntity profileEntity) {
		TradingInstrumentEntity instrumententity = new TradingInstrumentEntity();
		instrumententity.setTradingprofile(profileEntity);
		instrumententity.setInstument(instrument.getInstument());
		instrumententity.setUnits(instrument.getUnits());
		instrumententity.setTradingInterval(instrument.getTradingInterval());
		instrumententity.setMassTrade(instrument.isMassTrade());
		instrumententity.setTradingEngine(instrument.isTradingEngine());
		if (instrument.getDbID() != null) {
			instrumententity.setId(instrument.getDbID());
		}
		return instrumententity;
	}

	private static List<TradingAlgorithmEntity> convertToTradingAlgorithmEntityList(List<TradingAlgorithm> tradingAlgorithms, TradingProfileEntity profile) {
		List<TradingAlgorithmEntity> resultList = new ArrayList<>();
		for (TradingAlgorithm algorithm : tradingAlgorithms) {
			resultList.add(convertToTradingAlgorithmEntity(algorithm, profile));
		}

		return resultList;
	}

	public static TradingAlgorithmEntity convertToTradingAlgorithmEntity(TradingAlgorithm algorithm, TradingProfileEntity profile) {
		TradingAlgorithmEntity tradingAlgorithmEntity = new TradingAlgorithmEntity();

		tradingAlgorithmEntity.setBuy(algorithm.isBuy());
		tradingAlgorithmEntity.setName(algorithm.getName());
		tradingAlgorithmEntity.setId(algorithm.getDbID());
		tradingAlgorithmEntity.setTradingSignals(convertToTradinSignalEntityList(algorithm.getTradingSignals(), tradingAlgorithmEntity));
		tradingAlgorithmEntity.setPostTradingSignals(convertToPostTradingSignalEntityList(algorithm.getPostTradingSignals(), tradingAlgorithmEntity));
		tradingAlgorithmEntity.setStopLossPips(algorithm.getStopLossPips());
		tradingAlgorithmEntity.setTakeProfitPips(algorithm.getTakeProfitPips());

		return tradingAlgorithmEntity;
	}

	private static List<PostTradeSignalEntity> convertToPostTradingSignalEntityList(List<PostTradingSignal> postTradingSignals, TradingAlgorithmEntity algorithm) {
		List<PostTradeSignalEntity> resultList = new ArrayList<>();
		for (PostTradingSignal tradingSignal : postTradingSignals) {
			resultList.add(convertToPostTradingSignalEntity(tradingSignal, algorithm));
		}
		return resultList;
	}

	private static PostTradeSignalEntity convertToPostTradingSignalEntity(PostTradingSignal tradingSignal, TradingAlgorithmEntity algorithm) {
		PostTradeSignalEntity resultEntity = new PostTradeSignalEntity();
		resultEntity.setValue(tradingSignal.getValue());
		resultEntity.setTradingSignal(tradingSignal.getPostTradingSignalEnum());
		resultEntity.setDynamic(tradingSignal.isDynamic());
		return resultEntity;
	}


	private static List<TradingSignalEntity> convertToTradinSignalEntityList(List<TradingSignal> tradingSignals, TradingAlgorithmEntity algorithm) {
		List<TradingSignalEntity> resultList = new ArrayList<>();
		for (TradingSignal tradingSignal : tradingSignals) {
			resultList.add(convertToTradingSignalEntity(tradingSignal, algorithm));
		}
		return resultList;
	}

	private static TradingSignalEntity convertToTradingSignalEntity(TradingSignal tradingSignal, TradingAlgorithmEntity algorithm) {
		TradingSignalEntity resultEntity = new TradingSignalEntity();
		resultEntity.setCandleRange(tradingSignal.getCandleRange());
		resultEntity.setHigh(tradingSignal.getHigh());
		resultEntity.setLow(tradingSignal.getLow());
		resultEntity.setSignalvalue(tradingSignal.getValue());
		resultEntity.setTradingSignal(tradingSignal.getTradingSignalEnum());
		// resultEntity.getTradingalgorithms().add(algorithm);
		return resultEntity;
	}

	private static List<TradingInstrument> convertTradingInstrumentList(List<TradingInstrumentEntity> tradingInstruments) {
		List<TradingInstrument> instruments = new ArrayList<>();
		for (TradingInstrumentEntity tradingInstrumentEntity : tradingInstruments) {
			instruments.add(convertTradingInstrumentEntity(tradingInstrumentEntity));
		}
		return instruments;
	}

	private static List<TradingAlgorithm> convertTradingAlgorithmList(List<TradingAlgorithmEntity> tradingAlgorithms) {
		List<TradingAlgorithm> algorithms = new ArrayList<>();
		for (TradingAlgorithmEntity tradingAlgorithmEntity : tradingAlgorithms) {
			algorithms.add(convertTradingAlgorithmEntity(tradingAlgorithmEntity));
		}
		return algorithms;
	}

	public static TradingAlgorithm convertTradingAlgorithmEntity(TradingAlgorithmEntity tradingAlgorithmEntity) {
		TradingAlgorithm algorithm = new TradingAlgorithm();
		algorithm.setTradingSignals(convertTradingSignalEntities(tradingAlgorithmEntity.getTradingSignals()));
		algorithm.setPostTradingSignals(convertPostTradingSignalEntities(tradingAlgorithmEntity.getPostTradingSignals()));
		algorithm.setDbID(tradingAlgorithmEntity.getId());
		algorithm.setBuy(tradingAlgorithmEntity.isBuy());
		algorithm.setName(tradingAlgorithmEntity.getName());
		algorithm.setStopLossPips(tradingAlgorithmEntity.getStopLossPips());
		algorithm.setTakeProfitPips(tradingAlgorithmEntity.getTakeProfitPips());
		return algorithm;
	}

	private static List<PostTradingSignal> convertPostTradingSignalEntities(List<PostTradeSignalEntity> postTradingSignals) {
		List<PostTradingSignal> resultSignals = new ArrayList<>();
		for (PostTradeSignalEntity tradingSignalEntity : postTradingSignals) {
			resultSignals.add(convertPostTradingSignalEntity(tradingSignalEntity));
		}
		return resultSignals;
	}

	private static PostTradingSignal convertPostTradingSignalEntity(PostTradeSignalEntity tradingSignalEntity) {
		PostTradingSignal resultSignal = PostTradingSignalEnum.createInstance(tradingSignalEntity);
		resultSignal.setDBID(tradingSignalEntity.getId());
		resultSignal.setDynamic(tradingSignalEntity.isDynamic());
		return resultSignal;

	}

	private static List<TradingSignal> convertTradingSignalEntities(List<TradingSignalEntity> tradingSignals) {
		List<TradingSignal> resultSignals = new ArrayList<>();
		for (TradingSignalEntity tradingSignalEntity : tradingSignals) {
			resultSignals.add(convertTradingSignalEntity(tradingSignalEntity));
		}
		return resultSignals;
	}

	private static TradingSignal convertTradingSignalEntity(TradingSignalEntity tradingSignalEntity) {
		TradingSignal resultSignal = TradingSignalEnum.createInstance(tradingSignalEntity);
		resultSignal.setDBID(tradingSignalEntity.getId());
		return resultSignal;
	}

	private static TradingInstrument convertTradingInstrumentEntity(TradingInstrumentEntity tradingInstrumentEntity) {
		TradingInstrument instrument = new TradingInstrument();
		instrument.setUnits(tradingInstrumentEntity.getUnits());
		instrument.setTradingInterval(tradingInstrumentEntity.getTradingInterval());
		instrument.setInstument(tradingInstrumentEntity.getInstument());
		instrument.setMassTrade(tradingInstrumentEntity.isMassTrade());
		instrument.setTradingEngine(tradingInstrumentEntity.isTradingEngine());
		instrument.setDbID(tradingInstrumentEntity.getId());
		return instrument;
	}

	public static void applyChanges(TradingProfileEntity targetProfile, TradingProfileEntity sourceProfile) {
		targetProfile.setUserID(sourceProfile.getUserID());
		// applyAlgorithmListChanges(targetProfile.getTradingAlgorithms(),
		// sourceProfile.getTradingAlgorithms());
		applyInstrumentListChanges(targetProfile.getTradingInstruments(), sourceProfile.getTradingInstruments());
	}

	private static void applyInstrumentListChanges(List<TradingInstrumentEntity> targetInstruments, List<TradingInstrumentEntity> sourceInstruments) {
		for (TradingInstrumentEntity instrument : sourceInstruments) {
			TradingInstrumentEntity targetInstrument = fetchInstrument(targetInstruments, instrument);
			if (targetInstrument == null) {
				targetInstrument = new TradingInstrumentEntity();
				targetInstruments.add(targetInstrument);
			}
			applyChangesTradingInstrument(targetInstrument, instrument);
		}
	}

	private static void applyChangesTradingInstrument(TradingInstrumentEntity targetInstrument, TradingInstrumentEntity sourceInstrument) {
		targetInstrument.setId(sourceInstrument.getId());
		targetInstrument.setInstument(sourceInstrument.getInstument());
		targetInstrument.setTradingprofile(sourceInstrument.getTradingprofile());
		targetInstrument.setUnits(sourceInstrument.getUnits());
		targetInstrument.setTradingInterval(sourceInstrument.getTradingInterval());
	}

	private static TradingInstrumentEntity fetchInstrument(List<TradingInstrumentEntity> targetInstruments, TradingInstrumentEntity searchInstrument) {
		for (TradingInstrumentEntity instrument : targetInstruments) {
			if (instrument.getId() == searchInstrument.getId()) {
				return instrument;
			}
		}
		return null;
	}

	private static void applyAlgorithmListChanges(List<TradingAlgorithmEntity> tradingAlgorithms, List<TradingAlgorithmEntity> sourceAlgorithms) {
		for (TradingAlgorithmEntity sourceAlgorithm : sourceAlgorithms) {
			TradingAlgorithmEntity targetAlgorithm = fetchTradingAlgorithmEntity(tradingAlgorithms, sourceAlgorithm);
			if (targetAlgorithm == null) {
				targetAlgorithm = new TradingAlgorithmEntity();
				tradingAlgorithms.add(targetAlgorithm);
			}
			applyAlgorithmchanges(targetAlgorithm, sourceAlgorithm);
		}

	}

	private static void applyAlgorithmchanges(TradingAlgorithmEntity targetAlgorithm, TradingAlgorithmEntity sourceAlgorithm) {
		targetAlgorithm.setId(sourceAlgorithm.getId());
		applyTradingSignalListChanges(targetAlgorithm.getTradingSignals(), sourceAlgorithm.getTradingSignals());
	}

	private static void applyTradingSignalListChanges(List<TradingSignalEntity> targetSignals, List<TradingSignalEntity> sourceSignals) {
		if (targetSignals == null) {
			return;
		}
		for (TradingSignalEntity sourceSignal : sourceSignals) {
			TradingSignalEntity targetSignal = fetchTradingSignalEntity(targetSignals, sourceSignal);
			if (targetSignal == null) {
				targetSignal = new TradingSignalEntity();
				targetSignals.add(targetSignal);
			}
			applyTadingSignalChanges(targetSignal, sourceSignal);
		}

	}

	private static void applyTadingSignalChanges(TradingSignalEntity targetSignal, TradingSignalEntity sourceSignal) {
		targetSignal.setCandleRange(sourceSignal.getCandleRange());
		targetSignal.setHigh(sourceSignal.getHigh());
		targetSignal.setId(sourceSignal.getId());
		targetSignal.setLow(sourceSignal.getHigh());
		targetSignal.setSignalvalue(sourceSignal.getHigh());
		// targetSignal.setTradingalgorithm(sourceSignal.getTradingalgorithms());
		targetSignal.setTradingSignal(sourceSignal.getTradingSignal());
	}

	private static TradingSignalEntity fetchTradingSignalEntity(List<TradingSignalEntity> targetSignals, TradingSignalEntity sourceSignal) {
		for (TradingSignalEntity signal : targetSignals) {
			if (signal.getId() == sourceSignal.getId()) {
				return signal;
			}
		}
		return null;
	}

	private static TradingAlgorithmEntity fetchTradingAlgorithmEntity(List<TradingAlgorithmEntity> tradingAlgorithms, TradingAlgorithmEntity sourceAlgorithm) {
		for (TradingAlgorithmEntity algorithm : tradingAlgorithms) {
			if (algorithm.getId() == sourceAlgorithm.getId()) {
				return algorithm;
			}
		}

		return null;
	}

	public static TradingProfileForm convertToTradingProfileForm(TradingProfileEntity profile) {
		TradingProfileForm resultForm = new TradingProfileForm();
		resultForm.setDbID(String.valueOf(profile.getId()));
		resultForm.setUserID(profile.getUserID());
		for (TradingInstrumentEntity instrument : profile.getTradingInstruments()) {
			resultForm.getTradingInstruments().add(convertInstrumentToInstrumentForm(instrument));
		}

		for (TradingAlgorithmEntity algorithm : profile.getTradingAlgorithms()) {
			resultForm.getTradingAlgorithms().add(TradingAlgorithmFormConverter.convertEntityToForm(algorithm));
		}
		return resultForm;
	}

	private static TradingInstrumentForm convertInstrumentToInstrumentForm(TradingInstrumentEntity instrument) {
		TradingInstrumentForm instrumentForm = new TradingInstrumentForm();
		instrumentForm.setDbID(instrument.getId() == null ? null : instrument.getId().toString());
		instrumentForm.setInstrument(instrument.getInstument());
		instrumentForm.setUnits(String.valueOf(instrument.getUnits()));
		instrumentForm.setMassTrade(instrument.isMassTrade());
		instrumentForm.setTradingEngine(instrument.isTradingEngine());
		instrumentForm.setTradingInterval(instrument.getTradingInterval() == null ? "Not Set" : instrument.getTradingInterval().getName());
		return instrumentForm;
	}

	public static void updateIDs(TradingProfileEntity profileEntity, TradingProfileForm profile) {
		profile.setDbID(profileEntity.getId().toString());
		for (TradingInstrumentForm instrumentForm : profile.getTradingInstruments()) {
			setInstrumentFormID(instrumentForm, profileEntity.getTradingInstruments());
		}
	}

	private static void setInstrumentFormID(TradingInstrumentForm instrumentForm, List<TradingInstrumentEntity> tradingInstruments) {
		for (TradingInstrumentEntity instrument : tradingInstruments) {
			if (instrument.getInstument().equals(instrumentForm.getInstrument()) && instrument.getTradingInterval().name().equals(instrumentForm.tradingInterval)) {
				instrumentForm.setDbID(String.valueOf(instrument.getId()));
				return;
			}
		}
	}

	public static ProfileDTO contvertTOProfileDTO(TradingProfileEntity profileEntity, String userName) {
		ProfileDTO resultDTO = new ProfileDTO();
		resultDTO.setUserID(profileEntity.getUserID());
		for (TradingInstrumentEntity instrument : profileEntity.getTradingInstruments()) {
			resultDTO.getInstruments().add(convertTOInstrumentDTO(instrument, userName));
		}
		return resultDTO;
	}

	private static InstrumentDTO convertTOInstrumentDTO(TradingInstrumentEntity instrument, String userName) {
		InstrumentDTO instrumentDTO = new InstrumentDTO();
		instrumentDTO.setUnits(String.valueOf(instrument.getUnits()));
		instrumentDTO.setInstrument(instrument.getInstument());
		instrumentDTO.setUserName(userName);
		instrumentDTO.setInterval(instrument.getTradingInterval());
		return instrumentDTO;
	}
}
