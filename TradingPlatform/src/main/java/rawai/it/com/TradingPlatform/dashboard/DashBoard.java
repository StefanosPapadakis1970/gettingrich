package rawai.it.com.TradingPlatform.dashboard;

import java.util.List;

import rawai.it.com.TradingPlatform.position.Position;

public class DashBoard {

	private String numberOfAccounts;
	private String totalValueOfAccounts;
	private String totalRealized;
	private String totalUnrealized;
	private String totalRealizedForCurrentMonth;
	private String totalUnRealizedForCurrentMonth;
	private String currentDay;
	private String currentMonth;
	private List<Position> loosers;
	private List<Position> winners;
	private String plToday;

	public String getNumberOfAccounts() {
		return numberOfAccounts;
	}

	public void setNumberOfAccounts(String numberOfAccounts) {
		this.numberOfAccounts = numberOfAccounts;
	}

	public String getTotalValueOfAccounts() {
		return totalValueOfAccounts;
	}

	public void setTotalValueOfAccounts(String totalValueOfAccounts) {
		this.totalValueOfAccounts = totalValueOfAccounts;
	}

	public String getTotalRealized() {
		return totalRealized;
	}

	public void setTotalRealized(String totalRealized) {
		this.totalRealized = totalRealized;
	}

	public String getTotalUnrealized() {
		return totalUnrealized;
	}

	public void setTotalUnrealized(String totalUnrealized) {
		this.totalUnrealized = totalUnrealized;
	}

	public String getTotalRealizedForCurrentMonth() {
		return totalRealizedForCurrentMonth;
	}

	public void setTotalRealizedForCurrentMonth(String totalRealizedForCurrentMonth) {
		this.totalRealizedForCurrentMonth = totalRealizedForCurrentMonth;
	}

	public String getTotalUnRealizedForCurrentMonth() {
		return totalUnRealizedForCurrentMonth;
	}

	public void setTotalUnRealizedForCurrentMonth(String totalUnRealizedForCurrentMonth) {
		this.totalUnRealizedForCurrentMonth = totalUnRealizedForCurrentMonth;
	}

	public String getCurrentDay() {
		return currentDay;
	}

	public void setCurrentDay(String currentDay) {
		this.currentDay = currentDay;
	}

	public String getCurrentMonth() {
		return currentMonth;
	}

	public void setCurrentMonth(String currentMonth) {
		this.currentMonth = currentMonth;
	}

	public void setWinners(List<Position> winners) {
		this.winners = winners;

	}

	public void setLoosers(List<Position> loosers) {
		this.loosers = loosers;
	}

	public List<Position> getLoosers() {
		return loosers;
	}

	public List<Position> getWinners() {
		return winners;
	}

	public void setPlToday(String plToday) {
		this.plToday = plToday;
	}

	public String getPlToday() {
		return plToday;
	}
}
