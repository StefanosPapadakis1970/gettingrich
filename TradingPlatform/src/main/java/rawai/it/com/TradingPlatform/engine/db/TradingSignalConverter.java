package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

@Converter
public class TradingSignalConverter implements AttributeConverter<TradingSignalEnum, String> {

	@Override
	public String convertToDatabaseColumn(TradingSignalEnum signal) {
		if (signal == null) {
			return null;
		}
		return signal.getName();
	}

	@Override
	public TradingSignalEnum convertToEntityAttribute(String dbData) {
		return TradingSignalEnum.getValue(dbData);
	}
}
