package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;

public class CandleDataPointBuilder {

	private Candle candle;

	private CandleDataPointBuilder() {

	}

	public static CandleDataPointBuilder aCandleDataPoint() {
		return new CandleDataPointBuilder();
	}

	public CandleDataPointBuilder withCandle(Candle candle) {
		this.candle = candle;
		return this;
	}

	public CandleDataPoint build() {
		CandleDataPoint candleDataPoint = new CandleDataPoint();
		candleDataPoint.setCandle(candle);
		return candleDataPoint;
	}
}
