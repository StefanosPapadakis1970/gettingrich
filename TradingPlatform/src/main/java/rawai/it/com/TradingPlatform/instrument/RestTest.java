package rawai.it.com.TradingPlatform.instrument;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RestTest {

	public static void main(String[] args) throws IOException {
		URL url = new URL("https://api-fxpractice.oanda.com/v3/accounts/101-011-5094777-001/instruments");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Authorization", "Bearer 2817ed2364eac24f9c42f200749d067d-20a1fc15b479d0cbd5c9b05325841011");
		con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		
		String message = con.getResponseMessage();
		message += "  "+con.getResponseCode();
		System.out.println(message);
	}
}
