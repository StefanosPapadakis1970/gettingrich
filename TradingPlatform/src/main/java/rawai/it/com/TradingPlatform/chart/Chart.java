package rawai.it.com.TradingPlatform.chart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Chart {

	private Title title;
	private boolean zoomEnabled = true;
	private AxisY axisY;
	private AxisX axisX;
	private List<ChartData> data;

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public boolean isZoomEnabled() {
		return zoomEnabled;
	}

	public void setZoomEnabled(boolean zoomEnabled) {
		this.zoomEnabled = zoomEnabled;
	}

	public AxisY getAxisY() {
		return axisY;
	}

	public void setAxisY(AxisY axisY) {
		this.axisY = axisY;
	}

	public AxisX getAxisX() {
		return axisX;
	}

	public void setAxisX(AxisX axisX) {
		this.axisX = axisX;
	}

	public List<ChartData> getData() {
		return data;
	}

	public void setData(List<ChartData> data) {
		this.data = data;
	}

}
