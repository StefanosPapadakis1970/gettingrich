package rawai.it.com.TradingPlatform.candle;


public interface CandleDataPointInterface extends BasicDataPointInterface {

	boolean getComplete();

	String getO();

	String getH();

	String getC();

	String getL();

	int getCandleIndex();

	boolean isTheFirstCandleOfTheDay();

	void setTheFirstCandleOfTheDay(boolean firstCandleOfTheDay);

	void setCandleIndex(int candleIndex);
}
