package rawai.it.com.TradingPlatform.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {

	private static final Logger log = LogManager.getLogger(GlobalDefaultExceptionHandler.class);

	@ExceptionHandler(value = { Throwable.class, RuntimeException.class })
    public void defaultErrorHandler(Throwable e) throws Throwable {
		log.error("global controller default exception handler", e);
        throw e;
    }
}