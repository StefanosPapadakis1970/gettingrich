package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class CandleRangeBetween extends AbstractTradingSignal {

	private int lowerPips;
	private int upperPips;

	public CandleRangeBetween(int lowerPips, int upperPips) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.CANDLERANGE_BETWEEN, 1);
		this.lowerPips = lowerPips;
		this.upperPips = upperPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface rangeCandle = candles.get(lastSignalCandleIndex);

		int high = Integer.valueOf(rangeCandle.getH().replace(".", ""));
		int low = Integer.valueOf(rangeCandle.getL().replace(".", ""));

		int diff = high - low;
		if (diff >= lowerPips && diff <= upperPips) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		return upperPips;
	}

	@Override
	public int getLow() {
		return lowerPips;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
