package rawai.it.com.TradingPlatform.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OpenOrdersRunner implements Runnable {

	public static final Logger log = LogManager.getLogger(OpenOrdersRunner.class);

	private boolean stop;
	private OrderController orderController;
	private String sessionID;

	public OpenOrdersRunner(OrderController orderController, String sessionID) {
		super();
		this.orderController = orderController;
		this.sessionID = sessionID;
	}

	@Override
	public void run() {
		while (!stop) {
			orderController.sendOrdersToClient();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				log.info("Interupted !!");
			}
		}
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

}
