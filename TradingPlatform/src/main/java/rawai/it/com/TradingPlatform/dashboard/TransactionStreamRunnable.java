package rawai.it.com.TradingPlatform.dashboard;

import java.io.IOException;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.transaction.TransactionStreamService;

public class TransactionStreamRunnable implements Runnable {

	private static final Logger log = LogManager.getLogger("transaction-log");
	private String userID;
	private String userName;
	private TransactionStreamService transactionController;
	private CloseableHttpClient httpClient;


	public TransactionStreamRunnable(String userID, String userName, TransactionStreamService transactionController) {
		this.userID = userID;
		this.userName = userName;
		this.transactionController = transactionController;
	}

	public boolean closeStream() {
		try {
			if (httpClient != null) {
				httpClient.close();
			} else {
				return true;
			}
		} catch (IOException e) {
			log.error(e);
			return false;
		}
		return true;
	}

	@Override
	public void run() {
		transactionController.streamTransactions(userID, userName, this);
	}

	public CloseableHttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(CloseableHttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
}
