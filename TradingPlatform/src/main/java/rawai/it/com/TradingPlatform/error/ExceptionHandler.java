package rawai.it.com.TradingPlatform.error;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

/**
 * General error handler for the application.
 */
@ControllerAdvice
class ExceptionHandler {
	private static final Logger logger = LogManager.getLogger(ExceptionHandler.class);
	/**
	 * Handle exceptions thrown by handlers.
	 */
	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)	
	public ModelAndView exception(Exception exception, WebRequest request) {
		logger.error("global controller default exception handler", exception);
		ModelAndView modelAndView = new ModelAndView("error/general");
		modelAndView.addObject("errorMessage", Throwables.getRootCause(exception));
		return modelAndView;
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = Throwable.class)
	public void defaultErrorHandler(Throwable e) throws Throwable {
		logger.error("global controller default exception handler", e);

		// If the exception is annotated with @ResponseStatus rethrow it and let
		// the framework handle it.
		// AnnotationUtils is a Spring Framework utility class.
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
			throw e;
		}

		// Otherwise log exception
		logger.error("global controller default exception handler", e);
		throw e;
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = RuntimeException.class)
	public void defaultRuntimeErrorHandler(RuntimeException e) throws Throwable {
		logger.error("global controller default exception handler", e);

		// If the exception is annotated with @ResponseStatus rethrow it and let
		// the framework handle it.
		// AnnotationUtils is a Spring Framework utility class.
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
			throw e;
		}

		// Otherwise log exception
		logger.error("global controller default exception handler", e);
		throw e;
	}

}