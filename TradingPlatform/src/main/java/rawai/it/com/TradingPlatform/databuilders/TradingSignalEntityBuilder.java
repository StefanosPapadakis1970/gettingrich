package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;
import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;

public class TradingSignalEntityBuilder {

	private Long id;

	private int low;

	private int high;

	private int signalvalue;

	private TradingSignalEnum tradingSignal;

	private RangeEnum candleRange;

	private TradingAlgorithmEntity tradingalgorithm;

	private TradingSignalEntityBuilder() {

	}

	public static TradingSignalEntityBuilder aTradingSignalEntityBuilder() {
		return new TradingSignalEntityBuilder();
	}

	public TradingSignalEntityBuilder withTradingAlgorithm(TradingAlgorithmEntity tradingalgorithm) {
		this.tradingalgorithm = tradingalgorithm;
		return this;
	}

	public TradingSignalEntityBuilder whithID(long id) {
		this.id = id;
		return this;
	}

	public TradingSignalEntityBuilder withHigh(int high) {
		this.high = high;
		return this;
	}

	public TradingSignalEntityBuilder withLow(int low) {
		this.low = low;
		return this;
	}

	public TradingSignalEntityBuilder withValue(int value) {
		this.signalvalue = value;
		return this;
	}

	public TradingSignalEntityBuilder withTradingSignal(TradingSignalEnum tradingSignal) {
		this.tradingSignal = tradingSignal;
		return this;
	}

	public TradingSignalEntityBuilder withRange(RangeEnum candleRange) {
		this.candleRange = candleRange;
		return this;
	}

	public TradingSignalEntity build() {
		TradingSignalEntity signal = new TradingSignalEntity();
		signal.setCandleRange(candleRange);
		signal.setHigh(high);
		signal.setId(id);
		signal.setLow(low);
		signal.setSignalvalue(signalvalue);
		// signal.getTradingalgorithms().add(tradingalgorithm);
		signal.setTradingSignal(tradingSignal);
		return signal;
	}

	public TradingSignalEntityBuilder withID(Long id) {
		this.id = id;
		return this;
	}
}
