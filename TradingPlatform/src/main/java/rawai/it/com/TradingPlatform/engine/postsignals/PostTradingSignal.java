package rawai.it.com.TradingPlatform.engine.postsignals;

import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;

public interface PostTradingSignal {

	public PostTradingSignalEnum getPostTradingSignalEnum();

	public int getValue();

	public Long getDBID();

	public void setDBID(Long dbID);

	public void postProcessTrade(AlgorithmTrade trade);

	public void dynamicPostProcessTrade(AlgorithmTrade trade, Double price);

	public boolean isDynamic();

	public void setDynamic(boolean dynamic);
}
