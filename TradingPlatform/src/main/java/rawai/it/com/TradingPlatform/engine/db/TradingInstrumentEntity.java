package rawai.it.com.TradingPlatform.engine.db;

import java.io.Serializable;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

@Entity
@Table(name = "tradinginstrument")
public class TradingInstrumentEntity implements Serializable{

	private static final long serialVersionUID = 3104441031726905760L;

	@Id
	@GeneratedValue
	private Long id;

	private int units;

	private String instument;

	private boolean massTrade;

	private boolean tradingEngine;

	@Convert(converter = IntervalEnumConverter.class)
	private IntervalEnum tradingInterval;

	@ManyToOne
	@JoinColumn(name = "tradingprofile_id")
	private TradingProfileEntity tradingprofile;

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String getInstument() {
		return instument;
	}

	public void setInstument(String instument) {
		this.instument = instument;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TradingProfileEntity getTradingprofile() {
		return tradingprofile;
	}

	public void setTradingprofile(TradingProfileEntity tradingprofile) {
		this.tradingprofile = tradingprofile;
	}

	public IntervalEnum getTradingInterval() {
		return tradingInterval;
	}

	public void setTradingInterval(IntervalEnum tradingInterval) {
		this.tradingInterval = tradingInterval;
	}

	public boolean isMassTrade() {
		return massTrade;
	}

	public void setMassTrade(boolean massTrade) {
		this.massTrade = massTrade;
	}

	public boolean isTradingEngine() {
		return tradingEngine;
	}

	public void setTradingEngine(boolean tradingEngine) {
		this.tradingEngine = tradingEngine;
	}

}
