package rawai.it.com.TradingPlatform.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OandaAccountSummaryResponse extends AccountResponse {

	private OandaAccountSummary account;

	public OandaAccountSummary getAccount() {
		return account;
	}

	public void setAccount(OandaAccountSummary account) {
		this.account = account;
	}
}
