package rawai.it.com.TradingPlatform.trade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeResponse {

	private String lastTransactionID;
	private Trade trade;

	public String getLastTransactionID() {
		return lastTransactionID;
	}

	public void setLastTransactionID(String lastTransactionID) {
		this.lastTransactionID = lastTransactionID;
	}

	public Trade getTrade() {
		return trade;
	}

	public void setTrade(Trade trade) {
		this.trade = trade;
	}
}
