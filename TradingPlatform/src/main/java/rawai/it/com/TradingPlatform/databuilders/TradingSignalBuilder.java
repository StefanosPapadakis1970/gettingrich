package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;

public class TradingSignalBuilder {

	private Long id;

	private int low;

	private int high;

	private int signalvalue;

	private TradingSignalEnum tradingSignal;

	private RangeEnum candleRange;

	private TradingAlgorithm tradingalgorithm;

	private TradingSignalBuilder() {

	}

	public static TradingSignalBuilder aTradingSignalEntityBuilder() {
		return new TradingSignalBuilder();
	}

	public TradingSignalBuilder withTradingAlgorithm(TradingAlgorithm tradingalgorithm) {
		this.tradingalgorithm = tradingalgorithm;
		return this;
	}

	public TradingSignalBuilder whithID(long id) {
		this.id = id;
		return this;
	}

	public TradingSignalBuilder withHigh(int high) {
		this.high = high;
		return this;
	}

	public TradingSignalBuilder withLow(int low) {
		this.low = low;
		return this;
	}

	public TradingSignalBuilder withValue(int value) {
		this.signalvalue = value;
		return this;
	}

	public TradingSignalBuilder withTradingSignal(TradingSignalEnum tradingSignal) {
		this.tradingSignal = tradingSignal;
		return this;
	}

	public TradingSignalBuilder withRange(RangeEnum candleRange) {
		this.candleRange = candleRange;
		return this;
	}

	public TradingSignal build() {
		TradingSignal signal = tradingSignal.createInstanceFromEnum(tradingSignal, signalvalue, candleRange, low, high);
		signal.setTradingalgorithm(tradingalgorithm);
		signal.setTradingSignal(tradingSignal);
		return signal;
	}
}
