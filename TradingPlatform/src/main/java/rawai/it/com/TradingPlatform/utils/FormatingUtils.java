package rawai.it.com.TradingPlatform.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;


public class FormatingUtils {

	// 2017-02-28T04:40:05.000000Z

	private static SimpleDateFormat startStopTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss'Z'");
	private static SimpleDateFormat candletimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH':'mm':'ss'.000000000Z'");

	public static String startStopTimeFormat(Date date) {
		return startStopTimeFormat.format(date);
	}

	public static String startStopTimeFormatDateTime(DateTime date) {
		return startStopTimeFormat.format(date);
	}

	public static String createCandleDateString(Date date){
		return candletimeFormat.format(date);
	}

	public static DateTime parseDate(String date) {
		DateTime resultDate = null;
		try {
			resultDate = new DateTime(candletimeFormat.parse(date));
		} catch (ParseException e) {
			long epoch = Long.parseLong(date.substring(0, date.lastIndexOf(".")));
			return new DateTime(epoch * 1000);
		}
		return resultDate;
	}

	public static String formatAmount(String amount) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		((DecimalFormat) numberFormat).applyPattern("##.00");
		String number = numberFormat.format(Double.valueOf(amount));
		return number;
	}

	public static String formatAmount(double amount) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		((DecimalFormat) numberFormat).applyPattern("##0.00");
		String number = numberFormat.format(amount);
		return number;
	}

	public static String formatAmountCurrency(String amount, String symbol) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		// xx,xxx.xx
		// ((DecimalFormat) numberFormat).applyPattern("##0.00");
		((DecimalFormat) numberFormat).applyPattern("##,##0.00");

		String number = numberFormat.format(Double.valueOf(amount));
		return symbol + number;
	}

	public static String formatAmountCurrency(double amount, String symbol) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		((DecimalFormat) numberFormat).applyPattern("##,##0.00");
		String number = numberFormat.format(amount);
		return symbol + number;
	}

}
