package rawai.it.com.TradingPlatform.tradingprofile;

public enum RunType {

	LIVE_DATA("LIVE DATA"), BACK_TESTING("Back Testing");

	private String name;

	private RunType(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
}
