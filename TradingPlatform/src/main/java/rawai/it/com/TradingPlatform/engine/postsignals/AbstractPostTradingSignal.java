package rawai.it.com.TradingPlatform.engine.postsignals;

import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;

public abstract class AbstractPostTradingSignal implements PostTradingSignal {

	private int value;
	private PostTradingSignalEnum postTradeSignalEnum;
	private Long dbID;
	private boolean dynamic;


	@Override
	public PostTradingSignalEnum getPostTradingSignalEnum() {
		return postTradeSignalEnum;
	}

	@Override
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public void setPostTradeSignalEnum(PostTradingSignalEnum postTradeSignalEnum) {
		this.postTradeSignalEnum = postTradeSignalEnum;
	}

	@Override
	public Long getDBID() {
		return dbID;
	}

	@Override
	public void setDBID(Long dbID) {
		this.dbID = dbID;
	}

	@Override
	public boolean isDynamic() {
		return dynamic;
	}

	@Override
	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}
}
