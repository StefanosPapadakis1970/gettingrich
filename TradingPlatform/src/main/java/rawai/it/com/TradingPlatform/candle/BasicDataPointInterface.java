package rawai.it.com.TradingPlatform.candle;

public interface BasicDataPointInterface {
	String getY();

	Long getX();

	String getTime();
}
