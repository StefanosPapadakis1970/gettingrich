package rawai.it.com.TradingPlatform.config;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SessionListener implements HttpSessionListener {

	private static final Logger log = LogManager.getLogger(SessionListener.class);
 
    @Override
    public void sessionCreated(HttpSessionEvent event) {
		log.info("==== Session is created ====" + event.getSession());
		event.getSession().setMaxInactiveInterval(60 * 60);
    }
 
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
		log.info("==== Session is destroyed ====");
    }
}