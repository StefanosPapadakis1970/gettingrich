package rawai.it.com.TradingPlatform.websocket;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

@Controller("messageController")
@EnableScheduling
public class MessageController {

	@SuppressWarnings("unused")
	private static final Logger log = LogManager.getLogger(MessageController.class);

	@MessageMapping("/chat")
	@SendTo("/topic/messages")
	public OutputMessage send(Message message) throws Exception {
		String time = new SimpleDateFormat("HH:mm").format(new Date());
		return new OutputMessage(message.getFrom(), message.getText(), time);
	}

	@MessageMapping("/fetchData")
	@SendTo("/topic/positions")
	public String sendPositions() throws Exception {
		String time = new SimpleDateFormat("HH:mm").format(new Date());
		return time;
	}
}
