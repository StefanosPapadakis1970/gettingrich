package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class CandleEndpoint extends AbstractTradingSignal {

	private RangeEnum range;

	public CandleEndpoint(RangeEnum range) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.CANDLE_ENDPOINT, 1);
		this.range = range;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface rangeCandle = candles.get(lastSignalCandleIndex);
		int high = Integer.valueOf(rangeCandle.getH().replace(".", ""));
		int low = Integer.valueOf(rangeCandle.getL().replace(".", ""));
		int close = Integer.valueOf(rangeCandle.getC().replace(".", ""));

		if (range.inRange(high, low, close)) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		return range;
	}

}
