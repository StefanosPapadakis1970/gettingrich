package rawai.it.com.TradingPlatform.account;

import java.util.List;

import rawai.it.com.TradingPlatform.order.Order;

public class TradeSummary {

	private String id;
	private String instrument;
	private String price;
	private String openTime;
	private TradeStateEnum state;
	private String initialUnits;
	private String currentUnits;
	private String realizedPL;
	private String unrealizedPL;
	private String averageClosePrice;
	private List<String> closingTransactionIDs;
	private String financing;
	private String closeTime;
	private ClientExtensions clientExtensions;
	private String takeProfitOrderID;
	private String stopLossOrderID;
	private String trailingStopLossOrderID;
	private Order takeProfitOrder;
	private Order stopLossOrder;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public TradeStateEnum getState() {
		return state;
	}

	public void setState(TradeStateEnum state) {
		this.state = state;
	}

	public String getInitialUnits() {
		return initialUnits;
	}

	public void setInitialUnits(String initialUnits) {
		this.initialUnits = initialUnits;
	}

	public String getCurrentUnits() {
		return currentUnits;
	}

	public void setCurrentUnits(String currentUnits) {
		this.currentUnits = currentUnits;
	}

	public String getRealizedPL() {
		return realizedPL;
	}

	public void setRealizedPL(String realizedPL) {
		this.realizedPL = realizedPL;
	}

	public String getUnrealizedPL() {
		return unrealizedPL;
	}

	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public String getAverageClosePrice() {
		return averageClosePrice;
	}

	public void setAverageClosePrice(String averageClosePrice) {
		this.averageClosePrice = averageClosePrice;
	}

	public List<String> getClosingTransactionIDs() {
		return closingTransactionIDs;
	}

	public void setClosingTransactionIDs(List<String> closingTransactionIDs) {
		this.closingTransactionIDs = closingTransactionIDs;
	}

	public String getFinancing() {
		return financing;
	}

	public void setFinancing(String financing) {
		this.financing = financing;
	}

	public String getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

	public String getTakeProfitOrderID() {
		return takeProfitOrderID;
	}

	public void setTakeProfitOrderID(String takeProfitOrderID) {
		this.takeProfitOrderID = takeProfitOrderID;
	}

	public String getStopLossOrderID() {
		return stopLossOrderID;
	}

	public void setStopLossOrderID(String stopLossOrderID) {
		this.stopLossOrderID = stopLossOrderID;
	}

	public String getTrailingStopLossOrderID() {
		return trailingStopLossOrderID;
	}

	public void setTrailingStopLossOrderID(String trailingStopLossOrderID) {
		this.trailingStopLossOrderID = trailingStopLossOrderID;
	}

	public Order getTakeProfitOrder() {
		return takeProfitOrder;
	}

	public void setTakeProfitOrder(Order takeProfitOrder) {
		this.takeProfitOrder = takeProfitOrder;
	}

	public Order getStopLossOrder() {
		return stopLossOrder;
	}

	public void setStopLossOrder(Order stopLossOrder) {
		this.stopLossOrder = stopLossOrder;
	}

}
