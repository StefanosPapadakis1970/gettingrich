package rawai.it.com.TradingPlatform.databuilders;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;
import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;

public class TestDataProvider {

	public static TradingProfileEntity buildTradingProfileEntity(String userID) {
		List<TradingAlgorithmEntity> algorithms = new ArrayList<>();
		List<TradingInstrumentEntity> instruments = new ArrayList<>();
		List<TradingSignalEntity> signals = new ArrayList<>();
		TradingAlgorithmEntityBuilder algorithmBuilder1 = TradingAlgorithmEntityBuilder.aTradingAlgorithmEntityBuilder();
		algorithmBuilder1.withName("AlgorithmTest1");

		TradingSignalEntityBuilder signalBuilder1 = TradingSignalEntityBuilder.aTradingSignalEntityBuilder();
		signalBuilder1.withTradingSignal(TradingSignalEnum.UPWARD_ANY);
		signalBuilder1.withTradingAlgorithm(algorithmBuilder1.build());

		TradingSignalEntityBuilder signalBuilder2 = TradingSignalEntityBuilder.aTradingSignalEntityBuilder();
		signalBuilder2.withTradingSignal(TradingSignalEnum.UPWARD_LESS);
		signalBuilder2.withValue(20);
		signalBuilder2.withTradingAlgorithm(algorithmBuilder1.build());

		TradingSignalEntityBuilder signalBuilder3 = TradingSignalEntityBuilder.aTradingSignalEntityBuilder();
		signalBuilder3.withTradingSignal(TradingSignalEnum.CANDLE_ENDPOINT);
		signalBuilder3.withRange(RangeEnum.TOP_QUARTER);
		signalBuilder3.withTradingAlgorithm(algorithmBuilder1.build());

		signals.add(signalBuilder1.build());
		signals.add(signalBuilder2.build());
		signals.add(signalBuilder3.build());

		TradingProfileEntityBuilder profile = TradingProfileEntityBuilder.aTradingProfileEntityBuilder();
		TradingProfileEntity profileEntity = profile.build();

		algorithmBuilder1.withTradingSignals(signals);
		// algorithmBuilder1.withID(1);
		algorithms.add(algorithmBuilder1.build());

		TradingInstrumentEntityBuilder instrumentBuilder1 = TradingInstrumentEntityBuilder.aTradingInstrumentEntityBuilder();
		instrumentBuilder1.withTradingProfileEntity(profileEntity);
		instrumentBuilder1.withUnits(21);
		instrumentBuilder1.withInstrument("EUR_USD");

		instruments.add(instrumentBuilder1.build());

		profile.withTradingAlgorithms(algorithms);
		profile.withTradingInstruments(instruments);
		profile.withUserID(userID);

		return profile.build();
	}

}
