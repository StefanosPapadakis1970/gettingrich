package rawai.it.com.TradingPlatform.engine.db;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

@Entity
@Table(name = "algorithmtrade")
@NamedQueries({ @NamedQuery(name = AlgorithmTradeEntity.FIND_BY_ID, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.id = :id"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_BY_UUID, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.uuid = :uuid"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_BY_REQUESTID, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.requestID = :requestID"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_BY_USERID_TRADEID, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.userID = :userID and algorithm.tradeID = :tradeID"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_ALL_ENGINETRADES, query = "select algorithm from AlgorithmTradeEntity algorithm where  algorithm.userID != null and algorithm.algorithmID  != null and algorithm.realTrade = true order by algorithm.algorithmID"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_ALL_OPEN_TRADES, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.userID != null and algorithm.algorithmID  != null and algorithm.realTrade = true and algorithm.instrument = :instrument and algorithm.tradingInterval = :tradingInterval and algorithm.finished = false  order by algorithm.algorithmID"),
		@NamedQuery(name = AlgorithmTradeEntity.FIND_ALL_OPEN_TRADES_IGNORE_INTERVAL, query = "select algorithm from AlgorithmTradeEntity algorithm where algorithm.userID != null and algorithm.algorithmID  != null and algorithm.realTrade = true and algorithm.instrument = :instrument and algorithm.finished = false  order by algorithm.algorithmID")})

public class AlgorithmTradeEntity {

	public static final String FIND_BY_ID = "AlgorithmTradeEntity.findByID";
	public static final String FIND_BY_UUID = "AlgorithmTradeEntity.findByUUID";
	public static final String FIND_BY_REQUESTID = "AlgorithmTradeEntity.findByRequestID";
	public static final String FIND_BY_USERID_TRADEID = "AlgorithmTradeEntity.findByTradeUser";
	public static final String FIND_ALL_ENGINETRADES = "AlgorithmTradeEntity.findAllEngineTrades";
	public static final String FIND_ALL_OPEN_TRADES = "AlgorithmTradeEntity.findAllOpenTrades";
	public static final String FIND_ALL_OPEN_TRADES_IGNORE_INTERVAL = "AlgorithmTradeEntity.findAllOpenTradesIgnoreInterval";
	
	

	@Id
	@GeneratedValue
	private Long id;

	private String instrument;

	@Convert(converter = IntervalEnumConverter.class)
	private IntervalEnum tradingInterval;

	private boolean buySell;

	private int tradingCandleIndex = 0;

	private int stopLossCandleIndex = 0;

	private int takeProfitCandleIndex = 0;

	// TODO 21.06 remove algorithmID
	private Long algorithmID;

	private int units;

	private boolean realTrade;

	private Date creationDate;

	private Double price;

	private Double stopLossPrice;

	private Double takeProfitPrice;

	private Double pl;

	private int stopLossPips;

	private int takeProfitPips;

	private boolean success;

	private boolean finished;

	private String uuid;

	private Long stopLossTradeID;

	private Long takeProfitID;

	private String requestID;

	private String tradeID;

	@OneToMany(mappedBy = "algorithmTrade", orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@OrderBy("id")
	private List<CandleEntity> candles;

	private Long accountID;

	private String userID;

	private boolean trade;

	@OneToOne(orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@JoinColumn(name = "takeProfitDBID")
	private AlgorithmTradeEntity takeProfitTrade;

	@OneToOne(orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@JoinColumn(name = "stopLossDBID")
	private AlgorithmTradeEntity stopLossTrade;

	@ManyToOne
	@JoinColumn(name = "testrun_id")
	private TestRunEntity testRun;

	public AlgorithmTradeEntity(String instrument, boolean buySell) {
		super();
		this.instrument = instrument;
		this.buySell = buySell;
	}

	public AlgorithmTradeEntity() {
		// TODO Auto-generated constructor stub
	}

	public boolean isBuySell() {
		return buySell;
	}

	public void setBuySell(boolean buySell) {
		this.buySell = buySell;
	}

	public int getTradingCandleIndex() {
		return tradingCandleIndex;
	}

	public void setTradingCandleIndex(int tradingCandleIndex) {
		this.tradingCandleIndex = tradingCandleIndex;
	}

	public boolean isTrade() {
		return trade;
	}

	public void setTrade(boolean trade) {
		this.trade = trade;
	}


	public List<CandleEntity> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleEntity> candles) {
		this.candles = candles;
	}

	public Long getAccountID() {
		return accountID;
	}

	public void setAccountID(Long accountID) {
		this.accountID = accountID;
	}

	public IntervalEnum getTradingInterval() {
		return tradingInterval;
	}

	public void setTradingInterval(IntervalEnum tradingInterval) {
		this.tradingInterval = tradingInterval;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInstrument() {
		return instrument;
	}

	public Long getAlgorithmID() {
		return algorithmID;
	}

	public void setAlgorithmID(Long algorithmID) {
		this.algorithmID = algorithmID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public boolean isRealTrade() {
		return realTrade;
	}

	public void setRealTrade(boolean realTrade) {
		this.realTrade = realTrade;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public TestRunEntity getTestRun() {
		return testRun;
	}

	public void setTestRun(TestRunEntity testRun) {
		this.testRun = testRun;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Double getStopLossPrice() {
		return stopLossPrice;
	}

	public void setStopLossPrice(Double stopLossPrice) {
		this.stopLossPrice = stopLossPrice;
	}

	public Double getTakeProfitPrice() {
		return takeProfitPrice;
	}

	public void setTakeProfitPrice(Double takeProfitPrice) {
		this.takeProfitPrice = takeProfitPrice;
	}

	public int getStopLossCandleIndex() {
		return stopLossCandleIndex;
	}

	public void setStopLossCandleIndex(int stopLossCandleIndex) {
		this.stopLossCandleIndex = stopLossCandleIndex;
	}

	public int getTakeProfitCandleIndex() {
		return takeProfitCandleIndex;
	}

	public void setTakeProfitCandleIndex(int takeProfitCandleIndex) {
		this.takeProfitCandleIndex = takeProfitCandleIndex;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Long getStopLossTradeID() {
		return stopLossTradeID;
	}

	public void setStopLossTradeID(Long stopLossTradeID) {
		this.stopLossTradeID = stopLossTradeID;
	}

	public Long getTakeProfitID() {
		return takeProfitID;
	}

	public void setTakeProfitID(Long takeProfitID) {
		this.takeProfitID = takeProfitID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public AlgorithmTradeEntity getTakeProfitTrade() {
		return takeProfitTrade;
	}

	public void setTakeProfitTrade(AlgorithmTradeEntity takeProfitTrade) {
		this.takeProfitTrade = takeProfitTrade;
	}

	public AlgorithmTradeEntity getStopLossTrade() {
		return stopLossTrade;
	}

	public void setStopLossTrade(AlgorithmTradeEntity stopLossTrade) {
		this.stopLossTrade = stopLossTrade;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Double getPl() {
		return pl;
	}

	public void setPl(Double pl) {
		this.pl = pl;
	}

}
