package rawai.it.com.TradingPlatform.chart;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AxisX {

	private int interval;
	private String intervalType;
	private String valueFormatString;
	private int labelAngle;

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getIntervalType() {
		return intervalType;
	}

	public void setIntervalType(String intervalType) {
		this.intervalType = intervalType;
	}

	public String getValueFormatString() {
		return valueFormatString;
	}

	public void setValueFormatString(String valueFormatString) {
		this.valueFormatString = valueFormatString;
	}

	public int getLabelAngle() {
		return labelAngle;
	}

	public void setLabelAngle(int labelAngle) {
		this.labelAngle = labelAngle;
	}
}
