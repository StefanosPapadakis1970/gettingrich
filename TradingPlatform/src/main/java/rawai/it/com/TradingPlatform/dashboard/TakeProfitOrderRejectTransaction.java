package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitOrderRejectTransaction extends TakeProfitOrder {

	private String intendedReplacesOrderID;
	private TransactionRejectReason rejectReason;

	public String getIntendedReplacesOrderID() {
		return intendedReplacesOrderID;
	}

	public void setIntendedReplacesOrderID(String intendedReplacesOrderID) {
		this.intendedReplacesOrderID = intendedReplacesOrderID;
	}

	public TransactionRejectReason getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(TransactionRejectReason rejectReason) {
		this.rejectReason = rejectReason;
	}
}
