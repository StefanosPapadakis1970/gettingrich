package rawai.it.com.TradingPlatform.engine.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.algorithmtesting.TestEntity;

@Entity
@Table(name = "testrun")
@NamedQueries({ @NamedQuery(name = TestRunEntity.FIND_BY_ID, query = "select testRun from TestRunEntity testRun where testRun.id = :id") })
public class TestRunEntity {

	public static final String FIND_BY_ID = "TestRunEntity.findByID";

	@Id
	@GeneratedValue
	private Long id;

	private Date executionDate;

	private int totalNumberOfTrades;
	private int sucessFullTrades = 0;
	private int failedTrades = 0;
	private int openTrades = 0;
	private double profitTotal = 0;
	private double lossTotal = 0;
	private double balance = 0;
	private int takeProfitPips = 0;
	private int stopLossPips = 0;

	@ManyToOne
	@JoinColumn(name = "test_id")
	private TestEntity test;

	@OneToMany(mappedBy = "testRun", orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@OrderBy("id")
	private List<AlgorithmTradeEntity> trades = new ArrayList<>();

	@OneToMany(mappedBy = "testRun", orphanRemoval = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@OrderBy("id")
	private List<CandleEntity> candles = new ArrayList<>();

	public List<AlgorithmTradeEntity> getTrades() {
		return trades;
	}

	public void setTrades(List<AlgorithmTradeEntity> trades) {
		this.trades = trades;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public TestEntity getTest() {
		return test;
	}

	public void setTest(TestEntity test) {
		this.test = test;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getTotalNumberOfTrades() {
		return totalNumberOfTrades;
	}

	public void setTotalNumberOfTrades(int totalNumberOfTrades) {
		this.totalNumberOfTrades = totalNumberOfTrades;
	}

	public int getSucessFullTrades() {
		return sucessFullTrades;
	}

	public void setSucessFullTrades(int sucessFullTrades) {
		this.sucessFullTrades = sucessFullTrades;
	}

	public int getFailedTrades() {
		return failedTrades;
	}

	public void setFailedTrades(int failedTrades) {
		this.failedTrades = failedTrades;
	}

	public double getProfitTotal() {
		return profitTotal;
	}

	public void setProfitTotal(double profitTotal) {
		this.profitTotal = profitTotal;
	}

	public double getLossTotal() {
		return lossTotal;
	}

	public void setLossTotal(double lossTotal) {
		this.lossTotal = lossTotal;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getOpenTrades() {
		return openTrades;
	}

	public void setOpenTrades(int openTrades) {
		this.openTrades = openTrades;
	}

	public List<CandleEntity> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleEntity> candles) {
		this.candles = candles;
	}
}
