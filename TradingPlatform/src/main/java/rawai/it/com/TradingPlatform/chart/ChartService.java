package rawai.it.com.TradingPlatform.chart;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.candle.BasicDataPointInterface;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.candle.LineDataPoint;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.engine.db.TestRunEntity;
import rawai.it.com.TradingPlatform.engine.db.TestRunRepository;
import rawai.it.com.TradingPlatform.instrument.CandleService;
import rawai.it.com.TradingPlatform.trade.TradeService;
import rawai.it.com.TradingPlatform.transaction.TransactionService;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.TradingUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oanda.v20.ExecuteException;
import com.oanda.v20.RequestException;
import com.oanda.v20.instrument.Candlestick;
import com.oanda.v20.primitives.DateTime;
import com.oanda.v20.trade.Trade;
import com.oanda.v20.transaction.OrderFillTransaction;

@Service
public class ChartService {

	@Autowired
	private AlgorithmTradeRepository algorithmTradeRepository;
	@Autowired
	private TestRunRepository testRunRepository;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private CandleService candleService;
	
	@Autowired
	private TradeService tradeService;

	private static final Logger log = LogManager.getLogger(ChartService.class);

	public Double callculateOnePip(Double price) {
		String priceString = price.toString();
		int exponent = priceString.indexOf('.') - 6;
		return Math.pow(10, exponent);
	}

	public ModelAndView showChartForTade(Long tradeID, boolean zoomTradeStart) {
		ModelAndView resultView = new ModelAndView("/chart/chartstatic");

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();
		List<BasicDataPointInterface> takeProfitDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> stopLossDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> priceDataPoints = new ArrayList<>();

		AlgorithmTradeEntity tradeEntity = algorithmTradeRepository.findAlgorithmByID(tradeID);
		AlgorithmTrade trade = AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);
		List<Candle> candles = AlgorithmTradeConverter.convertCandlesEntitiesToCandles(tradeEntity.getCandles());

		double pip = callculateOnePip(tradeEntity.getPrice());

		if (!tradeEntity.isBuySell()) {
			pip = pip * -1;
		}

		double takeProfitPrice = tradeEntity.getTakeProfitPrice();
		double stopLossPrice = tradeEntity.getStopLossPrice();
		double price = tradeEntity.getPrice();

		int additionalCandleCounter = 0;
		boolean zoomTradeStartDetected = false;
		boolean endReached = false;

		for (Candle candle : candles) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			candleDataPoint.setCandleIndex(candle.getCandleIndex());
			candleSticks.add(candleDataPoint);

			if (!zoomTradeStart) {
				LineDataPoint takeProfitPoint = new LineDataPoint();
				takeProfitPoint.setLineColor("green");
				takeProfitPoint.setTime(candle.getTime());
				takeProfitPoint.setY(String.valueOf(takeProfitPrice));
				takeProfitDataPoints.add(takeProfitPoint);

				LineDataPoint stopLossPoint = new LineDataPoint();
				stopLossPoint.setLineColor("red");
				stopLossPoint.setTime(candle.getTime());
				stopLossPoint.setY(String.valueOf(stopLossPrice));
				stopLossDataPoints.add(stopLossPoint);
			}


			LineDataPoint pricePoint = new LineDataPoint();
			pricePoint.setLineColor("blue");
			pricePoint.setTime(candle.getTime());
			pricePoint.setY(String.valueOf(price));
			priceDataPoints.add(pricePoint);
			if (zoomTradeStart && candle.getCandleIndex() == trade.getTradingCandleIndex()) {
				zoomTradeStartDetected = true;
			}

			if (zoomTradeStart && zoomTradeStartDetected) {
				additionalCandleCounter++;
			}

			if (zoomTradeStart && additionalCandleCounter == 5) {
				endReached = true;
			}

			if (zoomTradeStart && zoomTradeStartDetected && endReached) {
				break;
			}
		}

		Chart chart = new Chart();
		Title title = new Title(trade.getInstrument().getInstument());
		chart.setTitle(title);

		AxisX axisX = new AxisX();
		axisX.setInterval(20);
		axisX.setIntervalType(trade.getInstrument().getTradingInterval().getIntervalName().toLowerCase());
		axisX.setValueFormatString("hh-mm-ss");
		axisX.setLabelAngle(-85);
		AxisY axisY = new AxisY();
		axisY.setIncludeZero(false);
		axisY.setTitle("Price");
		axisY.setPrefix("$");
		List<ChartData> dataList = new ArrayList<>();
		chart.setData(dataList);
		chart.setAxisY(axisY);
		chart.setAxisX(axisX);

		ChartData<CandleDataPointInterface> candleStickData = new ChartData<>();
		ChartData<BasicDataPointInterface> takeProfit = new ChartData<>();
		ChartData<BasicDataPointInterface> stopLoss = new ChartData<>();
		ChartData<BasicDataPointInterface> priceLine = new ChartData<>();

		dataList.add(takeProfit);
		dataList.add(stopLoss);
		dataList.add(priceLine);
		dataList.add(candleStickData);

		candleStickData.setxValueType("dateTime");
		candleStickData.setType("candlestick");
		candleStickData.setDataPoints(candleSticks);

		takeProfit.setxValueType("dateTime");
		takeProfit.setType("line");
		takeProfit.setDataPoints(takeProfitDataPoints);

		stopLoss.setxValueType("dateTime");
		stopLoss.setType("line");
		stopLoss.setDataPoints(stopLossDataPoints);

		priceLine.setxValueType("dateTime");
		priceLine.setType("line");
		priceLine.setDataPoints(priceDataPoints);

		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			charJSON = mapper.writeValueAsString(chart);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		resultView.addObject("chart", charJSON);
		resultView.addObject("instrument", trade.getInstrument().getInstument());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("userID", trade.getAccount().getUserId());
		resultView.addObject("granularity", trade.getInstrument().getTradingInterval().name());
		resultView.addObject("tradingIndex", trade.getTradingCandleIndex());
		resultView.addObject("success", trade.isSuccess());
		resultView.addObject("price", trade.getPrice());

		String takeProfitString = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getTakeProfitPips(), trade.isBuySell(), true).toString();
		String stopLossString = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getStopLossPips(), trade.isBuySell(), false).toString();
		// resultView.addObject("takeProfitPrice",
		// takeProfitString.subSequence(0, takeProfitString.length() >= 7 ? 7 :
		// takeProfitString.length()));
		// resultView.addObject("stopLossPrice", stopLossString.subSequence(0,
		// stopLossString.length() >= 7 ? 7 : stopLossString.length()));
		resultView.addObject("takeProfitPrice", trade.getTakeProfitPrice());
		resultView.addObject("stopLossPrice", trade.getStopLossPrice());
		resultView.addObject("stopLossCandleIndex", trade.getStopLossCandleIndex());
		resultView.addObject("takeProfitCandleIndex", trade.getTakeProfitCandleIndex());
		return resultView;
	}

	
	public ModelAndView showChartForRealTade(Long tradeID, boolean zoomTradeStart) {
		ModelAndView resultView = new ModelAndView("/chart/chartstatic");

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();
		List<BasicDataPointInterface> takeProfitDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> stopLossDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> priceDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> closeOutPriceDataPoints = new ArrayList<>();

		AlgorithmTradeEntity tradeEntity = algorithmTradeRepository.findAlgorithmByID(tradeID);
		AlgorithmTrade trade = AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);
		
		Trade apiTrade = null;
		
		try {
			apiTrade = tradeService.fetchTradeByID(trade.getAccount(), trade.getTradeID());
		} catch (RequestException e1) {
			log.error("Error", e1);
		} catch (ExecuteException e1) {
			log.error("Error", e1);
		}
	
		Long closeoutOrderID = trade.getCloseOutTradeID();
		
		double closeOutPrice = transactionService.getCloseOutPrice(String.valueOf(closeoutOrderID), trade.getAccount().getUserId());
		
		OrderFillTransaction closeOutOrderFillTransaction = (OrderFillTransaction) transactionService.getCloseOutTransaction(String.valueOf(closeoutOrderID), trade.getAccount().getUserId());
		
		//DateTime closeOutTime = closeOutOrderFillTransaction.getTime();
		
		DateTime closeOutTime = apiTrade.getCloseTime();
		
		DateTime opentTime = apiTrade.getOpenTime();
		
		List<Candlestick> clouseOutCandles = candleService.fetchCloseOutCandles(tradeID, closeoutOrderID);
		
		List<Candle> candles = candleService.convertCandleStickToCandleList(clouseOutCandles);
		
		candleService.markTradingCandle(candles, trade.getTradingCandle().getTime(), trade.getTradingCandleIndex());
		
		Instant closeOutDate = Instant.parse(closeOutTime.toString());
		
		Instant openDate = Instant.parse(opentTime.toString());
		
		
		
		double pip = callculateOnePip(tradeEntity.getPrice());

		if (!tradeEntity.isBuySell()) {
			pip = pip * -1;
		}

		double takeProfitPrice = tradeEntity.getTakeProfitPrice();
		double stopLossPrice = tradeEntity.getStopLossPrice();
		double price = tradeEntity.getPrice();

		int additionalCandleCounter = 0;
		boolean zoomTradeStartDetected = false;
		boolean endReached = false;

		for (Candle candle : candles) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			candleDataPoint.setCandleIndex(candle.getCandleIndex());
			candleSticks.add(candleDataPoint);

			if (!zoomTradeStart) {
				LineDataPoint takeProfitPoint = new LineDataPoint();
				takeProfitPoint.setLineColor("green");
				takeProfitPoint.setTime(candle.getTime());
				takeProfitPoint.setY(String.valueOf(takeProfitPrice));
				takeProfitDataPoints.add(takeProfitPoint);

				LineDataPoint stopLossPoint = new LineDataPoint();
				stopLossPoint.setLineColor("red");
				stopLossPoint.setTime(candle.getTime());
				stopLossPoint.setY(String.valueOf(stopLossPrice));
				stopLossDataPoints.add(stopLossPoint);
			}


			LineDataPoint pricePoint = new LineDataPoint();
			pricePoint.setLineColor("blue");
			pricePoint.setTime(candle.getTime());
			pricePoint.setY(String.valueOf(price));
			priceDataPoints.add(pricePoint);
			

			LineDataPoint closeOutPricePoint = new LineDataPoint();
			closeOutPricePoint.setLineColor("orange");
			closeOutPricePoint.setTime(candle.getTime());
			closeOutPricePoint.setY(String.valueOf(closeOutPrice));
			closeOutPriceDataPoints.add(closeOutPricePoint);

			
			if (zoomTradeStart && candle.getCandleIndex() == trade.getTradingCandleIndex()) {
				zoomTradeStartDetected = true;
			}

			if (zoomTradeStart && zoomTradeStartDetected) {
				additionalCandleCounter++;
			}

			if (zoomTradeStart && additionalCandleCounter == 5) {
				endReached = true;
			}

			if (zoomTradeStart && zoomTradeStartDetected && endReached) {
				break;
			}
		}

		Chart chart = new Chart();
		Title title = new Title(trade.getInstrument().getInstument());
		chart.setTitle(title);

		AxisX axisX = new AxisX();
		axisX.setInterval(trade.getInstrument().getTradingInterval().getIntervalNumberOfElements());
		axisX.setIntervalType(trade.getInstrument().getTradingInterval().getIntervalName().toLowerCase());
		axisX.setValueFormatString("hh-mm-ss");
		axisX.setLabelAngle(-85);
		AxisY axisY = new AxisY();
		axisY.setIncludeZero(false);
		axisY.setTitle("Price");
		axisY.setPrefix("$");
		List<ChartData> dataList = new ArrayList<>();
		chart.setData(dataList);
		chart.setAxisY(axisY);
		chart.setAxisX(axisX);

		ChartData<CandleDataPointInterface> candleStickData = new ChartData<>();
		ChartData<BasicDataPointInterface> takeProfit = new ChartData<>();
		ChartData<BasicDataPointInterface> stopLoss = new ChartData<>();
		ChartData<BasicDataPointInterface> priceLine = new ChartData<>();
		ChartData<BasicDataPointInterface> closeOutPriceLine = new ChartData<>();
		
		dataList.add(takeProfit);
		dataList.add(stopLoss);
		dataList.add(priceLine);
		dataList.add(candleStickData);
		dataList.add(closeOutPriceLine);


		candleStickData.setxValueType("dateTime");
		candleStickData.setType("candlestick");
		candleStickData.setDataPoints(candleSticks);

		takeProfit.setxValueType("dateTime");
		takeProfit.setType("line");
		takeProfit.setDataPoints(takeProfitDataPoints);

		stopLoss.setxValueType("dateTime");
		stopLoss.setType("line");
		stopLoss.setDataPoints(stopLossDataPoints);

		priceLine.setxValueType("dateTime");
		priceLine.setType("line");
		priceLine.setDataPoints(priceDataPoints);
		

		LineDataPoint extenstionPoint = new LineDataPoint();
		
		extenstionPoint.setX(closeOutDate.toEpochMilli());
		extenstionPoint.setY(priceDataPoints.get(0).getY());
		extenstionPoint.setTime(closeOutDate.toString());
		
		
		priceLine.getDataPoints().add(extenstionPoint);
		
		
		closeOutPriceLine.setxValueType("dateTime");
		closeOutPriceLine.setType("line");
		closeOutPriceLine.setDataPoints(closeOutPriceDataPoints);

		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			charJSON = mapper.writeValueAsString(chart);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		resultView.addObject("chart", charJSON);
		resultView.addObject("instrument", trade.getInstrument().getInstument());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("userID", trade.getAccount().getUserId());
		resultView.addObject("granularity", trade.getInstrument().getTradingInterval().name());
		resultView.addObject("tradingIndex", trade.getTradingCandleIndex());
		resultView.addObject("success", trade.isSuccess());
		resultView.addObject("price", trade.getPrice());
		resultView.addObject("closeOutPrice", closeOutPrice);
		resultView.addObject("closeOutTime", closeOutDate.toEpochMilli());
		resultView.addObject("openTime",openDate.toEpochMilli());
		//resultView.addObject("closeOutTime", closeOutDate.toEpochMilli()-25200000);
		

		String takeProfitString = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getTakeProfitPips(), trade.isBuySell(), true).toString();
		String stopLossString = TradingUtils.calculateLossProfitPrice(trade.getPrice(), trade.getStopLossPips(), trade.isBuySell(), false).toString();
		// resultView.addObject("takeProfitPrice",
		// takeProfitString.subSequence(0, takeProfitString.length() >= 7 ? 7 :
		// takeProfitString.length()));
		// resultView.addObject("stopLossPrice", stopLossString.subSequence(0,
		// stopLossString.length() >= 7 ? 7 : stopLossString.length()));
		resultView.addObject("takeProfitPrice", trade.getTakeProfitPrice());
		resultView.addObject("stopLossPrice", trade.getStopLossPrice());
		resultView.addObject("stopLossCandleIndex", trade.getStopLossCandleIndex());
		resultView.addObject("takeProfitCandleIndex", trade.getTakeProfitCandleIndex());
		return resultView;
	}

	
	
	
	public ModelAndView showChartForTestRun(Long testRunID) {
		ModelAndView resultView = new ModelAndView("/chart/charttestrun");

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();
		List<BasicDataPointInterface> takeProfitDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> stopLossDataPoints = new ArrayList<>();
		List<BasicDataPointInterface> priceDataPoints = new ArrayList<>();

		// AlgorithmTradeEntity tradeEntity =
		// algorithmTradeRepository.findAlgorithmByID(tradeID);
		// AlgorithmTrade trade =
		// AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);

		TestRunEntity testRun = testRunRepository.findByIdWithCandles(testRunID);

		List<Candle> candles = AlgorithmTradeConverter.convertCandlesEntitiesToCandles(testRun.getCandles());

		// double pip = callculateOnePip(tradeEntity.getPrice());

		/*
		 * if (!tradeEntity.isBuySell()) { pip = pip * -1; }
		 */

		// double takeProfitPrice = tradeEntity.getTakeProfitPrice();
		// double stopLossPrice = tradeEntity.getStopLossPrice();
		// double price = tradeEntity.getPrice();

		int additionalCandleCounter = 0;
		boolean zoomTradeStartDetected = false;
		boolean endReached = false;

		for (Candle candle : candles) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			candleDataPoint.setCandleIndex(candle.getCandleIndex());
			candleSticks.add(candleDataPoint);

			/*
			 * if (!zoomTradeStart) { LineDataPoint takeProfitPoint = new
			 * LineDataPoint(); takeProfitPoint.setLineColor("green");
			 * takeProfitPoint.setTime(candle.getTime());
			 * takeProfitPoint.setY(String.valueOf(takeProfitPrice));
			 * takeProfitDataPoints.add(takeProfitPoint);
			 * 
			 * LineDataPoint stopLossPoint = new LineDataPoint();
			 * stopLossPoint.setLineColor("red");
			 * stopLossPoint.setTime(candle.getTime());
			 * stopLossPoint.setY(String.valueOf(stopLossPrice));
			 * stopLossDataPoints.add(stopLossPoint); }
			 */

			/*
			 * LineDataPoint pricePoint = new LineDataPoint();
			 * pricePoint.setLineColor("blue");
			 * pricePoint.setTime(candle.getTime());
			 * pricePoint.setY(String.valueOf(price));
			 * priceDataPoints.add(pricePoint); if (zoomTradeStart &&
			 * candle.getCandleIndex() == trade.getTradingCandleIndex()) {
			 * zoomTradeStartDetected = true; }
			 * 
			 * if (zoomTradeStart && zoomTradeStartDetected) {
			 * additionalCandleCounter++; }
			 * 
			 * if (zoomTradeStart && additionalCandleCounter == 5) { endReached
			 * = true; }
			 * 
			 * if (zoomTradeStart && zoomTradeStartDetected && endReached) {
			 * break; }
			 */
		}

		String instrument = testRun.getTrades().get(0).getInstrument();

		Chart chart = new Chart();
		Title title = new Title(instrument);
		chart.setTitle(title);


		AxisX axisX = new AxisX();
		axisX.setInterval(20);
		axisX.setIntervalType(testRun.getTrades().get(0).getTradingInterval().getIntervalName().toLowerCase());
		axisX.setValueFormatString("hh-mm-ss");
		axisX.setLabelAngle(-85);
		AxisY axisY = new AxisY();
		axisY.setIncludeZero(false);
		axisY.setTitle("Price");
		axisY.setPrefix("$");
		List<ChartData> dataList = new ArrayList<>();
		chart.setData(dataList);
		chart.setAxisY(axisY);
		chart.setAxisX(axisX);

		ChartData<CandleDataPointInterface> candleStickData = new ChartData<>();
		ChartData<BasicDataPointInterface> takeProfit = new ChartData<>();
		ChartData<BasicDataPointInterface> stopLoss = new ChartData<>();
		ChartData<BasicDataPointInterface> priceLine = new ChartData<>();

		dataList.add(takeProfit);
		dataList.add(stopLoss);
		dataList.add(priceLine);
		dataList.add(candleStickData);

		candleStickData.setxValueType("dateTime");
		candleStickData.setType("candlestick");
		candleStickData.setDataPoints(candleSticks);

		takeProfit.setxValueType("dateTime");
		takeProfit.setType("line");
		takeProfit.setDataPoints(takeProfitDataPoints);

		stopLoss.setxValueType("dateTime");
		stopLoss.setType("line");
		stopLoss.setDataPoints(stopLossDataPoints);

		priceLine.setxValueType("dateTime");
		priceLine.setType("line");
		priceLine.setDataPoints(priceDataPoints);

		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			charJSON = mapper.writeValueAsString(chart);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		resultView.addObject("chart", charJSON);
		resultView.addObject("instrument", instrument);
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("userID", "");
		resultView.addObject("granularity", "");
		resultView.addObject("tradingIndex", "");
		resultView.addObject("success", "");
		resultView.addObject("price", "");

		// String takeProfitString =
		// TradingUtils.calculateLossProfitPrice(trade.getPrice(),
		// trade.getTakeProfitPips(), trade.isBuySell(), true).toString();
		// String stopLossString =
		// TradingUtils.calculateLossProfitPrice(trade.getPrice(),
		// trade.getStopLossPips(), trade.isBuySell(), false).toString();
		// resultView.addObject("takeProfitPrice",
		// takeProfitString.subSequence(0, takeProfitString.length() >= 7 ? 7 :
		// takeProfitString.length()));
		// resultView.addObject("stopLossPrice", stopLossString.subSequence(0,
		// stopLossString.length() >= 7 ? 7 : stopLossString.length()));
		resultView.addObject("takeProfitPrice", "");
		resultView.addObject("stopLossPrice", "");
		resultView.addObject("stopLossCandleIndex", "");
		resultView.addObject("takeProfitCandleIndex", "");
		return resultView;
	}
}
