package rawai.it.com.TradingPlatform.signup;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.account.Account;

public class SignupForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
	private static final String EMAIL_MESSAGE = "{email.message}";

//    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
//	@Email(message = SignupForm.EMAIL_MESSAGE)
//	private String email;
//
//    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
//	private String password;

	@NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String apiKey;

	@NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String userName;

	@NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	@Pattern(regexp = "[0-9]{1,10}-[0-9]{1,10}-[0-9]{1,10}-[0-9]{1,10}", message = "Not a valid UserID: '101-011-5094777-001'")
	private String userId;

	private String dbID = "0";

//    public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public static String getNotBlankMessage() {
		return NOT_BLANK_MESSAGE;
	}

	public Account createAccount() {
		return new Account("ROLE_USER", getApiKey(), getUserName(), getUserId());
	}

	public Account createAccountUpdate() {
		return new Account("ROLE_USER", getApiKey(), getUserName(), getUserId(), dbID);
	}

	public String getDbID() {
		return dbID;
	}

	public void setDbID(String dbID) {
		this.dbID = dbID;
	}
}
