package rawai.it.com.TradingPlatform.engine.signals;

import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.postsignals.DynamicStopLossPostTradingSignal;
import rawai.it.com.TradingPlatform.engine.postsignals.PostTradingSignal;
import rawai.it.com.TradingPlatform.engine.postsignals.StopLossPostTradingSignal;
import rawai.it.com.TradingPlatform.engine.postsignals.TakeProfitPostTradingSignal;

public enum PostTradingSignalEnum {

	STOPLOSS {
		@Override
		public PostTradingSignal getInstance(int value) {
			return new StopLossPostTradingSignal(value);
		}

		@Override
		public String getDescription(PostTradeSignalEntity signal) {
			return signal.getTradingSignal().name() + " " + signal.getValue();
		}
	},
	TAKEPROFIT {
		@Override
		public PostTradingSignal getInstance(int value) {
			return new TakeProfitPostTradingSignal(value);
		}

		@Override
		public String getDescription(PostTradeSignalEntity signal) {
			return signal.getTradingSignal().name() + " " + signal.getValue();
		}
	},
	DYNAMIC_STOPLOSS {
		@Override
		public PostTradingSignal getInstance(int value) {
			return new DynamicStopLossPostTradingSignal(value);
		}

		@Override
		public String getDescription(PostTradeSignalEntity signal) {
			return signal.getTradingSignal().name() + " " + signal.getValue();
		}
	};

	public abstract PostTradingSignal getInstance(int value);

	public abstract String getDescription(PostTradeSignalEntity signal);

	public static PostTradingSignal createInstance(PostTradeSignalEntity postTradingSignalEntity) {
		PostTradingSignalEnum postTradingSignalEnum = postTradingSignalEntity.getTradingSignal();
		PostTradingSignal postTradingSignal = postTradingSignalEnum.getInstance(postTradingSignalEntity.getValue());
		return postTradingSignal;
	}
}
