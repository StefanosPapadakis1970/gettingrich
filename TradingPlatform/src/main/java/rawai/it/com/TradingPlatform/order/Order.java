package rawai.it.com.TradingPlatform.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import rawai.it.com.TradingPlatform.DTO.InstrumentDTO;
import rawai.it.com.TradingPlatform.dashboard.ClientExtensions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	// @JsonIgnore
	// @NotBlank(message = Order.NOT_BLANK_MESSAGE)
	private String userID;
	private String userName;
	// @NotBlank(message = Order.NOT_BLANK_MESSAGE)
	// @Pattern(regexp = "^[1-9][0-9]*", message = "Not a valid number (>0)")
	private String units;
	// @NotBlank(message = Order.NOT_BLANK_MESSAGE)
	private String instrument;
	private String timeInForce;
	private String type;
	private String positionFill;
	private String tradeID;
	private String price;
	private String id;
	private String interval;
	private String uuid;

	@JsonIgnore
	private boolean buy = true;

	private ClientExtensions clientExtensions;

	@Valid
	private List<InstrumentDTO> instruments = new ArrayList<>();


	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPositionFill() {
		return positionFill;
	}

	public void setPositionFill(String positionFill) {
		this.positionFill = positionFill;
	}


	public boolean isBuy() {
		return buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<InstrumentDTO> getInstruments() {
		return instruments;
	}

	public void setInstruments(List<InstrumentDTO> instruments) {
		this.instruments = instruments;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
