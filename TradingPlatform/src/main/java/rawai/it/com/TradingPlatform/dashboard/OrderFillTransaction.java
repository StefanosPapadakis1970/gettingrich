package rawai.it.com.TradingPlatform.dashboard;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderFillTransaction extends Transaction {

	private TransactionType type;
	private String orderID;
	private String clientOrderID;
	private String instrument;
	private String units;
	private String price;
	private OrderFillReason reason;
	private String pl;
	private String financing;
	private String accountBalance;
	private TradeOpen tradeOpened;
	private TradeReduce tradeReduced;
	private List<TradeReduce> tradesClosed;

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getClientOrderID() {
		return clientOrderID;
	}

	public void setClientOrderID(String clientOrderID) {
		this.clientOrderID = clientOrderID;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public OrderFillReason getReason() {
		return reason;
	}

	public void setReason(OrderFillReason reason) {
		this.reason = reason;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getFinancing() {
		return financing;
	}

	public void setFinancing(String financing) {
		this.financing = financing;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public TradeOpen getTradeOpened() {
		return tradeOpened;
	}

	public void setTradeOpened(TradeOpen tradeOpened) {
		this.tradeOpened = tradeOpened;
	}

	public List<TradeReduce> getTradesClosed() {
		return tradesClosed;
	}

	public void setTradesClosed(List<TradeReduce> tradesClosed) {
		this.tradesClosed = tradesClosed;
	}

	public TradeReduce getTradeReduced() {
		return tradeReduced;
	}

	public void setTradeReduced(TradeReduce tradeReduced) {
		this.tradeReduced = tradeReduced;
	}
}
