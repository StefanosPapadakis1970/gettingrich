package rawai.it.com.TradingPlatform.instrument;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleListResponse;
import rawai.it.com.TradingPlatform.candle.CandleTimeValidator;
import rawai.it.com.TradingPlatform.price.Price;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@Service
public class InstrumentService {

	private static final long MAX_RESULTS = 4000l;

	private static final Logger logger = LogManager.getLogger(InstrumentService.class);

	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	private AccountRepository accountRepository;

	public List<Candle> fetchCandlesFromTo(String userID, String instrument, String granularity, Date startDate, Date endDate) {

		CandleTimeValidator candleTimeValidator = new CandleTimeValidator();

		Calendar gc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		gc.setTime(startDate);

		DateTime startTime = new DateTime(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DAY_OF_MONTH), 0, 0, 0);

		gc.setTime(endDate);

		DateTime endTime = new DateTime(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DAY_OF_MONTH), 23, 59, 59);

		Seconds seconds = Seconds.secondsBetween(startTime, endTime);

		Double numberOfCandles = new Double(seconds.getSeconds()) / new Double(IntervalEnum.valueOf(granularity).toSeconds());

		List<TimeRange> timeRanges = new ArrayList<>();
		timeRanges.add(new TimeRange(startTime.toDate(), endTime.toDate()));

		if (numberOfCandles > MAX_RESULTS) {
			timeRanges.clear();
			timeRanges = createTimeRanges(startTime, endTime, IntervalEnum.valueOf(granularity).toSeconds());
		}



		if (SessionUtils.getAccounts().isEmpty()) {
			SessionUtils.setAccounts(accountRepository.findAll());
		}

		Account account = SessionUtils.findAccountByUserId(userID);
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CandleListResponse> resp = null;
		// Zeitzone


		List<Candle> resultCandles = new ArrayList<>();

		for (TimeRange timeRange : timeRanges) {
			String start = FormatingUtils.createCandleDateString(timeRange.getStartDate());
			String end = FormatingUtils.createCandleDateString(timeRange.getEndDate());

			try {
				logger.info(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity + "&price=ABM&from=" + start + "&to=" + end);
				resp = restTemplate.exchange(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity + "&price=ABM&from=" + start + "&to=" + end,
						HttpMethod.GET, apiHeader, CandleListResponse.class);
				List<Candle> candlesToAppend = resp.getBody().getCandles();

				if (resultCandles.size() > 0 && candlesToAppend.size() > 0 && resultCandles.get(resultCandles.size() - 1).getTime().equals(candlesToAppend.get(0).getTime())) {
					candlesToAppend.remove(0);
					logger.info("Candle removed!!");
				}
				candlesToAppend = candleTimeValidator.extractValidTimeCandles(candlesToAppend);
				resultCandles.addAll(candlesToAppend);
			} catch (HttpClientErrorException e1) {
				logger.error(e1.getResponseBodyAsString());
				e1.printStackTrace();
			}

		}

		logger.info("#########################################################");
		logger.info("#########################################################");
		List<DateTime> days = getDaysForTimeRange(startTime, endTime);
		setFirstCandleOfTheDay(resultCandles, days);
		for (Candle candle : resultCandles) {
			logger.info(candle.getTime() + " " + candle.isFirstCandleOfTheDay());
		}
		logger.info("#########################################################");
		logger.info("#########################################################");
		return resultCandles;
	}

	private List<Candle> setFirstCandleOfTheDay(List<Candle> candles, List<DateTime> days){
		ArrayList<DateTime> daysProcessed = new ArrayList<>();
		for(Candle candle : candles){
			for(DateTime day : days){
				DateTime candleTime = new DateTime(candle.getTime(), DateTimeZone.UTC);
				if (candleTime.getYear() == day.getYear() && candleTime.getMonthOfYear() == day.getMonthOfYear() && candleTime.getDayOfMonth() == day.getDayOfMonth()
						&& !daysProcessed.contains(day)) {
					daysProcessed.add(day);
					logger.info("First Candle o11f the Day " + candle.getTime() + " " + candleTime);
					candle.setFirstCandleOfTheDay(true);
					break;
				}
			}		
		}
		return candles;
	}

	private List<DateTime> getDaysForTimeRange(DateTime start, DateTime stop) {
		int days = Days.daysBetween(start, stop).getDays();

		List<DateTime> dates = new ArrayList<DateTime>(days);
		for (int i = 0; i < days; i++) {
			dates.add(start.withFieldAdded(DurationFieldType.days(), i));
		}
		dates.add(stop);
		return dates;
	}

	public List<TimeRange> createTimeRanges(DateTime startTime, DateTime endTime, int interValSeconds) {

		List<TimeRange> timeRanges = new ArrayList<>();

		Seconds seconds = Seconds.secondsBetween(startTime, endTime);
		int totalNumberOfSeconds = seconds.getSeconds();

		logger.info("Total Number of seconds between  start and end " + totalNumberOfSeconds);

		Double numberOfRanges = new Double(totalNumberOfSeconds) / new Double(interValSeconds * MAX_RESULTS);


		for (int i = 0; i < numberOfRanges; i++) {

			long startMillis = startTime.getMillis() + ((long) interValSeconds * 1000l * MAX_RESULTS * (long) i);
			DateTime startTimeTest = new DateTime(startMillis, DateTimeZone.UTC);
			logger.info("Callcuated Start Millies " + startMillis + " " + startTimeTest);

			Date startDate = new Date();
			startDate.setTime(startMillis);

			Date endDate = new Date();
			endDate.setTime(startMillis + (interValSeconds * 1000 * MAX_RESULTS));

			timeRanges.add(new TimeRange(startDate, endDate));
		}

		timeRanges.get(timeRanges.size() - 1).setEndDate(new Date(endTime.getMillis()));

		for (TimeRange timeRange : timeRanges) {
			logger.info("TimeRange Start->   " + timeRange.getStartDate() + "  End ->" + timeRange.getEndDate());
		}

		return timeRanges;
	}

	public List<Candle> fetchCandlesWithDate(String instrument, String granularity, Date startDate) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		Account account = SessionUtils.findAccountByUserName(username);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CandleListResponse> resp = null;
		try {
			logger.info(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity);
			resp = restTemplate.exchange(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?count=5&granularity=" + granularity, HttpMethod.GET, apiHeader,
					CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
		}
		return resp.getBody().getCandles();
	}

	public void setupInstruments(Account account) {
		RestTemplate tmpl = new RestTemplate();
		List<Instrument> instruments;
		if (SessionUtils.getInstrumentMap().get(account.getUserId()) == null) {
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
			logger.info("#######################################");
			logger.info(curlUtil.createAccountUrl() + "/" + account.getUserId() + "/instruments");
			logger.info("#######################################");
			ResponseEntity<InstrumentListResponse> resp = null;

			try {
			resp = tmpl.exchange(curlUtil.createAccountUrl() + "/" + account.getUserId() + "/instruments", HttpMethod.GET, apiHeader,
					InstrumentListResponse.class);
			} catch(HttpClientErrorException clientError) {
				logger.error("API-Access-Problem", clientError);
				System.exit(1);
			}
	
			instruments = resp.getBody().getInstruments();
			Collections.sort(instruments, new Comparator<Instrument>() {
	
				public int compare(Instrument o1, Instrument o2) {
					return o1.getDisplayName().compareTo(o2.getDisplayName());
				}
			});
	
			SessionUtils.getInstrumentMap().put(account.getUserId(), instruments);
		}
	}

	public void setupInstruments() {
		if (SessionUtils.getAccounts().isEmpty()) {
			SessionUtils.setAccounts(accountRepository.findAll());
		}
		
		for (Account account : SessionUtils.getAccounts()) {
			setupInstruments(account);
		}
	}

	@PostConstruct
	public void startUpTransactionService() {
		setupInstruments();
	}

}
