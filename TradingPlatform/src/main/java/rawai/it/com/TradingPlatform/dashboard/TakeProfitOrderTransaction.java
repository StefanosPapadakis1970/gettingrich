package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TakeProfitOrderTransaction extends TakeProfitOrder {

	private String replacesOrderID;
	private String cancellingTransactionID;

	public String getReplacesOrderID() {
		return replacesOrderID;
	}

	public void setReplacesOrderID(String replacesOrderID) {
		this.replacesOrderID = replacesOrderID;
	}

	public String getCancellingTransactionID() {
		return cancellingTransactionID;
	}

	public void setCancellingTransactionID(String cancellingTransactionID) {
		this.cancellingTransactionID = cancellingTransactionID;
	}

}
