package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketOrderTradeClose {

	private String tradeID;
	private String clientTradeID;
	private String units;

	public String getTradeID() {
		return tradeID;
	}

	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}

	public String getClientTradeID() {
		return clientTradeID;
	}

	public void setClientTradeID(String clientTradeID) {
		this.clientTradeID = clientTradeID;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}
}
