package rawai.it.com.TradingPlatform.chart;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;

public class ChartCandleData {

	private String type;
	private String xValueType;
	private List<CandleDataPointInterface> dataPoints;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<CandleDataPointInterface> getDataPoints() {
		return dataPoints;
	}

	public void setDataPoints(List<CandleDataPointInterface> dataPoints) {
		this.dataPoints = dataPoints;
	}

	public String getxValueType() {
		return xValueType;
	}

	public void setxValueType(String xValueType) {
		this.xValueType = xValueType;
	}
}
