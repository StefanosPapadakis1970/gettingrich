package rawai.it.com.TradingPlatform.engine.signals;

public enum BUYSELL {
	BUY(true), SELL(false), NEUTRAL(true);

	private boolean buySell;

	private BUYSELL(boolean buySell) {
		this.buySell = buySell;
	}

	public boolean isBuySell() {
		return buySell;
	}
}
