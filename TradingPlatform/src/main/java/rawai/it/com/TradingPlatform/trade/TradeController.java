package rawai.it.com.TradingPlatform.trade;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class TradeController {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private CurlUtil curlUtil;


	@RequestMapping(value = "trade/closeTrade", method = RequestMethod.GET)
	@ResponseBody
	public String closeTrade(@RequestParam("tradeId") String tradeId, @RequestParam("userID") String userID, HttpServletRequest request) {
		RestTemplate tmpl = new RestTemplate();
		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountRepository.findAll());
		}
		Account account = SessionUtils.findAccountByUserId(userID);

		String url = curlUtil.createAccountUrl() + "/" + userID + "/trades/" + tradeId + "/close";
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<TradeResponse> resp = tmpl.exchange(url, HttpMethod.PUT, apiHeader, TradeResponse.class);
		if (resp.getStatusCode() == HttpStatus.OK && resp.hasBody()) {
			return "OK";
		}
		return "ERROR";
	}
}
