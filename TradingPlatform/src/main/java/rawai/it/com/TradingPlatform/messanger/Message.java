package rawai.it.com.TradingPlatform.messanger;

import java.util.List;

public class Message {

	private String header = "";
	private String message = "";
	private String type = MessageType.TYPE_PRIMARY.getType();

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMessage(List<String> messages) {
		this.message = "";
		for (String message : messages) {
			this.message += message + "<br>";
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
