package rawai.it.com.TradingPlatform.trade;

import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.oanda.v20.Context;
import com.oanda.v20.ContextBuilder;
import com.oanda.v20.ExecuteException;
import com.oanda.v20.RequestException;
import com.oanda.v20.account.AccountID;
import com.oanda.v20.order.LimitOrderRequest;
import com.oanda.v20.order.MarketOrderRequest;
import com.oanda.v20.order.OrderCreateRequest;
import com.oanda.v20.order.OrderCreateResponse;
import com.oanda.v20.order.OrderRequest;
import com.oanda.v20.trade.TradeID;
import com.oanda.v20.trade.TradeSpecifier;
import com.oanda.v20.transaction.ClientExtensions;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.price.Price;
import rawai.it.com.TradingPlatform.price.PriceResponseList;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Service
public class TradeService {
	
	private static final Logger logger = LogManager.getLogger(TradeService.class);


	@Autowired
	private CurlUtil curlUtil;
	
	public boolean hasOpenTrade(String userID, String instrument) throws HttpClientErrorException {
		String url = curlUtil.PRAXIS_URL + "v3/accounts/"+userID+"/openTrades";
		RestTemplate tmpl = new RestTemplate();
		String apiKey = SessionUtils.findAccountByUserId(userID).getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);		
		ResponseEntity<TradeListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, TradeListResponse.class);
		Stream<Trade> tradeStream = resp.getBody().getTrades().stream().filter(new Predicate<Trade>() {

			@Override
			public boolean test(Trade t) {
                return t.getInstrument().equals(instrument);
			}
		});
		
		return tradeStream.toArray().length > 0;
	}
	
	
	public com.oanda.v20.trade.Trade fetchTradeByID(Account account, String tradeID) throws RequestException, ExecuteException {
		
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		return ctx.trade.get(new AccountID(account.getUserId()), new TradeSpecifier(new TradeID(tradeID))).getTrade();

	}
}
