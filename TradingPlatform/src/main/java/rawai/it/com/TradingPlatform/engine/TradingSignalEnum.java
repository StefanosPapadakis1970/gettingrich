package rawai.it.com.TradingPlatform.engine;

import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;
import rawai.it.com.TradingPlatform.engine.signals.CandleEndpoint;
import rawai.it.com.TradingPlatform.engine.signals.CandleRangeBetween;
import rawai.it.com.TradingPlatform.engine.signals.CandleRangeLessThan;
import rawai.it.com.TradingPlatform.engine.signals.CandleRangeMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.DeltaAny;
import rawai.it.com.TradingPlatform.engine.signals.DeltaBetween;
import rawai.it.com.TradingPlatform.engine.signals.DeltaLessThan;
import rawai.it.com.TradingPlatform.engine.signals.DeltaMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.DownwardDeltaAny;
import rawai.it.com.TradingPlatform.engine.signals.DownwardDeltaBetween;
import rawai.it.com.TradingPlatform.engine.signals.DownwardDeltaLessThan;
import rawai.it.com.TradingPlatform.engine.signals.DownwardDeltaMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.DownwardsInversionAny;
import rawai.it.com.TradingPlatform.engine.signals.DownwardsInversionBetween;
import rawai.it.com.TradingPlatform.engine.signals.DownwardsInversionLessThan;
import rawai.it.com.TradingPlatform.engine.signals.DownwardsInversionMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.GreaterThanBefore;
import rawai.it.com.TradingPlatform.engine.signals.HammerAny;
import rawai.it.com.TradingPlatform.engine.signals.LessThanBefore;
import rawai.it.com.TradingPlatform.engine.signals.NegativeCandleRangeOpenCloseBetween;
import rawai.it.com.TradingPlatform.engine.signals.NegativeCandleRangeOpenCloseLessThan;
import rawai.it.com.TradingPlatform.engine.signals.NegativeCandleRangeOpenCloseMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.PositiveCandleRangeOpenCloseBetween;
import rawai.it.com.TradingPlatform.engine.signals.PositiveCandleRangeOpenCloseLessThan;
import rawai.it.com.TradingPlatform.engine.signals.PositiveCandleRangeOpenCloseMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.engine.signals.UpwardDeltaAny;
import rawai.it.com.TradingPlatform.engine.signals.UpwardDeltaBetween;
import rawai.it.com.TradingPlatform.engine.signals.UpwardDeltaMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.UpwardsInversionAny;
import rawai.it.com.TradingPlatform.engine.signals.UpwardsInversionBetween;
import rawai.it.com.TradingPlatform.engine.signals.UpwardsInversionLessThan;
import rawai.it.com.TradingPlatform.engine.signals.UpwardsInversionMoreThan;
import rawai.it.com.TradingPlatform.engine.signals.WaitState;

public enum TradingSignalEnum {

	DELTA_LESS_THAN("DELTA_LESS_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DeltaLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DELTA_MORE_THAN("DELTA_MORE_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DeltaMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DELTA_BETWEEN("DELTA_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new DeltaBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	UPWARD_ANY("UPWARD_INVERSION_ANY") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return new UpwardsInversionAny();
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	UPWARD_BETWEEN("UPWARD_INVERSION_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {

			return new UpwardsInversionBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	UPWARD_LESS("UPWARD_INVERSION_LESS") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new UpwardsInversionLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	UPWARD_MORE("UPWARD_INVERSION_MORE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new UpwardsInversionMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DOWNWARD_ANY("DOWNWARD_INVERSION_ANY") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return new DownwardsInversionAny();
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DOWNWARD_BETWEEN("DOWNWARD_INVERSION_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new DownwardsInversionBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DOWNWARD_LESS("DOWNWARD_INVERSION_LESS") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DownwardsInversionLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	DOWNWARD_MORE("DOWNWARD_INVERSION_MORE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DownwardsInversionMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	CANDLERANGE_LESS("CANDLERANGE_LESS") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new CandleRangeLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	CANDLERANGE_MORE("CANDLERANGE_MORE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new CandleRangeMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	CANDLERANGE_BETWEEN("CANDLERANGE_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new CandleRangeBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	POSITIVE_CANDLERANGE_BETWEEN_OC("POSITIVE_CANDLERANGE_BETWEEN_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new PositiveCandleRangeOpenCloseBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub			
		}
	},
	POSITIVE_CANDLERANGE_LESS_OC("POSITIVE_CANDLERANGE_LESS_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new PositiveCandleRangeOpenCloseLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	POSITIVE_CANDLERANGE_MORE_OC("POSITIVE_CANDLERANGE_MORE_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new PositiveCandleRangeOpenCloseMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	NEGATIVE_CANDLERANGE_BETWEEN_OC("NEGATIVE_CANDLERANGE_BETWEEN_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new NegativeCandleRangeOpenCloseBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub			
		}
	},
	NEGATIVE_CANDLERANGE_LESS_OC("NEGATIVE_CANDLERANGE_LESS_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new NegativeCandleRangeOpenCloseLessThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	NEGATIVE_CANDLERANGE_MORE_OC("NEGATIVE_CANDLERANGE_MORE_OC") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new NegativeCandleRangeOpenCloseMoreThan(value);
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub
			
		}
	},
	CANDLE_ENDPOINT("CANDLERANGE_ENDPOINT") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return new CandleEndpoint(range);
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getCandleRange().getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {

		}
	},
	DELTA_ANY("DELTA_ANY") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return new DeltaAny();
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {

		}
	},
	UPWARD_DELTA_ANY("UPWARD_DELTA_ANY") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return new UpwardDeltaAny();
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {

		}
	},
	UPWARD_DELTA_BETWEEN("UPWARD_DELTA_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new UpwardDeltaBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	},
	UPWARD_DELTA_LESS_THAN("UPWARD_DELTA_LESS_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new UpwardsInversionLessThan(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {

		}
	},
	UPWARD_DELTA_MORE_THAN("UPWARD_DELTA_MORE_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new UpwardDeltaMoreThan(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {

		}
	},
	DOWNWARD_DELTA_ANY("DOWNWARD_DELTA_ANY") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return new DownwardDeltaAny();
		}

		@Override
		public TradingSignal getInstance() {
			return new DownwardDeltaAny();
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	},
	DOWNWARD_DELTA_BETWEEN("DOWNWARD_DELTA_BETWEEN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return new DownwardDeltaBetween(lower, upper);
		}

		@Override
		public TradingSignal getInstance(int value) {
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getLow() + " " + signal.getHigh();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	}, DOWNWARD_DELTA_LESS_THAN("DOWNWARD_DELTA_LESS_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DownwardDeltaLessThan(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	},
	DOWNWARD_DELTA_MORE_THAN("DOWNWARD_DELTA_MORE_THAN") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new DownwardDeltaMoreThan(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub

		}
	},
	GREATER_THAN_BEFORE("GREATER_THAN_BEFORE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new GreaterThanBefore(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub

		}
	},
	LESS_THAN_BEFORE("LESS_THAN_BEFORE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			return new LessThanBefore(value);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + signal.getSignalvalue();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
			// TODO Auto-generated method stub

		}
	},
	HAMMER("HAMMER") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {			
			return new HammerAny(value > 0);
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return null;
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName() + " " + (signal.getSignalvalue() > 0 ? "UP" : "DOWN");
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	},
	WAIT_STATE("WAIT_STATE") {
		@Override
		public TradingSignal getInstance(int lower, int upper) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(int value) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance(RangeEnum range) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TradingSignal getInstance() {
			return new WaitState();
		}

		@Override
		public String getDescription(TradingSignalEntity signal) {
			return this.getName();
		}

		@Override
		public void accept(TradingSignalVisitor visitor) {
		}
	};

	private String name;

	public abstract TradingSignal getInstance(int lower, int upper);

	public abstract TradingSignal getInstance(int value);

	public abstract TradingSignal getInstance(RangeEnum range);

	public abstract TradingSignal getInstance();

	public abstract String getDescription(TradingSignalEntity signal);

	public abstract void accept(TradingSignalVisitor visitor);

	public interface TradingSignalVisitor {
		void visitDescription(TradingSignalEnum signalEnum);

		String createDescription(String description);
	}


	private TradingSignalEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public static TradingSignalEnum getValue(String name) {
		for (TradingSignalEnum signal : TradingSignalEnum.values()) {
			if (signal.name.equals(name)) {
				return signal;
			}
		}
		return null;
	}

	public static TradingSignal createInstance(TradingSignalEntity tradingSignalEntity) {
		TradingSignalEnum signal = tradingSignalEntity.getTradingSignal();
		TradingSignal resultSignal;


		resultSignal = signal.getInstance();
		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(tradingSignalEntity.getSignalvalue());

		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(tradingSignalEntity.getCandleRange());
		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(tradingSignalEntity.getLow(), tradingSignalEntity.getHigh());
		return resultSignal;
	}

	public static TradingSignal createInstanceFromEnum(TradingSignalEnum tradingSignal, int singnalValue, RangeEnum candleRange, int low, int high) {
		TradingSignalEnum signal = tradingSignal;
		TradingSignal resultSignal;

		resultSignal = signal.getInstance();
		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(singnalValue);

		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(candleRange);
		if (resultSignal != null) {
			return resultSignal;
		}
		resultSignal = signal.getInstance(low, high);
		return resultSignal;
	}
}
