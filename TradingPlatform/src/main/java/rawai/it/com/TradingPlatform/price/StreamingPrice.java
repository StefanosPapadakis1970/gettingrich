package rawai.it.com.TradingPlatform.price;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StreamingPrice  implements CandleDataPointInterface{

	private String instrument;
	private String closeoutBid;
	private String closeoutAsk;

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getCloseoutBid() {
		return closeoutBid;
	}

	public void setCloseoutBid(String closeoutBid) {
		this.closeoutBid = closeoutBid;
	}

	public String getCloseoutAsk() {
		return closeoutAsk;
	}

	public void setCloseoutAsk(String closeoutAsk) {
		this.closeoutAsk = closeoutAsk;
	}

	@Override
	public String getY() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getX() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getComplete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getH() {		
		return closeoutAsk;
	}

	@Override
	public String getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getL() {		
		return closeoutBid;
	}

	@Override
	public int getCandleIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isTheFirstCandleOfTheDay() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTheFirstCandleOfTheDay(boolean firstCandleOfTheDay) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCandleIndex(int candleIndex) {
		// TODO Auto-generated method stub
		
	}

}
