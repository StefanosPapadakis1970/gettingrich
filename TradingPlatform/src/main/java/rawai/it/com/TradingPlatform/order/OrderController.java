package rawai.it.com.TradingPlatform.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oanda.v20.Context;
import com.oanda.v20.ContextBuilder;
import com.oanda.v20.ExecuteException;
import com.oanda.v20.RequestException;
import com.oanda.v20.account.AccountID;
import com.oanda.v20.order.LimitOrderRequest;
import com.oanda.v20.order.MarketOrderRequest;
import com.oanda.v20.order.OrderCancelResponse;
import com.oanda.v20.order.OrderCreateRequest;
import com.oanda.v20.order.OrderCreateResponse;
import com.oanda.v20.order.OrderReplace400RequestException;
import com.oanda.v20.order.OrderReplace404RequestException;
import com.oanda.v20.order.OrderReplaceRequest;
import com.oanda.v20.order.OrderRequest;
import com.oanda.v20.order.OrderSpecifier;
import com.oanda.v20.order.OrderType;
import com.oanda.v20.order.StopLossOrderRequest;
import com.oanda.v20.order.TakeProfitOrderRequest;
import com.oanda.v20.primitives.InstrumentName;
import com.oanda.v20.transaction.ClientExtensions;
import com.oanda.v20.transaction.ClientID;

import rawai.it.com.TradingPlatform.DTO.InstrumentDTO;
import rawai.it.com.TradingPlatform.DTO.ProfileDTO;
import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.conditions.PageConditionEnum;
import rawai.it.com.TradingPlatform.databuilders.TradingAlgorithmBuilder;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.instrument.Instrument;
import rawai.it.com.TradingPlatform.instrument.InstrumentController;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.price.PriceStreamService;
import rawai.it.com.TradingPlatform.tradingprofile.TradingProfileService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class OrderController {

	private static final Logger log = LogManager.getLogger(OrderController.class);

	@Autowired
	private AccountRepository accountrepository;
	@Autowired
	private InstrumentController instrumentController;
	@Autowired
	private TradingRepository tradingrepository;
	@Autowired
	private TradingProfileService tradingProfileService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private PriceStreamService priceStreamService;
	@Autowired
	private CurlUtil curlUtil;

	@RequestMapping(value = "order/createMassMarketOrder", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView createMassOrder(@Valid @ModelAttribute Order order, Errors errors) {
		RestTemplate tmpl = new RestTemplate();

		ModelAndView openOrderForm = new ModelAndView("/order/openMassMarketOrderView");

		String errorMessage = "";
		String successMessage = "";

		List<ProfileDTO> profilesDTOs = tradingProfileService.getAllTradingProfileDTOs(true);
		List<String> instruments = new ArrayList<>();
		List<String> granularities = new ArrayList<>();
		for (ProfileDTO profile : profilesDTOs) {
			for (InstrumentDTO instrument : profile.getInstruments()) {
				if (!granularities.contains(instrument.getInterval().name())) {
					granularities.add(instrument.getInterval().name());
				}
				if (!instruments.contains(instrument.getInstrument())) {
					instruments.add(instrument.getInstrument());
				}
			}
		}

		openOrderForm.addObject("instruments", instruments);
		openOrderForm.addObject("profiles", profilesDTOs);
		openOrderForm.addObject("order", order);

		if (errors.hasErrors()) {
			return openOrderForm;
		}

		for (InstrumentDTO instrumentDTO : order.getInstruments()) {
			if (!instrumentDTO.isTrade()) {
				continue;
			}
			
			if (SessionUtils.getAccounts() == null) {
				SessionUtils.setAccounts(accountrepository.findAll());
			}

			Account account = SessionUtils.findAccountByUserId(instrumentDTO.getUserID());
			
			Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

			ClientExtensions clientExtensions = new ClientExtensions();
			clientExtensions.setComment("Manual Trade");
			clientExtensions.setTag("Manual");
			clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));
			
			OrderRequest  orderRequest;
			
			if (order.getPrice() != null && !order.getPrice().isEmpty() && Double.valueOf(order.getPrice()) != 0) {
				orderRequest  = new LimitOrderRequest();
				((LimitOrderRequest) orderRequest).setClientExtensions(clientExtensions);			
				((LimitOrderRequest) orderRequest).setUnits(instrumentDTO.getUnits());
				((LimitOrderRequest) orderRequest).setInstrument(instrumentDTO.getInstrument());
				((LimitOrderRequest) orderRequest).setPrice(order.getPrice());
				((LimitOrderRequest) orderRequest).setClientExtensions(clientExtensions);
			} else {
				orderRequest  = new MarketOrderRequest();
				((MarketOrderRequest) orderRequest).setClientExtensions(clientExtensions);			
				((MarketOrderRequest) orderRequest).setUnits(instrumentDTO.getUnits());
				((MarketOrderRequest) orderRequest).setInstrument(instrumentDTO.getInstrument());
				((MarketOrderRequest) orderRequest).setClientExtensions(clientExtensions);
			}
			
			

			OrderCreateRequest orderCreateRequest = new OrderCreateRequest(new AccountID(account.getUserId()));

			// Attach the body parameter to the request
			orderCreateRequest.setOrder(orderRequest);
			// Execute the request and obtain the response object
			OrderCreateResponse response;
			try {
				response = ctx.order.create(orderCreateRequest);
			} catch (RequestException e) {
				log.error("Problem !", e);
				openOrderForm.addObject("error", true);
			} catch (ExecuteException e) {
				log.error("Problem !", e);
				openOrderForm.addObject("error", true);
			}			
			
		}

		openOrderForm.addObject("error", errorMessage);
		openOrderForm.addObject("granularity", granularities);
		openOrderForm.addObject("successMessage", successMessage);

		return openOrderForm;
	}

	@RequestMapping(value = "order/openMassMarketOrderView", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView openMarketOrderView() {
		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountrepository.findAll());
		}

		List<TradingProfile> profiles = new ArrayList<>();
		if (SessionUtils.getTradingProfiles() == null || SessionUtils.getTradingProfiles().size() == 0) {
			for (TradingProfileEntity profileEntity : tradingrepository.findAllProfiles()) {
				TradingProfile tradingProfile = TradingProfileConverter.convertToTradingProfile(profileEntity, SessionUtils.findAccountByUserId(profileEntity.getUserID()));
				tradingProfile.initMassTrade();
				profiles.add(tradingProfile);
			}
		}

		List<String> instruments = new ArrayList<>();

		for (TradingProfile profile : profiles) {
			for (TradingInstrument instrument : profile.getTradingInstruments()) {
				if (!instruments.contains(instrument.getInstument())) {
					instruments.add(instrument.getInstument());
				}
			}
		}

		ModelAndView marketOrderForm = new ModelAndView("/order/openMassMarketOrderView");
		marketOrderForm.addObject("accounts", SessionUtils.getAccounts());

		List<String> userIDs = new ArrayList<>();
		for (Account account : SessionUtils.getAccounts()) {
			userIDs.add(account.getUserId());
		}

		List<ProfileDTO> profilesDTOs = tradingProfileService.getAllTradingProfileDTOs(true);
		List<String> granularities = new ArrayList<>();
		for (ProfileDTO profile : profilesDTOs) {
			for (InstrumentDTO instrument : profile.getInstruments()) {
				if (!granularities.contains(instrument.getInterval().name())) {
					granularities.add(instrument.getInterval().name());
				}
			}
		}

		// SessionUtils.startPriceRunners(PriceRunner.ORDERS,
		// priceStreamService, instruments);

		marketOrderForm.addObject("instruments", instruments);
		marketOrderForm.addObject("userIDs", userIDs);
		marketOrderForm.addObject("profiles", profilesDTOs);
		marketOrderForm.addObject("order", new Order());
		marketOrderForm.addObject("granularity", granularities);
		marketOrderForm.addObject("statusConditions", PageConditionEnum.TRADE.getConditions());
		return marketOrderForm;
	}

	@RequestMapping(value = "order/openMarketOrderView", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView openMarketOrderView(HttpServletRequest request) {
		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountrepository.findAll());
		}

		if (SessionUtils.getInstrumentMap().isEmpty()) {
			instrumentController.instrumentService.setupInstruments();
		}

		ModelAndView marketOrderForm = new ModelAndView("/order/openMarketOrderView");
		marketOrderForm.addObject("accounts", SessionUtils.getAccounts());

		List<String> userIDs = new ArrayList<>();
		for (Account account : SessionUtils.getAccounts()) {
			userIDs.add(account.getUserId());
		}

		List<String> instruments = new ArrayList<>();
		for (Account account : SessionUtils.getAccounts()) {
			for (Instrument instrument : SessionUtils.getInstrumentMap().get(account.getUserId())) {
				if (!instruments.contains(instrument.getName())) {
					instruments.add(instrument.getName());
				}
			}
		}

		// maybe for every user one runner

		// SessionUtils.startPriceRunners(PriceRunner.ORDERS,
		// priceStreamService, instruments);

		marketOrderForm.addObject("instrumentMap", SessionUtils.getInstrumentMap());
		marketOrderForm.addObject("userIDs", userIDs);
		marketOrderForm.addObject("order", new Order());
		marketOrderForm.addObject("statusConditions", PageConditionEnum.TRADE.getConditions());
		return marketOrderForm;
	}

	@RequestMapping(value = "order/coverMarketOrderView", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView coverMarketOrderView(@RequestParam(value = "userID") String userID, @RequestParam(value = "instrument") String instrunemt, @RequestParam(value = "units") String units, HttpServletRequest request) {
		boolean buysell = true;
		if (Integer.valueOf(units) < 0) {
			buysell = false;
			units = units.substring(1);
		}
		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountrepository.findAll());
		}

		if (SessionUtils.getInstrumentMap().isEmpty()) {
			instrumentController.instrumentService.setupInstruments();
		}

		ModelAndView marketOrderForm = new ModelAndView("/order/openMarketOrderView");
		marketOrderForm.addObject("accounts", SessionUtils.getAccounts());

		List<String> userIDs = new ArrayList<>();
		for (Account account : SessionUtils.getAccounts()) {
			userIDs.add(account.getUserId());
		}
		Order order = new Order();
		order.setInstrument(instrunemt);
		order.setUserID(userID);
		order.setUnits(units);
		order.setBuy(!buysell);
		marketOrderForm.addObject("instrumentMap", SessionUtils.getInstrumentMap());
		marketOrderForm.addObject("userIDs", userIDs);
		marketOrderForm.addObject("userID", userID);
		marketOrderForm.addObject("order", order);
		marketOrderForm.addObject("statusConditions", PageConditionEnum.TRADE.getConditions());
		return marketOrderForm;
	}

	@RequestMapping(value = "order/openStopLossOrderView", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView openStopLossOrderView(@RequestParam(value = "userID", required = false) String userID, @RequestParam(value = "tradeID", required = false) String tradeID) {
		ModelAndView resultView = new ModelAndView("/order/openStopLossOrderView");
		resultView.addObject("userID", userID);
		resultView.addObject("tradeID", userID);
		return resultView;
	}

	@RequestMapping(value = "order/openTakeProfitOrderView", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView openTakeProfiOrderView(@RequestParam(value = "userID", required = false) String userID, @RequestParam(value = "tradeID", required = false) String tradeID, @RequestParam(value = "price", required = false) String price) {
		ModelAndView resultView = new ModelAndView("/order/openTakeProfitOrderView");
		resultView.addObject("userID", userID);
		resultView.addObject("tradeID", tradeID);
		resultView.addObject("price", price);
		TakeProfitOrder takeProfitOrder = new TakeProfitOrder();
		takeProfitOrder.setPrice(price);
		takeProfitOrder.setTradeID(tradeID);
		resultView.addObject("takeProfitOrder", takeProfitOrder);
		return resultView;
	}

	@RequestMapping(value = "order/openTakeProfitOrder", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView openTakeProfiOrder(@Valid @ModelAttribute TakeProfitOrder takeProfitOrder, Errors errors, RedirectAttributes ra, @RequestParam("userID") String userID, HttpServletRequest request) {
		ModelAndView openOrderForm = new ModelAndView("/order/openTakeProfitOrderView");
		openOrderForm.addObject("takeProfitOrder", takeProfitOrder);
		openOrderForm.addObject("userID", userID);
		RestTemplate tmpl = new RestTemplate();

		if (errors.hasErrors()) {
			openOrderForm.addObject("success", false);
			return openOrderForm;
		}

		Account account = SessionUtils.findAccountByUserId(userID);

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment("Manual Trade");
		clientExtensions.setTag("Manual");
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		TakeProfitOrderRequest takeProfitOrderRequest = new TakeProfitOrderRequest();
		takeProfitOrderRequest.setClientExtensions(clientExtensions);
		takeProfitOrderRequest.setTradeID(takeProfitOrder.getTradeID());
		takeProfitOrderRequest.setPrice(takeProfitOrder.getPrice());

		OrderCreateRequest orderRequest = new OrderCreateRequest(new AccountID(account.getUserId()));

		// Attach the body parameter to the request
		orderRequest.setOrder(takeProfitOrderRequest);
		// Execute the request and obtain the response object
		OrderCreateResponse response;
		try {
			response = ctx.order.create(orderRequest);
		} catch (RequestException e) {
			log.error("Problem !", e);
		} catch (ExecuteException e) {
			log.error("Problem !", e);
		}
		openOrderForm.addObject("success", true);
		return openOrderForm;
	}

	@RequestMapping(value = "order/openTakeProfitOrderDialog", method = RequestMethod.GET)
	@ResponseBody
	public String openTakeProfiOrderDialog(@RequestParam("userID") String userID, @RequestParam("tradeID") String tradeID, @RequestParam("price") String price, HttpServletRequest request) {

		Account account = SessionUtils.findAccountByUserId(userID);
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment("Manual Trade");
		clientExtensions.setTag("Manual");
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		TakeProfitOrderRequest takeProfitOrderRequest = new TakeProfitOrderRequest();
		takeProfitOrderRequest.setClientExtensions(clientExtensions);
		takeProfitOrderRequest.setTradeID(tradeID);
		takeProfitOrderRequest.setPrice(price);

		OrderCreateRequest orderRequest = new OrderCreateRequest(new AccountID(account.getUserId()));

		// Attach the body parameter to the request
		orderRequest.setOrder(takeProfitOrderRequest);
		// Execute the request and obtain the response object
		OrderCreateResponse response;
		try {
			response = ctx.order.create(orderRequest);
		} catch (RequestException e) {
			log.error("Problem !", e);
			return e.getErrorMessage();
		} catch (ExecuteException e) {
			log.error("Problem !", e);
			return e.getMessage();
		}

		return "OK";
	}

	@RequestMapping(value = "order/openStopLossOrderDialog", method = RequestMethod.GET)
	@ResponseBody
	public String openStopLossDialog(@RequestParam("userID") String userID, @RequestParam("tradeID") String tradeID, @RequestParam("price") String price, HttpServletRequest request) {

		Account account = SessionUtils.findAccountByUserId(userID);

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment("Manual Trade");
		clientExtensions.setTag("Manual");
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		StopLossOrderRequest stopLossOrderRequest = new StopLossOrderRequest();
		stopLossOrderRequest.setClientExtensions(clientExtensions);
		stopLossOrderRequest.setTradeID(tradeID);
		stopLossOrderRequest.setPrice(price);

		OrderCreateRequest orderRequest = new OrderCreateRequest(new AccountID(account.getUserId()));

		// Attach the body parameter to the request
		orderRequest.setOrder(stopLossOrderRequest);
		// Execute the request and obtain the response object
		OrderCreateResponse response;
		try {
			response = ctx.order.create(orderRequest);
		} catch (RequestException e) {
			log.error("Problem !", e);
			return e.getErrorMessage();
		} catch (ExecuteException e) {
			log.error("Problem !", e);
			return e.getMessage();
		}
		return "OK";
	}

	// order/closeOrderDialog?userID="+userID+"&orderID="+orderID+"&tradeID="+tradeID
	// , function( data ) {
	@RequestMapping(value = "order/closeOrderDialog", method = RequestMethod.GET)
	@ResponseBody
	public String closeOrder(@RequestParam("userID") String userID, @RequestParam("orderID") String orderID, @RequestParam("tradeID") String tradeID, HttpServletRequest request) {

		Account account = SessionUtils.findAccountByUserId(userID);

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		// Attach the body parameter to the request

		// Execute the request and obtain the response object
		OrderCancelResponse response;
		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);

		try {
			response = ctx.order.cancel(new AccountID(account.getUserId()), orderSpecifier);
		} catch (RequestException e) {
			log.error("Problem !", e);
			return e.getErrorMessage();
		} catch (ExecuteException e) {
			log.error("Problem !", e);
			return e.getMessage();
		}
		return "OK";

	}

	// order/closeOrder?orderId="+orderId+"&userID="+userID
	@RequestMapping(value = "order/closeOrder", method = RequestMethod.GET)
	@ResponseBody
	public String closePendingOrder(@RequestParam("orderID") String orderID, @RequestParam("userID") String userID) {

		Account account = SessionUtils.findAccountByUserId(userID);

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		// Attach the body parameter to the request

		// Execute the request and obtain the response object
		OrderCancelResponse response;
		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);

		try {
			response = ctx.order.cancel(new AccountID(account.getUserId()), orderSpecifier);
		} catch (RequestException e) {
			log.error("Problem !", e);
			return e.getErrorMessage();
		} catch (ExecuteException e) {
			log.error("Problem !", e);
			return e.getMessage();
		}
		return "OK";
	}

	@RequestMapping(value = "order/updateOrderDialog", method = RequestMethod.GET)
	@ResponseBody
	public String updateOrder(@RequestParam("userID") String userID, @RequestParam("orderID") String orderID, @RequestParam("price") String price, @RequestParam("tradeID") String tradeID, @RequestParam("orderType") String orderType, HttpServletRequest request) {

		Account account = SessionUtils.findAccountByUserId(userID);
		AccountID accountID = new AccountID(userID);

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment("Manual Trade");
		clientExtensions.setTag("Manual");
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);
		OrderReplaceRequest orderReplaceRequest = new OrderReplaceRequest(accountID, orderSpecifier);

		OrderType orderTypeEnum = OrderType.valueOf(orderType);

		OrderRequest orderRequest = null;

		switch (orderTypeEnum) {
		case STOP_LOSS:
			orderRequest = new StopLossOrderRequest();
			((StopLossOrderRequest) orderRequest).setTradeID(tradeID);
			((StopLossOrderRequest) orderRequest).setClientTradeID(orderID);
			((StopLossOrderRequest) orderRequest).setPrice(price);
			break;
		case TAKE_PROFIT:
			orderRequest = new TakeProfitOrderRequest();
			((TakeProfitOrderRequest) orderRequest).setTradeID(tradeID);
			((TakeProfitOrderRequest) orderRequest).setClientTradeID(orderID);
			((TakeProfitOrderRequest) orderRequest).setPrice(price);
			break;
		default:
			break;
		}

		orderReplaceRequest.setOrder(orderRequest);

		try {
			ctx.order.replace(orderReplaceRequest);
		} catch (OrderReplace400RequestException e2) {
			e2.printStackTrace();
			return e2.getErrorMessage();
		} catch (OrderReplace404RequestException e2) {
			e2.getErrorMessage();
			return "ERROR";
		} catch (RequestException e2) {
			e2.printStackTrace();
			return "ERROR";
		} catch (ExecuteException e2) {
			e2.printStackTrace();
			return "ERROR";
		}
		return "OK";
	}

	@RequestMapping(value = "order/openMarketOrder", method = RequestMethod.POST)
	public ModelAndView openMarketOrder(@Valid @ModelAttribute Order order, Errors errors) {

		ModelAndView openOrderForm = new ModelAndView("/order/openMarketOrderView");
		openOrderForm.addObject("accounts", SessionUtils.getAccounts());
		openOrderForm.addObject("order", order);
		openOrderForm.addObject("instrumentMap", SessionUtils.getInstrumentMap());
		openOrderForm.addObject("userID", order.getUserID());

		RestTemplate tmpl = new RestTemplate();

		if (errors.hasErrors()) {
			openOrderForm.addObject("success", false);
			return openOrderForm;
		}

		if (order.getUserID() == null || order.getUserID().equals("")) {
			errors.rejectValue("userID", "NOT_BLANK", "UserID cannot be empty");
		}
		Pattern pattern = Pattern.compile("^[1-9][0-9]*");
		if (!pattern.matcher(order.getUnits()).matches()) {
			errors.rejectValue("units", "NOT_BLANK", "Not a valid number (>0)");
		}
		if (order.getInstrument() == null || order.getInstrument().equals("")) {
			errors.rejectValue("instrument", "NOT_BLANK", "Please select Instrument");
		}

		if (errors.hasErrors()) {
			openOrderForm.addObject("success", false);
			return openOrderForm;
		}

		Account account = SessionUtils.findAccountByUserId(order.getUserID());

		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment("Manual Trade");
		clientExtensions.setTag("Manual");
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		OrderRequest orderRequest;

		if (order.getPrice() != null && !order.getPrice().isEmpty() && Double.valueOf(order.getPrice()) != 0) {
			// order.setTimeInForce("GTC");
			// order.setType("LIMIT"); // Was is da mit TimeForce
			orderRequest = new LimitOrderRequest();
			((LimitOrderRequest) orderRequest).setInstrument(new InstrumentName(order.getInstrument()));
			((LimitOrderRequest) orderRequest).setUnits(order.getUnits());
			((LimitOrderRequest) orderRequest).setClientExtensions(clientExtensions);
			((LimitOrderRequest) orderRequest).setPrice(order.getPrice());		
		} else {
			orderRequest = new MarketOrderRequest();
			((MarketOrderRequest) orderRequest).setInstrument(new InstrumentName(order.getInstrument()));
			((MarketOrderRequest) orderRequest).setUnits(order.getUnits());
			((MarketOrderRequest) orderRequest).setClientExtensions(clientExtensions);
			// order.setTimeInForce("FOK");
			// order.setType("MARKET");
		}

		// order.setPositionFill("DEFAULT"); Was is das ??

		// Create the new request
		OrderCreateRequest request = new OrderCreateRequest(new AccountID(account.getUserId()));

		// Attach the body parameter to the request
		request.setOrder(orderRequest);
		// Execute the request and obtain the response object
		OrderCreateResponse response;
		try {
			response = ctx.order.create(request);
		} catch (RequestException e) {
			openOrderForm.addObject("error", e.getErrorMessage());
			return openOrderForm;
		} catch (ExecuteException e) {
			e.printStackTrace();
			openOrderForm.addObject("error", e.getCause());
			return openOrderForm;
		}

//		ObjectMapper mapper = new ObjectMapper();
//		String jsonInString = "";
//		OrderJSON orderJSON = new OrderJSON();
//		if (!order.isBuy()) {
//			order.setUnits("-" + order.getUnits());
//		}

//		orderJSON.setOrder(order);
//		try {
//			jsonInString = mapper.writeValueAsString(orderJSON);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//
//		HttpEntity<?> apiHeader = curlUtil.createAPIHeaderWithBody(account.getApiKey(), jsonInString);
//		ResponseEntity<String> resp = null;
//		ResultMessage error = null;
//		try {
//			String url = curlUtil.createAccountUrl() + "/" + order.getUserID() + "/orders";
//			resp = tmpl.exchange(url, HttpMethod.POST, apiHeader, String.class);
//		} catch (HttpClientErrorException e) {
//			e.printStackTrace();
//			try {
//				error = mapper.readValue(e.getResponseBodyAsString(), ResultMessage.class);
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//
//			openOrderForm.addObject("error", error.getErrorMessage());
//			return openOrderForm;
//		}

		openOrderForm.addObject("success", true);

		if (!order.isBuy()) {
			order.setUnits(order.getUnits().substring(1));
		}
		openOrderForm.addObject("statusConditions", PageConditionEnum.TRADE.getConditions());
		return openOrderForm;

	}

	@RequestMapping(value = "order/showAllOpenOrders", method = RequestMethod.GET)
	public ModelAndView showAllOpenOrders() {
		List<String> instruments = new ArrayList<>();
		List<Order> openOrders = orderService.getAllOpenOrders();
		for (Order order : openOrders) {
			if (!instruments.contains(order.getInstrument())) {
				instruments.add(order.getInstrument());
			}
		}

		ModelAndView resultView = new ModelAndView("/order/showAllOpenOrders");
		resultView.addObject("instruments", instruments);
		resultView.addObject("orders", openOrders);
		resultView.addObject("statusConditions", PageConditionEnum.OPENORDERS.getConditions());
		return resultView;
	}

	@RequestMapping(value = "order/testOrder", method = RequestMethod.GET)
	@ResponseBody
	public String dummyOrder() throws Throwable {

		TradingAlgorithm algorithm = TradingAlgorithmBuilder.aTradingAlgorithmEntityBuilder().withInterval(IntervalEnum.S5).withStopLossPips(20).withTakeProfitPips(10).build();

		List<AlgorithmTrade> trades = new ArrayList<>();
		AlgorithmTrade trade = new AlgorithmTrade();
		trade.setTradingAlgorithm(algorithm);
		trade.setBuySell(true);

		trade.setTrade(true);
		TradingInstrument instrument = new TradingInstrument();
		instrument.setInstument("EUR_USD");
		instrument.setUnits(1);
		instrument.setTradingEngine(true);
		trade.setInstrument(instrument);
		Account account = new Account();
		account.setUserId("101-011-5094777-001");
		account.setId(11l);
		trade.setAccount(account);
		trade.setUuid(UUID.randomUUID().toString());
		trade.setTakeProfitPips(200);
		trade.setStopLossPips(200);
		trades.add(trade);

		return orderService.createTrades(trades) + " Berti";
	}

	public void sendOrdersToClient() {
		log.info("sending orders");
		template.convertAndSend("/topic/orders", orderService.getAllOpenOrders());
	}
}
