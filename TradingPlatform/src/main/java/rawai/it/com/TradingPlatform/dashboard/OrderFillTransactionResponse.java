package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderFillTransactionResponse {

	private OrderFillTransaction orderFillTransaction;

	public OrderFillTransaction getOrderFillTransaction() {
		return orderFillTransaction;
	}

	public void setOrderFillTransaction(OrderFillTransaction orderFillTransaction) {
		this.orderFillTransaction = orderFillTransaction;
	}

}

