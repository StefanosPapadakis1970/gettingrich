package rawai.it.com.TradingPlatform.messanger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MessageService {
	@Autowired
	private SimpMessagingTemplate template;

	public boolean sendGlobalMessage(Message message) {
		try {
			template.convertAndSend("/topic/globalMessage", message);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return false;
	}

	@RequestMapping(value = "testmessage", method = RequestMethod.GET)
	@ResponseBody
	public String showDashboard() {
		Message message = new Message();
		message.setHeader("TestHeader");
		message.setMessage("TestMessage");
		if (sendGlobalMessage(message)) {
			return "OK";
		}
		return "Message could not be send!";
	}
}
