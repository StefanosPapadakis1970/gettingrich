package rawai.it.com.TradingPlatform.tradingprofile;


import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hsqldb.lib.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithmForm;
import rawai.it.com.TradingPlatform.engine.TradingInstrumentForm;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.TradingProfileForm;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.instrument.Instrument;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class TradingProfileController {

	@Autowired
	private TradingRepository tradingRepository;


	@RequestMapping(value = "tradingProfile/editProfile", method = RequestMethod.GET)
	public ModelAndView editTradingProfile(@RequestParam("userID") String userID) {
		ModelAndView resultView = new ModelAndView("tradingprofile/tradingProfile");

		TradingProfileEntity profileEntity = tradingRepository.findByUserID(userID);

		Account account = SessionUtils.findAccountByUserId(userID);

		TradingProfileForm profile = profileEntity == null ? new TradingProfileForm() : TradingProfileConverter.convertToTradingProfileForm(profileEntity);

		if (profileEntity == null) {
			profile.setUserID(userID);
			profile.getTradingInstruments().add(new TradingInstrumentForm());
		}

		if (profile.getTradingInstruments().size() == 0) {
			profile.getTradingInstruments().add(new TradingInstrumentForm());
		}

		profile.setUserName(account.getUserName());


		List<TradingAlgorithmForm> tradingAlgorithmForms = new ArrayList<>();

		List<TradingAlgorithmEntity> tradingAlgorithms = tradingRepository.findAllTradingAlgorithms();
		for (TradingAlgorithmEntity algorithmEntity : tradingAlgorithms) {
			tradingAlgorithmForms.add(TradingAlgorithmFormConverter.convertEntityToForm(algorithmEntity));
		}
		resultView.addObject("allAlgorithms", tradingAlgorithmForms);
		resultView.addObject("tradingProfileForm", profile);
		return resultView;
	}

	@RequestMapping(value = "/update/tradingprofile", method = RequestMethod.POST)
	public ModelAndView updateTradingProfile(@Valid @ModelAttribute TradingProfileForm profile, Errors errors) {
		ModelAndView resultView = new ModelAndView("tradingprofile/tradingProfile");
		HashSet doubles = new HashSet();
		HashSet doubleEntrie = new HashSet();
		if (errors.hasErrors()) {
			return resultView;
		}
		
		int i = 0;
		for (TradingInstrumentForm instrument : profile.getTradingInstruments()) {
			if (!isValidInstrument(profile.getUserID(), instrument)) {
				errors.rejectValue("tradingInstruments[" + i + "].instrument", "NOT_BLANK", "Invalid Instrument!");
			}

			if (IntervalEnum.getValue(instrument.getTradingInterval()) == null) {
				errors.rejectValue("tradingInstruments[" + i + "].tradingInterval", "NOT_BLANK", "Invalid Interval!");
			}

			if (!doubles.add(instrument.getInstrument() + " " + instrument.getTradingInterval())) {
				doubleEntrie.add(instrument.getInstrument() + " " + instrument.getTradingInterval());

				errors.rejectValue("tradingInstruments[" + i + "].instrument", "NOT_BLANK", "Interval and Instrument have to be unique!");

				break;
			}
			i++;
		}

		if (errors.hasErrors()) {
			return resultView;
		}


		TradingProfileEntity profileEntity = TradingProfileConverter.convertToTradingProfileEntity(profile.createTradingProfile());
		if (profileEntity.getId() != null) {
			tradingRepository.updateTradingProfile(profileEntity);
		} else {
			tradingRepository.saveTradingProfile(profileEntity);
		}
		TradingProfileConverter.updateIDs(profileEntity, profile);

		List<TradingAlgorithmForm> tradingAlgorithmForms = new ArrayList<>();
		List<TradingAlgorithmEntity> tradingAlgorithms = tradingRepository.findAllTradingAlgorithms();
		for (TradingAlgorithmEntity algorithmEntity : tradingAlgorithms) {
			tradingAlgorithmForms.add(TradingAlgorithmFormConverter.convertEntityToForm(algorithmEntity));
		}

		resultView.addObject("success", true);
		resultView.addObject("allAlgorithms", tradingAlgorithmForms);
		return resultView;
	}

	private boolean isValidInstrument(String userID, TradingInstrumentForm instrument) {
		List<Instrument> instruments = SessionUtils.getInstrumentMap().get(userID);
		for (Instrument loopInstrument : instruments) {
			if (loopInstrument.getName().equalsIgnoreCase(instrument.getInstrument())) {
				return true;
			}
		}
		return false;
	}

	@RequestMapping(value = "tradingProfile/editProfile1", method = RequestMethod.GET)
	public String test1() {
		return "Was geht?";
	}
}
