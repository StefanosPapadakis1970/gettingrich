package rawai.it.com.TradingPlatform.algorithmtesting;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TestStatusEnumConverter implements AttributeConverter<TestStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(TestStatusEnum testStatus) {
		if (testStatus == null) {
			return null;
		}
		return testStatus.name();
	}

	@Override
	public TestStatusEnum convertToEntityAttribute(String dbData) {
		return TestStatusEnum.valueOf(dbData);
	}

}
