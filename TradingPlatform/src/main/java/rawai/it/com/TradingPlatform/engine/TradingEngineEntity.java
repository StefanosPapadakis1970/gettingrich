package rawai.it.com.TradingPlatform.engine;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "engine")
@NamedQueries({ @NamedQuery(name = TradingEngineEntity.GET_ENGINE, query = "select engine from TradingEngineEntity engine") })
public class TradingEngineEntity {

	public static final String GET_ENGINE = "TradingEngineEntity.GET_ENGINE";

	@Id
	@GeneratedValue
	private Long id;

	private boolean running;

	private Date startUpDate;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public Date getStartUpDate() {
		return startUpDate;
	}

	public void setStartUpDate(Date startUpDate) {
		this.startUpDate = startUpDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
