package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.CandleEntity;

public class CandleEntityBuilder {

	private String time;
	private String o;
	private String h;
	private String l;
	private String c;
	private AlgorithmTradeEntity algorithmTrade;

	private CandleEntityBuilder() {

	}

	public static CandleEntityBuilder aCandle() {
		return new CandleEntityBuilder();
	}

	public CandleEntityBuilder withTime(String time) {
		this.time = time;
		return this;
	}

	public CandleEntityBuilder withO(String o) {
		this.o = o;
		return this;
	}

	public CandleEntityBuilder withH(String h) {
		this.h = h;
		return this;
	}

	public CandleEntityBuilder withL(String l) {
		this.l = l;
		return this;
	}

	public CandleEntityBuilder withC(String c) {
		this.c = c;
		return this;
	}

	public CandleEntityBuilder withTrade(AlgorithmTradeEntity algorithmTrade) {
		this.algorithmTrade = algorithmTrade;
		return this;
	}

	public CandleEntity build(){
		CandleEntity candle = new CandleEntity();
		candle.setC(c);
		candle.setO(o);
		candle.setL(l);
		candle.setH(h);
		candle.setTime(time);
		candle.setAlgorithmTrade(algorithmTrade);
		return candle;
	}
}
