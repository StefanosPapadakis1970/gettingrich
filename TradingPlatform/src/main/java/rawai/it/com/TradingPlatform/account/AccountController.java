package rawai.it.com.TradingPlatform.account;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import rawai.it.com.TradingPlatform.DTO.AccountSummayDTO;
import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.signup.SignupForm;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
@Secured("ROLE_USER")
public class AccountController {

	private static final Logger logger = LogManager.getLogger(AccountController.class);

	private static final String UPDATE_VIEW_NAME = "signup/updateaccount";
	@Autowired
    private AccountRepository accountRepository;
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	private TradingRepository tradingRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @RequestMapping(value = "account/current", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public Account accounts(Principal principal) {
        Assert.notNull(principal);
        return accountRepository.findByEmail(principal.getName());
    }

	@RequestMapping(value = "account/showall", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView showAll(HttpServletRequest request) {
		ModelAndView resultView = new ModelAndView("/account/accountlist");
		if (SessionUtils.getAccounts() == null) {
			List<Account> accounts = accountRepository.findAll();
			request.getSession().setAttribute("accounts", accounts);
			SessionUtils.setAccounts(accounts);
		}
		resultView.addObject("accounts", SessionUtils.getAccounts());
		return resultView;
	}

	@RequestMapping(value = "account/refresh", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView refreshAccounts() {
		ModelAndView resultView = new ModelAndView("/account/accountlist");
		List<Account> accounts = accountRepository.findAll();
		SessionUtils.setAccounts(accounts);
		resultView.addObject("accounts", SessionUtils.getAccounts());
		return resultView;
	}

	@RequestMapping(value = "account/delete", method = RequestMethod.GET)
	public ModelAndView deleteAccountByID(@RequestParam("ID") String ID) {
		accountRepository.deleteAccountByID(Long.valueOf(ID));
		return refreshAccounts();
	}

	@RequestMapping(value = "account/edit", method = RequestMethod.GET)
	public ModelAndView editAccount(@RequestParam("ID") String ID) {
		ModelAndView view = new ModelAndView("signup/updateaccount");
		Account account = accountRepository.findByUserId(ID);
		SignupForm form = new SignupForm();
		form.setApiKey(account.getApiKey());
		form.setUserId(account.getUserId());
		form.setUserName(account.getUserName());
		form.setDbID(String.valueOf(account.getId()));
		view.addObject("signupForm", form);
		return view;
	}

	// /account/summary
	@RequestMapping(value = "account/summary", method = RequestMethod.GET)
	public ModelAndView showAccountSummay() {
		List<TradingProfileEntity> profiles = tradingRepository.findAllProfiles();
		List<AccountSummayDTO> summaries = new ArrayList<>();

		for (TradingProfileEntity profile : profiles) {
			String userName = SessionUtils.findAccountByUserId(profile.getUserID()).getUserName();
			for (TradingInstrumentEntity instrument : profile.getTradingInstruments()) {
				AccountSummayDTO summary = new AccountSummayDTO();
				summary.setUserID(profile.getUserID());
				summary.setUsername(userName);
				summary.setInstrument(instrument.getInstument());
				summary.setInterval(instrument.getTradingInterval().name());
				summary.setUnits(String.valueOf(instrument.getUnits()));
				summary.setEngine(instrument.isTradingEngine() ? "yes" : "no");
				summary.setMasssTrade(instrument.isMassTrade() ? "yes" : "no");
				summaries.add(summary);
			}
		}

		ModelAndView view = new ModelAndView("account/summary");
		view.addObject("summaries", summaries);
		return view;
	}

	@RequestMapping(value = "account/detail", method = RequestMethod.GET)
	public ModelAndView showAccountDetails(HttpServletRequest request, @RequestParam("userid") String userid) {
		RestTemplate tmpl = new RestTemplate();
		ModelAndView resultView = new ModelAndView("/account/accountdetails");
		Account account = SessionUtils.findAccountByUserId(userid);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);

		ResponseEntity<OandaAccountSummaryResponse> resp = tmpl.exchange(curlUtil.createAccountUrl() + "/" + userid + "/summary", HttpMethod.GET, apiHeader,
				OandaAccountSummaryResponse.class);

		resultView.addObject("username", account.getUserName());
		resultView.addObject("accountdetails", resp.getBody().getAccount());
		resultView.addObject("accounts", request.getSession().getAttribute("accounts"));
		resultView.addObject("account", account);
		return resultView;
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra, HttpServletRequest request) {
		if (errors.hasErrors()) {
			return UPDATE_VIEW_NAME;
		}
		Account account = signupForm.createAccountUpdate();
		RestTemplate tmpl = new RestTemplate();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<AccountIDListResponse> resp = null;
		try {
			resp = tmpl.exchange(curlUtil.createAccountUrl(), HttpMethod.GET, apiHeader, AccountIDListResponse.class);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				errors.rejectValue("apiKey", "NOT_BLANK", "API-Key ist not Authorized!");
				return UPDATE_VIEW_NAME;
			}
		}

		boolean idFound = false;

		for (AccountIdResponse accountID : resp.getBody().getAccounts()) {
			if (accountID.getId().equals(account.getUserId())) {
				idFound = true;
			}
		}

		if (!idFound) {
			errors.rejectValue("userId", "NOT_BLANK", "UserId is not supported by given API-Key!");
			return UPDATE_VIEW_NAME;
		}

		// if (SessionUtils.findAccountByUserId(signupForm.getUserId()) != null)
		// {
		// errors.rejectValue("userId", "NOT_BLANK",
		// "UserId is alread registered!");
		// return UPDATE_VIEW_NAME;
		// }

		accountRepository.updateAccount(account);

		List<Account> accounts = accountRepository.findAll();
		SessionUtils.setAccounts(accounts);

		return "redirect:/account/showall";
	}


}
