package rawai.it.com.TradingPlatform.instrument;

public class OpenClosePrice {

	private String closePrice;
	private String openPrice;

	public String getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(String closePrice) {
		this.closePrice = closePrice;
	}

	public String getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(String openPrice) {
		this.openPrice = openPrice;
	}
}
