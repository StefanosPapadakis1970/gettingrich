package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class HammerAny extends AbstractTradingSignal {

	private boolean up;
	
	public HammerAny(boolean up) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.HAMMER, 1);
		this.up = up;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface candle = candles.get(lastSignalCandleIndex);
		
		if (isHammer(candle)) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean isHammer(CandleDataPointInterface candle) {		
		
		int high = Integer.valueOf(candle.getH().replace(".", ""));
		int low = Integer.valueOf(candle.getL().replace(".", ""));
		int close = Integer.valueOf(candle.getC().replace(".", ""));		
		int open = Integer.valueOf(candle.getO().replace(".", ""));		
		
		
		boolean valid = false;
		
		if (up) {
			valid = close == high;
			valid &= RangeEnum.TOP_QUARTER.inRange(high, low, open);
			valid &= open > low;
		} else {
			valid = open == low;
			valid &= RangeEnum.BOTTOM_QUARTER.inRange(high, low, close);
			valid &= close > low;
		}
		
		if (valid && SignalUtility.isPositiveCandleRange(candle)) {
          return true;
		}		

		return false;		
	}

}
