package rawai.it.com.TradingPlatform.dashboard;

public enum OrderTriggerCondition {
	DEFAULT, INVERSE, BID, ASK, MID;
}
