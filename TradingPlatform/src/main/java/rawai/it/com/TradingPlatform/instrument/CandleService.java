package rawai.it.com.TradingPlatform.instrument;

import java.time.Instant;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.oanda.v20.Context;
import com.oanda.v20.ContextBuilder;
import com.oanda.v20.ExecuteException;
import com.oanda.v20.RequestException;
import com.oanda.v20.instrument.Candlestick;
import com.oanda.v20.instrument.CandlestickData;
import com.oanda.v20.instrument.CandlestickGranularity;
import com.oanda.v20.instrument.InstrumentCandlesRequest;
import com.oanda.v20.instrument.InstrumentCandlesResponse;
import com.oanda.v20.primitives.InstrumentName;
import com.oanda.v20.transaction.ClientExtensions;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.candle.Ask;
import rawai.it.com.TradingPlatform.candle.Bid;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.Mid;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.transaction.TransactionService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@Service
public class CandleService {
	
	@Autowired
	private CurlUtil curlUtil;
	
	@Autowired
	private AlgorithmTradeRepository algorithmTradeRepository;
	
	@Autowired
	private TransactionService transactionService;
	
	
	
	public List<Candlestick> fetchCloseOutCandles(Long tradeID, Long clouseOutTradeID) {
		
 		AlgorithmTradeEntity tradeEntity = algorithmTradeRepository.findAlgorithmByID(tradeID);
		AlgorithmTrade trade = AlgorithmTradeConverter.convertToAlgorithmTrade(tradeEntity);
		List<Candle> candles = AlgorithmTradeConverter.convertCandlesEntitiesToCandles(tradeEntity.getCandles());
		
		Account account = SessionUtils.findAccountByUserId(trade.getAccount().getUserId());		
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();
		
		String clouseOutTime = transactionService.getCloseOutTime(trade.getCloseOutTradeID().toString(), trade.getAccount().getUserId());
		  

		InstrumentCandlesRequest candlesRequest = new InstrumentCandlesRequest(new InstrumentName(trade.getInstrument().getInstument()));
		candlesRequest.setFrom(candles.get(0).getTime());		
		candlesRequest.setTo(clouseOutTime);
		CandlestickGranularity granularity = CandlestickGranularity.valueOf(trade.getTradingInterVal().name());
		candlesRequest.setGranularity(granularity);      
		
		
		try {
			InstrumentCandlesResponse response = ctx.instrument.candles(candlesRequest);
			return response.getCandles();
		} catch (RequestException | ExecuteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return null;
	}
	
	public List<Candle> convertCandleStickToCandleList(List<Candlestick> candleSticks){
		List<Candle> resultCandles = new ArrayList<>();
		candleSticks.stream().forEach(c -> resultCandles.add(convertCandlestickToCandle(c)));		
		return resultCandles;
	}

	public Candle convertCandlestickToCandle(Candlestick candleStick) {
		Candle candle = new Candle();		
		candle.setAsk(convertToAsk(candleStick.getAsk()));
		candle.setBid(convertToBid(candleStick.getBid()));
		//candle.setCandleIndex(candleIndex);
		candle.setComplete(candleStick.getComplete());
		candle.setMid(convertToMid(candleStick.getMid()));
		candle.setTime(candleStick.getTime().toString());
		candle.setVolume(candleStick.getVolume().toString());	
		return candle;
	}

	private Mid convertToMid(CandlestickData mid) {
		Mid resultMid = new Mid();
		if (mid == null) {
			return null;
		}
		resultMid.setC(mid.getC().toString());
		resultMid.setH(mid.getH().toString());
		resultMid.setL(mid.getL().toString());
		resultMid.setO(mid.getO().toString());		
		return resultMid;
	}

	private Bid convertToBid(CandlestickData bid) {
		Bid resultBid = new Bid();
		if (bid == null) {
			return null;
		}
		resultBid.setC(bid.getC().toString());
		resultBid.setH(bid.getH().toString());
		resultBid.setL(bid.getL().toString());
		resultBid.setO(bid.getO().toString());		
		return resultBid;
	}

	private Ask convertToAsk(CandlestickData ask) {
		Ask resultAsk = new Ask();
		if (ask == null) {
			return null;
		}
		resultAsk.setC(ask.getC().toString());
		resultAsk.setH(ask.getH().toString());
		resultAsk.setL(ask.getL().toString());
		resultAsk.setO(ask.getO().toString());		
		return resultAsk;
	}
	
	public void markTradingCandle(List<Candle> candles, String time, int tradingCanldeIndex) {		
		candles.stream().forEach(c -> {
			if(c.getTime().equals(time)) {
				c.setCandleIndex(tradingCanldeIndex);
			}
		});
	}
}
