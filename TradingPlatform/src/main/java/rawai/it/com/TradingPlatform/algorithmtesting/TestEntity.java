package rawai.it.com.TradingPlatform.algorithmtesting;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.engine.db.IntervalEnumConverter;
import rawai.it.com.TradingPlatform.engine.db.RunTypeConverter;
import rawai.it.com.TradingPlatform.engine.db.TestRunEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.tradingprofile.RunType;

@Entity
@Table(name = "test")
@NamedQueries({ @NamedQuery(name = TestEntity.FIND_BY_TEST_ID, query = "select test from TestEntity test where test.id = :testID"),
		@NamedQuery(name = TestEntity.DELETE_BY_TEST_ID, query = "delete from TestEntity test where test.id = :testID"),
		@NamedQuery(name = TestEntity.FIND_ALL, query = "select test from TestEntity test") })
public class TestEntity {

	public static final String FIND_BY_TEST_ID = "TestEntity.findByTestID";
	public static final String FIND_ALL = "TestEntity.findALL";
	public static final String DELETE_BY_TEST_ID = "TestEntity.deleteByTestID";

	@Id
	@GeneratedValue
	private Long id;


	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private TradingAlgorithmEntity algorithm;

	private String instrument;
	@Convert(converter = RunTypeConverter.class)
	private RunType runType;
	@Convert(converter = IntervalEnumConverter.class)
	private IntervalEnum granularity;
	private int units;
	private String userID;
	private int maxNumberOfTransactions;
	// TODO 21.06 remove start and enddate and move to tradeRun
	private Date startDate = new Date();
	private Date endDate = new Date();
	private boolean generateTrades;
	@Convert(converter = TestStatusEnumConverter.class)
	private TestStatusEnum status;

	// Change from Eage to lazy
	@OneToMany(mappedBy = "test", orphanRemoval = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@OrderBy("id")
	private List<TestRunEntity> testRuns = new ArrayList<>();

	public TradingAlgorithmEntity getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(TradingAlgorithmEntity algorithm) {
		this.algorithm = algorithm;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public RunType getRunType() {
		return runType;
	}

	public void setRunType(RunType runType) {
		this.runType = runType;
	}

	public IntervalEnum getInterval() {
		return granularity;
	}

	public void setInterval(IntervalEnum interval) {
		this.granularity = interval;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getMaxNumberOfTransactions() {
		return maxNumberOfTransactions;
	}

	public void setMaxNumberOfTransactions(int maxNumberOfTransactions) {
		this.maxNumberOfTransactions = maxNumberOfTransactions;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isGenerateTrades() {
		return generateTrades;
	}

	public void setGenerateTrades(boolean generateTrades) {
		this.generateTrades = generateTrades;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IntervalEnum getGranularity() {
		return granularity;
	}

	public void setGranularity(IntervalEnum granularity) {
		this.granularity = granularity;
	}

	public TestStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TestStatusEnum status) {
		this.status = status;
	}

	public List<TestRunEntity> getTestRuns() {
		return testRuns;
	}

	public void setTestRuns(List<TestRunEntity> testRuns) {
		this.testRuns = testRuns;
	}

}
