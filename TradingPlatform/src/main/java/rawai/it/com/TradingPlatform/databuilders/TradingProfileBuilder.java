package rawai.it.com.TradingPlatform.databuilders;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;

public class TradingProfileBuilder {

	private Long dbID;

	private Account account;

	private List<TradingInstrument> tradingInstruments = new ArrayList<>();

	private List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

	private TradingProfileBuilder() {

	}

	public static TradingProfileBuilder aTradingProfileBuilder() {
		return new TradingProfileBuilder();
	}

	public TradingProfileBuilder withdbID(Long dbID) {
		this.dbID = dbID;
		return this;
	}

	public TradingProfileBuilder withAccount(Account account) {
		this.account = account;
		return this;
	}

	public TradingProfileBuilder withTradingInstruments(List<TradingInstrument> instruments) {
		this.tradingInstruments = instruments;
		return this;
	}

	public TradingProfileBuilder withTradingAlgorithms(List<TradingAlgorithm> algorithms) {
		this.tradingAlgorithms = algorithms;
		return this;
	}

	public TradingProfileBuilder withTradingInstrument(TradingInstrument instrument) {
		this.tradingInstruments.add(instrument);
		return this;
	}

	public TradingProfileBuilder withTradingAlgorithm(TradingAlgorithm algorithm) {
		this.tradingAlgorithms.add(algorithm);
		return this;
	}

	public TradingProfile build() {
		TradingProfile profile = new TradingProfile();
		profile.setAccount(account);
		if (dbID != null && dbID != 0) {
			profile.setDbID(dbID);
		}
		profile.setTradingAlgorithms(tradingAlgorithms);
		profile.setTradingInstruments(tradingInstruments);
		return profile;
	}
}
