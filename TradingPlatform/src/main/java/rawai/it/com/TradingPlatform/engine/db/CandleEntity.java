package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "candle")
@NamedQueries({ @NamedQuery(name = CandleEntity.FIND_BY_TRADE_ID, query = "select candle from CandleEntity candle where candle.algorithmTrade.id = :id") })
public class CandleEntity {

	public static final String FIND_BY_TRADE_ID = "CandleEntity.findByTradeID";

	@Id
	@GeneratedValue
	private Long id;

	private String time;
	private String o;
	private String h;
	private String l;
	private String c;

	private int candleIndex;

	@ManyToOne
	@JoinColumn(name = "algorithmtrade_id")
	private AlgorithmTradeEntity algorithmTrade;

	@ManyToOne
	@JoinColumn(name = "testrun_id")
	private TestRunEntity testRun;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getO() {
		return o;
	}

	public void setO(String o) {
		this.o = o;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public AlgorithmTradeEntity getAlgorithmTrade() {
		return algorithmTrade;
	}

	public void setAlgorithmTrade(AlgorithmTradeEntity algorithmTrade) {
		this.algorithmTrade = algorithmTrade;
	}

	public int getCandleIndex() {
		return candleIndex;
	}

	public void setCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
	}

	public TestRunEntity getTestRun() {
		return testRun;
	}

	public void setTestRun(TestRunEntity testRun) {
		this.testRun = testRun;
	}
}
