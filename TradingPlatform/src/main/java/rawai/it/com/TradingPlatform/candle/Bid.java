package rawai.it.com.TradingPlatform.candle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bid {

	private String c;
	private String h;
	private String l;
	private String o;

	public Bid() {
	}

	public Bid(Bid mid) {
		c = mid.getC();
		h = mid.getH();
		l = mid.getL();
		o = mid.getO();
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getO() {
		return o;
	}

	public void setO(String o) {
		this.o = o;
	}

}
