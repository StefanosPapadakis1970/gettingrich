package rawai.it.com.TradingPlatform.dashboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DashBoardRunner implements Runnable {

	private static final Logger logger = LogManager.getLogger(DashBoardRunner.class);
	private DashboardController dashBoardController;
	private String sessionID;
	private boolean stop;


	public DashBoardRunner(DashboardController dashBoardController, String sessionID) {
		super();
		this.dashBoardController = dashBoardController;
		this.sessionID = sessionID;
	}

	@Override
	public void run() {
		while (!stop) {
			try {
				dashBoardController.sendDachBoartToClient();
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				logger.info("Dashboard-Runner interrupted!");
			}
		}

	}

	public DashboardController getDashBoardController() {
		return dashBoardController;
	}

	public void setDashBoardController(DashboardController dashBoardController) {
		this.dashBoardController = dashBoardController;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
