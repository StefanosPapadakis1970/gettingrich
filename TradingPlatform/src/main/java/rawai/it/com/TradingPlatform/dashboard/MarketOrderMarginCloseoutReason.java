package rawai.it.com.TradingPlatform.dashboard;

public enum MarketOrderMarginCloseoutReason {
	MARGIN_CHECK_VIOLATION, REGULATORY_MARGIN_CALL_VIOLATION;
}
