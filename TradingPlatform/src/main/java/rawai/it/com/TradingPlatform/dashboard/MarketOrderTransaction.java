package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketOrderTransaction extends Transaction {

	private TransactionType type;
	private String instrument;
	private String units;
	private String timeInForce;
	private String priceBound;
	private String currencySymbol;
	private OrderPositionFill positionFill;
	private MarketOrderTradeClose tradeClose;
	private MarketOrderPositionCloseout longPositionCloseout;
	private MarketOrderPositionCloseout shortPositionCloseout;
	private MarketOrderMarginCloseout marginCloseout;
	private MarketOrderDelayedTradeClose delayedTradeClose;
	private MarketOrderReason reason;
	private ClientExtensions clientExtensions;
	private TakeProfitDetails takeProfitOnFill;
	private StopLossDetails stopLossOnFill;
	private TrailingStopLossDetails trailingStopLossOnFill;
	private ClientExtensions tradeClientExtensions;

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getPriceBound() {
		return priceBound;
	}

	public void setPriceBound(String priceBound) {
		this.priceBound = priceBound;
	}

	public OrderPositionFill getPositionFill() {
		return positionFill;
	}

	public void setPositionFill(OrderPositionFill positionFill) {
		this.positionFill = positionFill;
	}

	public MarketOrderTradeClose getTradeClose() {
		return tradeClose;
	}

	public void setTradeClose(MarketOrderTradeClose tradeClose) {
		this.tradeClose = tradeClose;
	}

	public MarketOrderPositionCloseout getLongPositionCloseout() {
		return longPositionCloseout;
	}

	public void setLongPositionCloseout(MarketOrderPositionCloseout longPositionCloseout) {
		this.longPositionCloseout = longPositionCloseout;
	}

	public MarketOrderPositionCloseout getShortPositionCloseout() {
		return shortPositionCloseout;
	}

	public void setShortPositionCloseout(MarketOrderPositionCloseout shortPositionCloseout) {
		this.shortPositionCloseout = shortPositionCloseout;
	}

	public MarketOrderMarginCloseout getMarginCloseout() {
		return marginCloseout;
	}

	public void setMarginCloseout(MarketOrderMarginCloseout marginCloseout) {
		this.marginCloseout = marginCloseout;
	}

	public MarketOrderDelayedTradeClose getDelayedTradeClose() {
		return delayedTradeClose;
	}

	public void setDelayedTradeClose(MarketOrderDelayedTradeClose delayedTradeClose) {
		this.delayedTradeClose = delayedTradeClose;
	}

	public MarketOrderReason getReason() {
		return reason;
	}

	public void setReason(MarketOrderReason reason) {
		this.reason = reason;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

	public TakeProfitDetails getTakeProfitOnFill() {
		return takeProfitOnFill;
	}

	public void setTakeProfitOnFill(TakeProfitDetails takeProfitOnFill) {
		this.takeProfitOnFill = takeProfitOnFill;
	}

	public StopLossDetails getStopLossOnFill() {
		return stopLossOnFill;
	}

	public void setStopLossOnFill(StopLossDetails stopLossOnFill) {
		this.stopLossOnFill = stopLossOnFill;
	}

	public TrailingStopLossDetails getTrailingStopLossOnFill() {
		return trailingStopLossOnFill;
	}

	public void setTrailingStopLossOnFill(TrailingStopLossDetails trailingStopLossOnFill) {
		this.trailingStopLossOnFill = trailingStopLossOnFill;
	}

	public ClientExtensions getTradeClientExtensions() {
		return tradeClientExtensions;
	}

	public void setTradeClientExtensions(ClientExtensions tradeClientExtensions) {
		this.tradeClientExtensions = tradeClientExtensions;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}
}
