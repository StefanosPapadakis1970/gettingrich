package rawai.it.com.TradingPlatform.price;

import java.io.IOException;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PriceStreamRunnable implements Runnable {

	private static final Logger log = LogManager.getLogger(PriceStreamRunnable.class);
	private String userID;
	private String userName;
	private PriceStreamService priceStreamService;
	private CloseableHttpClient httpClient;
	private PriceRunner priceRunner;
	private List<String> instruments;
	private String sessionID;

	public PriceStreamRunnable(String userID, String userName, PriceStreamService priceStreamService, PriceRunner priceRunner, List<String> instruments, String sessionID) {
		this.instruments = instruments;
		this.userID = userID;
		this.userName = userName;
		this.priceStreamService = priceStreamService;
		this.priceRunner = priceRunner;
		this.sessionID = sessionID;
	}

	public boolean closeStream() {
		try {
			httpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void run() {
		priceStreamService.streamPrices(userID, userName, this, instruments);
	}

	public CloseableHttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(CloseableHttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public PriceRunner getPriceRunner() {
		return priceRunner;
	}

	public void setPriceRunner(PriceRunner priceRunner) {
		this.priceRunner = priceRunner;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
}
