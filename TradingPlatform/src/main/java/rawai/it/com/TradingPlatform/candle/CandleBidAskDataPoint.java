package rawai.it.com.TradingPlatform.candle;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandleBidAskDataPoint implements CandleDataPointInterface {

	private CandleBidAsk candle;

	private int candleIndex;

	private boolean isFirstcandleOfTheDay;

	// TODO Bid Ask
	public String getY() {
		return "[" + candle.getOpenAsk() + "," + candle.getHighAsk() + "," + candle.getLowAsk() + "," + candle.getCloseAsk() + "]";
	}


	public String getTime() {
		return candle.getTime();
	}

	@Override
	public boolean getComplete() {
		return candle.isComplete();
	}

	@Override
	public String getO() {
		return candle.getOpenAsk();
	}

	@Override
	public String getH() {
		return candle.getHighAsk();
	}

	@Override
	public String getC() {
		return candle.getCloseAsk();
	}

	@Override
	public String getL() {
		return candle.getLowAsk();
	}

	public CandleBidAsk getCandle() {
		return candle;
	}

	public void setCandle(CandleBidAsk candle) {
		this.candle = candle;
	}

	@Override
	public Long getX() {
		return Instant.parse(candle.getTime()).toEpochMilli();
	}

	@Override
	public int getCandleIndex() {
		return candleIndex;
	}

	@Override
	public void setCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
	}

	@Override
	public boolean isTheFirstCandleOfTheDay() {
		return isFirstcandleOfTheDay;
	}

	@Override
	public void setTheFirstCandleOfTheDay(boolean firstCandleOfTheDay) {
		this.isFirstcandleOfTheDay = firstCandleOfTheDay;
	}
}
