package rawai.it.com.TradingPlatform.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OandaAccountResponse extends AccountResponse {

	private OandaAccount account;

	public OandaAccount getAccount() {
		return account;
	}

	public void setAccount(OandaAccount account) {
		this.account = account;
	}
}
