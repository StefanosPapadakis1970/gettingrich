package rawai.it.com.TradingPlatform.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
@Entity
@Table(name = "account")
@NamedQueries({ @NamedQuery(name = Account.FIND_BY_EMAIL, query = "select a from Account a where a.email = :email"),
		@NamedQuery(name = Account.DELETE_BY_ID, query = "delete from Account a where a.id = :id"),
		@NamedQuery(name = Account.FIND_BY_USERID, query = "select a from Account a where a.userId = :userId"),
		@NamedQuery(name = Account.FIND_BY_ID, query = "select a from Account a where a.id = :id") })
public class Account implements java.io.Serializable {

	public static final String FIND_BY_EMAIL = "Account.findByEmail";
	public static final String FIND_BY_USERID = "Account.findByUserId";
	public static final String DELETE_BY_ID = "Account.deleteByID";
	public static final String FIND_BY_ID = "Account.findByID";


	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String email;
	
	@JsonIgnore
	private String password;

	private String role = "ROLE_USER";

	private String apiKey;

	private String userName;

	private String userId;

	@Transient
	private CurrencyEnum currency;

	public Account() {

	}
	
	public Account(String email, String password, String role, String apiKey, String userName, String userId) {
		super();
		this.email = email;
		this.password = password;
		this.role = role;
		this.apiKey = apiKey;
		this.userName = userName;
		this.userId = userId;
	}

	public Account(String role, String apiKey, String userName, String userId, String dbID) {
		super();
		this.role = role;
		this.apiKey = apiKey;
		this.userName = userName;
		this.userId = userId;
		this.id = Long.valueOf(dbID);
	}

	public Account(String role, String apiKey, String userName, String userId) {
		super();
		this.role = role;
		this.apiKey = apiKey;
		this.userName = userName;
		this.userId = userId;
	}



	public Long getId() {
		return id;
	}

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CurrencyEnum getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyEnum currency) {
		this.currency = currency;
	}
}
