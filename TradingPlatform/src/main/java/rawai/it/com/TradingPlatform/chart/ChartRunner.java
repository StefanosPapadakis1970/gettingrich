package rawai.it.com.TradingPlatform.chart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.instrument.InstrumentController;

public class ChartRunner implements Runnable {

	private static final Logger logger = LogManager.getLogger(ChartRunner.class);
	private String instrument;
	private String interval;
	private String userID;
	private String sessionID;
	private boolean stop = false;
	private InstrumentController instrumentController;


	public ChartRunner(String instrument, String interval, String userID, InstrumentController instrumentController, String sessionID) {
		super();
		this.instrument = instrument;
		this.interval = interval;
		this.userID = userID;
		this.sessionID = sessionID;
		this.instrumentController = instrumentController;
	}

	@Override
	public void run() {
		while (!stop) {
			synchronized (this) {
				String candle = instrumentController.fetchCandleForChartWindow(userID, instrument, interval);
				instrumentController.sentCandleToChart(instrument, interval, candle);
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// Maybee send errorCandle???
				// e.printStackTrace();
			}
		}
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

}
