package rawai.it.com.TradingPlatform.engine.postsignals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;
import rawai.it.com.TradingPlatform.utils.TradingUtils;

public class DynamicStopLossPostTradingSignal extends AbstractPostTradingSignal {
	private static final Logger logger = LogManager.getLogger("tradingengine-log");

	public DynamicStopLossPostTradingSignal(int value) {
		setValue(value);
		setPostTradeSignalEnum(PostTradingSignalEnum.DYNAMIC_STOPLOSS);
		setDynamic(true);
	}

	@Override
	public void postProcessTrade(AlgorithmTrade trade) {

	}

	@Override
	public void dynamicPostProcessTrade(AlgorithmTrade trade, Double price) {
		Double pip = TradingUtils.calculateOnePip(price);		
		logger.info("PL of Trade is "+trade.getPl()+" Beginn of Dynamic PostProcess BuySell: " + trade.isBuySell() + " Trade ID " + trade.getTradeID() + " DBid " + trade.getDbID() + " UUID " + trade.getUuid());
		logger.info("Trade Price "+trade.getPrice());
		logger.info("Current Price "+price);
		logger.info("PIPS "+pip);
		logger.info("Trade Price + Pipval "+trade.getPrice() + (pip * getValue()));
		
		if (trade.getStopLossPrice() == null || trade.getTakeProfitPrice() == null) {
			return;
		}

		if (trade.isBuySell()) {
			if (price > trade.getStopLossPrice() && price > trade.getPrice() && price > trade.getPrice() + (pip * getValue())
					&& price < trade.getTakeProfitPrice()) {
				logger.info("POSTSIGNAL STOP LOS " + trade.getStopLossPrice() + " NEW VALUE buy " + price);
				trade.setStopLossPrice(price);
				trade.setStopLossMoved(true);
				logger.info("StopLoss moved "+trade.isStopLossMoved());			
			}
		} else {
			if (price < trade.getStopLossPrice() && price < trade.getPrice() && price < trade.getPrice() - (pip * getValue())
					&& price > trade.getTakeProfitPrice()) {
				logger.info("POSTSIGNAL STOP LOS " + trade.getStopLossPrice() + " NEW VALUE sell " + price);
				trade.setStopLossPrice(price);
				trade.setStopLossMoved(true);
				logger.info("StopLoss moved "+trade.isStopLossMoved());
			}
		}
		logger.info("End of PostProcessing -> StopLoss moved "+trade.isStopLossMoved());
	}	
}
