package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.Mid;

public class CandleBuilder {
	
	private boolean complete;
	private String time;
	private String volume;
	private Mid mid;
	private int candleIndex;

	private CandleBuilder(){
		
	}
	
	public static CandleBuilder aCandle() {
        return new CandleBuilder();
    }
	
	public CandleBuilder withCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
		return this;
	}

	public CandleBuilder withEmptyMid() {
		mid = new Mid();
		mid.setO("0");
		mid.setL("0");
		mid.setC("0");
		mid.setH("0");
		return this;
	}
	
	// tO()getH() getL() getC() + "]";
	public CandleBuilder withFilledMid(String o, String h, String l, String c) {
		mid = new Mid();
		mid.setO(o);
		mid.setH(h);
		mid.setL(l);
		mid.setC(c);
		return this;
	}

	public CandleBuilder isComplete(boolean complete){
		this.complete = complete;
		return this;
	}
	
	public CandleBuilder withTime(String time){
		this.time = time;
		return this;
	}
	
	public CandleBuilder withVolume(String volume){
		this.volume = volume;
		return this;
	}
	
	
	public  Candle build() {
		Candle candle = new Candle();
		candle.setComplete(complete);
		candle.setTime(time);
		candle.setVolume(volume);
		candle.setMid(mid);
		candle.setCandleIndex(candleIndex);
		return candle;
    }

	public CandleDataPoint buildDataPoint() {
		CandleDataPoint candle = new CandleDataPoint();
		candle.setCandle(build());
		return candle;
	}

}
