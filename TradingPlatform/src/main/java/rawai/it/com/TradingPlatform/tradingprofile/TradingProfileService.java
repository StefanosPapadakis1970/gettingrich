package rawai.it.com.TradingPlatform.tradingprofile;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rawai.it.com.TradingPlatform.DTO.ProfileDTO;
import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.TradingProfileConverter;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Service
public class TradingProfileService {

	@Autowired
	private TradingRepository tradingRepository;

	public List<ProfileDTO> getAllTradingProfileDTOs(boolean isMassTrade) {
		List<TradingProfileEntity> profiles = tradingRepository.findAllProfiles();		
		
		for (TradingProfileEntity profile : profiles) {
			profile.initMassTrade();
		}

		List<ProfileDTO> resultList = new ArrayList<>();

		for (TradingProfileEntity profile : profiles) {
			Account account = SessionUtils.findAccountByUserId(profile.getUserID());
			ProfileDTO profileDTO = TradingProfileConverter.contvertTOProfileDTO(profile, account.getUserName());

			profileDTO.setUserName(SessionUtils.findAccountByUserId(profileDTO.getUserID()).getUserName());

			resultList.add(profileDTO);
		}
		return resultList;
	}
	
	public List<String> getAllTradingInstruments() {
		List<TradingProfile> profiles = new ArrayList<>();
		if (SessionUtils.getTradingProfiles() == null || SessionUtils.getTradingProfiles().size() == 0) {
			for (TradingProfileEntity profileEntity : tradingRepository.findAllProfiles()) {
				profiles.add(TradingProfileConverter.convertToTradingProfile(profileEntity, SessionUtils.findAccountByUserId(profileEntity.getUserID())));
			}
			//SessionUtils.setTradingProfiles(profiles);
		}

		List<String> instruments = new ArrayList<>();

		for (TradingProfile profile : profiles) {
			for (TradingInstrument instrument : profile.getTradingInstruments()) {
				if (!instruments.contains(instrument.getInstument())) {
					instruments.add(instrument.getInstument());
				}
			}
		}
		return instruments;
	}

}
