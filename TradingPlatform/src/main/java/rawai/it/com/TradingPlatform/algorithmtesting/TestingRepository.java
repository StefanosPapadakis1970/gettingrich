package rawai.it.com.TradingPlatform.algorithmtesting;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rawai.it.com.TradingPlatform.engine.db.TestRunEntity;

@Repository
@Transactional(readOnly = false)
public class TestingRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public TestEntity save(TestEntity test) {
		entityManager.persist(test);
		return test;
	}

	@Transactional
	public TestEntity update(TestEntity test) {
		test = entityManager.merge(test);
		return test;
	}

	public TestEntity findByID(Long testID) {
		TestEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(TestEntity.FIND_BY_TEST_ID, TestEntity.class).setParameter("testID", testID).getSingleResult();
			Hibernate.initialize(resultEntity.getAlgorithm());
			Hibernate.initialize(resultEntity.getAlgorithm().getTradingprofiles());
			Hibernate.initialize(resultEntity.getAlgorithm().getTradingSignals());
			Hibernate.initialize(resultEntity.getAlgorithm().getPostTradingSignals());
			Hibernate.initialize(resultEntity.getTestRuns());
			for (TestRunEntity run : resultEntity.getTestRuns()) {
				Hibernate.initialize(run.getTrades());
			}
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public TestEntity findByIDLazy(Long testID) {
		TestEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(TestEntity.FIND_BY_TEST_ID, TestEntity.class).setParameter("testID", testID).getSingleResult();
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public void deleteByID(Long testID) {
		TestEntity testEntity = findByID(testID);
		entityManager.remove(testEntity);
	}

	@Transactional
	public void deleteTestEntity(TestEntity testEntity) {
		testEntity = findByID(testEntity.getId());
		entityManager.remove(testEntity);
	}

	public List<TestEntity> findAll() {
		return entityManager.createNamedQuery(TestEntity.FIND_ALL, TestEntity.class).getResultList();
	}

}
