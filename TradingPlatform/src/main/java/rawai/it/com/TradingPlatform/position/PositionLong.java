package rawai.it.com.TradingPlatform.position;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PositionLong {

	private String pl;
	private String averagePrice;
	private String resettablePL;
	private String units;
	private String unrealizedPL;
	private List<String> tradeIDs;

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getResettablePL() {
		return resettablePL;
	}

	public void setResettablePL(String resettablePL) {
		this.resettablePL = resettablePL;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getUnrealizedPL() {
		return unrealizedPL;
	}

	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public String getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(String averagePrice) {
		this.averagePrice = averagePrice;
	}

	public List<String> getTradeIDs() {
		return tradeIDs;
	}

	public void setTradeIDs(List<String> tradeIDs) {
		this.tradeIDs = tradeIDs;
	}
}
