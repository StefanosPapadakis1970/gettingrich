package rawai.it.com.TradingPlatform.databuilders;

import java.util.List;

import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;

public class TradingAlgorithmEntityBuilder {
	
	private Long id;
	private String name;
	private List<TradingSignalEntity> tradingSignals;
	private TradingAlgorithmEntity algorithm = new TradingAlgorithmEntity();
	private boolean buy;

	private TradingAlgorithmEntityBuilder() {

	}

	public static TradingAlgorithmEntityBuilder aTradingAlgorithmEntityBuilder() {
		return new TradingAlgorithmEntityBuilder();
	}

	public TradingAlgorithmEntityBuilder withID(long id) {
		this.id = id;
		return this;
	}

	public TradingAlgorithmEntityBuilder withTradingSignals(List<TradingSignalEntity> tradingSignals) {
		this.tradingSignals = tradingSignals;
		return this;
	}


	public TradingAlgorithmEntityBuilder withBuy(boolean buy) {
		this.buy = buy;
		return this;
	}

	public TradingAlgorithmEntityBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public TradingAlgorithmEntity build() {
		algorithm.setId(id);
		algorithm.setTradingSignals(tradingSignals);
		algorithm.setName(name);
		algorithm.setBuy(buy);
		return algorithm;
	}

	public TradingAlgorithmEntity buildClean() {
		TradingAlgorithmEntity algorithm = new TradingAlgorithmEntity();
		algorithm.setId(id);
		algorithm.setTradingSignals(tradingSignals);
		algorithm.setName(name);
		algorithm.setBuy(buy);
		return algorithm;
	}
}

