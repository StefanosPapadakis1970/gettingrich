package rawai.it.com.TradingPlatform.instrument;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.candle.CandleListResponse;

public class RequestRester {
	private static final Logger logger = LogManager.getLogger(RequestRester.class);

	private DummyClass dummyGlobal = new DummyClass();

	public void request() {

		ModelAndView resultView = new ModelAndView("/chart/chart");
		String apiKey = "4711";
		HttpEntity<Object> apiHeader = null;// CurlUtil.createAPIHeader(apiKey);
		RestTemplate restTemplate = new RestTemplate();

		DummyClass dummy = new DummyClass();
		logger.info(dummy.getDummyResult());

		ResponseEntity<CandleListResponse> resp = null;
		try {
			resp = restTemplate.exchange("https://api-fxpractice.oanda.com/v3/instruments/candles?count=5&granularity=", HttpMethod.GET, apiHeader, CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			e1.printStackTrace();
		}

		logger.info("Response " + resp);
	}

}
