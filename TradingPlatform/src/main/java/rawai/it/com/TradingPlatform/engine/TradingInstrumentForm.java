package rawai.it.com.TradingPlatform.engine;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import rawai.it.com.TradingPlatform.databuilders.TradingInstrumentBuilder;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class TradingInstrumentForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	@NotBlank(message = TradingInstrumentForm.NOT_BLANK_MESSAGE)
	@Pattern(regexp = "^[1-9][0-9]*", message = "Not a valid number (>0)")
	public String units = "0";

	@Pattern(regexp = "^[A-Z]{3}[_][A-Z]{3}", message = "Not a valid instrument (EUS_AUD)")
	@NotBlank(message = TradingInstrumentForm.NOT_BLANK_MESSAGE)
	public String instrument;

	public String dbID;

	@NotBlank(message = TradingInstrumentForm.NOT_BLANK_MESSAGE)
	@Pattern(regexp = "^[A-Z]{1}[0-9]{0,2}", message = "Not a valid Interval (S5, M10)")
	public String tradingInterval;

	public String userName;

	public boolean massTrade;

	public boolean tradingEngine;

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public TradingInstrument createTradingInstrument() {
		return TradingInstrumentBuilder.aTradingInstrumentEntityBuilder().withInstrument(instrument).withUnits(Integer.valueOf(units))
				.withInterval(IntervalEnum.getValue(tradingInterval)).withMassTrade(massTrade).withTradingEngine(tradingEngine)
				.withID((dbID == null || dbID.equals("")) ? null : Long.valueOf(dbID)).build();
	}

	public String getDbID() {
		return dbID;
	}

	public void setDbID(String dbID) {
		this.dbID = dbID;
	}

	public void setTradingInterval(String tradingInterval) {
		this.tradingInterval = tradingInterval;
	}

	public String getTradingInterval() {
		return tradingInterval;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isMassTrade() {
		return massTrade;
	}

	public void setMassTrade(boolean massTrade) {
		this.massTrade = massTrade;
	}

	public boolean isTradingEngine() {
		return tradingEngine;
	}

	public void setTradingEngine(boolean tradingEngine) {
		this.tradingEngine = tradingEngine;
	}
}
