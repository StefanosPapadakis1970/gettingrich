package rawai.it.com.TradingPlatform.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.CurrencyEnum;
import rawai.it.com.TradingPlatform.account.OandaAccountSummaryResponse;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Service
public class DashBoardService {

	@Autowired
	private CurlUtil curlUtil;

	public List<String> checkAccountsCurrencies() {
		RestTemplate tmpl = new RestTemplate();
		ModelAndView resultView = new ModelAndView("/account/accountdetails");
		List<String> resultList = new ArrayList<String>();
		for (Account account : SessionUtils.getAccounts()) {

			String apiKey = account.getApiKey();
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);

			ResponseEntity<OandaAccountSummaryResponse> resp = tmpl.exchange(curlUtil.createAccountUrl() + "/" + account.getUserId() + "/summary", HttpMethod.GET, apiHeader,
					OandaAccountSummaryResponse.class);
			if (!resp.getBody().getAccount().getCurrency().equals("USD")) {
				resultList.add(account.getUserName() + " uses " + resp.getBody().getAccount().getCurrency());
			}
		}
		return resultList;
	}

	public void prepareAccountCurrencies() {
		for (Account account : SessionUtils.getAccounts()) {

			String apiKey = account.getApiKey();
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
			RestTemplate tmpl = new RestTemplate();

			ResponseEntity<OandaAccountSummaryResponse> resp = tmpl.exchange(curlUtil.createAccountUrl() + "/" + account.getUserId() + "/summary", HttpMethod.GET, apiHeader,
					OandaAccountSummaryResponse.class);
			account.setCurrency(CurrencyEnum.valueOf(resp.getBody().getAccount().getCurrency()));

		}
	}

}
