package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class TradingInstrumentBuilder {

	private Long id;

	private int units;

	private String instument;

	private TradingProfile tradingprofile;

	private IntervalEnum tradingInterval;

	private boolean massTrade;

	private boolean tradingEngine;

	private TradingInstrumentBuilder() {

	}

	public static TradingInstrumentBuilder aTradingInstrumentEntityBuilder() {
		return new TradingInstrumentBuilder();
	}

	public TradingInstrumentBuilder withID(Long id) {
		this.id = id;
		return this;
	}

	public TradingInstrumentBuilder withInterval(IntervalEnum interval) {
		this.tradingInterval = interval;
		return this;
	}

	public TradingInstrumentBuilder withUnits(int units) {
		this.units = units;
		return this;
	}

	public TradingInstrumentBuilder withInstrument(String instrument) {
		this.instument = instrument;
		return this;
	}

	public TradingInstrumentBuilder withTradingProfile(TradingProfile profile) {
		this.tradingprofile = profile;
		return this;
	}

	public TradingInstrument build() {
		TradingInstrument instrument = new TradingInstrument();
		if (id != null) {
			instrument.setDbID(id);
		}
		instrument.setTradingprofile(tradingprofile);
		instrument.setInstument(instument);
		instrument.setUnits(units);
		instrument.setTradingInterval(tradingInterval);
		instrument.setMassTrade(massTrade);
		instrument.setTradingEngine(tradingEngine);
		return instrument;
	}

	public TradingInstrumentBuilder withMassTrade(boolean massTrade) {
		this.massTrade = massTrade;
		return this;
	}

	public TradingInstrumentBuilder withTradingEngine(boolean tradingEngine) {
		this.tradingEngine = tradingEngine;
		return this;
	}
}
