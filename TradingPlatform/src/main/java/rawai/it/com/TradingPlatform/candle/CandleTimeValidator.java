package rawai.it.com.TradingPlatform.candle;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameter;
import de.jollyday.ManagerParameters;

public class CandleTimeValidator {

	private static final Logger logger = LogManager.getLogger(CandleTimeValidator.class);
	
	private static HolidayManager newYorkManager;
	private static HolidayManager londonManager;
	 
	{
		ManagerParameter parameter = ManagerParameters.create("nyse");
		newYorkManager = HolidayManager.getInstance(parameter);
		parameter = ManagerParameters.create("gb");
		londonManager = HolidayManager.getInstance(parameter);
	}

	public boolean isValidCandleTime(Instant time) {

		return validTimeinterval(time);
	}

	private boolean validTimeinterval(Instant candleTime) {

		ZoneId euroZone = ZoneId.of("Europe/London");
		ZonedDateTime candleLondonTime = candleTime.atZone(euroZone);
		ZoneId usaZone = ZoneId.of("America/New_York");
		ZonedDateTime candleNewYorkTime = candleTime.atZone(usaZone);
		ZoneId utcZone = ZoneId.of("UTC");


		ArrayList<String> args = new ArrayList<>();
		boolean newYorkHolyDay = newYorkManager.isHoliday(candleNewYorkTime.toLocalDate(), args.toArray(new String[] {}));
		boolean londonHoliday = londonManager.isHoliday(candleLondonTime.toLocalDate(), args.toArray(new String[] {}));
		
		if (newYorkHolyDay || londonHoliday) {
			return false;
		}


		ZonedDateTime londonStartMark = ZonedDateTime.of(candleLondonTime.getYear(), candleLondonTime.getMonthValue(), candleLondonTime.getDayOfMonth(), 8, 0, 0, 0, euroZone);
		ZonedDateTime newYorkStopMark = ZonedDateTime.of(candleNewYorkTime.getYear(), candleNewYorkTime.getMonthValue(), candleNewYorkTime.getDayOfMonth(), 16, 0, 0, 0, usaZone);

		return (londonStartMark.toLocalDateTime().isBefore(candleLondonTime.toLocalDateTime()) || londonStartMark.toLocalDateTime().isEqual(candleLondonTime.toLocalDateTime()))
				&& (newYorkStopMark.toLocalDateTime().isAfter(candleNewYorkTime.toLocalDateTime()) || newYorkStopMark.toLocalDateTime()
						.isEqual(candleNewYorkTime.toLocalDateTime()));
	}

	public ArrayList<Candle> extractValidTimeCandles(List<Candle> candlesToAppend) {
		ArrayList<Candle> resultList = new ArrayList<>();
		for (Candle candle : candlesToAppend) {
			if (validTimeinterval(candle.getTime())) {
				resultList.add(candle);
			}
		}

		return resultList;
	}

	public boolean validTimeinterval(String time) {

		Instant candleTime = Instant.ofEpochMilli(new DateTime(time, DateTimeZone.UTC).getMillis());

		return validTimeinterval(candleTime);
	}
}
