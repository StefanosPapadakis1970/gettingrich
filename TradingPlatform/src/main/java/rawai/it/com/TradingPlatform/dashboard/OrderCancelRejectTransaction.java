package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderCancelRejectTransaction extends BaseOrderCancelTransaction {

	private TransactionRejectReason rejectReason;

	public TransactionRejectReason getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(TransactionRejectReason rejectReason) {
		this.rejectReason = rejectReason;
	}

}
