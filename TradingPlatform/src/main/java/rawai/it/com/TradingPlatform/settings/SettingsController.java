package rawai.it.com.TradingPlatform.settings;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.engine.TradingAlgorithmForm;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.tradingprofile.TradingAlgorithmFormConverter;

@Controller
public class SettingsController {

	@Autowired
	private TradingRepository tradingRepository;

	@RequestMapping(value = "settings", method = RequestMethod.GET)
	public ModelAndView settings(){
		ModelAndView resultView = new ModelAndView("settings/settings");

		List<TradingAlgorithmForm> tradingAlgorithmForms = new ArrayList<>();
		List<TradingAlgorithmEntity> tradingAlgorithms = tradingRepository.findAllTradingAlgorithms();
		for (TradingAlgorithmEntity algorithmEntity : tradingAlgorithms) {
			tradingAlgorithmForms.add(TradingAlgorithmFormConverter.convertEntityToForm(algorithmEntity));
		}
		resultView.addObject("algorithms", tradingAlgorithmForms);

		return resultView;
	}


}
