package rawai.it.com.TradingPlatform.utils;


public class TradingUtils {

	public static Double calculateOnePip(Double price) {
		String priceString = price.toString();
		int exponent = priceString.indexOf('.') - 6;
		return Math.pow(10, exponent);
	}

	public static int calculateOnePipExponent(Double price) {
		String priceString = price.toString();
		priceString = priceString.replace("-", "");
		int exponent = priceString.indexOf('.') - 6;
		return exponent;
	}

	public static Double calculateLossProfitPrice(Double price, int pips, boolean buy, boolean takeProfit) {
		double pip = calculateOnePip(price);

		if (!buy) {
			pip = pip * -1;
		}

		double takeProfitPrice = price + (pip * pips);
		double stopLossPrice = price - (pip * pips);

		// DecimalFormat df2 = new DecimalFormat("###,#####");
		// return Double.valueOf(df2.format(takeProfit ? takeProfitPrice :
		// stopLossPrice));

		return takeProfit ? takeProfitPrice : stopLossPrice;
	}

	public static Double callculateDoubleValue(long price, int exponent) {
		boolean addMinus = false;
		if (price < 0) {
			addMinus = true;
		}
		String priceString = String.valueOf(price);
		priceString = priceString.replace("-", "");
		String zeroString = "000000";
		String properPrice = zeroString.substring(0, zeroString.length() - priceString.length()) + priceString;
		if (addMinus) {
			return -1 * Double.valueOf(properPrice.substring(0, properPrice.length() - (-exponent)) + "." + properPrice.substring(properPrice.length() - (-exponent)));
		}
		return Double.valueOf(properPrice.substring(0, properPrice.length() - (-exponent)) + "." + properPrice.substring(properPrice.length() - (-exponent)));
	}
}
