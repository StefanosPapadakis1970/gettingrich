package rawai.it.com.TradingPlatform.engine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.postsignals.PostTradingSignal;
import rawai.it.com.TradingPlatform.engine.signals.SignalResult;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;


public class TradingAlgorithm {

	private static final Logger logger = LogManager.getLogger("tradingengine-log");

	private Long dbID;

	private List<TradingSignal> tradingSignals = new ArrayList<>();
	private List<PostTradingSignal> postTradingSignals = new ArrayList<>();
	private List<AlgorithmTrade> trades = new ArrayList<>();

	private TradingInstrument tradingInstrument;
	private IntervalEnum interval;
	private int lastProcessedCandleIndex = 0;
	private boolean buy;
	private String name;
	private int totalCandlesRequired = 0;
	private int stopLossPips;
	private int takeProfitPips;
	private int openedPositions;
	private int stillOpenPositions;
	private int numberOfTakeProfitTrades;
	private int numberOfStopLossTrades;
	private BigDecimal totalWin;
	private BigDecimal totalLoss;
	private BigDecimal totalProfit;


	// TODO If signal fails because of to less candle has to be checked

	public TradingAlgorithm(List<TradingSignal> tradingSignals, IntervalEnum interval) {
		super();
		this.tradingSignals = tradingSignals;
		this.interval = interval;
	}

	public TradingAlgorithm() {
		// TODO Auto-generated constructor stub
	}

	public int getTotalNumberOfCandlesRequired() {
		return tradingSignals.size() * 5;
	}

	public List<TradingSignal> getTradingSignals() {
		return tradingSignals;
	}

	public void setTradingSignals(List<TradingSignal> tradingSignals) {
		this.tradingSignals = tradingSignals;
	}

	public synchronized AlgorithmTrade generateTrade(List<CandleDataPointInterface> candles, boolean backTesting) {

		AlgorithmTrade trade = new AlgorithmTrade();
		trade.setTrade(false);

		SignalResult result = new SignalResult(false, 0, false);

		logger.info("Number of Candles " + candles.size() + " lastProcessed Candle " + lastProcessedCandleIndex + " Object ID of Algorithm " + this);
		for (TradingSignal tradingSignal : tradingSignals) {
			logger.info(tradingSignal.getDBID() +" "+tradingSignal.getTradingSignalEnum().name() + " " + tradingSignal.isSuccess());
		}


		TradingSignal currentSignal = null;

		int signalIndex = 0;
		for (TradingSignal tradingSignal : tradingSignals) {
			if (!tradingSignal.isSuccess()) {
				currentSignal = tradingSignal;
				break;
			}
			signalIndex++;
		}

		int i = lastProcessedCandleIndex;
		int candleStart = tradingSignals.get(0).getStartCandleIndex();
		
		
		
		logger.info("First algorithm Candle " + candleStart);
		for (int j = candleStart; j < candles.size(); j++) {
			if (i == lastProcessedCandleIndex) {
				logger.info(name + " -> time " + candles.get(j).getTime() + "CandleIndex " + candles.get(j).getCandleIndex() + "  - Candle  " + candles.get(j).getC()
						+ "  <<<  " + lastProcessedCandleIndex);
			} else {
				logger.info(name + " -> time " + candles.get(j).getTime() + "CandleIndex " + candles.get(j).getCandleIndex() + "  - Candle  " + candles.get(j).getC()
						+ "        " + lastProcessedCandleIndex);
			}
			i++;
			if (i > lastProcessedCandleIndex + 10) {
				break;
			}
		}

		logger.info("Number of Candles before processing " + candles.size() + " lastProcessed Candle " + lastProcessedCandleIndex + " Object ID of Algorithm " + this);
		if (currentSignal == null) {
			logger.info("------------- Current Signal is null ----- signalindex " + signalIndex + " ----------" + tradingSignals.size() + " Successcounter " + signalsSecceeded());
			for (TradingSignal tradingSignal : tradingSignals) {
				logger.info(tradingSignal.getTradingSignalEnum().name() + " " + tradingSignal.isSuccess());
			}
		}

		result = currentSignal.processCandles(candles, lastProcessedCandleIndex);
		lastProcessedCandleIndex = result.getCandleIndex();
		logger.info("Evaluating Signal " + currentSignal.getTradingSignalEnum().getName() + " index " + signalIndex + " " + currentSignal.getValue() + " " + currentSignal.getLow()
				+ " " + currentSignal.getHigh() + " -> result " + result.isSuccess() + " newIndex " + lastProcessedCandleIndex + "-->Successcounter " + signalsSecceeded());


		// TOTO Maybe have to change resetbehaviour
		// Maybee in the signals
		if (backTesting && candles.get(lastProcessedCandleIndex).isTheFirstCandleOfTheDay()) {
			logger.info("First Candle. of the day reached ---> Resetting algorithm " + candles.get(lastProcessedCandleIndex).getTime() + " Last CandleIndex is mow "
					+ lastProcessedCandleIndex);
			resetAlgorithmForBackTesting();
			trade.setTrade(false);
			trade.setResetCandles(true);
			return trade;
		}

		if (!backTesting && candles.size()-1 >= lastProcessedCandleIndex && candles.get(lastProcessedCandleIndex).isTheFirstCandleOfTheDay()) {
			logger.info("First Candle. of the day reached ---> Resetting algorithm " + candles.get(lastProcessedCandleIndex).getTime() + " Last CandleIndex is mow "
					+ lastProcessedCandleIndex);
			resetAlgorithmForBackTesting();
			trade.setTrade(false);
			trade.setResetCandles(false);
			return trade;
		}

		if (!result.isSuccess()) {
			logger.info("Signal failed -> ");
			trade.setTrade(false);
			trade.setTradingCandleIndex(result.getCandleIndex());
			currentSignal.setSuccess(false);

			// If not the first signal fails then also reset
			// If had enough candles to run
			if ((signalIndex > 0 && !result.isNotEnoughCandles()) || result.isResetAnyWay()) {
				logger.info("Signal failed -> Reset");
				trade.setResetCandles(true);
			}
			return trade;
		}

		currentSignal.setSuccess(true);

		if (signalsSecceeded() >= tradingSignals.size()) {
			trade.setTrade(true);
			trade.setBuySell(buy);
			trade.setTradingCandleIndex(result.getCandleIndex());
			trade.setResetCandles(true);
			logger.info("Trade initiated successcounter " + signalsSecceeded());
		}

		return trade;
	}

	private int signalsSecceeded() {
		int signalsSucceed = 0;
		for (TradingSignal tradingSignal : tradingSignals) {
			if (tradingSignal.isSuccess()) {
				signalsSucceed++;
			}
		}
		return signalsSucceed;
	}

	public void resetAlgorithm() {
		lastProcessedCandleIndex = 0;
		for (TradingSignal tradingSignal : tradingSignals) {
			tradingSignal.setSuccess(false);
		}
	}

	public void resetAlgorithmForBackTesting() {
		for (TradingSignal tradingSignal : tradingSignals) {
			tradingSignal.setSuccess(false);
		}
		logger.info("All Signals Resetted  success to false");
	}

	public IntervalEnum getInterval() {
		return interval;
	}

	public void setInterval(IntervalEnum interval) {
		this.interval = interval;
	}



	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long dbID) {
		this.dbID = dbID;
	}

	public boolean isBuy() {
		return buy;
	}

	public boolean getBuy() {
		return buy;
	}

	public void setBuy(boolean buye) {
		this.buy = buye;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TradingInstrument getTradingInstrument() {
		return tradingInstrument;
	}

	public void setTradingInstrument(TradingInstrument tradingInstrument) {
		this.tradingInstrument = tradingInstrument;
	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public void postProcessTrade(AlgorithmTrade trade) {
		for (PostTradingSignal postTradingSignal : postTradingSignals) {
			if (!postTradingSignal.isDynamic()) {
				postTradingSignal.postProcessTrade(trade);
			}
		}
	}

	public void dynamicPostProcessTrades(AlgorithmTrade trade, Double price) {
		for (PostTradingSignal postTradingSignal : postTradingSignals) {
			if (postTradingSignal.isDynamic()) {
				logger.info("########### Trades before PostProcessing #################");
				logger.info("Trade StopLossMoved "+trade.isStopLossMoved());
				postTradingSignal.dynamicPostProcessTrade(trade, price);
				logger.info("########### Trades After PostProcessing #################");
				logger.info("Trade StopLossMoved "+trade.isStopLossMoved());
			}
		}
	}

	public List<PostTradingSignal> getPostTradingSignals() {
		return postTradingSignals;
	}

	public void setPostTradingSignals(List<PostTradingSignal> postTradingSignals) {
		this.postTradingSignals = postTradingSignals;
	}

	public boolean hasDynamicPostSignals() {
		for (PostTradingSignal postTradingSignal : postTradingSignals) {
			if (postTradingSignal.isDynamic()) {
				return true;
			}
		}
		return false;
	}

	public List<AlgorithmTrade> getTrades() {
		return trades;
	}

	public void setTrades(List<AlgorithmTrade> trades) {
		this.trades = trades;
	}

	public int getLastProcessedCandleIndex() {
		return lastProcessedCandleIndex;
	}

	public void setLastProcessedCandleIndex(int lastProcessedCandleIndex) {
		this.lastProcessedCandleIndex = lastProcessedCandleIndex;
	}

	public int getTotalCandlesRequired() {
		return totalCandlesRequired;
	}

	public void setTotalCandlesRequired(int totalCandlesRequired) {
		this.totalCandlesRequired = totalCandlesRequired;
	}

	public int getOpenedPositions() {
		return openedPositions;
	}

	public void setOpenedPositions(int openedPositions) {
		this.openedPositions = openedPositions;
	}

	public int getStillOpenPositions() {
		return stillOpenPositions;
	}

	public void setStillOpenPositions(int stillOpenPositions) {
		this.stillOpenPositions = stillOpenPositions;
	}

	public int getNumberOfTakeProfitTrades() {
		return numberOfTakeProfitTrades;
	}

	public void setNumberOfTakeProfitTrades(int numberOfTakeProfitTrades) {
		this.numberOfTakeProfitTrades = numberOfTakeProfitTrades;
	}

	public int getNumberOfStopLossTrades() {
		return numberOfStopLossTrades;
	}

	public void setNumberOfStopLossTrades(int numberOfStopLossTrades) {
		this.numberOfStopLossTrades = numberOfStopLossTrades;
	}

	public BigDecimal getTotalWin() {
		return totalWin;
	}

	public void setTotalWin(BigDecimal totalWin) {
		this.totalWin = totalWin;
	}

	public BigDecimal getTotalLoss() {
		return totalLoss;
	}

	public void setTotalLoss(BigDecimal totalLoss) {
		this.totalLoss = totalLoss;
	}

	public BigDecimal getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(BigDecimal totalProfit) {
		this.totalProfit = totalProfit;
	}
}
