package rawai.it.com.TradingPlatform.engine.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;

@Repository
@Transactional(readOnly = false)
public class AlgorithmTradeRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public void save(AlgorithmTradeEntity algorithmTrade) {
		entityManager.persist(algorithmTrade);
	}

	@Transactional
	public List<AlgorithmTradeEntity> findAllTrades() {
		Query query = entityManager.createQuery("SELECT trade FROM AlgorithmTradeEntity trade");
		return (List<AlgorithmTradeEntity>) query.getResultList();
	}

	@Transactional
	public AlgorithmTradeEntity findAlgorithmByUUID(String uuid) {
		AlgorithmTradeEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_BY_UUID, AlgorithmTradeEntity.class).setParameter("uuid", uuid).getSingleResult();
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	@Transactional
	public AlgorithmTradeEntity findAlgorithmByID(Long tradeID) {
		AlgorithmTradeEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_BY_ID, AlgorithmTradeEntity.class).setParameter("id", tradeID).getSingleResult();
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public List<CandleEntity> fetchCandlesForTradeID(Long tradeID) {
		return entityManager.createNamedQuery(CandleEntity.FIND_BY_TRADE_ID, CandleEntity.class).setParameter("id", tradeID).getResultList();
	}

	@Transactional
	public AlgorithmTradeEntity findAlgorithmByRequestID(String requestID) {
		AlgorithmTradeEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_BY_REQUESTID, AlgorithmTradeEntity.class).setParameter("requestID", requestID)
					.getSingleResult();
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public AlgorithmTradeEntity findAlgorithmTradeByUserIdTradeID(String userID, String tradeID) {
		AlgorithmTradeEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_BY_USERID_TRADEID, AlgorithmTradeEntity.class).setParameter("userID", userID)
					.setParameter("tradeID", tradeID).getSingleResult();
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public void update(AlgorithmTradeEntity algorithmTrade) {
		entityManager.merge(algorithmTrade);
	}

	public List<AlgorithmTradeEntity> findAllEngineTrades() {
		return entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_ALL_ENGINETRADES, AlgorithmTradeEntity.class).getResultList();
	}

	public List<AlgorithmTradeEntity> findAllOpenTradesByInstrument(TradingInstrument instrument) {
		return entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_ALL_OPEN_TRADES, AlgorithmTradeEntity.class).setParameter("instrument", instrument.getInstument())
				.setParameter("tradingInterval", instrument.getTradingInterval()).getResultList();

	}
	
	public List<AlgorithmTradeEntity> findAllOpenTradesByInstrument(String instrument) {
		return entityManager.createNamedQuery(AlgorithmTradeEntity.FIND_ALL_OPEN_TRADES_IGNORE_INTERVAL, AlgorithmTradeEntity.class).setParameter("instrument", instrument).getResultList();

	}
	

}
