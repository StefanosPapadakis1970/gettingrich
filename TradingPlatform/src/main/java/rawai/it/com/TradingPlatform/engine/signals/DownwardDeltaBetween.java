package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class DownwardDeltaBetween extends AbstractTradingSignal {

	private int lowerPips;
	private int upperPips;



	public DownwardDeltaBetween(int lowerPips, int upperPips) {
		super(BUYSELL.BUY, TradingSignalEnum.DOWNWARD_DELTA_BETWEEN, 2);
		this.lowerPips = lowerPips;
		this.upperPips = upperPips;
	}


	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}
		CandleDataPointInterface compareCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface loopCandle = candles.get(lastSignalCandleIndex + 1);
		int loop = Integer.valueOf(loopCandle.getC().replace(".", ""));
		int compare = Integer.valueOf(compareCandle.getC().replace(".", ""));
		int diff = loop - compare;
		diff = diff * -1;
		return new SignalResult(diff >= lowerPips && diff <= upperPips, lastSignalCandleIndex + 1, false);
	}

	@Override
	public int getHigh() {
		return upperPips;
	}

	@Override
	public int getLow() {
		return lowerPips;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
