package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public interface TradingSignal {
	
	public static final double PIP = 0.00001;

	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex);

	public BUYSELL getBuySell();

	public void setBuySell(BUYSELL buySell);

	public TradingSignalEnum getTradingSignalEnum();

	public int getHigh();

	public int getLow();

	public int getValue();

	public RangeEnum getCandleRange();

	public Long getDBID();

	public void setDBID(Long dbID);

	public void setTradingalgorithm(TradingAlgorithm tradingalgorithm);

	public void setTradingSignal(TradingSignalEnum tradingSignal);

	void setSuccess(boolean success);

	boolean isSuccess();

	public int getStartCandleIndex();

	public void setStartCandleIndex(int startCandleIndex);

	public int getEndCandleIndex();

	public void setEndCandleIndex(int endCandleIndex);

	public int getSuccessCandleIndex();

	public void setSuccessCandleIndex(int successCandleIndex);
}
