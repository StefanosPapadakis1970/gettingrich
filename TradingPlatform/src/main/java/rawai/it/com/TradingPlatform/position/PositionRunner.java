package rawai.it.com.TradingPlatform.position;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.TradingPlatform.dashboard.DashBoardRunner;

public class PositionRunner implements Runnable {

	private static final Logger logger = LogManager.getLogger(DashBoardRunner.class);
	private boolean stop;
	private PositionController positionController;
	private String sessionID;

	public PositionRunner(PositionController positionController, String sessionID) {
		super();
		this.positionController = positionController;
		this.sessionID = sessionID;
	}

	@Override
	public void run() {
		while (!stop) {
			positionController.sendPositionsToClient();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				logger.info("PositionRunner --> interrupted");
			}
		}
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public PositionController getPositionController() {
		return positionController;
	}

	public void setPositionController(PositionController positionController) {
		this.positionController = positionController;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

}
