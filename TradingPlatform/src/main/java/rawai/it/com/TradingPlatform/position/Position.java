package rawai.it.com.TradingPlatform.position;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.account.TradeSummary;
import rawai.it.com.TradingPlatform.trade.Trade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Position {

	private String instrument;
	private String pl;
	private String resettablePL;
	private String unrealizedPL;
	private PositionLong positionLong;
	private PositionShort positionShort;
	
	private String accountName;
	private String userID;
	private List<Trade> trades = new ArrayList<>();
	private List<TradeSummary> tradesSummaries = new ArrayList<>();
	private List<String> tradesIDs = new ArrayList<>();

	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public String getPl() {
		return pl;
	}
	public void setPl(String pl) {
		this.pl = pl;
	}
	public String getResettablePL() {
		return resettablePL;
	}
	public void setResettablePL(String resettablePL) {
		this.resettablePL = resettablePL;
	}
	public String getUnrealizedPL() {
		return unrealizedPL;
	}
	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public PositionLong getLong() {
		return positionLong;
	}

	public void setLong(PositionLong positionLong) {
		this.positionLong = positionLong;
	}

	public PositionShort getShort() {
		return positionShort;
	}

	public void setShort(PositionShort positionShort) {
		this.positionShort = positionShort;
	}

	public PositionLong getPositionLong() {
		return positionLong;
	}

	public void setPositionLong(PositionLong positionLong) {
		this.positionLong = positionLong;
	}

	public PositionShort getPositionShort() {
		return positionShort;
	}

	public void setPositionShort(PositionShort positionShort) {
		this.positionShort = positionShort;
	}

	public List<Trade> getTrades() {
		return trades;
	}

	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public List<String> getTradesIDs() {
		return tradesIDs;
	}
	public void setTradesIDs(List<String> tradesIDs) {
		this.tradesIDs = tradesIDs;
	}

	public List<TradeSummary> getTradesSummaries() {
		return tradesSummaries;
	}

	public void setTradesSummaries(List<TradeSummary> tradesSummaries) {
		this.tradesSummaries = tradesSummaries;
	}

}
