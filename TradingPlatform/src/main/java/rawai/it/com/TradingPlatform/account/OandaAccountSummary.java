package rawai.it.com.TradingPlatform.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OandaAccountSummary {

	@JsonProperty("NAV")
	private String NAV;
	private String alias;
	private String balance;
	private String createdByUserID;
	private String createdTime;
	private String currency;
	private String hedgingEnabled;
	private String id;
	private String lastTransactionID;
	private String marginAvailable;
	private String marginCloseoutMarginUsed;
	private String marginCloseoutNAV;
	private String marginCloseoutPercent;
	private String marginCloseoutPositionValue;
	private String marginCloseoutUnrealizedPL;
	private String marginRate;
	private String marginUsed;
	private String openPositionCount;
	private String openTradeCount;
	private String pendingOrderCount;
	private String pl;
	private String positionValue;
	private String resettablePL;
	private String unrealizedPL;
	private String withdrawalLimit;

	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getCreatedByUserID() {
		return createdByUserID;
	}
	public void setCreatedByUserID(String createdByUserID) {
		this.createdByUserID = createdByUserID;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getHedgingEnabled() {
		return hedgingEnabled;
	}

	public void setHedgingEnabled(String hedgingEnabled) {
		this.hedgingEnabled = hedgingEnabled;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getLastTransactionID() {
		return lastTransactionID;
	}
	public void setLastTransactionID(String lastTransactionID) {
		this.lastTransactionID = lastTransactionID;
	}

	public String getMarginAvailable() {
		return marginAvailable;
	}

	public void setMarginAvailable(String marginAvailable) {
		this.marginAvailable = marginAvailable;
	}

	public String getMarginCloseoutMarginUsed() {
		return marginCloseoutMarginUsed;
	}

	public void setMarginCloseoutMarginUsed(String marginCloseoutMarginUsed) {
		this.marginCloseoutMarginUsed = marginCloseoutMarginUsed;
	}

	public String getMarginCloseoutNAV() {
		return marginCloseoutNAV;
	}

	public void setMarginCloseoutNAV(String marginCloseoutNAV) {
		this.marginCloseoutNAV = marginCloseoutNAV;
	}

	public String getMarginCloseoutPercent() {
		return marginCloseoutPercent;
	}

	public void setMarginCloseoutPercent(String marginCloseoutPercent) {
		this.marginCloseoutPercent = marginCloseoutPercent;
	}

	public String getMarginCloseoutPositionValue() {
		return marginCloseoutPositionValue;
	}

	public void setMarginCloseoutPositionValue(String marginCloseoutPositionValue) {
		this.marginCloseoutPositionValue = marginCloseoutPositionValue;
	}

	public String getMarginCloseoutUnrealizedPL() {
		return marginCloseoutUnrealizedPL;
	}

	public void setMarginCloseoutUnrealizedPL(String marginCloseoutUnrealizedPL) {
		this.marginCloseoutUnrealizedPL = marginCloseoutUnrealizedPL;
	}

	public String getMarginRate() {
		return marginRate;
	}

	public void setMarginRate(String marginRate) {
		this.marginRate = marginRate;
	}

	public String getMarginUsed() {
		return marginUsed;
	}

	public void setMarginUsed(String marginUsed) {
		this.marginUsed = marginUsed;
	}

	public String getOpenPositionCount() {
		return openPositionCount;
	}

	public void setOpenPositionCount(String openPositionCount) {
		this.openPositionCount = openPositionCount;
	}

	public String getOpenTradeCount() {
		return openTradeCount;
	}

	public void setOpenTradeCount(String openTradeCount) {
		this.openTradeCount = openTradeCount;
	}

	public String getPendingOrderCount() {
		return pendingOrderCount;
	}

	public void setPendingOrderCount(String pendingOrderCount) {
		this.pendingOrderCount = pendingOrderCount;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getPositionValue() {
		return positionValue;
	}

	public void setPositionValue(String positionValue) {
		this.positionValue = positionValue;
	}

	public String getResettablePL() {
		return resettablePL;
	}

	public void setResettablePL(String resettablePL) {
		this.resettablePL = resettablePL;
	}

	public String getUnrealizedPL() {
		return unrealizedPL;
	}

	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public String getWithdrawalLimit() {
		return withdrawalLimit;
	}

	public void setWithdrawalLimit(String withdrawalLimit) {
		this.withdrawalLimit = withdrawalLimit;
	}

	public String getNAV() {
		return NAV;
	}

	public void setNAV(String NAV) {
		this.NAV = NAV;
	}

}
