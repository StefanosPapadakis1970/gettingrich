package rawai.it.com.TradingPlatform.urlutils;

public class SingleInstrument {

	private String instrument;
	private String userID;

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
}
