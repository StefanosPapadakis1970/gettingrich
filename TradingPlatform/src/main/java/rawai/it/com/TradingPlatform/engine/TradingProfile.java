package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;

public class TradingProfile {

	private static final Logger log = LogManager.getLogger(TradingProfile.class);

	private Long dbID;

	private Account account;

	private List<TradingInstrument> tradingInstruments;

	private List<TradingAlgorithm> tradingAlgorithms = new ArrayList<>();

	private HashMap<TradingInstrument, List<CandleDataPointInterface>> candleMap = new HashMap<>();

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public List<TradingInstrument> getTradingInstruments() {
		return tradingInstruments;
	}

	public void setTradingInstruments(List<TradingInstrument> tradingInstruments) {
		this.tradingInstruments = tradingInstruments;
	}

	public List<TradingAlgorithm> getTradingAlgorithms() {
		return tradingAlgorithms;
	}

	public void setTradingAlgorithms(List<TradingAlgorithm> tradingAlgorithms) {
		this.tradingAlgorithms = tradingAlgorithms;
	}

	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long dbID) {
		this.dbID = dbID;
	}

	public TradingInstrument fetchTradingInstrument(TradingInstrument candleInstrument) {
		for (TradingInstrument tradingInstrument : tradingInstruments) {
			if (tradingInstrument.getInstument().equals(candleInstrument.getInstument()) && tradingInstrument.getTradingInterval().equals(candleInstrument.getTradingInterval())) {
				return tradingInstrument;
			}
		}
		return null;
	}

	public void initMassTrade() {
		List<TradingInstrument> delList = new ArrayList<>();
		for (TradingInstrument instrument : tradingInstruments) {
			if (!instrument.isMassTrade()) {
				delList.add(instrument);
			}
		}
		tradingInstruments.removeAll(delList);
	}

	public List<CandleDataPointInterface> fetchCandleList(TradingInstrument instrument) {
		if (candleMap.containsKey(instrument)) {
			return candleMap.get(instrument);
		} else {
			candleMap.put(instrument, new ArrayList<CandleDataPointInterface>());
		}

		return candleMap.get(instrument);
	}

	public void addChandle(TradingInstrument instrument, CandleDataPointInterface candle) {
		List<CandleDataPointInterface> candles = fetchCandleList(instrument);

		// Check for First candle of the day
		if (candles.isEmpty()) {
			candle.setTheFirstCandleOfTheDay(true);
		} else {

			if (!candleWithThatDateAlreadyExists(candle, candles)) {
				candle.setTheFirstCandleOfTheDay(true);
				log.info("First Candle of the Day " + candle.getTime());
			}
		}


		if (insertCandle(fetchCandleList(instrument), candle)) {
			candles.add(candle);
		}

		Collections.sort(fetchCandleList(instrument), new Comparator<CandleDataPointInterface>() {

			@Override
			public int compare(CandleDataPointInterface o1, CandleDataPointInterface o2) {
				return o1.getTime().compareTo(o2.getTime());
			}
		});
	}

	public boolean candleWithThatDateAlreadyExists(CandleDataPointInterface checkCandle, List<CandleDataPointInterface> candles) {

		DateTime checkCandleTime = new DateTime(checkCandle.getTime(), DateTimeZone.UTC);
		for (CandleDataPointInterface candle : candles) {
			DateTime candleTime = new DateTime(candle.getTime(), DateTimeZone.UTC);
			if (candleTime.getYear() == checkCandleTime.getYear() && candleTime.getMonthOfYear() == checkCandleTime.getMonthOfYear()
					&& candleTime.getDayOfMonth() == checkCandleTime.getDayOfMonth()) {
				return true;
			}
		}
		return false;
	}

	private boolean insertCandle(List<CandleDataPointInterface> candles, CandleDataPointInterface candle) {
		for (CandleDataPointInterface loopCandle : candles) {
			if (loopCandle.getTime().equals(candle.getTime())) {
				return false;
			}
		}
		return true;
	}

	public void resetCandles(TradingInstrument tradingInstrument) {
		log.info("##################### PROFILE RESETS CANDLES " + account.getUserId() + "###############################################");
		fetchCandleList(tradingInstrument).clear();
	}

	public void resetAlgorithms(TradingInstrument tradingInstrument) {
		for (TradingAlgorithm algorithm : tradingAlgorithms) {
			if (algorithm.getTradingInstrument().getInstument().equals(tradingInstrument.getInstument())
					&& algorithm.getTradingInstrument().getTradingInterval().equals(tradingInstrument.getTradingInterval())) {
				algorithm.resetAlgorithm();
			}
		}
	}
}
