package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.AttributeConverter;

import rawai.it.com.TradingPlatform.engine.signals.PostTradingSignalEnum;

public class PostTradingSignalEnumConverter implements AttributeConverter<PostTradingSignalEnum, String> {

	@Override
	public String convertToDatabaseColumn(PostTradingSignalEnum attribute) {
		return attribute.name();
	}

	@Override
	public PostTradingSignalEnum convertToEntityAttribute(String dbData) {
		return PostTradingSignalEnum.valueOf(dbData);
	}
}
