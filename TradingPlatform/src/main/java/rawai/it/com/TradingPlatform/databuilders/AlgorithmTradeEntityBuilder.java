package rawai.it.com.TradingPlatform.databuilders;

import java.util.List;

import rawai.it.com.TradingPlatform.algorithmtesting.TestEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.CandleEntity;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class AlgorithmTradeEntityBuilder {

	private Long id;
	private String instrument;
	private IntervalEnum tradingInterval;
	private boolean buySell;
	private int tradingCandleIndex = 0;
	private Long algorithmID;
	private int units;
	private Boolean realTrade;
	private List<CandleEntity> candles;
	private Long accountID;
	private boolean trade;
	private TestEntity test;

	private AlgorithmTradeEntityBuilder() {

	}

	public static AlgorithmTradeEntityBuilder aTrade() {
		return new AlgorithmTradeEntityBuilder();
	}

	public AlgorithmTradeEntityBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public AlgorithmTradeEntityBuilder withInstrument(String instrument) {
		this.instrument = instrument;
		return this;
	}

	public AlgorithmTradeEntityBuilder withTradingInterval(IntervalEnum tradingInterval) {
		this.tradingInterval = tradingInterval;
		return this;
	}

	public AlgorithmTradeEntityBuilder withBuySell(boolean buySell) {
		this.buySell = buySell;
		return this;
	}

	public AlgorithmTradeEntityBuilder withTradingCandleIndex(int tradingCandleIndex) {
		return this;
	}

	public AlgorithmTradeEntityBuilder withAlgorithmID(Long algorithmID) {
		return this;
	}

	public AlgorithmTradeEntityBuilder withUnits(int units) {
		return this;
	}

	public AlgorithmTradeEntityBuilder withRealTrade(Boolean realTrade) {
		return this;
	}

	public AlgorithmTradeEntityBuilder withCandles(List<CandleEntity> candles) {
		this.candles = candles;
		return this;
	}

	public AlgorithmTradeEntityBuilder withAccountID(Long accountID) {
		this.accountID = accountID;
		return this;
	}

	public AlgorithmTradeEntityBuilder withTrade(boolean trade) {
		this.trade = trade;
		return this;
	}

	public AlgorithmTradeEntityBuilder withTest(TestEntity test) {
		this.test = test;
		return this;
	}

	public AlgorithmTradeEntity build() {
		AlgorithmTradeEntity trade = new AlgorithmTradeEntity();
		trade.setAccountID(accountID);
		trade.setAlgorithmID(algorithmID);
		trade.setBuySell(buySell);
		trade.setCandles(candles);
		trade.setId(id);
		trade.setInstrument(instrument);
		trade.setRealTrade(realTrade);
		// TODO 21.6 change DBmodel
		// trade.setTest(test);
		trade.setTrade(this.trade);
		trade.setTradingCandleIndex(tradingCandleIndex);
		trade.setTradingInterval(tradingInterval);
		trade.setUnits(units);
		return trade;

	}
}
