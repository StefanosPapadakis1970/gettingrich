package rawai.it.com.TradingPlatform.engine;

import rawai.it.com.TradingPlatform.instrument.IntervalEnum;



public class TradingInstrument {

	private Long dbID = null;

	private int units;

	private String instument;

	private IntervalEnum tradingInterval;

	private TradingProfile profile;

	private boolean massTrade;

	private boolean tradingEngine;

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String getInstument() {
		return instument;
	}

	public void setInstument(String instument) {
		this.instument = instument;
	}

	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long dbID) {
		this.dbID = dbID;
	}

	public void setTradingprofile(TradingProfile tradingprofile) {
		this.profile = tradingprofile;
	}

	public IntervalEnum getTradingInterval() {
		return tradingInterval;
	}

	public void setTradingInterval(IntervalEnum tradingInterval) {
		this.tradingInterval = tradingInterval;
	}

	public boolean isMassTrade() {
		return massTrade;
	}

	public void setMassTrade(boolean massTrade) {
		this.massTrade = massTrade;
	}

	public boolean isTradingEngine() {
		return tradingEngine;
	}

	public void setTradingEngine(boolean tradingEngine) {
		this.tradingEngine = tradingEngine;
	}
}
