package rawai.it.com.TradingPlatform.engine.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "tradingalgorithm")
@NamedQueries({ @NamedQuery(name = TradingAlgorithmEntity.FIND_BY_NAME, query = "select algorithm from TradingAlgorithmEntity algorithm where algorithm.name = :name"),
		@NamedQuery(name = TradingAlgorithmEntity.FIND_ALL, query = "select algorithm from TradingAlgorithmEntity algorithm"),
		@NamedQuery(name = TradingAlgorithmEntity.FIND_BY_ID, query = "select algorithm from TradingAlgorithmEntity algorithm where algorithm.id = :id") })
public class TradingAlgorithmEntity implements Serializable{

	private static final long serialVersionUID = -7987704292126780519L;

	public static final String FIND_BY_NAME = "TradingAlgorithmEntity.findByName";
	public static final String FIND_ALL = "TradingAlgorithmEntity.findAll";
	public static final String FIND_BY_ID = "TradingAlgorithmEntity.findByID";

	@Id
	@GeneratedValue
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "algorithm_signal", joinColumns = { @JoinColumn(name = "algorithm_id") }, inverseJoinColumns = { @JoinColumn(name = "signal_id") })
	@OrderColumn(name = "signalOrder")
	private List<TradingSignalEntity> tradingSignals = new ArrayList<>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "algorithm_postsignal", joinColumns = { @JoinColumn(name = "algorithm_id") }, inverseJoinColumns = { @JoinColumn(name = "signal_id") })
	private List<PostTradeSignalEntity> postTradingSignals = new ArrayList<>();

	@Column(unique = true)
	private String name;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "profile_algorithm", joinColumns = { @JoinColumn(name = "algorithm_id") }, inverseJoinColumns = { @JoinColumn(name = "profile_id") })
	private List<TradingProfileEntity> tradingprofiles;


	@Column(columnDefinition = "tinyint(1) default 1")
	private boolean buy = false;

	private int stopLossPips;

	private int takeProfitPips;

	public List<TradingSignalEntity> getTradingSignals() {
		return tradingSignals;
	}

	public void setTradingSignals(List<TradingSignalEntity> tradingSignals) {
		this.tradingSignals = tradingSignals;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TradingProfileEntity> getTradingprofiles() {
		return tradingprofiles;
	}

	public void setTradingprofiles(List<TradingProfileEntity> tradingprofiles) {
		this.tradingprofiles = tradingprofiles;
	}

	public boolean isBuy() {
		return buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public List<PostTradeSignalEntity> getPostTradingSignals() {
		return postTradingSignals;
	}

	public void setPostTradingSignals(List<PostTradeSignalEntity> postTradingSignals) {
		this.postTradingSignals = postTradingSignals;
	}
}
