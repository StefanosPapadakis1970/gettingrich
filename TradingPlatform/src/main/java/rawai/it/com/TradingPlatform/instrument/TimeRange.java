package rawai.it.com.TradingPlatform.instrument;

import java.util.Date;

public class TimeRange {

	private Date startDate;
	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public TimeRange(Date startDate, Date endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
