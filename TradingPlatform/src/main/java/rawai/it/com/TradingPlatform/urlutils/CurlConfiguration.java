package rawai.it.com.TradingPlatform.urlutils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CurlConfiguration {

	@Value("${oanda.url}")
	public String PRAXIS_URL = null;
	// @Value("${oanda.streaming.url}")
	public String PRAXIS_STREAMING_URL = null;
	// @Value("${accounts.endpoint}")
	public String ACCOUNTS_ENDPOINT = null;
	// @Value("${candle.stream.endpoint}")
	// public String CANDLE_STREAM_ENDPOINT = null;
	@Value("${candle.stream.endpoint}")
	public static String CANDLE_STREAM_ENDPOINT = null;
	@Value("${dataSource.driverClassName}")
	private String driver;

	public String getPRAXIS_URL() {
		return PRAXIS_URL;
	}

	public void setPRAXIS_URL(String pRAXIS_URL) {
		PRAXIS_URL = pRAXIS_URL;
	}

	public String getPRAXIS_STREAMING_URL() {
		return PRAXIS_STREAMING_URL;
	}

	public void setPRAXIS_STREAMING_URL(String pRAXIS_STREAMING_URL) {
		PRAXIS_STREAMING_URL = pRAXIS_STREAMING_URL;
	}

	public String getACCOUNTS_ENDPOINT() {
		return ACCOUNTS_ENDPOINT;
	}

	public void setACCOUNTS_ENDPOINT(String aCCOUNTS_ENDPOINT) {
		ACCOUNTS_ENDPOINT = aCCOUNTS_ENDPOINT;
	}

	public String getCANDLE_STREAM_ENDPOINT() {
		return CANDLE_STREAM_ENDPOINT;
	}

	public void setCANDLE_STREAM_ENDPOINT(String cANDLE_STREAM_ENDPOINT) {
		CANDLE_STREAM_ENDPOINT = cANDLE_STREAM_ENDPOINT;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
}
