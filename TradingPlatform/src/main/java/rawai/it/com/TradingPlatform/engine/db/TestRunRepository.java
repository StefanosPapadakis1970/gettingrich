package rawai.it.com.TradingPlatform.engine.db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = false)
public class TestRunRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public void save(TestRunEntity testRun) {
		entityManager.persist(testRun);
	}

	@Transactional
	public void deleteTestRun(TestRunEntity testRun) {
		testRun = findById(testRun.getId());
		testRun.setTest(null);
		entityManager.remove(testRun);
	}

	@Transactional
	public void deleteTestRunByID(Long id) {
		TestRunEntity testRun = findById(id);
		testRun.setTest(null);
		entityManager.remove(testRun);
	}

	@Transactional
	public void deleteTestRunData(TestRunEntity testRun) {
		testRun = findById(testRun.getId());
		testRun.getTrades().clear();
		entityManager.merge(testRun);
	}

	@Transactional
	public void deleteTestRunDataByID(Long id) {
		TestRunEntity testRun = findById(id);
		testRun.getTrades().clear();
		entityManager.merge(testRun);
	}

	@Transactional
	public TestRunEntity findById(Long id) {
		TestRunEntity resultEntity = entityManager.createNamedQuery(TestRunEntity.FIND_BY_ID, TestRunEntity.class).setParameter("id", id).getSingleResult();
		return resultEntity;
	}

	@Transactional
	public TestRunEntity findByIdWithCandles(Long id) {
		TestRunEntity resultEntity = entityManager.createNamedQuery(TestRunEntity.FIND_BY_ID, TestRunEntity.class).setParameter("id", id).getSingleResult();
		Hibernate.initialize(resultEntity.getCandles());
		return resultEntity;
	}

	@Transactional
	public List<TestRunEntity> findAllTestRuns() {
		Query query = entityManager.createQuery("SELECT testRun FROM TestRnEntity testRun");
		return (List<TestRunEntity>) query.getResultList();
	}

}
