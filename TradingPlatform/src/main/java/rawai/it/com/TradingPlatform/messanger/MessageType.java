package rawai.it.com.TradingPlatform.messanger;

public enum MessageType {
	BTYPE_DEFAULT("type-default"), TYPE_INFO("type-info"), TYPE_PRIMARY("type-primary"), TYPE_SUCCESS("type-success"), TYPE_WARNING("type-warning"), TYPE_DANGER("type-danger");

	private String type;

	MessageType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
