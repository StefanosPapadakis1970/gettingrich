package rawai.it.com.TradingPlatform.account;

import java.util.List;

import rawai.it.com.TradingPlatform.order.Order;
import rawai.it.com.TradingPlatform.position.Position;
import rawai.it.com.TradingPlatform.trade.Trade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountComplex {

	private String id;
	private String alias;
	private String currency;
	private String balance;
	private String createdByUserID;
	private String createdTime;
	private String pl;
	private String resettablePL;
	private String resettabledPLTime;
	private String marginRate;
	private String marginCallEnterTime;
	private String marginCallExtensionCount;
	private String lastMarginCallExtensionTime;
	private String openTradeCount;
	private String openPositionCount;
	private String pendingOrderCount;
	private String hedgingEnabled;
	private String unrealizedPL;
	private String NAV;
	private String marginUsed;
	private String marginAvailable;
	private String positionValue;
	private String marginCloseoutUnrealizedPL;
	private String marginCloseoutNAV;
	private String marginCloseoutMarginUsed;
	private String marginCloseoutPercent;
	private String marginCloseoutPositionValue;
	private String withdrawalLimit;
	private String marginCallMarginUsed;
	private String marginCallPercent;
	private String lastTransactionID;
	private List<Trade> trades;
	private List<Position> positions;
	private List<Order> orders;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCreatedByUserID() {
		return createdByUserID;
	}

	public void setCreatedByUserID(String createdByUserID) {
		this.createdByUserID = createdByUserID;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getResettablePL() {
		return resettablePL;
	}

	public void setResettablePL(String resettablePL) {
		this.resettablePL = resettablePL;
	}

	public String getResettabledPLTime() {
		return resettabledPLTime;
	}

	public void setResettabledPLTime(String resettabledPLTime) {
		this.resettabledPLTime = resettabledPLTime;
	}

	public String getMarginRate() {
		return marginRate;
	}

	public void setMarginRate(String marginRate) {
		this.marginRate = marginRate;
	}

	public String getMarginCallEnterTime() {
		return marginCallEnterTime;
	}

	public void setMarginCallEnterTime(String marginCallEnterTime) {
		this.marginCallEnterTime = marginCallEnterTime;
	}

	public String getMarginCallExtensionCount() {
		return marginCallExtensionCount;
	}

	public void setMarginCallExtensionCount(String marginCallExtensionCount) {
		this.marginCallExtensionCount = marginCallExtensionCount;
	}

	public String getLastMarginCallExtensionTime() {
		return lastMarginCallExtensionTime;
	}

	public void setLastMarginCallExtensionTime(String lastMarginCallExtensionTime) {
		this.lastMarginCallExtensionTime = lastMarginCallExtensionTime;
	}

	public String getOpenTradeCount() {
		return openTradeCount;
	}

	public void setOpenTradeCount(String openTradeCount) {
		this.openTradeCount = openTradeCount;
	}

	public String getOpenPositionCount() {
		return openPositionCount;
	}

	public void setOpenPositionCount(String openPositionCount) {
		this.openPositionCount = openPositionCount;
	}

	public String getPendingOrderCount() {
		return pendingOrderCount;
	}

	public void setPendingOrderCount(String pendingOrderCount) {
		this.pendingOrderCount = pendingOrderCount;
	}

	public String getHedgingEnabled() {
		return hedgingEnabled;
	}

	public void setHedgingEnabled(String hedgingEnabled) {
		this.hedgingEnabled = hedgingEnabled;
	}

	public String getUnrealizedPL() {
		return unrealizedPL;
	}

	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public String getNAV() {
		return NAV;
	}

	public void setNAV(String nAV) {
		NAV = nAV;
	}

	public String getMarginUsed() {
		return marginUsed;
	}

	public void setMarginUsed(String marginUsed) {
		this.marginUsed = marginUsed;
	}

	public String getMarginAvailable() {
		return marginAvailable;
	}

	public void setMarginAvailable(String marginAvailable) {
		this.marginAvailable = marginAvailable;
	}

	public String getPositionValue() {
		return positionValue;
	}

	public void setPositionValue(String positionValue) {
		this.positionValue = positionValue;
	}

	public String getMarginCloseoutUnrealizedPL() {
		return marginCloseoutUnrealizedPL;
	}

	public void setMarginCloseoutUnrealizedPL(String marginCloseoutUnrealizedPL) {
		this.marginCloseoutUnrealizedPL = marginCloseoutUnrealizedPL;
	}

	public String getMarginCloseoutNAV() {
		return marginCloseoutNAV;
	}

	public void setMarginCloseoutNAV(String marginCloseoutNAV) {
		this.marginCloseoutNAV = marginCloseoutNAV;
	}

	public String getMarginCloseoutMarginUsed() {
		return marginCloseoutMarginUsed;
	}

	public void setMarginCloseoutMarginUsed(String marginCloseoutMarginUsed) {
		this.marginCloseoutMarginUsed = marginCloseoutMarginUsed;
	}

	public String getMarginCloseoutPercent() {
		return marginCloseoutPercent;
	}

	public void setMarginCloseoutPercent(String marginCloseoutPercent) {
		this.marginCloseoutPercent = marginCloseoutPercent;
	}

	public String getMarginCloseoutPositionValue() {
		return marginCloseoutPositionValue;
	}

	public void setMarginCloseoutPositionValue(String marginCloseoutPositionValue) {
		this.marginCloseoutPositionValue = marginCloseoutPositionValue;
	}

	public String getWithdrawalLimit() {
		return withdrawalLimit;
	}

	public void setWithdrawalLimit(String withdrawalLimit) {
		this.withdrawalLimit = withdrawalLimit;
	}

	public String getMarginCallMarginUsed() {
		return marginCallMarginUsed;
	}

	public void setMarginCallMarginUsed(String marginCallMarginUsed) {
		this.marginCallMarginUsed = marginCallMarginUsed;
	}

	public String getMarginCallPercent() {
		return marginCallPercent;
	}

	public void setMarginCallPercent(String marginCallPercent) {
		this.marginCallPercent = marginCallPercent;
	}

	public String getLastTransactionID() {
		return lastTransactionID;
	}

	public void setLastTransactionID(String lastTransactionID) {
		this.lastTransactionID = lastTransactionID;
	}

	public List<Trade> getTrades() {
		return trades;
	}

	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}


}
