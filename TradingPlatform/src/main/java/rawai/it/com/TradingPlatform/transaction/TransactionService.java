package rawai.it.com.TradingPlatform.transaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oanda.v20.Context;
import com.oanda.v20.ContextBuilder;
import com.oanda.v20.ExecuteException;
import com.oanda.v20.RequestException;
import com.oanda.v20.account.AccountID;
import com.oanda.v20.order.OrderSpecifier;
import com.oanda.v20.order.StopLossOrder;
import com.oanda.v20.order.TakeProfitOrder;
import com.oanda.v20.trade.TradeGetResponse;
import com.oanda.v20.trade.TradeSpecifier;
import com.oanda.v20.transaction.OrderFillTransaction;
import com.oanda.v20.transaction.Transaction;
import com.oanda.v20.transaction.TransactionID;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Service
public class TransactionService {
	private static final Logger log = LogManager.getLogger(TransactionService.class);
	@Autowired
	private CurlUtil curlUtil;

	public double getCloseOutPrice(String orderID, String accountID) {
		Account account = SessionUtils.findAccountByUserId(accountID);
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();
		TransactionID transactionID = new TransactionID("");
		AccountID accountIDNumber = new AccountID(accountID);
		TradeSpecifier tradeSpecifier = new TradeSpecifier(orderID);
		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);
		TransactionID fillingTransactionID = null;
		try {

			switch (ctx.order.get(accountIDNumber, orderSpecifier).getOrder().getType()) {
			case STOP_LOSS:
				fillingTransactionID = ((StopLossOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			case TAKE_PROFIT:
				fillingTransactionID = ((TakeProfitOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			}

			return ((OrderFillTransaction)ctx.transaction.get(accountIDNumber, fillingTransactionID).getTransaction()).getPrice().doubleValue();
		} catch (RequestException | ExecuteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public Transaction getCloseOutTransaction(String orderID, String accountID) {
		Account account = SessionUtils.findAccountByUserId(accountID);
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();
		TransactionID transactionID = new TransactionID("");
		AccountID accountIDNumber = new AccountID(accountID);
		TradeSpecifier tradeSpecifier = new TradeSpecifier(orderID);
		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);
		TransactionID fillingTransactionID = null;
		try {

			switch (ctx.order.get(accountIDNumber, orderSpecifier).getOrder().getType()) {
			case STOP_LOSS:
				fillingTransactionID = ((StopLossOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			case TAKE_PROFIT:
				fillingTransactionID = ((TakeProfitOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			}

			return ((OrderFillTransaction)ctx.transaction.get(accountIDNumber, fillingTransactionID).getTransaction());
		} catch (RequestException | ExecuteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	public String getCloseOutTime(String orderID, String accountID) {
		Account account = SessionUtils.findAccountByUserId(accountID);
		Context ctx = new ContextBuilder(curlUtil.PRAXIS_URL).setToken(account.getApiKey()).setApplication("Trader").build();
		TransactionID transactionID = new TransactionID("");
		AccountID accountIDNumber = new AccountID(accountID);
		TradeSpecifier tradeSpecifier = new TradeSpecifier(orderID);
		OrderSpecifier orderSpecifier = new OrderSpecifier(orderID);
		TransactionID fillingTransactionID = null;
		try {

			switch (ctx.order.get(accountIDNumber, orderSpecifier).getOrder().getType()) {
			case STOP_LOSS:
				fillingTransactionID = ((StopLossOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			case TAKE_PROFIT:
				fillingTransactionID = ((TakeProfitOrder)ctx.order.get(accountIDNumber, orderSpecifier).getOrder()).getFillingTransactionID();
				break;
			default:
				break;
			}

			return ((OrderFillTransaction)ctx.transaction.get(accountIDNumber, fillingTransactionID).getTransaction()).getTime().toString();
		} catch (RequestException | ExecuteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Error";
	}
}
