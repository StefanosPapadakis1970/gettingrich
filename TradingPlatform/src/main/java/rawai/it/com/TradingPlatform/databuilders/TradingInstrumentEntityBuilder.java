package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

public class TradingInstrumentEntityBuilder {

	private Long id;

	private int units;

	private String instument;

	private TradingProfileEntity tradingprofile;

	private IntervalEnum tradingInterval;

	private TradingInstrumentEntityBuilder() {

	}

	public static TradingInstrumentEntityBuilder aTradingInstrumentEntityBuilder() {
		return new TradingInstrumentEntityBuilder();
	}

	public TradingInstrumentEntityBuilder withTradingInterval(IntervalEnum interval) {
		this.tradingInterval = interval;
		return this;
	}

	public TradingInstrumentEntityBuilder withID(long id) {
		this.id = id;
		return this;
	}

	public TradingInstrumentEntityBuilder withUnits(int units) {
		this.units = units;
		return this;
	}

	public TradingInstrumentEntityBuilder withInstrument(String instrument) {
		this.instument = instrument;
		return this;
	}
	public TradingInstrumentEntityBuilder withTradingProfileEntity(TradingProfileEntity profile) {
		this.tradingprofile = profile;
		return this;
	}

	public TradingInstrumentEntity build() {
		TradingInstrumentEntity instrument = new TradingInstrumentEntity();
		instrument.setId(id);
		instrument.setTradingprofile(tradingprofile);
		instrument.setInstument(instument);
		instrument.setUnits(units);
		instrument.setTradingInterval(tradingInterval);
		return instrument;
	}
}
