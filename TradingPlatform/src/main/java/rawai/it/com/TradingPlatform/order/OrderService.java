package rawai.it.com.TradingPlatform.order;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.dashboard.ClientExtensions;
import rawai.it.com.TradingPlatform.dashboard.MarketOrderTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderFillTransaction;
import rawai.it.com.TradingPlatform.dashboard.TakeProfitOrderTransaction;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeConverter;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.position.PositionService;
import rawai.it.com.TradingPlatform.price.Price;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.trade.Trade;
import rawai.it.com.TradingPlatform.trade.TradeService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.TradingUtils;

@Service
public class OrderService {

	private static final Logger log = LogManager.getLogger(OrderService.class);
	private static final Logger transactionLog = LogManager.getLogger("transaction-log");
	private static final Logger orderLog = LogManager.getLogger("ordercreation-log");

	@Autowired
	private AccountRepository accountrepository;
	@Autowired
	private PositionService positionService;
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	private AlgorithmTradeRepository algorithmTradeRepository;
	@Autowired
	private PriceService priceService;
	@Autowired
	private TradeService tradeService;


	public String openOrder(Order order) {
		orderLog.info("######### Processing Opening order at -> "+Instant.now().toString());

		RestTemplate tmpl = new RestTemplate();

		if (order.getPrice() != null && !order.getPrice().isEmpty() && Double.valueOf(order.getPrice()) != 0) {
			order.setTimeInForce("GTC");
			order.setType("LIMIT");
		} else {
			order.setTimeInForce("FOK");
			order.setType("MARKET");
		}

		order.setPositionFill("DEFAULT");

		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountrepository.findAll());
		}

		Account account = SessionUtils.findAccountByUserId(order.getUserID());
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		OrderJSON orderJSON = new OrderJSON();
		if (!order.isBuy()) {
			order.setUnits("-" + order.getUnits());
		}

		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment(ClientExtensions.ENGINE_TRADE);
		clientExtensions.setTag(order.getUuid());
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));
		order.setClientExtensions(clientExtensions);

		orderJSON.setOrder(order);
		try {
			jsonInString = mapper.writeValueAsString(orderJSON);
			transactionLog.info(jsonInString);
		} catch (JsonProcessingException e) {
			transactionLog.error(e.fillInStackTrace());
		}

		HttpEntity<?> apiHeader = curlUtil.createAPIHeaderWithBody(account.getApiKey(), jsonInString);
		ResponseEntity<String> resp = null;
		ResultMessage error = null;
		try {
			String url = curlUtil.createAccountUrl() + "/" + order.getUserID() + "/orders";
			orderLog.info("##### Now Sending Request for creating order ################### "+order.getUuid()+" "+Instant.now().toString());
			transactionLog.info("##### Now Sending Request for creating order ################### "+order.getUuid());
			resp = tmpl.exchange(url, HttpMethod.POST, apiHeader, String.class);
			transactionLog.info("##### Sending Request Finish ###################");
			orderLog.info("##### Sending Request finished ################### "+order.getUuid()+" "+Instant.now().toString());
		} catch (HttpClientErrorException e) {
			transactionLog.error(e.fillInStackTrace());
			try {
				error = mapper.readValue(e.getResponseBodyAsString(), ResultMessage.class);
				transactionLog.error(error);
			} catch (IOException e1) {
				transactionLog.error(e1.fillInStackTrace());
			}

			return error.getErrorMessage();
		}

		if (resp.getStatusCode() == HttpStatus.CREATED && resp.hasBody()) {
			orderLog.info("######### Processing Opening order finished success -> "+Instant.now().toString());
			return "ok";
		} else {
			orderLog.info("######### Processing Opening order finished Fail -> "+Instant.now().toString());
			return "error";
		}
	}

	public String createTrades(List<AlgorithmTrade> trades) {
		orderLog.info("#### Creating all Trades at "+Instant.now().toString());
		String resultString = "";
		//Only tmp
		if(SessionUtils.getTradeCounter() >= 10) {
			//return "OK";
		}
		
		for (AlgorithmTrade trade : trades) {
			if (trade.isTrade() && trade.getInstrument().isTradingEngine()) {				
				// Skip Trade at the moment if one open already exists				
				if(tradeService.hasOpenTrade(trade.getAccount().getUserId(), trade.getInstrument().getInstument())) {
					transactionLog.info("##### Skiped Trade because already one open  -> " + trade.getInstrument().getInstument());
					orderLog.info("##### Skiped Trade because already one open  -> " + trade.getInstrument().getInstument());
					continue;
				}
				orderLog.info("#### Creating  Trade at "+Instant.now().toString()+ " "+trade.getInstrument().getInstument());
				
				List<Order> orders = createOrder(trade);
				for (Order order : orders) {
					transactionLog.info("##### Saving Trade with UUID -> " + trade.getUuid());
					trade.setRealTrade(true);
					trade.setCreationDate(new Date());
					trade.setUnits(trade.getInstrument().getUnits());
					int candleCounter = 0;
					for (CandleDataPointInterface candle : trade.getCandles()) {
						candle.setCandleIndex(candleCounter++);
					}
					algorithmTradeRepository.save(AlgorithmTradeConverter.convertToAlgorithmTradeEntity(trade));
					transactionLog.info("##### Opening Order ###################" + trade.getUuid());
					String orderResult = openOrder(order);
					transactionLog.info("##### Opening ORDER Finish ###################" + trade.getUuid());
					orderLog.info("##### Opening ORDER Procedure called ###################" + trade.getUuid()+" ->"+Instant.now().toString());
					if (orderResult != "ok") {
						resultString += orderResult + "\\n";
					}
					//TODO 11.08
					SessionUtils.setTradeCounter(SessionUtils.getTradeCounter()+1);
				}
			}
		}
		return resultString;
	}

	private List<Order> createOrder(AlgorithmTrade trade) {
		List<Order> orders = new ArrayList<>();
		Order order = new Order();
		order.setBuy(trade.isBuySell());
		order.setUnits(String.valueOf(trade.getInstrument().getUnits()));
		order.setInstrument(trade.getInstrument().getInstument());
		order.setUserID(trade.getAccount().getUserId());
		order.setUuid(trade.getUuid().toString());
		orders.add(order);
		return orders;
	}


	public List<Order> getAllOpenOrders() {
		List<Order> pendingOrders = new ArrayList<>();
		List<Account> accounts = SessionUtils.getAccounts();

		for (Account account : accounts) {
			List<Trade> trades = positionService.getAllOpenTrades(account.getUserId());
			List<Order> allPendingOrders = positionService.getAllPendingOrders(account.getUserId());
			for (Order order : allPendingOrders) {
				order.setUserID(account.getUserId());
				order.setUserName(account.getUserName());
				if (order.getTradeID() != null && !order.getTradeID().isEmpty()) {
					for (Trade trade : trades) {
						if (order.getTradeID().equals(trade.getId())) {
							order.setInstrument(trade.getInstrument());
							order.setUnits(trade.getInitialUnits());
						}
					}
				}
			}
			pendingOrders.addAll(allPendingOrders);
		}
		return pendingOrders;
	}




	public boolean openTakeProfitOrder(MarketOrderTransaction marketOrderTransaction) {
		String requestID = marketOrderTransaction.getRequestID();
		String tradeID = marketOrderTransaction.getId();
		String price = marketOrderTransaction.getPrice();

		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmByRequestID(requestID);
		algorithmTrade.setTakeProfitID(Long.valueOf(tradeID));
		algorithmTrade.setPrice(Double.valueOf(price));

		TakeProfitOrder takeProfitOrder = new TakeProfitOrder();
		takeProfitOrder.setTradeID(tradeID);
		Double takeProfitPrice = TradingUtils.calculateLossProfitPrice(Double.valueOf(price), algorithmTrade.getTakeProfitPips(), algorithmTrade.isBuySell(), true);
		algorithmTrade.setTakeProfitPrice(takeProfitPrice);
		algorithmTradeRepository.save(algorithmTrade);

		return true;
	}

	public boolean openTakeProfitOrder(TakeProfitOrder takeProfitOrder) {

		RestTemplate tmpl = new RestTemplate();
		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment(ClientExtensions.ENGINE_TRADE);
		clientExtensions.setTag(takeProfitOrder.getUuid());
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		takeProfitOrder.setClientExtensions(clientExtensions);

		TakeProfitJSON orderJSON = new TakeProfitJSON();
		orderJSON.setOrder(takeProfitOrder);
		
		transactionLog.info("#######  OrderCreation with following Parameters ###########");
		transactionLog.info("#######  Price : "+orderJSON.getOrder().getPrice());
		
		

		String jsonInString = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonInString = mapper.writeValueAsString(orderJSON);
			transactionLog.info("####### JSONSTRING "+jsonInString);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			log.error(e.fillInStackTrace());
		}

		Account account = SessionUtils.findAccountByUserId(takeProfitOrder.getUserID());
		log.info(jsonInString);
		HttpEntity<?> apiHeader = curlUtil.createAPIHeaderWithBody(account.getApiKey(), jsonInString);
		ResponseEntity<String> resp = null;
		String url = "";
		ResultMessage error = null;
		try {
			url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/orders";
			resp = tmpl.exchange(url, HttpMethod.POST, apiHeader, String.class);
		} catch (HttpClientErrorException e) {
			log.error(e.fillInStackTrace());
			try {
				error = mapper.readValue(e.getResponseBodyAsString(), ResultMessage.class);
			} catch (IOException e1) {
				log.error(e1.fillInStackTrace());
			}
			return false;
		} catch (RestClientException e) {
			log.error(e.fillInStackTrace());
		}

		if (resp.getStatusCode() == HttpStatus.CREATED && resp.hasBody()) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public boolean updateOrder(TakeProfitOrder takeProfitOrder, Account account) {

		RestTemplate tmpl = new RestTemplate();
		ClientExtensions clientExtensions = new ClientExtensions();
		clientExtensions.setComment(ClientExtensions.ENGINE_TRADE);
		clientExtensions.setTag(takeProfitOrder.getUuid());
		clientExtensions.setId(String.valueOf(SessionUtils.getExtensionID()));

		takeProfitOrder.setClientExtensions(clientExtensions);

		TakeProfitJSON orderJSON = new TakeProfitJSON();
		orderJSON.setOrder(takeProfitOrder);

		String jsonInString = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonInString = mapper.writeValueAsString(orderJSON);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			log.error(e.fillInStackTrace());
		}

		
		log.info(jsonInString);
		HttpEntity<?> apiHeader = curlUtil.createAPIHeaderWithBody(account.getApiKey(), jsonInString);
		ResponseEntity<String> resp = null;
		String url = "";
		ResultMessage error = null;
		try {
			url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/orders/"+takeProfitOrder.getId();
			resp = tmpl.exchange(url, HttpMethod.PUT, apiHeader, String.class);
		} catch (HttpClientErrorException e) {
			log.error(e.fillInStackTrace());
			try {
				error = mapper.readValue(e.getResponseBodyAsString(), ResultMessage.class);
			} catch (IOException e1) {
				log.error(e1.fillInStackTrace());
			}
			return false;
		} catch (RestClientException e) {
			log.error(e.fillInStackTrace());
		}

		if (resp.getStatusCode() == HttpStatus.CREATED && resp.hasBody()) {
			return true;
		} else {
			return false;
		}
	}

	public void processOrderFillTransaction(OrderFillTransaction orderFillTransaction) {
		transactionLog.info("############## START processOrderFillTransaction ###################################");
		AlgorithmTradeEntity trade = algorithmTradeRepository.findAlgorithmByRequestID(orderFillTransaction.getRequestID());
		if (trade == null) {
			transactionLog.info("TRADE NOT FOUND! "+orderFillTransaction.getRequestID());
			return;
		}
		transactionLog.info("TRADE FOUND EVERITHING FINE");
		String tradeID = "";
		if (orderFillTransaction.getTradeOpened() != null) {
			tradeID = orderFillTransaction.getTradeOpened().getTradeID();
		}
		if (orderFillTransaction.getTradeReduced() != null) {
			tradeID = orderFillTransaction.getTradeReduced().getTradeID();
		}

		trade.setTradeID(tradeID);
		trade.setPrice(Double.valueOf(orderFillTransaction.getPrice()));
		algorithmTradeRepository.update(trade);
		// TODO 06.07 maybe session??
		Account account = accountrepository.findByID(trade.getAccountID());

		TakeProfitOrder takeProfitOrder = new TakeProfitOrder();
		takeProfitOrder.setTradeID(tradeID);
		takeProfitOrder.setUuid(trade.getUuid());
		takeProfitOrder.setUserID(account.getUserId());
		
		
		// Fixed Problem with correct Ask-Bid 
		Price currentPrice = priceService.fetchPrice(SessionUtils.getAccounts().get(0).getUserId(), trade.getInstrument());
		Double currentPriceForOrderCreation = 0d;
/*		if (trade.isBuySell()) {// Buy Trade. If you buy you get the BID-Price if u sell u get the ASK-price
			currentPriceForOrderCreation = Double.valueOf(currentPrice.getBid());
		} else {
			currentPriceForOrderCreation = Double.valueOf(currentPrice.getAsk());
		} */
		
		currentPriceForOrderCreation = trade.getPrice();
		
		Double price = TradingUtils.calculateLossProfitPrice(currentPriceForOrderCreation, trade.getTakeProfitPips(), trade.isBuySell(), true);

		
		
		BigDecimal bigPrice = new BigDecimal(price).setScale(5, RoundingMode.HALF_UP);
		takeProfitOrder.setPrice(bigPrice.toString());
		transactionLog.info("CREATE TAKE PROFIT");		
		openTakeProfitOrder(takeProfitOrder);
		
		transactionLog.info("###################Broker - Trade - ID "+trade.getTradeID() +" ##############");
		transactionLog.info("###################Trade was generated from Algorithm ##############");
		transactionLog.info("################  "+trade.getAlgorithmID()+"  ######################");
		transactionLog.info("####################################################################");
		transactionLog.info("############  CREATE TAKE PROFIT ORDER          ####################");
		transactionLog.info("####################################################################");
        transactionLog.info("######  Trade is Buy or Sell -> "+trade.isBuySell()+" units -> "+trade.getUnits()+"##################");
        transactionLog.info("######  Price fetched at the  moment  of creation ##################");
        transactionLog.info("######  BID "+currentPrice.getBid()+" ##################");
        transactionLog.info("######  ASK "+currentPrice.getAsk()+" ##################");
        transactionLog.info("###### Price used to create the take Profit Order "+currentPriceForOrderCreation );
        transactionLog.info("###### Calculated price according to the PIPS "+trade.getTakeProfitPips()+" ->  "+bigPrice.toString());
		transactionLog.info("####################################################################");
		transactionLog.info("####################################################################");  
		
		
		price = TradingUtils.calculateLossProfitPrice(currentPriceForOrderCreation, trade.getStopLossPips(), trade.isBuySell(), false);
		bigPrice = new BigDecimal(price).setScale(5, RoundingMode.HALF_UP);
		takeProfitOrder.setPrice(bigPrice.toString());
		takeProfitOrder.setType("STOP_LOSS");
		takeProfitOrder.setUuid(trade.getUuid());
		transactionLog.info("CREATE STOP LOSS");
		openTakeProfitOrder(takeProfitOrder);
		
		transactionLog.info("###################Broker - Trade - ID "+trade.getTradeID() +" ##############");
		transactionLog.info("###################Trade was generated from Algorithm ##############");
		transactionLog.info("################  "+trade.getAlgorithmID()+"  ######################");
		transactionLog.info("####################################################################");
		transactionLog.info("############  CREATE STOP LOSST ORDER           ####################");
		transactionLog.info("####################################################################");
		transactionLog.info("######  Trade is Buy or Sell -> "+trade.isBuySell()+" units -> "+trade.getUnits()+"##################");
        transactionLog.info("######  Price fetched at the  moment  of creation ##################");
        transactionLog.info("######  BID "+currentPrice.getBid()+" ##################");
        transactionLog.info("######  ASK "+currentPrice.getAsk()+" ##################");
        transactionLog.info("###### Price used to create the stop loss Order "+currentPriceForOrderCreation );
        transactionLog.info("###### Calculated price according to the PIPS "+trade.getStopLossPips()+" ->  "+bigPrice.toString());
		transactionLog.info("####################################################################");
		transactionLog.info("####################################################################");  

		
		
		transactionLog.info("############## END processOrderFillTransaction ###################################");
	}

	public void processEngineTradeMarketOrder(MarketOrderTransaction marketOrderTransaction) {
		transactionLog.info("############## START processEngineTradeMarketOrder ###################################");
		if (marketOrderTransaction.getType().name().equals("MARKET_ORDER")) {
			String uuid = marketOrderTransaction.getClientExtensions().getTag();
			AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmByUUID(uuid);
			algorithmTrade.setUserID(marketOrderTransaction.getAccountID());
			if (algorithmTrade == null) {
				transactionLog.info("TRADE NOT FOUND! " + uuid);
				return;
			}
			algorithmTrade.setRequestID(marketOrderTransaction.getRequestID());
			algorithmTradeRepository.update(algorithmTrade);
		}
		transactionLog.info("############## END processEngineTradeMarketOrder ###################################");
	}

	public void processStopLossOrderTransaction(MarketOrderTransaction marketOrderTransaction) {
		transactionLog.info("############## START processStopLossOrderTransaction ###################################");
		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmByUUID(marketOrderTransaction.getClientExtensions().getTag());
		if (algorithmTrade == null) {
			transactionLog.info("TRADE NOT FOUND! "+marketOrderTransaction.getClientExtensions().getTag());
			return;
		}
		algorithmTrade.setStopLossTradeID(Long.valueOf(marketOrderTransaction.getId()));
		algorithmTrade.setStopLossPrice(Double.valueOf(marketOrderTransaction.getPrice()));

		AlgorithmTradeEntity stopLossOrder = new AlgorithmTradeEntity();
		stopLossOrder.setFinished(false);
		stopLossOrder.setStopLossPrice(Double.valueOf(marketOrderTransaction.getPrice()));
		stopLossOrder.setStopLossPips(algorithmTrade.getStopLossPips());
		stopLossOrder.setStopLossTradeID(algorithmTrade.getStopLossTradeID());
		stopLossOrder.setCreationDate(new Date());
		stopLossOrder.setTradeID(algorithmTrade.getTradeID());

		algorithmTrade.setStopLossTrade(stopLossOrder);
		algorithmTrade.setFinished(false);

		algorithmTradeRepository.update(algorithmTrade);
		transactionLog.info("############## END processStopLossOrderTransaction ###################################");
	}

	public void processTakeProfitOrderTransaction(MarketOrderTransaction marketOrderTransaction) {
		transactionLog.info("############## START processTakeProfitOrderTransaction ###################################");
		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmByUUID(marketOrderTransaction.getClientExtensions().getTag());
		if (algorithmTrade == null) {
			transactionLog.info("TRADE NOT FOUND! "+marketOrderTransaction.getClientExtensions().getTag());
			return;
		}
		algorithmTrade.setTakeProfitID(Long.valueOf(marketOrderTransaction.getId()));
		algorithmTrade.setTakeProfitPrice(Double.valueOf(marketOrderTransaction.getPrice()));

		AlgorithmTradeEntity takeProfitTrade = new AlgorithmTradeEntity();
		takeProfitTrade.setFinished(false);
		takeProfitTrade.setTakeProfitPrice(Double.valueOf(marketOrderTransaction.getPrice()));
		takeProfitTrade.setTakeProfitPips(algorithmTrade.getTakeProfitPips());
		takeProfitTrade.setTakeProfitID(algorithmTrade.getTakeProfitID());
		takeProfitTrade.setCreationDate(new Date());
		takeProfitTrade.setTradeID(algorithmTrade.getTradeID());

		algorithmTrade.setTakeProfitTrade(takeProfitTrade);
		algorithmTrade.setFinished(false);
		algorithmTradeRepository.update(algorithmTrade);
		transactionLog.info("############## END processTakeProfitOrderTransaction ###################################");
	}

	public void processOrderFillStopLoss(OrderFillTransaction orderFillTransaction) {
		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmTradeByUserIdTradeID(orderFillTransaction.getAccountID(),
				orderFillTransaction.getTradesClosed().get(0).getTradeID());

		algorithmTrade.setFinished(true);
		algorithmTrade.setPl(Double.valueOf(orderFillTransaction.getPl()));
		algorithmTrade.setSuccess(false);

		algorithmTrade.getStopLossTrade().setFinished(true);
		algorithmTrade.getStopLossTrade().setPrice(Double.valueOf(orderFillTransaction.getPrice()));
		algorithmTrade.getStopLossTrade().setPl(Double.valueOf(orderFillTransaction.getPl()));

		algorithmTradeRepository.update(algorithmTrade);
	}

	public void processOrderFillTakeProfit(OrderFillTransaction orderFillTransaction) {
		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmTradeByUserIdTradeID(orderFillTransaction.getAccountID(),
				orderFillTransaction.getTradesClosed().get(0).getTradeID());

		algorithmTrade.setFinished(true);
		algorithmTrade.setPl(Double.valueOf(orderFillTransaction.getPl()));
		algorithmTrade.setSuccess(true);

		algorithmTrade.getTakeProfitTrade().setFinished(true);
		algorithmTrade.getTakeProfitTrade().setPrice(Double.valueOf(orderFillTransaction.getPrice()));
		algorithmTrade.getTakeProfitTrade().setPl(Double.valueOf(orderFillTransaction.getPl()));

		algorithmTrade.setStopLossPrice(
				TradingUtils.calculateLossProfitPrice(Double.valueOf(orderFillTransaction.getPrice()), algorithmTrade.getStopLossPips(), algorithmTrade.isBuySell(), false));
		algorithmTrade.setTakeProfitPrice(
				TradingUtils.calculateLossProfitPrice(Double.valueOf(orderFillTransaction.getPrice()), algorithmTrade.getTakeProfitPips(), algorithmTrade.isBuySell(), true));

		algorithmTradeRepository.update(algorithmTrade);
	}

	public void updateStopLossTrade(AlgorithmTrade trade) {
		log.info("################## Updateing StopLossOrder #############################");
		TakeProfitOrder order = new TakeProfitOrder();
		order.setId(trade.getStopLossTradeID().toString());
		order.setTradeID(trade.getTradeID());
		order.setPrice(trade.getPrice().toString());		
		order.setType("STOP_LOSS");
		order.setUuid(trade.getUuid());
		updateOrder(order,trade.getAccount());
		algorithmTradeRepository.update(AlgorithmTradeConverter.convertToAlgorithmTradeEntity(trade));
	}

	public List<AlgorithmTrade> fetchPostProcessTrades(TradingInstrument instrument) {
		List<AlgorithmTradeEntity> trades = algorithmTradeRepository.findAllOpenTradesByInstrument(instrument);
		List<AlgorithmTrade> resultTrades = new ArrayList<>();
		for (AlgorithmTradeEntity trade : trades) {
			resultTrades.add(AlgorithmTradeConverter.convertToAlgorithmTrade(trade));
		}
		return resultTrades;
	}
	
	public List<AlgorithmTrade> fetchPostProcessTrades(String instrument) {
		List<AlgorithmTradeEntity> trades = algorithmTradeRepository.findAllOpenTradesByInstrument(instrument);
		List<AlgorithmTrade> resultTrades = new ArrayList<>();
		for (AlgorithmTradeEntity trade : trades) {
			resultTrades.add(AlgorithmTradeConverter.convertToAlgorithmTrade(trade));
		}
		return resultTrades;
	}

	public void updateStopLossOrderID(TakeProfitOrderTransaction order) {
		log.info("########## updateStopLossOrderID #####################");
		AlgorithmTradeEntity algorithmTrade = algorithmTradeRepository.findAlgorithmTradeByUserIdTradeID(order.getAccountID(),order.getTradeID());
		Long fromID = algorithmTrade.getStopLossTradeID();
		// algorithmTrade.setStopLossTradeID(Long.valueOf(order.getCancellingTransactionID()));
		algorithmTrade.setStopLossTradeID(Long.valueOf(order.getId()));
		algorithmTradeRepository.update(algorithmTrade);		
		log.info("##################StopLossId successgully updated from  "+fromID+"  to "+algorithmTrade.getStopLossTradeID()+"#####################");
	}
}
