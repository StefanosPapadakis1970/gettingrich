package rawai.it.com.TradingPlatform.databuilders;

import java.util.Date;
import java.util.List;

import rawai.it.com.TradingPlatform.algorithmtesting.TestEntity;
import rawai.it.com.TradingPlatform.algorithmtesting.TestStatusEnum;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.tradingprofile.RunType;

public class TestEntityBuilder {

	private Long id;
	private TradingAlgorithmEntity algorithm;
	private String instrument;
	private RunType runType;
	private IntervalEnum granularity;
	private int units;
	private String userID;
	private int maxNumberOfTransactions;
	private Date startDate = new Date();
	private Date endDate = new Date();
	private boolean generateTrades;
	private List<AlgorithmTradeEntity> trades;
	private TestStatusEnum status;

	private TestEntityBuilder() {

	}

	public static TestEntityBuilder aTest() {
		return new TestEntityBuilder();
	}

	public TestEntityBuilder withStatus(TestStatusEnum status) {
		this.status = status;
		return this;
	}

	public TestEntityBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public TestEntityBuilder withAlgorithm(TradingAlgorithmEntity algorithm) {
		this.algorithm = algorithm;
		return this;
	}

	public TestEntityBuilder withInstrument(String instrument) {
		this.instrument = instrument;
		return this;
	}

	public TestEntityBuilder withRunType(RunType runType) {
		this.runType = runType;
		return this;
	}

	public TestEntityBuilder withGranularity(IntervalEnum granularity) {
		this.granularity = granularity;
		return this;
	}

	public TestEntityBuilder withUnits(int units) {
		this.units = units;
		return this;
	}

	public TestEntityBuilder withUserID(String userId) {
		this.userID = userId;
		return this;
	}

	public TestEntityBuilder withMaxNumberOfTransactions(int maxTransaactions) {
		this.maxNumberOfTransactions = maxTransaactions;
		return this;
	}

	public TestEntityBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;

	}

	public TestEntityBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}

	public TestEntityBuilder withGenerateTrades(boolean generateTrade) {
		this.generateTrades = generateTrade;
		return this;
	}

	public TestEntityBuilder withTrades(List<AlgorithmTradeEntity> trades) {
		this.trades = trades;
		return this;
	}

	public TestEntity build() {
		TestEntity test = new TestEntity();
		test.setAlgorithm(algorithm);
		test.setEndDate(endDate);
		test.setGenerateTrades(generateTrades);
		test.setGranularity(granularity);
		test.setId(id);
		test.setInstrument(instrument);
		test.setInterval(granularity);
		test.setMaxNumberOfTransactions(maxNumberOfTransactions);
		test.setRunType(runType);
		test.setStartDate(startDate);
		test.setUnits(units);
		test.setUserID(userID);
		// TODO 21.06 change dbmodel
		// test.setTrades(trades);
		return test;
	}

}
