package rawai.it.com.TradingPlatform.engine.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rawai.it.com.TradingPlatform.engine.TradingEngineEntity;

@Repository
@Transactional(readOnly = false)
public class TradingRepository {

	@Autowired
	private EntityManager entityManager;

	@PostConstruct
	protected void initialize() {
		// getTradingEngine();
	}

	@Transactional
	public TradingEngineEntity getTradingEngine() {
		try {
			return entityManager.createNamedQuery(TradingEngineEntity.GET_ENGINE, TradingEngineEntity.class).getSingleResult();
		} catch (NoResultException e) {
			return createTradingEngine();
		} catch (NonUniqueResultException e) {
			List<TradingEngineEntity> engines = entityManager.createNamedQuery(TradingEngineEntity.GET_ENGINE, TradingEngineEntity.class).getResultList();
			for (TradingEngineEntity engine : engines) {
				entityManager.remove(engine);
			}
			return createTradingEngine();
		}
	}

	@Transactional
	private TradingEngineEntity createTradingEngine() {
		TradingEngineEntity engine = new TradingEngineEntity();
		engine.setRunning(false);
		entityManager.persist(engine);
		return engine;
	}

	@Transactional
	public TradingEngineEntity startTradingEngine() {
		TradingEngineEntity engine = getTradingEngine();
		engine.setRunning(true);
		engine.setStartUpDate(new Date());
		entityManager.persist(engine);
		return engine;
	}

	@Transactional
	public TradingEngineEntity stopTradingEngine() {
		TradingEngineEntity engine = getTradingEngine();
		engine.setRunning(false);
		return engine;
	}

	@Transactional
	public void saveTradingProfile(TradingProfileEntity profile) {
		entityManager.persist(entityManager.contains(profile) ? entityManager.merge(profile) : profile);
	}

	@Transactional
	public void saveDeletedTradingProfile(TradingProfileEntity profile) {
		entityManager.flush();
		entityManager.refresh(profile);
		entityManager.persist(profile);
	}

	public void updateTradingProfile(TradingProfileEntity profile) {
		entityManager.persist(entityManager.merge(profile));
	}

	public TradingProfileEntity findByUserID(String userID) {
		try {
			TradingProfileEntity resultEntity = entityManager.createNamedQuery(TradingProfileEntity.FIND_BY_USERID, TradingProfileEntity.class).setParameter("userID", userID)
					.getSingleResult();
			Hibernate.initialize(resultEntity.getTradingAlgorithms());
			for (TradingAlgorithmEntity algorithm : resultEntity.getTradingAlgorithms()) {
				Hibernate.initialize(algorithm.getTradingSignals());
			}
			Hibernate.initialize(resultEntity.getTradingInstruments());
			return resultEntity;
		} catch (PersistenceException e) {
			return null;
		}
	}

	public void deleteTradingProfileByUserID(String id) {
		Query query = entityManager.createQuery("DELETE FROM TradingProfileEntity AS profile WHERE profile.userID=:id");
		query.setParameter("id", id);
		try {
			int result = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteAllTradingProfiles() {
		Query query = entityManager.createQuery("DELETE FROM TradingSignalEntity");
		query.executeUpdate();
		query = entityManager.createQuery("DELETE FROM TradingInstrumentEntity");
		query.executeUpdate();
		query = entityManager.createQuery("DELETE FROM TradingAlgorithmEntity");
		query.executeUpdate();
		query = entityManager.createQuery("DELETE FROM TradingProfileEntity");
		query.executeUpdate();
	}

	
	public TradingAlgorithmEntity saveTradingAlgorithm(TradingAlgorithmEntity algorithm) {
		return entityManager.merge(algorithm);
		// entityManager.persist(entityManager.contains(algorithm) ?
		// entityManager.merge(algorithm) : algorithm);
	}
	
	public TradingAlgorithmEntity findByName(String name) {
		TradingAlgorithmEntity resultEntity = null;
		try {
			resultEntity = entityManager.createNamedQuery(TradingAlgorithmEntity.FIND_BY_NAME, TradingAlgorithmEntity.class).setParameter("name", name).getSingleResult();
			Hibernate.initialize(resultEntity.getTradingSignals());
			Hibernate.initialize(resultEntity.getPostTradingSignals());
			Hibernate.initialize(resultEntity.getTradingprofiles());
		} catch (NoResultException e) {
			resultEntity = null;
		} catch (NonUniqueResultException e) {
			resultEntity = null;
		}
		return resultEntity;
	}

	public void deleteTradingProfile(TradingProfileEntity profile) {
		entityManager.remove(entityManager.contains(profile) ? profile : entityManager.merge(profile));
	}

	@SuppressWarnings("unchecked")
	public List<TradingSignalEntity> findAllTradingSignals() {
		return entityManager.createNamedQuery(TradingSignalEntity.FIND_ALL).getResultList();
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@SuppressWarnings("unchecked")
	public List<TradingAlgorithmEntity> findAllTradingAlgorithms() {
		List<TradingAlgorithmEntity> algorithmList = entityManager.createNamedQuery(TradingAlgorithmEntity.FIND_ALL).getResultList();
		for (TradingAlgorithmEntity algorithm : algorithmList) {
			Hibernate.initialize(algorithm.getTradingSignals());
			Hibernate.initialize(algorithm.getPostTradingSignals());
		}
		return algorithmList;
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllTradingAlgorithmNames() {
		List<TradingAlgorithmEntity> algorithmList = entityManager.createNamedQuery(TradingAlgorithmEntity.FIND_ALL).getResultList();
		List<String> resultList = new ArrayList<>();
		for (TradingAlgorithmEntity algorithm : algorithmList) {
			Hibernate.initialize(algorithm.getTradingSignals());
			resultList.add(algorithm.getName());
		}
		return resultList;
	}

	public boolean deleteTradingAlgorithmID(Long id) {
		Query query = entityManager.createQuery("DELETE FROM TradingAlgorithmEntity AS algorithm WHERE algorithm.id=:id");
		query.setParameter("id", id);
		int result = -1;
		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result > 0;
	}

	@SuppressWarnings("unchecked")
	public List<TradingProfileEntity> findAllProfiles() {
		try {
			List<TradingProfileEntity> profiles = entityManager.createNamedQuery(TradingProfileEntity.FIND_ALL).getResultList();
			for (TradingProfileEntity profile : profiles) {
				Hibernate.initialize(profile.getTradingAlgorithms());
				for (TradingAlgorithmEntity algorithm : profile.getTradingAlgorithms()) {
					Hibernate.initialize(algorithm.getTradingSignals());
					Hibernate.initialize(algorithm.getPostTradingSignals());
				}
				Hibernate.initialize(profile.getTradingInstruments());
			}
			return profiles;
		} catch (PersistenceException e) {
			return null;
		}

	}

	public List<PostTradeSignalEntity> findAllPostTradingSignals() {
		List<PostTradeSignalEntity> postTradingSignalList = entityManager.createNamedQuery(PostTradeSignalEntity.FIND_ALL).getResultList();
		return postTradingSignalList;
	}

	public TradingAlgorithmEntity findTradingAlgorithmByID(Long dbID) {
		try {
			TradingAlgorithmEntity algorithm = (TradingAlgorithmEntity) entityManager.createNamedQuery(TradingAlgorithmEntity.FIND_BY_ID).setParameter("id", dbID).getSingleResult();
			Hibernate.initialize(algorithm.getTradingSignals());
			Hibernate.initialize(algorithm.getPostTradingSignals());
			return algorithm;
		} catch (NoResultException e) {
            return null;
		}
	}

	@Transactional
	public void saveTransactionMessage(TransactionMessageEntity message) {
		entityManager.persist(message);
	}

	@Transactional
	public List<TransactionMessageEntity> findMessageByTradeID(Long tradeID) {
		return entityManager.createNamedQuery(TransactionMessageEntity.FIND_BY_TRADE_ID).setParameter("tradeID", tradeID).getResultList();
	}
}
