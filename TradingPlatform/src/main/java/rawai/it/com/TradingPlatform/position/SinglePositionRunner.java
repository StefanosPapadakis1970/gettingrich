package rawai.it.com.TradingPlatform.position;

public class SinglePositionRunner implements Runnable {

	private PositionController positioncontroller;
	private boolean stop = false;
	private String sessionID;

	@Override
	public void run() {
		while (!stop) {
			positioncontroller.sendSinglePositionToClient();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public SinglePositionRunner(PositionController positioncontroller, String sessionID) {
		super();
		this.positioncontroller = positioncontroller;
		this.sessionID = sessionID;
	}

	public PositionController getPositioncontroller() {
		return positioncontroller;
	}

	public void setPositioncontroller(PositionController positioncontroller) {
		this.positioncontroller = positioncontroller;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

}
