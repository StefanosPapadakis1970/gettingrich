package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrailingStopLossDetails {

	private String distance;
	private String timeInForce;
	private String gtdTime;
	private ClientExtensions clientExtensions;

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}

	public String getGtdTime() {
		return gtdTime;
	}

	public void setGtdTime(String gtdTime) {
		this.gtdTime = gtdTime;
	}

	public ClientExtensions getClientExtensions() {
		return clientExtensions;
	}

	public void setClientExtensions(ClientExtensions clientExtensions) {
		this.clientExtensions = clientExtensions;
	}

}
