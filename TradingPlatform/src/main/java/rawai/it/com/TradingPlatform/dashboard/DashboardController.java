package rawai.it.com.TradingPlatform.dashboard;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.account.CurrencyEnum;
import rawai.it.com.TradingPlatform.account.OandaAccountSummaryResponse;
import rawai.it.com.TradingPlatform.conditions.PageConditionEnum;
import rawai.it.com.TradingPlatform.messanger.Message;
import rawai.it.com.TradingPlatform.messanger.MessageService;
import rawai.it.com.TradingPlatform.messanger.MessageType;
import rawai.it.com.TradingPlatform.position.Position;
import rawai.it.com.TradingPlatform.position.PositionService;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.transaction.TransactionStreamService;
import rawai.it.com.TradingPlatform.urlutils.CurlConfig;
import rawai.it.com.TradingPlatform.urlutils.CurlConfiguration;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@Controller
public class DashboardController {

	private static final Logger log = LogManager.getLogger(DashboardController.class);
	
	@Autowired	
	private AccountRepository accountRepository;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private TransactionService transactionService;
	@Autowired
	private DashBoardService dashBoardService;
	@Autowired
	private PositionService positionservice;
	@Autowired
	private TransactionStreamService transactionController;
	@Autowired
	private MessageService messageService;
	@Autowired
	private CurlConfig curlConfigO;
	@Autowired
	private CurlConfiguration config;
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	@Inject
	private PasswordEncoder passwordEncoder;

	@Autowired
	private PriceService priceService;

	private static final CurrencyEnum targetCurrency = CurrencyEnum.EUR;

	@RequestMapping(value = "makepass", method = RequestMethod.GET)
	@ResponseBody
	public String makePassword(@RequestParam("pass") String pass) {
		return passwordEncoder.encode(pass);
	}

	@RequestMapping(value = "dashboard", method = RequestMethod.GET)
	public ModelAndView showDashboard(HttpServletRequest request) {


		ModelAndView resultView = new ModelAndView("/dashboard/dashboard");

		List<Account> accounts = accountRepository.findAll();
		request.getSession().setAttribute("accounts", accounts);
		SessionUtils.setAccounts(accounts);
		dashBoardService.prepareAccountCurrencies();

		Date today = new Date();

		log.info("##########START GETTING UPL-DAY###########################");
		
		double plToday = transactionService.getAllRealizedProfit(today, today, targetCurrency);
		log.info("##########STOP GETTING UPL-DAY###########################");

		Calendar gc = GregorianCalendar.getInstance();

		gc.set(Calendar.DAY_OF_MONTH, 1);

		Date firstDayOfMonth = gc.getTime();

		log.info("##########START GETTING UPL-MONTH###########################");
		double plMonth = transactionService.getAllRealizedProfit(firstDayOfMonth, today, targetCurrency);
		log.info("##########STOP UPL-MONTH###########################");

		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("plToday", FormatingUtils.formatAmountCurrency(plToday, targetCurrency.getCurrencySymbol()));
		resultView.addObject("plMonth", FormatingUtils.formatAmountCurrency(plMonth, targetCurrency.getCurrencySymbol()));

		List<Position> winners = positionservice.getTopPositions(5, true);
		List<Position> loosers = positionservice.getTopPositions(5, false);

		for (Position winner : winners) {
			winner.setUnrealizedPL(FormatingUtils.formatAmountCurrency(winner.getUnrealizedPL(), SessionUtils.findAccountByUserId(winner.getUserID()).getCurrency()
					.getCurrencySymbol()));
		}

		for (Position looser : loosers) {
			looser.setUnrealizedPL(FormatingUtils.formatAmountCurrency(looser.getUnrealizedPL(), SessionUtils.findAccountByUserId(looser.getUserID()).getCurrency()
					.getCurrencySymbol()));
		}

		resultView.addObject("topWinners", winners);
		resultView.addObject("topLoosers", loosers);
		resultView.addObject("statusConditions", PageConditionEnum.DASHBOARD.getConditions());
		return resultView;
	}

	// @RequestMapping(value = "dashboard/starttransactionservice", method =
	// RequestMethod.GET)
	// @ResponseBody
	// public String startTransactionService() {
	// if (!SessionUtils.transactionStreamsRunning()) {
	// SessionUtils.startUpTransactionStreams(transactionController);
	// }
	// return "OK";
	// }

	@RequestMapping(value = "dashboard/checkcurrencies", method = RequestMethod.GET)
	@ResponseBody
	public String checkCurrencies() {
		Message message = new Message();
		message.setType(MessageType.TYPE_DANGER.getType());
		message.setHeader("Customers with wrong Currencies!");
		message.setMessage(dashBoardService.checkAccountsCurrencies());
		messageService.sendGlobalMessage(message);
		return "OK";
	}

	private DashBoard updateDashBoard() {

		DashBoard dashBoard = new DashBoard();
		RestTemplate tmpl = new RestTemplate();
		// Fetch Number Of Accounts

		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountRepository.findAll());
		}
		dashBoardService.prepareAccountCurrencies();


		dashBoard.setNumberOfAccounts(String.valueOf(SessionUtils.getAccounts().size()));

		double totalNAV = 0;
		double totalUnrealizedPL = 0;
		double totalRealized = 0;
		double totalPL = 0;


		for (Account account : SessionUtils.getAccounts()) {
			String apiKey = account.getApiKey();
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
			ResponseEntity<OandaAccountSummaryResponse> resp = tmpl.exchange(curlUtil.createAccountUrl() + "/" + account.getUserId() + "/summary", HttpMethod.GET, apiHeader,
					OandaAccountSummaryResponse.class);
			Double exchangeRate = 1d;


			if (!account.getCurrency().equals(targetCurrency)) {
				priceService.fetchExchangeRate(account.getUserId(), account.getCurrency(), targetCurrency);
				exchangeRate = 1d / exchangeRate;
			}

			totalNAV += Double.valueOf(resp.getBody().getAccount().getNAV()) * exchangeRate;
			totalUnrealizedPL += Double.valueOf(resp.getBody().getAccount().getUnrealizedPL()) * exchangeRate;
			totalPL += Double.valueOf(resp.getBody().getAccount().getPl()) * exchangeRate;

		}

		totalRealized = totalPL - totalUnrealizedPL;

		Date today = new Date();
		double plToday = transactionService.getAllRealizedProfit(today, today, CurrencyEnum.EUR);

		dashBoard.setPlToday(FormatingUtils.formatAmountCurrency(plToday, targetCurrency.getCurrencySymbol()));
		dashBoard.setTotalValueOfAccounts(FormatingUtils.formatAmountCurrency(totalNAV, targetCurrency.getCurrencySymbol()));
		dashBoard.setTotalRealized(FormatingUtils.formatAmountCurrency(totalRealized, targetCurrency.getCurrencySymbol()));
		dashBoard.setTotalUnrealized(FormatingUtils.formatAmountCurrency(totalUnrealizedPL, targetCurrency.getCurrencySymbol()));

		List<Position> winners = positionservice.getTopPositions(5, true);
		List<Position> loosers = positionservice.getTopPositions(5, false);

		for (Position winner : winners) {
			winner.setUnrealizedPL(FormatingUtils.formatAmountCurrency(winner.getUnrealizedPL(), SessionUtils.findAccountByUserId(winner.getUserID()).getCurrency()
					.getCurrencySymbol()));
		}

		for (Position looser : loosers) {
			looser.setUnrealizedPL(FormatingUtils.formatAmountCurrency(looser.getUnrealizedPL(), SessionUtils.findAccountByUserId(looser.getUserID()).getCurrency()
					.getCurrencySymbol()));
		}

		dashBoard.setWinners(winners);
		dashBoard.setLoosers(loosers);

		return dashBoard;
	}

	@PostConstruct
	public void startUpTransactionService() {
		SessionUtils.startUpTransactionStreams(transactionController);
	}

	@PreDestroy
	public void destroy() {
		log.info("Stopping TransactionStreams !!!");
		// SessionUtils.stopTransactionStreamRunners();
		log.info("Stopping TransactionStreams  SUCCESS SUCCESS!!!");
		SessionUtils.stopAllPriceRunners();
	}


	public void sendDachBoartToClient() {
		log.info("Sending data to Dasshboard!");
		template.convertAndSend("/topic/dashboard", updateDashBoard());
	}
}
