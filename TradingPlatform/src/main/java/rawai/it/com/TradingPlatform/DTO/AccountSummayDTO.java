package rawai.it.com.TradingPlatform.DTO;

public class AccountSummayDTO {

	String username;
	String instrument;
	String interval;
	String units;
	String masssTrade;
	String engine;
	String userID;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getMasssTrade() {
		return masssTrade;
	}

	public void setMasssTrade(String masssTrade) {
		this.masssTrade = masssTrade;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
