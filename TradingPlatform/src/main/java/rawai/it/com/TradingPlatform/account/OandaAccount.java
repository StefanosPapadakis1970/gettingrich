package rawai.it.com.TradingPlatform.account;

import java.util.List;

import rawai.it.com.TradingPlatform.position.Position;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OandaAccount extends OandaAccountSummary {

	private List<Position> positions;


	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}
}
