package rawai.it.com.TradingPlatform.home;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(Principal principal) {
		return principal != null ? new ModelAndView("home/homeSignedIn") : new ModelAndView("signin/signin");
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		// String username = authentication.getName();
		// return principal != null ? "redirect:/home/homeSignedIn" :
		// "redirect:/signin";
	}

	@RequestMapping(value = "/comingsoon", method = RequestMethod.GET)
	public ModelAndView commingSoon() {
		return new ModelAndView("home/comingsoon");
	}
}
