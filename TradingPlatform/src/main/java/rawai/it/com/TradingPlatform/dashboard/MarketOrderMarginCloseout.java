package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketOrderMarginCloseout {

	private MarketOrderMarginCloseoutReason reason;

	public MarketOrderMarginCloseoutReason getReason() {
		return reason;
	}

	public void setReason(MarketOrderMarginCloseoutReason reason) {
		this.reason = reason;
	}
}
