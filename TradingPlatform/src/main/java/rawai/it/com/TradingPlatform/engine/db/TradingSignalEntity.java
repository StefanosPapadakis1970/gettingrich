package rawai.it.com.TradingPlatform.engine.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;
import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;


@Entity
@Table(name = "tradingsignal")
@NamedQueries({ @NamedQuery(name = TradingSignalEntity.FIND_ALL, query = "select signal from TradingSignalEntity signal order by signal.tradingSignal") })
public class TradingSignalEntity implements Serializable{

	private static final long serialVersionUID = -5568526123824398665L;

	public static final String FIND_ALL = "TradingSignalEntity.findAll";

	@Id
	@GeneratedValue
	private Long id;

	private int low;

	private int high;

	private int signalvalue;


	@Convert(converter = TradingSignalConverter.class)
	private TradingSignalEnum tradingSignal;

	@Convert(converter = RangeConverter.class)
	private RangeEnum candleRange;



	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "algorithm_signal", joinColumns = { @JoinColumn(name = "signal_id") }, inverseJoinColumns = { @JoinColumn(name = "algorithm_id") })
	private List<TradingAlgorithmEntity> tradingalgorithm = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLow() {
		return low;
	}

	public void setLow(int low) {
		this.low = low;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public TradingSignalEnum getTradingSignal() {
		return tradingSignal;
	}

	public void setTradingSignal(TradingSignalEnum tradingSignal) {
		this.tradingSignal = tradingSignal;
	}

	public RangeEnum getCandleRange() {
		return candleRange;
	}

	public void setCandleRange(RangeEnum candleRange) {
		this.candleRange = candleRange;
	}


	public int getSignalvalue() {
		return signalvalue;
	}

	public void setSignalvalue(int signalvalue) {
		this.signalvalue = signalvalue;
	}

	public List<TradingAlgorithmEntity> getTradingalgorithms() {
		return tradingalgorithm;
	}

	public void setTradingalgorithm(List<TradingAlgorithmEntity> tradingalgorithm) {
		this.tradingalgorithm = tradingalgorithm;
	}

}
