package rawai.it.com.TradingPlatform.candle;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;

import rawai.it.com.TradingPlatform.utils.FormatingUtils;


public class CandleDataPoint implements CandleDataPointInterface {

	@JsonProperty("candle")
	private Candle candle;

	private int candleIndex;

	@Override
	public String getY() {
		return "[" + candle.getMid().getO() + "," + candle.getMid().getH() + "," + candle.getMid().getL() + "," + candle.getMid().getC() + "]";
	}

	public String getTime() {
		return candle.getTime();
	}

	@Override
	public boolean getComplete() {
		return candle.isComplete();
	}

	@Override
	public String getO() {
		return candle.getMid().getO().toString();
	}

	@Override
	public String getH() {
		return candle.getMid().getH().toString();
	}

	@Override
	public String getC() {
		return candle.getMid().getC().toString();
	}

	@Override
	public String getL() {
		return candle.getMid().getL().toString();
	}

	public Candle getCandle() {
		return candle;
	}

	public void setCandle(Candle candle) {
		this.candle = candle;
	}

	@Override
	public Long getX() {
		return Instant.parse(candle.getTime()).toEpochMilli();
    }

	public int getCandleIndex() {
		return candleIndex;
	}

	public void setCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
	}

	@Override
	public boolean isTheFirstCandleOfTheDay() {
		return candle.isFirstCandleOfTheDay();
	}

	@Override
	public void setTheFirstCandleOfTheDay(boolean firstCandleOfTheDay) {
		candle.setFirstCandleOfTheDay(firstCandleOfTheDay);
	}

}
