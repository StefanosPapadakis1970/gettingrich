package rawai.it.com.TradingPlatform.price;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.CurrencyEnum;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Service
public class PriceService {	

	private static final Logger logger = LogManager.getLogger(PriceService.class);


	@Autowired
	private CurlUtil curlUtil;


	public Price fetchPrice(String userID, String instrument) throws HttpClientErrorException {
		String url = curlUtil.PRAXIS_URL + "v1/prices?instruments=" + instrument;
		RestTemplate tmpl = new RestTemplate();
		String apiKey = SessionUtils.findAccountByUserId(userID).getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		ResponseEntity<PriceResponseList> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PriceResponseList.class);
		return resp.getBody().getPrices().get(0);
	}

	public Double fetchExchangeRate(String userID, CurrencyEnum sourceCurrency, CurrencyEnum targetCurrency) {
		String instrument = targetCurrency.name() + "_" + sourceCurrency.name();
		Price price = null;
		Double rate = 0d;
		try {
			price = fetchPrice(userID, instrument);
			rate = Double.valueOf(price.getAsk());
		} catch (HttpClientErrorException e) {
			instrument = sourceCurrency.name() + "_" + targetCurrency.name();
			price = fetchPrice(userID, instrument);
			rate = 1d / Double.valueOf(price.getAsk());
		}
		return rate;
	}

}
