package rawai.it.com.TradingPlatform.config;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.stereotype.Controller;

import rawai.it.com.TradingPlatform.dashboard.DashboardController;
import rawai.it.com.TradingPlatform.instrument.InstrumentController;
import rawai.it.com.TradingPlatform.order.OrderController;
import rawai.it.com.TradingPlatform.position.PositionController;
import rawai.it.com.TradingPlatform.price.PriceRunner;
import rawai.it.com.TradingPlatform.price.PriceStreamService;
import rawai.it.com.TradingPlatform.transaction.TransactionStreamService;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class PresenceChannelInterceptor extends ChannelInterceptorAdapter {
	@Autowired
	private TransactionStreamService transactionservice;
	@Autowired
	private InstrumentController instrumentController;
	@Autowired
	private PriceStreamService priceStreamService;
	@Autowired
	private DashboardController dashBoardController;
	@Autowired
	private PositionController positionController;
	@Autowired
	private OrderController orderController;

	private static final Logger logger = LogManager.getLogger(PositionController.class);

	@Override
	public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

		StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);

		// ignore non-STOMP messages like heartbeat messages
		if (sha.getCommand() == null) {
			return;
		}

		String sessionId = sha.getSessionId();

		switch (sha.getCommand()) {
		case CONNECT:
			logger.info("STOMP Connect  [sessionId: " + sessionId + "]");
			if (sha.getNativeHeader("token") != null) {
				sha.getSessionAttributes().put("Service", WebSocketServiceEnum.valueOf(sha.getNativeHeader("token").get(0)).name());
				sha.getSessionAttributes().put("sessionId", sessionId);
				processToken(sha.getNativeHeader("token"), sha.getNativeHeader("instruments"), sha.getNativeHeader("interval"), sha.getNativeHeader("userID"), sessionId, true);
			}
			break;
		case CONNECTED:
			logger.info("STOMP Connected [sessionId: " + sessionId + "]");
			break;
		case DISCONNECT:
			logger.info("STOMP Disconnect [sessionId: " + sessionId + "]");
			processSession(WebSocketServiceEnum.valueOf(sha.getSessionAttributes().get("Service").toString()), sessionId, false);
			break;
		case SEND:
			logger.info("STOMP SEND [sessionId: " + sessionId + "]");
			break;
		default:
			break;

		}
	}

	// PRICES_POSITION

	private void processSession(WebSocketServiceEnum webSocketServiceEnum, String sessionID, boolean startService) {
		switch (webSocketServiceEnum) {
		case ALL_POSITIONS:
			SessionUtils.stopPositionRunner(sessionID);
			break;
		case DASHBOARD: {
			SessionUtils.stopDashboardRunner(sessionID);
		}
			break;
		case SINGLE_POSITION:
			SessionUtils.stopSinglePositionRunner(sessionID);
			break;
		case TRANSACTIONS:
			// SessionUtils.stopTransactionStreamRunners();
			break;
		case CHART:
			SessionUtils.stopChartService(sessionID);
			break;
		case ALL_ORDERS:
			SessionUtils.stopOpenOrderRunner(sessionID);
			break;
		case PRICES_POSITION:
			SessionUtils.stopPriceRunners(PriceRunner.POSITIONS, sessionID);
			break;
		case PRICES_OPEN_ORDERS:
			SessionUtils.stopPriceRunners(PriceRunner.PRICE, sessionID);
			break;
		case PRICES_MARKET_ORDER:
			SessionUtils.stopPriceRunners(PriceRunner.PRICE, sessionID);
			break;
		case PRICES_MASS_ORDER:
			// SessionUtils.stopPriceRunners(PriceRunner.ORDERS);
			break;
		case PRICES_OPEN_POSITIONS:
			// SessionUtils.stopPriceRunners(PriceRunner.POSITIONS);
			break;

		default:
			break;
		}
	}

	private void processToken(List<String> nativeHeader, List<String> instruments, List<String> interval, List<String> userID, String sessionID, boolean startService) {
		if (nativeHeader == null || nativeHeader.size() == 0) {
			return;
		}

		WebSocketServiceEnum seviceName = WebSocketServiceEnum.valueOf(nativeHeader.get(0));
		switch (seviceName) {
		case ALL_POSITIONS:
			SessionUtils.startPositionRunner(positionController, sessionID);
			break;
		case DASHBOARD:
			SessionUtils.startDashBoardStream(dashBoardController, sessionID);
			break;
		case SINGLE_POSITION:
			SessionUtils.startSinglePositionRunner(positionController, sessionID);
			break;
		case TRANSACTIONS:
			// SessionUtils.startUpTransactionStreams(transactionservice);
			break;
		case CHART:
			SessionUtils.startUpChartStream(instruments.get(0), interval.get(0), userID.get(0), instrumentController, sessionID);
			break;
		case ALL_ORDERS:
			SessionUtils.startOpenOrderRunner(orderController, sessionID);
			break;
		case PRICES_POSITION:
			if (instruments != null) {
				SessionUtils.startPriceRunners(PriceRunner.POSITIONS, priceStreamService, instruments, sessionID);
			}
			break;
		case PRICES_OPEN_ORDERS:
			if (instruments != null) {
				SessionUtils.startPriceRunners(PriceRunner.PRICE, priceStreamService, instruments, sessionID);
			}
		case PRICES_MARKET_ORDER:
			if (instruments != null) {
				SessionUtils.startPriceRunners(PriceRunner.PRICE, priceStreamService, instruments, sessionID);
			}
		default:
			break;
		}
	}
}