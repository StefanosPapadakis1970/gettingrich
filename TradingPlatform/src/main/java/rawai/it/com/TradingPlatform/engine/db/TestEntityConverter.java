package rawai.it.com.TradingPlatform.engine.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.algorithmtesting.TestEntity;
import rawai.it.com.TradingPlatform.algorithmtesting.TestForm;
import rawai.it.com.TradingPlatform.algorithmtesting.TestStatusEnum;

public class TestEntityConverter {
	
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat(TestForm.DATE_PATTERN);

	public static TestEntity convertFormToEntity(TestForm testForm) {
		TestEntity testEntity = new TestEntity();
		testEntity.setEndDate(testForm.getEndDateDateObject());
		testEntity.setStartDate(testForm.getStartDateDateObject());
		testEntity.setGenerateTrades(testForm.isGenerateTrades());
		testEntity.setGranularity(testForm.getInterval());
		testEntity.setInstrument(testForm.getInstrument());
		testEntity.setInterval(testForm.getInterval());
		testEntity.setMaxNumberOfTransactions(Integer.valueOf(testForm.getMaxNumberOfTransactions()));
		testEntity.setRunType(testForm.getRunType());
		testEntity.setStatus(TestStatusEnum.RUNNING);
		testEntity.setUnits(Integer.valueOf(testForm.getUnits()));
		testEntity.setUserID(testForm.getUserID());
		return testEntity;
	}

	public static TestForm convertEntityToForm(TestEntity testEntity) throws ParseException{
		TestForm testForm = new TestForm();
		testForm.setAlgorithmName(testEntity.getAlgorithm().getName());
		testForm.setEndDateDateObject(testEntity.getEndDate());
		testForm.setGenerateTrades(testEntity.isGenerateTrades());		
		testForm.setInstrument(testEntity.getInstrument());
		testForm.setInterval(testEntity.getGranularity());
		testForm.setMaxNumberOfTransactions(String.valueOf(testEntity.getMaxNumberOfTransactions()));
		testForm.setRunType(testEntity.getRunType());
		testForm.setStartDateDateObject(testEntity.getStartDate());
		testForm.setUnits(String.valueOf(testEntity.getUnits()));
		testForm.setUserID(testEntity.getUserID());
		testForm.setDbID(testEntity.getId());
		testForm.setStatus(testEntity.getStatus());		
		
		testForm.setStartDate(dateFormat.format(testEntity.getStartDate()));
		testForm.setEndDate(dateFormat.format(testEntity.getEndDate()));	
		
		return testForm;
	}

	public static List<TestForm> convertEnityListToFormList(List<TestEntity> entities) throws ParseException {
		List<TestForm> formList = new ArrayList<>();
		for (TestEntity entity : entities) {
			formList.add(convertEntityToForm(entity));
		}
		return formList;
	}
}
