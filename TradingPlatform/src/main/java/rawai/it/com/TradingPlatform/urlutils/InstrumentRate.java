package rawai.it.com.TradingPlatform.urlutils;

public class InstrumentRate {

	private String instrument;
	private String interval;

	public InstrumentRate(String instrument, String interval) {
		super();
		this.instrument = instrument;
		this.interval = interval;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}
}
