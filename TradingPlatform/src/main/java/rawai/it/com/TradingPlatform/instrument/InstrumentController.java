package rawai.it.com.TradingPlatform.instrument;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Synchronized;
import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.candle.CandleDataPointListResponse;
import rawai.it.com.TradingPlatform.candle.CandleListResponse;
import rawai.it.com.TradingPlatform.candle.CandleTimeValidator;
import rawai.it.com.TradingPlatform.candle.Mid;
import rawai.it.com.TradingPlatform.chart.AxisX;
import rawai.it.com.TradingPlatform.chart.AxisY;
import rawai.it.com.TradingPlatform.chart.Chart;
import rawai.it.com.TradingPlatform.chart.ChartData;
import rawai.it.com.TradingPlatform.chart.Title;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.CandleListener;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.engine.TradingProfile;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeEntity;
import rawai.it.com.TradingPlatform.engine.db.AlgorithmTradeRepository;
import rawai.it.com.TradingPlatform.order.OrderService;
import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.RegisterEnum;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@Controller
public class InstrumentController {

	private static final Logger logger = LogManager.getLogger(InstrumentController.class);
	private static final Logger candleLogger = LogManager.getLogger("candle-log");	
	private static final Logger tradingLogger = LogManager.getLogger("tradingengine-log");
	private static final Logger orderLog = LogManager.getLogger("ordercreation-log");

	@Autowired
	private AlgorithmTradeRepository tradeRepository;
	@Autowired 
	private AccountRepository accountRepository;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	public HttpSession session;
	@Autowired
	private OrderService orderService;
	@Autowired
	public InstrumentService instrumentService;
	@Autowired
	CurlUtil curlUtil;
	private final CandleTimeValidator candleTimeValidator = new CandleTimeValidator();


	@RequestMapping(value = "instrument/showall", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ModelAndView showAllTradeableInstruments(@RequestParam("userid") String userid, HttpServletRequest request) {
		ModelAndView resultView = new ModelAndView("/instrument/showall");
		Account account = SessionUtils.findAccountByUserId(userid);

		instrumentService.setupInstruments();

		resultView.addObject("username", account.getUserName());
		resultView.addObject("instruments", SessionUtils.instrumentMap.get(userid));
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("account", account);
		return resultView;
	}

	public String fetchCandleForChartWindow(String userID, String instrument, String granularity) {
		Account account = SessionUtils.findAccountByUserId(userID);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		RestTemplate restTemplate = new RestTemplate();
		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();

		ChartInstrument lastCandle = SessionUtils.fetchLastCandle(userID, instrument, granularity);
		if (lastCandle == null) {
			// return createErrorCandleJSON("LastCandle not set!", null);
		}

		logger.info("################  Fetching candles for ChartWindow ################");
		logger.info("Start Fetching new Candle Last Candle from Session " + lastCandle.getTime());
		ResponseEntity<CandleListResponse> resp = null;

		try {
			logger.info(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity);
			resp = restTemplate.exchange(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?count=5&granularity=" + granularity, HttpMethod.GET,
					apiHeader,
					CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
		}

		if (resp == null || resp.getBody() == null || resp.getBody().getCandles() == null) {
			logger.info("Empty Candles!");
			// return createErrorCandleJSON("Error fetching candles!", null);
		}
		logger.info("Fetched Candles ");
		for (Candle candle : resp.getBody().getCandles()) {
			logger.info(candle.getTime() + " complete " + candle.isComplete());
		}
		boolean intervalHasGaps = false;
		int nextCandleIndex = 0;
		int lastCandleIndex = findLastCandleIndex(resp.getBody().getCandles(), lastCandle);
		logger.info("LastCandleIndex " + lastCandleIndex + " Time: " + lastCandle.getTime());
		for (Candle candle : resp.getBody().getCandles()) {
			nextCandleIndex++;
			if (nextCandleIndex < resp.getBody().getCandles().size() && nextCandleIndex > lastCandleIndex ) {
				DateTime currentCandleTime = FormatingUtils.parseDate(candle.getTime());
				DateTime nextCandleTime = FormatingUtils.parseDate(resp.getBody().getCandles().get(nextCandleIndex).getTime());
				Seconds seconds = Seconds.secondsBetween(currentCandleTime, nextCandleTime);
				logger.info("Current " + currentCandleTime + " Next " + nextCandleTime + " Diff " + seconds);
				if (seconds.getSeconds() > IntervalEnum.getValue(granularity).toSeconds()){
					logger.info("Candles missing!");
					SessionUtils.setLastFechtedCandle(userID, instrument, resp.getBody().getCandles().get(nextCandleIndex).getTime(), granularity);
					// return createErrorCandleJSON("Candles missing!",
					// resp.getBody().getCandles().get(nextCandleIndex));
				}
			}
		}

		CandleDataPointInterface lastCompleteCandle = null;
		Candle indexCandle = null;
		int candleIndex = 0;
		for (Candle candle : resp.getBody().getCandles()) {
			logger.info(resp.getBody().getCandles().size() + "----" + candleIndex + "---" + candle.getTime());
			if (candle.getTime().equals(lastCandle.getTime())) {
				indexCandle = candle;
			}
		}

		candleIndex = resp.getBody().getCandles().indexOf(indexCandle);
		

		logger.info("CandleIndex before candleAccess " + candleIndex);
		CandleDataPoint candleDataPoint = new CandleDataPoint();
		if (resp.getBody().getCandles().size() - 1 > candleIndex && resp.getBody().getCandles().get(candleIndex + 1).isComplete()) { // getNextCompleteCandle
			candleDataPoint.setCandle(resp.getBody().getCandles().get(candleIndex + 1));
		} else {
			candleDataPoint.setCandle(resp.getBody().getCandles().get(candleIndex));
		}
		lastCompleteCandle = candleDataPoint;

	    logger.info("Next Candle " + lastCompleteCandle.getTime());
		SessionUtils.setLastFechtedCandle(userID, instrument, lastCompleteCandle.getTime(), granularity);

		try {
			charJSON = mapper.writeValueAsString(lastCompleteCandle);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		logger.info("Result Candle " + lastCompleteCandle.getTime());
		return charJSON;
	}


	@Synchronized
	private synchronized CandleDataPointInterface fetchCandleForTradingEngine(String userID, String instrument, String granularity) {
		Account account = SessionUtils.findAccountByUserId(userID);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		RestTemplate restTemplate = new RestTemplate();
		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();


		logger.info("####### FETCH CANDLE FOR TRADING-ENGINE ########");
		logger.info("#" + userID + " " + instrument + " " + granularity);
		logger.info("################################################");
		
		orderLog.info("####### FETCH CANDLE FOR TRADING-ENGINE ######## starttime:-> "+Instant.now().toString());
		orderLog.info("#" + userID + " " + instrument + " " + granularity+" at "+Instant.now().toString());
		orderLog.info("################################################");
		
		candleLogger.info("####### FETCH CANDLE FOR TRADING-ENGINE ########");
		candleLogger.info("#" + userID + " " + instrument + " " + granularity);
		candleLogger.info("################################################");


		ChartInstrument lastCandle = SessionUtils.fetchLastCandle(userID, instrument, granularity);
		if (lastCandle == null) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			Candle candle = new Candle();
			candleDataPoint.setCandle(candle);
			candle.setTime("ERROR last Candle not Set");
			return candleDataPoint;
		}
		logger.info("Start Fetching new Candle Last Candle from Session " + lastCandle.getTime());
		candleLogger.info("Start Fetching new Candle Last Candle from Session " + lastCandle.getTime());
		ResponseEntity<CandleListResponse> resp = null;
		try {
			logger.info(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity);
			candleLogger.info(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?granularity=" + granularity);
			resp = restTemplate.exchange(curlUtil.PRAXIS_URL + "v3/instruments/" + instrument + "/candles?count=5&granularity=" + granularity, HttpMethod.GET,
					apiHeader, CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			candleLogger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
		}

		logger.info("####### Candles ARE FETCHED ########");
		logger.info("#" + userID + " " + instrument + " " + granularity);
		logger.info("################################################");
		
		
		candleLogger.info("####### Candles ARE FETCHED ########");
		candleLogger.info("#" + userID + " " + instrument + " " + granularity);
		candleLogger.info("################################################");


		if (resp == null || resp.getBody() == null || resp.getBody().getCandles() == null) {
			logger.info("Empty Candles!");
			candleLogger.info("Empty Candles!");
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			Candle candle = new Candle();
			candleDataPoint.setCandle(candle);
			candle.setTime("ERROR Empty Candles!");
			return candleDataPoint;
		}
		logger.info("Fetched Candles ");
		candleLogger.info("Fetched Candles ");
		for (Candle candle : resp.getBody().getCandles()) {
			logger.info(candle.getTime() + " complete " + candle.isComplete());
			candleLogger.info(candle.getTime() + " complete " + candle.isComplete());
		}
		boolean intervalHasGaps = false;
		int nextCandleIndex = 0;
		int lastCandleIndex = findLastCandleIndex(resp.getBody().getCandles(), lastCandle);


		CandleDataPointInterface lastCompleteCandle = null;
		Candle indexCandle = null;
		int candleIndex = 0;
		for (Candle candle : resp.getBody().getCandles()) {
			logger.info(resp.getBody().getCandles().size() + "----" + candleIndex + "---" + candle.getTime());
			candleLogger.info(resp.getBody().getCandles().size() + "----" + candleIndex + "---" + candle.getTime());
			if (candle.getTime().equals(lastCandle.getTime())) {
				indexCandle = candle;
			}
		}

		candleIndex = resp.getBody().getCandles().indexOf(indexCandle);

		logger.info("CandleIndex before candleAccess " + candleIndex);
		candleLogger.info("CandleIndex before candleAccess " + candleIndex);
		CandleDataPoint candlekDataPoint = new CandleDataPoint();
		if (resp.getBody().getCandles().size() - 1 > candleIndex && resp.getBody().getCandles().get(candleIndex + 1).isComplete()) { // getNextCompleteCandle
			candlekDataPoint.setCandle(resp.getBody().getCandles().get(candleIndex + 1));
			lastCompleteCandle = candlekDataPoint;
			logger.info("Next Candle " + lastCompleteCandle.getTime());
			candleLogger.info("Next Candle " + lastCompleteCandle.getTime());
			SessionUtils.setLastFechtedCandle(userID, instrument, lastCompleteCandle.getTime(), granularity);
		} else {
			// candlekDataPoint.setCandle(resp.getBody().getCandles().get(candleIndex));
			lastCompleteCandle = null;
		}



		try {
			charJSON = mapper.writeValueAsString(lastCompleteCandle);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		if (lastCompleteCandle != null) {
			logger.info("############     Result Candle " + lastCompleteCandle.getTime() + " closing " + lastCompleteCandle.getC() + "  ######################");
			candleLogger.info("############     Result Candle " + lastCompleteCandle.getTime() + " closing " + lastCompleteCandle.getC() + "  ######################");
		} else {
			logger.info("############     Result Candle " + lastCompleteCandle + "  ######################");
			candleLogger.info("############     Result Candle " + lastCompleteCandle + "  ######################");
		}

		orderLog.info("Candle fetching finished at -> "+Instant.now().toString());
		return lastCompleteCandle;
	}

	private void fireProcessCandle(CandleDataPointInterface candle, TradingInstrument instrument) {
		orderLog.info("Processcandle fired at -> "+Instant.now().toString()+" candletime -> "+candle.getTime());
		// Enable this Lines to make the candletimecheckwork
		// if (!candleTimeValidator.validTimeinterval(candle.getTime())) {
		// tradingLogger.info("Invalid CandleTime " + candle.getTime());
		// return;
		// }
		for (CandleListener candleListener : SessionUtils.candleListeners) {
			List<AlgorithmTrade> trades = candleListener.processCandle(candle, instrument);
			String result = orderService.createTrades(trades);
			logger.info("Status TradeGeneration " + result);
		}
	}

	public int findLastCandleIndex(List<Candle> candles, ChartInstrument lastCandle) {		
		for (Candle candle : candles){
			if (candle.getTime().equals(lastCandle.getTime())){
				return candles.lastIndexOf(candle);
			}
		}		
		return -1;
	}

	// Have to be tested
	public List<CandleDataPointInterface> fetchCandles(String userid, String instrument, String granularity) {
		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userid);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		ResponseEntity<CandleListResponse> resp = null;

		try {
			resp = tmpl.exchange(curlUtil.createBaseUrl() + "instruments/" + instrument + "/candles?count=10&price=M&granularity=" + granularity, HttpMethod.GET, apiHeader,
					CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
			return null;
		}

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();

		StringJoiner joiner = new StringJoiner(",");
		for (Candle candle : resp.getBody().getCandles()) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			if (candle.isComplete()) {
				candleSticks.add(candleDataPoint);
			}
		}
		return candleSticks;
	}

	@RequestMapping(value = "instrument/chart", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ModelAndView showChart(@RequestParam("userid") String userid, @RequestParam("instrument") String instrument, @RequestParam("granularity") String granularity,
			HttpServletRequest request) {
		RestTemplate tmpl = new RestTemplate();
		ModelAndView resultView = new ModelAndView("/chart/chart");
		Account account = SessionUtils.findAccountByUserId(userid);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		ResponseEntity<CandleListResponse> resp = null;

		try {
			resp = tmpl.exchange(curlUtil.createBaseUrl() + "instruments/" + instrument + "/candles?count=60&price=M&granularity=" + granularity, HttpMethod.GET, apiHeader,
					CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
		}

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();


		for (Candle candle : resp.getBody().getCandles()) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			if (candle.isComplete()) {
				candleSticks.add(candleDataPoint);
			}
		}
		CandleDataPointInterface lastCandle = candleSticks.get(candleSticks.size() - 1);
		// public static void setLastFechtedCandle(String userID, String
		// instrument, Date time, String interval) {
		logger.info("Last Candle from Chart ->" + lastCandle.getTime());
		SessionUtils.setLastFechtedCandle(userid, instrument, lastCandle.getTime(), granularity);

		Chart chart = new Chart();
		Title title = new Title(instrument);
		chart.setTitle(title);

		AxisX axisX = new AxisX();
		axisX.setInterval(20);
		axisX.setIntervalType(IntervalEnum.valueOf(granularity).getIntervalName().toLowerCase());
		axisX.setValueFormatString("hh-mm-ss");
		axisX.setLabelAngle(-85);
		AxisY axisY = new AxisY();
		axisY.setIncludeZero(false);
		axisY.setTitle("Price");
		axisY.setPrefix("$");
		List<ChartData> dataList = new ArrayList<>();
		chart.setData(dataList);
		chart.setAxisY(axisY);
		chart.setAxisX(axisX);
		ChartData candleStickData = new ChartData();
		dataList.add(candleStickData);
		candleStickData.setxValueType("dateTime");
		candleStickData.setType("candlestick");
		candleStickData.setDataPoints(candleSticks);

		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			charJSON = mapper.writeValueAsString(chart);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		resultView.addObject("chart", charJSON);
		resultView.addObject("instrument", instrument);
		resultView.addObject("candlesticks", candleSticks.toArray());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("userID", userid);
		resultView.addObject("granularity", granularity);
		return resultView;
	}
	

	@RequestMapping(value = "instrument/tradeChart", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ModelAndView showTradeChart(@RequestParam("userID") String userID, @RequestParam("tradeID") String tradeID, HttpServletRequest request) {
		RestTemplate tmpl = new RestTemplate();
		ModelAndView resultView = new ModelAndView("/chart/tradechart");
		Account account = SessionUtils.findAccountByUserId(userID);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		ResponseEntity<CandleListResponse> resp = null;
		AlgorithmTradeEntity chartTrade = tradeRepository.findAlgorithmTradeByUserIdTradeID(userID, tradeID);
		

		try {
			resp = tmpl.exchange(curlUtil.createBaseUrl() + "instruments/" + chartTrade.getInstrument() + "/candles?count=60&price=M&granularity=" + chartTrade.getTradingInterval().getName(), HttpMethod.GET, apiHeader,
					CandleListResponse.class);
		} catch (HttpClientErrorException e1) {
			logger.error(e1.getResponseBodyAsString());
			e1.printStackTrace();
		}

		List<CandleDataPointInterface> candleSticks = new ArrayList<>();


		for (Candle candle : resp.getBody().getCandles()) {
			CandleDataPoint candleDataPoint = new CandleDataPoint();
			candleDataPoint.setCandle(candle);
			if (candle.isComplete()) {
				candleSticks.add(candleDataPoint);
			}
		}
		CandleDataPointInterface lastCandle = candleSticks.get(candleSticks.size() - 1);
		// public static void setLastFechtedCandle(String userID, String
		// instrument, Date time, String interval) {
		logger.info("Last Candle from Chart ->" + lastCandle.getTime());
		SessionUtils.setLastFechtedCandle(userID, chartTrade.getInstrument(), lastCandle.getTime(), chartTrade.getTradingInterval().getName());

		Chart chart = new Chart();
		Title title = new Title("TradeChart"+chartTrade.getInstrument());
		chart.setTitle(title);

		AxisX axisX = new AxisX();
		axisX.setInterval(20);
		
		axisX.setIntervalType(chartTrade.getTradingInterval().getIntervalName().toLowerCase());
		axisX.setValueFormatString("hh-mm-ss");
		axisX.setLabelAngle(-85);
		AxisY axisY = new AxisY();
		axisY.setIncludeZero(false);
		axisY.setTitle("Price");
		axisY.setPrefix("$");
		List<ChartData> dataList = new ArrayList<>();
		chart.setData(dataList);
		chart.setAxisY(axisY);
		chart.setAxisX(axisX);
		ChartData candleStickData = new ChartData();
		dataList.add(candleStickData);
		candleStickData.setxValueType("dateTime");
		candleStickData.setType("candlestick");
		candleStickData.setDataPoints(candleSticks);

		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			charJSON = mapper.writeValueAsString(chart);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		charJSON = charJSON.replaceAll("\\]\\\"", "]");
		charJSON = charJSON.replaceAll("\\\"\\[", "[");
		resultView.addObject("chart", charJSON);
		resultView.addObject("instrument", chartTrade.getInstrument());
		resultView.addObject("candlesticks", candleSticks.toArray());
		resultView.addObject("accounts", SessionUtils.getAccounts());
		resultView.addObject("userID", userID);
		resultView.addObject("granularity", chartTrade.getTradingInterval().toString());
		return resultView;
	}

	@RequestMapping(value = "instrument/opencloseprice", method = RequestMethod.GET)
	@ResponseBody
	public OpenClosePrice fetchOpenClosePrice(@RequestParam("userid") String userid, @RequestParam("instrument") String instrument, HttpServletRequest request) {
		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userid);
		String apiKey = account.getApiKey();
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);
		ResponseEntity<CandleDataPointListResponse> resp = tmpl.exchange(curlUtil.createBaseUrl() + "instruments/" + instrument + "/candles?count=2&price=M&granularity=D",
				HttpMethod.GET,
				apiHeader, CandleDataPointListResponse.class);
		OpenClosePrice openclosePrice = new OpenClosePrice();
		openclosePrice.setOpenPrice(resp.getBody().getCandles().get(1).getO());
		openclosePrice.setClosePrice(resp.getBody().getCandles().get(0).getC());

		return openclosePrice;
	}

	@RequestMapping(value = "instrument/register", method = RequestMethod.GET)
	@ResponseBody
	public String registerForInstrumentPrice(@RequestParam(value = "userID") String userID, @RequestParam(value = "instrument") String instrument,
			@RequestParam(value = "interval") String interval, HttpServletRequest request) {
		if (SessionUtils.registerChartInstrument(userID, instrument, interval) == RegisterEnum.ALREADY_REGISTERED) {
			return "ALREADY_REGISTERED";
		}
		return "OK";
	}

	@RequestMapping(value = "instrument/unregister", method = RequestMethod.GET)
	@ResponseBody
	public String unregisterForInstrumentPrice(@RequestParam(value = "userID") String userID, @RequestParam(value = "instrument") String instrument,
			@RequestParam(value = "interval") String interval, HttpServletRequest request) {
		SessionUtils.unregisterChartInstrument(userID, instrument, interval);
		return "OK";
	}

	private String createErrorCandleJSON(String errorText, Candle candle) {
		String charJSON = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (candle == null) {
				candle = new Candle();
				Mid mid = new Mid();
				mid.setL("0");
				mid.setO("0");
				mid.setH("0");
				mid.setC("0");
				candle.setMid(mid);
			}
			CandleDataPoint errorCandle = new CandleDataPoint();
			candle.setTime("Error: " + errorText);
			errorCandle.setCandle(candle);
			charJSON = mapper.writeValueAsString(errorCandle);
			charJSON = charJSON.replaceAll("\\]\\\"", "]");
			charJSON = charJSON.replaceAll("\\\"\\[", "[");
			return charJSON;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}

	// Deliver Candle to open ChartViewWindows
	private void deliverCandleToChartView(String interval) {
		for (String key : SessionUtils.getChartInstrumentMap().keySet()) {
			for (ChartInstrument chartInstrument : SessionUtils.getChartInstrumentMap().get(key)) {
				if (chartInstrument.getInterval().equals(interval)) {
					String candle = fetchCandleForChartWindow(key, chartInstrument.getInstrument(), chartInstrument.getInterval());
					if (candle != null) {
						template.convertAndSend("/topic/chart/" + chartInstrument.getInstrument() + "/" + chartInstrument.getInterval(), candle);
					}
				}
			}
		}
	}

	public void sentCandleToChart(String instrument, String interval, String candle) {
		template.convertAndSend("/topic/chart/" + instrument + "/" + interval, candle);
	}
	
	// Can be optimized ... maybe create one list

	public synchronized void deliverCandleToTradingEngine(String interval) {
		logger.info("###########################################################################");
		logger.info("#####################Deliver Candels ######################################");
		logger.info("###########################################################################");
		candleLogger.info("###########################################################################");
		candleLogger.info("#####################Deliver Candels ######################################");
		candleLogger.info("###########################################################################");
		for (TradingProfile profile : SessionUtils.getTradingProfiles()) {
			for (TradingInstrument instrument : profile.getTradingInstruments()) {
				if (!instrument.isTradingEngine() && instrument.getTradingInterval().name().equals(interval)) {
					continue;
				}
				orderLog.info("#### Fetching the candle for the Trading-Engine -> "+Instant.now().toString());
				CandleDataPointInterface candle = fetchCandleForTradingEngine(profile.getAccount().getUserId(), instrument.getInstument(), interval);				
				if (candle != null && candle.getTime() != null && !candle.getTime().contains("ERROR")) {
					orderLog.info("#### Candle fetched for the Trading-Engine -> "+Instant.now().toString()+" candle time -> "+candle.getTime());
					fireProcessCandle(candle, instrument);
				}
			}
		}
		logger.info("###########################################################################");
		logger.info("#####################Deliver Candles Finish################################");
		logger.info("###########################################################################");
		candleLogger.info("###########################################################################");
		candleLogger.info("#####################Deliver Candles Finish################################");
		candleLogger.info("###########################################################################");
	}

	public void registerCandleListener(CandleListener candleListener) {
		SessionUtils.addCandleListener(candleListener);
	}
}
