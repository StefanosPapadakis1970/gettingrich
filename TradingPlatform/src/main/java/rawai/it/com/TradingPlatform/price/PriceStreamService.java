package rawai.it.com.TradingPlatform.price;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.CandleListener;
import rawai.it.com.TradingPlatform.instrument.InstrumentController;
import rawai.it.com.TradingPlatform.messanger.Message;
import rawai.it.com.TradingPlatform.messanger.MessageService;
import rawai.it.com.TradingPlatform.order.OrderService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class PriceStreamService {
	private static final Logger log = LogManager.getLogger(PriceStreamService.class);
	private static final Logger tradingLogger = LogManager.getLogger("tradingengine-log");

	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	@Lazy
	private MessageService messageService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private InstrumentController instrumentController;

	public void streamPrices(String userID, String userName, PriceStreamRunnable runner, List<String> instruments) {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		runner.setHttpClient(httpClient);
		try {

			// Set these variables to whatever personal ones are preferred
			String url = curlUtil.STREAMING_URL;
			String access_token = SessionUtils.findAccountByUserId(userID).getApiKey();
			String account_id = userID;

			String instrumentsString = Joiner.on("%2C").join(instruments);

			HttpUriRequest httpGet = new HttpGet(url + "v3/accounts/" + account_id + "/pricing/stream?instruments=" + instrumentsString);
			httpGet.setHeader(new BasicHeader("Authorization", "Bearer " + access_token));

			log.info("Executing request: " + httpGet.getRequestLine());

			HttpResponse resp = httpClient.execute(httpGet);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.BAD_GATEWAY.value() || resp.getStatusLine().getStatusCode() == HttpStatus.GATEWAY_TIMEOUT.value()) {

				Message message = new Message();
				message.setHeader("Service not available!");
				message.setMessage("Could not access price stream!");
				messageService.sendGlobalMessage(message);
			}

			HttpEntity entity = resp.getEntity();

			log.info(resp.getStatusLine().getStatusCode());
			if (resp.getStatusLine().getStatusCode() == 200 && entity != null) {
				InputStream stream = entity.getContent();
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(stream));

				while ((line = br.readLine()) != null) {
					log.debug("########################### PRICE ############################");
					log.debug(line);
					log.debug("########################### PRICE finish #####################");
					if (!line.contains("HEARTBEAT PriceRunner")) {

						ObjectMapper mapper = new ObjectMapper();

						StreamingPrice price = mapper.readValue(line, StreamingPrice.class);

						template.convertAndSend("/topic/prices", price);
						
						fireProcessPrice(price);
					}

				}
			} else {
				// print error message
				String responseString = EntityUtils.toString(entity, "UTF-8");
				log.info(responseString);
			}
		} catch (IllegalStateException e) {
			log.error("e");
		} catch (IOException e) {
			if (e instanceof SocketException) {
				log.info("Socket closed!");
			} else {
				e.printStackTrace();
			}
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fireProcessPrice(CandleDataPointInterface candle) {		
		for (CandleListener candleListener : SessionUtils.candleListeners) {			
			List<AlgorithmTrade> postProcessTrades = orderService.fetchPostProcessTrades(((StreamingPrice)candle).getInstrument());
			if (postProcessTrades != null && !postProcessTrades.isEmpty()) {
				candleListener.postProcessTrades(postProcessTrades, candle);
				// create new StopLoss in case of movement
				for (AlgorithmTrade trade : postProcessTrades) {
					if (trade.isStopLossMoved()) {
						tradingLogger.info("Stop Loss has moved Trade will be updated ");
						orderService.updateStopLossTrade(trade);
					}
					trade.setStopLossMoved(false);
				}
			}
		}
	}
}
