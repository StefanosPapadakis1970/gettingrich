package rawai.it.com.TradingPlatform.dashboard;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderFillTransactionListResponse {

	List<OrderFillTransaction> transactions;

	public List<OrderFillTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<OrderFillTransaction> transactions) {
		this.transactions = transactions;
	}
}
