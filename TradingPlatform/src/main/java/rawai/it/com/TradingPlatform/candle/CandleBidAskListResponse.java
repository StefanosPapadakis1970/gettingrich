package rawai.it.com.TradingPlatform.candle;

import java.util.List;

public class CandleBidAskListResponse {

	private List<CandleBidAsk> candles;

	public List<CandleBidAsk> getCandles() {
		return candles;
	}

	public void setCandles(List<CandleBidAsk> candles) {
		this.candles = candles;
	}
}
