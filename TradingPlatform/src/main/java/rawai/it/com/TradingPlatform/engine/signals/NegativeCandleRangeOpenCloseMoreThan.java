package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class NegativeCandleRangeOpenCloseMoreThan extends AbstractTradingSignal {

	private int moreThanPips;

	public NegativeCandleRangeOpenCloseMoreThan(int moreThanPips) {
		super(BUYSELL.NEUTRAL, TradingSignalEnum.POSITIVE_CANDLERANGE_MORE_OC, 1);
		this.moreThanPips = moreThanPips;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			return new SignalResult(false, lastSignalCandleIndex, true);
		}

		CandleDataPointInterface rangeCandle = candles.get(lastSignalCandleIndex);
		int open = Integer.valueOf(rangeCandle.getO().replace(".", ""));
		int close = Integer.valueOf(rangeCandle.getC().replace(".", ""));

		int diff = open - close;

		if (diff >= moreThanPips && diff >= 0) {
			return new SignalResult(true, lastSignalCandleIndex, false);
		} else {
			return new SignalResult(false, lastSignalCandleIndex + 1, false);
		}
	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		return 0;
	}

	@Override
	public int getValue() {
		// TODO Auto-generated method stub
		return moreThanPips;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
