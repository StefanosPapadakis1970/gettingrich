package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.instrument.InstrumentController;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

@Controller
public class TradingEngineContoller implements CandleListener {

	private static final Logger logger = LogManager.getLogger("tradingengine-log");
	private static final Logger logRoller  = LogManager.getLogger("rollingFile");
	private static final Logger orderLog = LogManager.getLogger("ordercreation-log");
	

	
	@Autowired
	private InstrumentController instrumentController;

	@Autowired
	private TradingRepository tradingRepository;

	private AlgorithmTrade lastTrade;

	@RequestMapping(value = "tradingengine/status", method = RequestMethod.GET)
	public ModelAndView engineStatus() {
		ModelAndView resultView = new ModelAndView("engine/enginestatus");
		TradingEngineEntity engine = tradingRepository.getTradingEngine();
		resultView.addObject("engine", engine);
		return resultView;
	}

	@RequestMapping(value = "tradingengine/startEngine", method = RequestMethod.GET)
	public ModelAndView startTradingEngine() {
		List<TradingProfileEntity> tradingProfileEntities = tradingRepository.findAllProfiles();
		SessionUtils.getTradingProfiles().clear();

		logger.info("################## Starting Trading Engine with following Parameters ###########################");
		logRoller.info("################## Starting Trading Engine with following Parameters ###########################");
		for (TradingProfileEntity profile : tradingProfileEntities) {
			logger.info("Profile: " + profile.getUserID());
			for (TradingAlgorithmEntity algorithm : profile.getTradingAlgorithms()) {
				logger.info("Algorithm: " + algorithm.getName());
			}
			for (TradingInstrumentEntity instrument : profile.getTradingInstruments()) {
				logger.info("Instrument: " + instrument.getInstument() + " " + instrument.getTradingInterval() + " MassTrade " + instrument.isMassTrade() + " Engine "
						+ instrument.isTradingEngine());
			}
		}

		for (TradingProfileEntity tradingProfileEntity : tradingProfileEntities) {
			TradingProfile profile = TradingProfileConverter.convertToTradingProfile(tradingProfileEntity, SessionUtils.findAccountByUserId(tradingProfileEntity.getUserID()));
			List<TradingAlgorithm> generatedAlgorithms = new ArrayList<>();

			for (TradingAlgorithm algorithm : profile.getTradingAlgorithms()) {
				for (TradingInstrument instrument : profile.getTradingInstruments()) {
					if (!instrument.isTradingEngine()) {
						continue;
					}
					TradingAlgorithm newAlgorithm = new TradingAlgorithm(algorithm.getTradingSignals(), instrument.getTradingInterval());
					newAlgorithm.setTradingInstrument(instrument);
					newAlgorithm.setName(algorithm.getName());
					newAlgorithm.setDbID(algorithm.getDbID());
					newAlgorithm.setStopLossPips(algorithm.getStopLossPips());
					newAlgorithm.setTakeProfitPips(algorithm.getTakeProfitPips());
					newAlgorithm.setBuy(algorithm.isBuy());
					newAlgorithm.setTradingSignals(algorithm.getTradingSignals());
					newAlgorithm.setPostTradingSignals(algorithm.getPostTradingSignals());
					generatedAlgorithms.add(newAlgorithm);
				}
			}

			profile.getTradingAlgorithms().clear();
			profile.getTradingAlgorithms().addAll(generatedAlgorithms);

			SessionUtils.getTradingProfiles().add(profile);
		}

		List<IntervalEnum> intervals = new ArrayList<>();
		List<String> msg = new ArrayList<>();
		msg.add("The Trading-Engine ist started with:");
		boolean alogithmDefined = false;

		for (TradingProfile profile : SessionUtils.getTradingProfiles()) {
			if (profile.getAccount() == null) {
				continue;
			}
			msg.add(profile.getAccount().getUserName() + " " + profile.getAccount().getUserId());
			for (TradingAlgorithm algorithm : profile.getTradingAlgorithms()) {
				alogithmDefined = true;
				msg.add("Algorithm : " + algorithm.getName());
			}
			for (TradingInstrument instrument : profile.getTradingInstruments()) {
				if (!instrument.isTradingEngine()) {
					continue;
				}
				msg.add(instrument.getInstument());
				if (!intervals.contains(instrument.getTradingInterval())) {
					intervals.add(instrument.getTradingInterval());
				}

				List<CandleDataPointInterface> initCandles = instrumentController.fetchCandles(profile.getAccount().getUserId(), instrument.getInstument(),
						instrument.getTradingInterval().getName());

				CandleDataPointInterface lastCandle = initCandles.get(initCandles.size() - 1);
				SessionUtils.setLastFechtedCandle(profile.getAccount().getUserId(), instrument.getInstument(), lastCandle.getTime(), instrument.getTradingInterval().getName());
			}
		}

		boolean startEngine = true;

		if (intervals.isEmpty()) {
			msg.add("No instruments defined !!!");
			startEngine = false;
		}
		if (!intervals.isEmpty() && !alogithmDefined) {
			msg.add("No algorithms defined !!");
			startEngine = false;
		}

		if (startEngine) {
			SessionUtils.addCandleListener(this);
			tradingRepository.startTradingEngine();
			for (IntervalEnum interval : intervals) {
				SessionUtils.startCandleRunner(instrumentController, interval);
			}
		} else {
			msg.add(" !!! Engine not started !!!");
		}

		ModelAndView resultView = new ModelAndView("engine/engine");
		resultView.addObject("msg", msg);
		return resultView;
	}

	@RequestMapping(value = "tradingengine/stopEngine", method = RequestMethod.GET)
	public String stopEngine() {
		SessionUtils.stopCandleRunners();
		tradingRepository.stopTradingEngine();
		return "redirect:/tradingengine/status";
	}

	@Override
	public List<ChartInstrument> getChartInstruments() {
		// TODO Auto-generated method stub
		return null;
	}

	// TODO maybe use only membercandles
	// TODO change to tradinginstrument
	@Override
	public synchronized List<AlgorithmTrade> processCandle(CandleDataPointInterface candle, TradingInstrument candleInstrument) {
		List<AlgorithmTrade> resultTrades = new ArrayList<>();
		orderLog.info("############## Candle Processing started t "+Instant.now().toString());
		orderLog.info("############## Candle being processesd -> "+candle.getTime());
		logger.info(
				"TradingEngine Process candle called!! " + candleInstrument.getInstument() + " " + candleInstrument.getTradingInterval().getName() + " Time " + candle.getTime());

		for (TradingProfile profile : SessionUtils.getTradingProfiles()) {
			TradingInstrument tradingInstrument = profile.fetchTradingInstrument(candleInstrument);
			if (tradingInstrument != null && tradingInstrument.getTradingInterval() != null
					&& tradingInstrument.getTradingInterval().equals(candleInstrument.getTradingInterval())) {
				logger.info("Profile " + profile.getAccount().getUserId() + " adding Candle " + tradingInstrument.getInstument() + " " + candle.getTime());
				logger.info("Size before " + profile.fetchCandleList(tradingInstrument).size());
				profile.addChandle(tradingInstrument, candle);
				logger.info("Size After " + profile.fetchCandleList(tradingInstrument).size());
			} else {
				continue;
			}
			for (TradingAlgorithm algorithm : profile.getTradingAlgorithms()) {
				if (algorithm.getTradingSignals().isEmpty()) {
					logger.info("###### WARNING ############### Algorithm has no signals " + algorithm.getName());
					continue;
				}

				if (tradingInstrument.getTradingInterval().equals(candleInstrument.getTradingInterval()) && tradingInstrument != null
						&& algorithm.getTradingInstrument().getInstument().equals(candleInstrument.getInstument())
						&& algorithm.getInterval().equals(candleInstrument.getTradingInterval())) {
					logger.info("################## PROCESSING TRADE " + profile.getAccount().getUserId() + " " + tradingInstrument.getInstument() + " "
							+ tradingInstrument.getTradingInterval().name() + " " + candle.getC() + " ##########################################");

					logger.info(tradingInstrument.getTradingInterval() + " " + candleInstrument.getTradingInterval() + " " + algorithm.getTradingInstrument().getInstument() + " "
							+ candleInstrument.getInstument() + " " + algorithm.getInterval() + " " + candleInstrument.getTradingInterval());
					logger.info("################## Number of Candles ->" + profile.fetchCandleList(tradingInstrument).size() + " empty "
							+ profile.fetchCandleList(tradingInstrument).isEmpty());
					if (profile.fetchCandleList(tradingInstrument).isEmpty()) {
						continue;
					}
					AlgorithmTrade trade = algorithm.generateTrade(profile.fetchCandleList(tradingInstrument), false);
					trade.setAccount(profile.getAccount());
					trade.setInstrument(tradingInstrument);
					trade.setBuySell(algorithm.isBuy());
					trade.setTradingInterVal(tradingInstrument.getTradingInterval());
					trade.setTradingAlgorithm(algorithm);
					trade.copyCandles(profile.fetchCandleList(tradingInstrument));
					trade.setUuid(UUID.randomUUID().toString());
					trade.setStopLossPips(algorithm.getStopLossPips());
					trade.setTakeProfitPips(algorithm.getTakeProfitPips());
					resultTrades.add(trade);
					
					if (trade.isTrade()) {
						orderLog.info("##############");
						orderLog.info("Trade created from Algorithm "+Instant.now().toString());
						orderLog.info(trade.getTradeID()+" "+trade.getInstrument().getInstument()+" "+trade.getInstrument().getUnits()+" "+trade.getInstrument().getTradingInterval().name()+" do the trade "+trade.isTrade()+" realTrade "+trade.isRealTrade());
						orderLog.info("Issuing candle "+candle.getTime());
						orderLog.info("##############");						
					}
					
					// TODO how and when to delete candles
					// del candles on reset
					if (trade.isResetCandles()) {
						logger.info("################## Resetting Candles ##########################################");
						logger.info("-----------Profile from USER " + profile.getAccount().getUserId());
						profile.resetCandles(tradingInstrument);
						profile.resetAlgorithms(tradingInstrument);
					}

					logger.info("Resulting Trade " + trade.getInstrument().getInstument() + " " + trade.isTrade());
					lastTrade = trade;
					logger.info("################## PROCESSING SUCCESS ##########################################");
				}
			}
		}		
		
		resultTrades.stream().forEach(t -> orderLog.info("Resulting trades ->  Trade "+t.isTrade()+" "+t.getInstrument().getInstument()+" "));
		orderLog.info("############## Candle Processing finished with success at "+Instant.now().toString());
		return resultTrades;
	}

	public AlgorithmTrade getLastTrade() {
		return lastTrade;
	}

	public void setLastTrade(AlgorithmTrade lastTrade) {
		this.lastTrade = lastTrade;
	}

	// MayBee wrong has to be tested 1
	@Override
	public List<AlgorithmTrade> postProcessTrades(List<AlgorithmTrade> trades, CandleDataPointInterface candle) {
		for (TradingProfile profile : SessionUtils.getTradingProfiles()) {
			for (TradingAlgorithm algorithm : profile.getTradingAlgorithms()) {
				for (AlgorithmTrade trade : trades) {
					if (trade.getTradingAlgorithm().getDbID() == algorithm.getDbID()) {
						algorithm.postProcessTrade(trade);
						algorithm.dynamicPostProcessTrades(trade, Double.valueOf(candle.getH()));
					}
				}
			}
		}
		logger.info("##################### StopLossMoved of all Trades after PostProcess ###########################");
		for(AlgorithmTrade trade : trades) {
			logger.info(trade.getInstrument().getInstument()+" "+trade.isStopLossMoved());
		}
		return trades;
	}
}
