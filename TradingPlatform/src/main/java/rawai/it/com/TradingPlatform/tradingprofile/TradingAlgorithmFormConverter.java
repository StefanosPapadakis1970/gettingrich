package rawai.it.com.TradingPlatform.tradingprofile;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.engine.PostTradingSignalForm;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithmForm;
import rawai.it.com.TradingPlatform.engine.TradingSignalForm;
import rawai.it.com.TradingPlatform.engine.db.PostTradeSignalEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingSignalEntity;

public class TradingAlgorithmFormConverter {

	public static TradingAlgorithmEntity convertFormToEntity(TradingAlgorithmForm algorithmForm, List<TradingSignalEntity> signals, List<PostTradeSignalEntity> postTradingSignals) {
		TradingAlgorithmEntity algorithmEntity = new TradingAlgorithmEntity();
		algorithmEntity.setName(algorithmForm.getAlgorithmName());
		algorithmEntity.setBuy(algorithmForm.isBuy());
		algorithmEntity.setStopLossPips(algorithmForm.getStopLossPips());
		algorithmEntity.setTakeProfitPips(algorithmForm.getTakeProfitPips());

		algorithmEntity.setTradingSignals(convertTradingFormSignals(algorithmForm.getSignals(), signals));
		algorithmEntity.setPostTradingSignals(convertTradingFormPostTradeSignals(algorithmForm.getPostTradngSignals(), postTradingSignals));
		algorithmEntity.setId(algorithmForm.getDbID() == null || algorithmForm.getDbID().equals("") ? null : Long.valueOf(algorithmForm.getDbID()));
		return algorithmEntity;
	}

	private static List<PostTradeSignalEntity> convertTradingFormPostTradeSignals(List<PostTradingSignalForm> postTradngSignals, List<PostTradeSignalEntity> postTradingSignals) {
		List<PostTradeSignalEntity> resultSignals = new ArrayList<>();
		for (PostTradingSignalForm signalForm : postTradngSignals) {
			resultSignals.add(fetchPostTradingSignalEntity(Long.valueOf(signalForm.getDbID()), postTradingSignals));
		}
		return resultSignals;
	}

	private static PostTradeSignalEntity fetchPostTradingSignalEntity(Long dbID, List<PostTradeSignalEntity> postTradingSignals) {
		for (PostTradeSignalEntity signal : postTradingSignals) {
			if (dbID.equals(signal.getId())) {
				return signal;
			}
		}
		return null;
	}

	private static List<TradingSignalEntity> convertTradingFormSignals(List<TradingSignalForm> signals, List<TradingSignalEntity> signalEntities) {
		List<TradingSignalEntity> resultSignals = new ArrayList<>();
		for (TradingSignalForm signalForm : signals) {
			resultSignals.add(fetchTradingSignalEntity(Long.valueOf(signalForm.getDbID()), signalEntities));
		}
		return resultSignals;
	}

	// TODO When creating signals
	private static TradingSignalEntity convertTradingSignalForm(TradingSignalForm signalForm) {
		TradingSignalEntity resultSignal = new TradingSignalEntity();
		resultSignal.setId(Long.valueOf(signalForm.getDbID()));
		return resultSignal;
	}

	private static TradingSignalEntity fetchTradingSignalEntity(Long dbID, List<TradingSignalEntity> signalEntities) {
		for (TradingSignalEntity signal : signalEntities) {
			if (dbID.equals(signal.getId())) {
				return signal;
			}
		}
		return null;
	}

	public static TradingAlgorithmForm convertEntityToForm(TradingAlgorithmEntity algorithm) {
		TradingAlgorithmForm algorithmForm = new TradingAlgorithmForm();
		algorithmForm.setBuy(algorithm.isBuy());
		algorithmForm.setAlgorithmName(algorithm.getName());
		algorithmForm.setDbID(algorithm.getId().toString());
		algorithmForm.setTakeProfitPips(algorithm.getTakeProfitPips());
		algorithmForm.setStopLossPips(algorithm.getStopLossPips());
		algorithmForm.setSignals(convertSingalEntityListSignalForms(algorithm.getTradingSignals()));
		return algorithmForm;

	}

	private static List<TradingSignalForm> convertSingalEntityListSignalForms(List<TradingSignalEntity> tradingSignals) {
		List<TradingSignalForm> resultList = new ArrayList<>();
		for (TradingSignalEntity entity : tradingSignals) {
			resultList.add(convertSignalEntity(entity));

		}
		return resultList;
	}

	private static TradingSignalForm convertSignalEntity(TradingSignalEntity entity) {
		TradingSignalForm signalForm = new TradingSignalForm();
		signalForm.setDbID(entity.getId().toString());
		signalForm.setDescription(entity.getTradingSignal().getDescription(entity));
		return signalForm;
	}

}
