package rawai.it.com.TradingPlatform.dashboard;

public class ClientExtensions {
	
	public final static String ENGINE_TRADE = "Engine Trade";
	public final static String MANUAL_TRADE = "Manual Trade";

	private String id;
	private String tag;
	private String comment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
