package rawai.it.com.TradingPlatform.instrument;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CandleRunner implements Runnable {
	private static final Logger logger = LogManager.getLogger(CandleRunner.class);


	private InstrumentController instrumentController;
	private boolean stop;
	private IntervalEnum interval;

	public CandleRunner(InstrumentController instrumentController, IntervalEnum interval) {
		super();
		this.instrumentController = instrumentController;
		this.interval = interval;
	}

	@Override
	public void run() {
		while (!stop) {
			logger.info("###########################################################################################################");
			logger.info("#################### CandleRunner delivers candles for interval " + interval.getName() + " #################");
			logger.info("###########################################################################################################");
			instrumentController.deliverCandleToTradingEngine(interval.getName());
			logger.info("###########################################################################################################");
			logger.info("###STOP STOP######## CandleRunner delivers candles for interval " + interval.getName() + " ### STOP STOP###");
			logger.info("###########################################################################################################");

			try {
				//Thread.sleep(interval.toSeconds() * 500);
				Thread.sleep(100);
			} catch (InterruptedException e) {
				logger.error("CandleRunner interrupted !! " + interval.getName());
			}
		}
	}



	public InstrumentController getInstrumentController() {
		return instrumentController;
	}

	public void setInstrumentController(InstrumentController instrumentController) {
		this.instrumentController = instrumentController;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public IntervalEnum getInterval() {
		return interval;
	}

	public void setInterval(IntervalEnum interval) {
		this.interval = interval;
	}
}
