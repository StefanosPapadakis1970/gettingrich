package rawai.it.com.TradingPlatform.instrument;

public enum IntervalEnum {
	S5("S5", 5) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.SECOND.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 5;
		}
	},
	S10("S10", 10) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.SECOND.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 10;
		}
	},
	S15("S15", 15) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.SECOND.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 15;
		}
	},
	S30("S30", 30) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.SECOND.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 30;
		}
	},
	M1("M1", 60) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 1;
		}
	},
	M2("M2", 120) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 2;
		}
	},
	M4("M4", 240) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 4;
		}
	},
	M5("M5", 300) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 5;
		}
	},
	M10("M10", 600) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 10;
		}
	},
	M15("M15", 900) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 15;
		}
	},
	M30("M30", 1800) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MINUTE.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 30;
		}
	},
	H1("H1", 3600) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 1;
		}
	},
	H2("H2", 7200) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 2;
		}
	},
	H3("H3", 10800) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 3;
		}
	},
	H4("H4", 14440) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 4;
		}
	},
	H6("H6", 21600) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 6;
		}
	},
	H8("H8", 28800) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 8;
		}
	},
	H12("H12", 43200) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.HOUR.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 12;
		}
	},
	D("D", 86400) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.DAY.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 1;
		}
	},
	W("W", 604800) {
		@Override
		public String getIntervalName() {
			return null;
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 0;
		}
	},
	M("M", 2592000) {
		@Override
		public String getIntervalName() {
			return ChartIntervalEnum.MONTH.name();
		}

		@Override
		public int getIntervalNumberOfElements() {
			return 0;
		}
	};
	
	private int seconds;
	private String name;
	
	private IntervalEnum(String name, int seconds){
		this.seconds = seconds;
		this.name = name;
	}

	public abstract String getIntervalName();

	public abstract int getIntervalNumberOfElements();

	public int toSeconds(){
		return this.seconds;
	}
	
	public static IntervalEnum getValue(String name){
		for (IntervalEnum interval : IntervalEnum.values()){
			if (interval.name.equals(name)){
				return interval;
			}
		}
		return null;
	}

	public String getName() {
		return this.name;
	}

	public String toString() {
		return this.name;
	}
}
