package rawai.it.com.TradingPlatform.engine.signals;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.engine.TradingSignalEnum;

public class LessThanBefore extends AbstractTradingSignal {

	private int stepsBack = 0;

	public LessThanBefore(int stepsBack) {
		super(BUYSELL.BUY, TradingSignalEnum.LESS_THAN_BEFORE, stepsBack + 1);
		this.stepsBack = stepsBack;
	}

	@Override
	public SignalResult processCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (!enoughCandles(candles, lastSignalCandleIndex)) {
			SignalResult result = new SignalResult(false, lastSignalCandleIndex, true);
			result.setResetAnyWay(true);
			return result;

		}

		CandleDataPointInterface currentCandle = candles.get(lastSignalCandleIndex);
		CandleDataPointInterface candleBefore = candles.get(lastSignalCandleIndex - stepsBack);

		int currentValue = Integer.valueOf(currentCandle.getC().replace(".", ""));
		int valueBefore = Integer.valueOf(candleBefore.getC().replace(".", ""));

		int diff = currentValue - valueBefore;

		return new SignalResult(diff < 0, lastSignalCandleIndex, false);

	}

	@Override
	public int getHigh() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getLow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getValue() {
		return stepsBack;
	}

	@Override
	public RangeEnum getCandleRange() {
		// TODO Auto-generated method stub
		return null;
	}

	// ----+
	@Override
	public boolean enoughCandles(List<CandleDataPointInterface> candles, int lastSignalCandleIndex) {
		if (candles.size() >= getMinimumNumberOfCandles() && candles.subList(0, lastSignalCandleIndex + 1).size() >= getMinimumNumberOfCandles()) {
			return true;
		}
		return false;
	}

}
