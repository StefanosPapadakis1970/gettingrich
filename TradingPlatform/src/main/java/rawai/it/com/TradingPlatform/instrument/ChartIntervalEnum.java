package rawai.it.com.TradingPlatform.instrument;

public enum ChartIntervalEnum {
	SECOND, MINUTE, HOUR, DAY, MONTH, YEAR;
}
