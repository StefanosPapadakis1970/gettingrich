package rawai.it.com.TradingPlatform.engine.signals;


public enum RangeEnum {

	TOP_HALF("TOP_HALF") {
		@Override
		public boolean inRange(int high, int low, int close) {
			int range = high - low;
			int closingPosition = close - low;
			double border = range * 0.5;
			return closingPosition >= border;
		}
	},
	BOTTOM_HALF("BOTTOM_HALF") {
		@Override
		public boolean inRange(int high, int low, int close) {
			int range = high - low;
			int closingPosition = close - low;
			double border = range * 0.5;
			return closingPosition <= border;

		}
	},
	TOP_QUARTER("TOP_QUARTER") {
		@Override
		public boolean inRange(int high, int low, int close) {
			int range = high - low;
			int closingPosition = close - low;
			double border = range * 0.75;
			return closingPosition >= border;
		}
	},
	BOTTOM_QUARTER("BOTTOM_QUARTER") {
		@Override
		public boolean inRange(int high, int low, int close) {
			int range = high - low;
			int closingPosition = close - low;
			double border = range * 0.25;
			return closingPosition <= border;
		}
	};

	private String name;

	private RangeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static RangeEnum getValue(String name) {
		for (RangeEnum range : RangeEnum.values()) {
			if (range.getName().equals(name)) {
				return range;
			}
		}
		return null;
	}

	public abstract boolean inRange(int high, int low, int close);

}

