package rawai.it.com.TradingPlatform.algorithmtesting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.tradingprofile.RunType;

public class TestForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
	
	public static final String DATE_PATTERN = "dd/MM/yyyy HH:mm";

	private Long dbID;

	@NotBlank(message = TestForm.NOT_BLANK_MESSAGE)
	private String algorithmName;
	@NotBlank(message = TestForm.NOT_BLANK_MESSAGE)
	private String instrument;

	@NotNull(message = TestForm.NOT_BLANK_MESSAGE)
	private RunType runType;
	@NotNull(message = TestForm.NOT_BLANK_MESSAGE)
	private IntervalEnum interval;
	@NotBlank(message = TestForm.NOT_BLANK_MESSAGE)
	@Pattern(regexp = "^[1-9][0-9]*", message = "Not a valid number (>0)")
	private String units;
	@NotBlank(message = TestForm.NOT_BLANK_MESSAGE)
	private String userID;
	private String maxNumberOfTransactions = "0";
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm")
	private String startDate = "";
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm")	
	private String endDate = "";
	
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm")
	private Date startDateDateObject = new Date();
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm")	
	private Date endDateDateObject = new Date();
	
	
	private boolean generateTrades;
	
	

	private TestStatusEnum status;

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public RunType getRunType() {
		return runType;
	}

	public void setRunType(RunType runType) {
		this.runType = runType;
	}


	public boolean isGenerateTrades() {
		return generateTrades;
	}

	public void setGenerateTrades(boolean generateTrades) {
		this.generateTrades = generateTrades;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public IntervalEnum getInterval() {
		return interval;
	}

	public void setInterval(IntervalEnum interval) {
		this.interval = interval;
	}

	public String getMaxNumberOfTransactions() {
		return maxNumberOfTransactions;
	}

	public void setMaxNumberOfTransactions(String maxNumberOfTransactions) {
		this.maxNumberOfTransactions = maxNumberOfTransactions;
	}

	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long dbID) {
		this.dbID = dbID;
	}

	public TestStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TestStatusEnum status) {
		this.status = status;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) throws ParseException {
		this.startDateDateObject = new SimpleDateFormat(DATE_PATTERN).parse(startDate);
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) throws ParseException {		
		this.endDateDateObject = new SimpleDateFormat(DATE_PATTERN).parse(endDate);
		this.endDate = endDate;
	}

	public Date getStartDateDateObject() {
		return startDateDateObject;
	}

	public void setStartDateDateObject(Date startDateDateObject) {
		this.startDateDateObject = startDateDateObject;
	}

	public Date getEndDateDateObject() {
		return endDateDateObject;
	}

	public void setEndDateDateObject(Date endDateDateObject) {
		this.endDateDateObject = endDateDateObject;
	}
}
