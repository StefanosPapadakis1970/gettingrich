package rawai.it.com.TradingPlatform.databuilders;

import java.util.List;

import rawai.it.com.TradingPlatform.engine.db.TradingAlgorithmEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingInstrumentEntity;
import rawai.it.com.TradingPlatform.engine.db.TradingProfileEntity;

public class TradingProfileEntityBuilder {

	private Long id;

	private String userID;

	private List<TradingInstrumentEntity> tradingInstruments;

	private List<TradingAlgorithmEntity> tradingAlgorithms;

	private TradingProfileEntity profile = new TradingProfileEntity();

	private TradingProfileEntityBuilder() {

	}

	public static TradingProfileEntityBuilder aTradingProfileEntityBuilder() {
		return new TradingProfileEntityBuilder();
	}

	public TradingProfileEntityBuilder withUserID(String userID) {
		this.userID = userID;
		return this;
	}

	public TradingProfileEntityBuilder withID(long id) {
		this.id = id;
		return this;
	}

	public TradingProfileEntityBuilder withTradingInstruments(List<TradingInstrumentEntity> tradingInstruments) {
		this.tradingInstruments = tradingInstruments;
		return this;
	}

	public TradingProfileEntityBuilder withTradingAlgorithms(List<TradingAlgorithmEntity> tradingAlgorithms) {
		this.tradingAlgorithms = tradingAlgorithms;
		return this;
	}

	public TradingProfileEntity build() {
		profile.setUserID(userID);
		profile.setId(id);
		profile.setTradingAlgorithms(tradingAlgorithms);
		profile.setTradingInstruments(tradingInstruments);
		return profile;
	}

	public TradingProfileEntity buildClean() {
		TradingProfileEntity tradingProfile = new TradingProfileEntity();
		tradingProfile.setUserID(userID);
		tradingProfile.setId(id);
		tradingProfile.setTradingAlgorithms(tradingAlgorithms);
		tradingProfile.setTradingInstruments(tradingInstruments);
		return tradingProfile;
	}
}
