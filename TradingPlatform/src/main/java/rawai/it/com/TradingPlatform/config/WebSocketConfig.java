package rawai.it.com.TradingPlatform.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
@Controller
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
 
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }
 
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/prices").withSockJS();
		registry.addEndpoint("/globalMessage").withSockJS();
		registry.addEndpoint("/fetchData").withSockJS();
		registry.addEndpoint("/fetchOrders").withSockJS();
		registry.addEndpoint("/updateProfit").withSockJS();
		registry.addEndpoint("/updateAccounts").withSockJS();
		registry.addEndpoint("/updateDashBoard").withSockJS();
		registry.addEndpoint("/updateRates").withSockJS();
		registry.addEndpoint("/updateCharts").withSockJS();
		registry.addEndpoint("/single").withSockJS();
		registry.addEndpoint("/transactionsStream").withSockJS();
    }

	@Bean
	public PresenceChannelInterceptor presenceChannelInterceptor() {
		return new PresenceChannelInterceptor();
	}

	@Override
	public void configureClientInboundChannel(ChannelRegistration registration) {
		registration.setInterceptors(presenceChannelInterceptor());
	}

	@Override
	public void configureClientOutboundChannel(ChannelRegistration registration) {
		registration.taskExecutor().corePoolSize(8);
		registration.setInterceptors(presenceChannelInterceptor());
	}
}