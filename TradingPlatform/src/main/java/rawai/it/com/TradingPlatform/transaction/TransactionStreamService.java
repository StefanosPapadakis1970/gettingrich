package rawai.it.com.TradingPlatform.transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.dashboard.ClientExtensions;
import rawai.it.com.TradingPlatform.dashboard.MarketOrderReason;
import rawai.it.com.TradingPlatform.dashboard.MarketOrderTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderCancelRejectTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderCancelTransaction;
import rawai.it.com.TradingPlatform.dashboard.OrderFillTransaction;
import rawai.it.com.TradingPlatform.dashboard.TakeProfitOrderRejectTransaction;
import rawai.it.com.TradingPlatform.dashboard.TakeProfitOrderTransaction;
import rawai.it.com.TradingPlatform.dashboard.TransactionStreamRunnable;
import rawai.it.com.TradingPlatform.dashboard.TransactionType;
import rawai.it.com.TradingPlatform.engine.db.TradingRepository;
import rawai.it.com.TradingPlatform.engine.db.TransactionMessageEntity;
import rawai.it.com.TradingPlatform.messanger.Message;
import rawai.it.com.TradingPlatform.messanger.MessageService;
import rawai.it.com.TradingPlatform.order.OrderService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;


@Controller
public class TransactionStreamService {

	private static final Logger log = LogManager.getLogger("transaction-log");
	private static final Logger orderLog = LogManager.getLogger("ordercreation-log");
	private static final Logger streamLog = LogManager.getLogger("transactionstream-log");

	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private MessageService messageService;
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	private OrderService orderService;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private TradingRepository tradingRepository;

	public void sendMessage(String title, String messageText) {
		Message message = new Message();
		message.setHeader(title);
		message.setMessage(messageText);
		messageService.sendGlobalMessage(message);
	}

	// Start Stop
	// Think about errorhandling
	public void streamTransactions(String userID, String userName, TransactionStreamRunnable runner) {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		runner.setHttpClient(httpClient);


		try {

			// Set these variables to whatever personal ones are preferred
			String url = curlUtil.PRAXIS_STREAMING_URL;
			String access_token = SessionUtils.findAccountByUserId(userID).getApiKey();
			String account_id = userID;
			String instruments = "EUR_USD";

			// HttpUriRequest httpGet = new HttpGet(domain + "/v3/accounts/" +
			// account_id + "/pricing/stream?instruments=" + instruments);
			HttpUriRequest httpGet = new HttpGet(url + "v3/accounts/" + account_id + "/transactions/stream");
			httpGet.setHeader(new BasicHeader("Authorization", "Bearer " + access_token));

			log.info("Executing request: " + httpGet.getRequestLine());

			HttpResponse resp = httpClient.execute(httpGet);
			HttpEntity entity = resp.getEntity();


			ExecutorService executor = Executors.newFixedThreadPool(1);
			MarketOrderTransaction marketOrderTransaction = null;

			log.info(resp.getStatusLine().getStatusCode() + " Everything went fine TransactioStream is up ! " + userID);
			if (resp.getStatusLine().getStatusCode() == 200 && entity != null) {
				InputStream stream = entity.getContent();
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(stream));

				while ((line = br.readLine()) != null) {
					if (!SessionUtils.transactionStreamsRunning()) {
						log.info("############################# Not all TransactionStreams are Running ###########################");
					}

					if (!line.contains("HEARTBEAT")) {
						log.info("Active TransactionStream Runners " + SessionUtils.getTransactionRunners().size());
						for (TransactionStreamRunnable element : SessionUtils.getTransactionRunners()) {
							log.info(element.getUserID());
						}
						streamLog.info(line);
						log.info(line);

						ObjectMapper mapper = new ObjectMapper();
						marketOrderTransaction = mapper.readValue(line, MarketOrderTransaction.class);

						TransactionMessageEntity messageEntity = new TransactionMessageEntity();
						setUPIds(messageEntity, line);
						messageEntity.setMessage(line);
						DateTime time = new DateTime(marketOrderTransaction.getTime());
						messageEntity.setTime(time.toDate());
						tradingRepository.saveTransactionMessage(messageEntity);


						// TODO 06.07 Npe
						if (marketOrderTransaction.getPrice() == null) {
							marketOrderTransaction.setPrice("N/A");
						} else {
							// marketOrderTransaction.setPrice(FormatingUtils.formatAmountCurrency(marketOrderTransaction.getPrice(),
							// SessionUtils.findAccountByUserId(userID)
							// .getCurrency().getCurrencySymbol()));
						}

						marketOrderTransaction.setUserName(userName);




						// Create StopLoss and Take in case of Machine-Trade
						if (marketOrderTransaction.getClientExtensions() != null && marketOrderTransaction.getClientExtensions().getComment().contains(ClientExtensions.ENGINE_TRADE)) {

                            //If the order will be replaced by an generated order throu postprocesssing-signal
						
							if (marketOrderTransaction.getType().equals(TransactionType.STOP_LOSS_ORDER) &&
									marketOrderTransaction.getReason() == MarketOrderReason.REPLACEMENT) {
								log.info("Processing Stop Loss increasement");
								
								TakeProfitOrderTransaction order  = mapper.readValue(line, TakeProfitOrderTransaction.class);								

								OrderServiceRunnable runnable = new OrderServiceRunnable(orderService, marketOrderTransaction) {
									@Override
									public void run() {
										orderService.updateStopLossOrderID(order);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
							}
							
							

							if (marketOrderTransaction.getType().equals(TransactionType.MARKET_ORDER)) {
								log.info("Processing Makret Order");

								OrderServiceRunnable runnable = new OrderServiceRunnable(orderService, marketOrderTransaction) {
									@Override
									public void run() {
										orderService.processEngineTradeMarketOrder(marketOrderTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
							}

							if (marketOrderTransaction.getType().equals(TransactionType.TAKE_PROFIT_ORDER)) {
								log.info("Processing TAKE_PROFIT");

								OrderServiceRunnable runnable = new OrderServiceRunnable(orderService, marketOrderTransaction) {
									@Override
									public void run() {
										orderService.processTakeProfitOrderTransaction(marketOrderTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
							}
							if (marketOrderTransaction.getType().equals(TransactionType.STOP_LOSS_ORDER)) {
								log.info("Processing STOPLOSS");

								OrderServiceRunnable runnable = new OrderServiceRunnable(orderService, marketOrderTransaction) {
									@Override
									public void run() {
										orderService.processStopLossOrderTransaction(marketOrderTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
							}
						}



						if (marketOrderTransaction.getType().equals(TransactionType.ORDER_FILL)) {							
							OrderFillTransaction orderFillTransaction = mapper.readValue(line, OrderFillTransaction.class);
						
							// saveOrderFillMessage(orderFillTransaction);
							orderFillTransaction.setUserName(userName);
							switch (orderFillTransaction.getReason()) {
							case MARKET_ORDER:
								log.info("Processing Makret Order FILL");
								streamLog.info("Market Order Filled  -> "+orderFillTransaction.getInstrument()+" "+orderFillTransaction.getClientOrderID()+" "+Instant.now().toString());
								orderLog.info("Market Order Filled  -> "+orderFillTransaction.getInstrument()+" "+orderFillTransaction.getClientOrderID()+" "+Instant.now().toString());
								OrderServiceRunnable runnable = new OrderServiceRunnable(orderService, orderFillTransaction) {
									@Override
									public void run() {
										orderService.processOrderFillTransaction(orderFillTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
								break;
							case LIMIT_ORDER:
								break;
							case STOP_LOSS_ORDER:
								log.info("Processing STOPP_LOSS_ORDER_FILL");
								runnable = new OrderServiceRunnable(orderService, orderFillTransaction) {
									@Override
									public void run() {
										orderService.processOrderFillStopLoss(orderFillTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
								break;
							case TAKE_PROFIT_ORDER:

								log.info("Processing TAKE_PROFIT Order Fill");
								runnable = new OrderServiceRunnable(orderService, orderFillTransaction) {
									@Override
									public void run() {
										orderService.processOrderFillTakeProfit(orderFillTransaction);
									}
								};

								executor.execute(runnable);

								try {
									executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
								} catch (InterruptedException e) {
									log.catching(e);
								}
								break;
							default:
								break;
							}
							Message message = new Message();
							message.setHeader("Order Filled");
							if (marketOrderTransaction.getClientExtensions() != null) {
								message.setMessage(orderFillTransaction.getUserName() + " " + orderFillTransaction.getReason() + " " + orderFillTransaction.getInstrument() + " "
										+ orderFillTransaction.getUnits() + " " + orderFillTransaction.getPrice() + " Tag "
										+ marketOrderTransaction.getClientExtensions().getTag());
							} else {
								message.setMessage(orderFillTransaction.getUserName() + " " + orderFillTransaction.getReason() + " " + orderFillTransaction.getInstrument() + " "
										+ orderFillTransaction.getUnits() + " " + orderFillTransaction.getPrice());
							}

							// messageService.sendGlobalMessage(message);

						}

						if (SessionUtils.getAccounts().isEmpty()) {
							List<Account> accounts = accountRepository.findAll();
							SessionUtils.setAccounts(accounts);
						}

						if (SessionUtils.findAccountByUserId(userID).getCurrency() != null) {
							String currencySymbol = SessionUtils.findAccountByUserId(userID).getCurrency().getCurrencySymbol();
							marketOrderTransaction.setCurrencySymbol(currencySymbol);
						}

						template.convertAndSend("/topic/transactions", marketOrderTransaction);
					}

				}
			} else {
				// print error message
				String responseString = EntityUtils.toString(entity, "UTF-8");
				log.info(responseString);
			}
		} catch (ClientProtocolException e) {
			log.info("ProtokollException!", e);
		} catch (IllegalStateException e) {
			log.info("Socket already cosed!", e);
		} catch (IOException e) {
			if (e instanceof SocketException) {
				log.info("Socket closed!", e);
			} else {
				log.error("Error", e);
			}
		} finally {
			try {
				log.info("Closing Socket!!");
				httpClient.close();
				SessionUtils.restartTransactionRunner(userID, this);
			} catch (IOException e) {
				log.error("Error", e);
			}
		}
	}

	private void setUPIds(TransactionMessageEntity messageEntity, String line) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		MarketOrderTransaction marketOrderTransaction = mapper.readValue(line, MarketOrderTransaction.class);
		String tradeID = "";

		switch (marketOrderTransaction.getType()) {
		case MARKET_ORDER:
			messageEntity.setBatchID(Long.valueOf(marketOrderTransaction.getBatchID()));
			if (marketOrderTransaction.getTradeClose() != null) {
				messageEntity.setTradeID(Long.valueOf(marketOrderTransaction.getTradeClose().getTradeID()));
			}
			break;
		case MARKET_ORDER_REJECT:
			messageEntity.setBatchID(Long.valueOf(marketOrderTransaction.getBatchID()));
			if (marketOrderTransaction.getTradeClose() != null) {
				messageEntity.setTradeID(Long.valueOf(marketOrderTransaction.getTradeClose().getTradeID()));
			}
			break;
		case ORDER_CANCEL:
			OrderCancelTransaction orderCancelTransaction = mapper.readValue(line, OrderCancelTransaction.class);
			messageEntity.setBatchID(Long.valueOf(orderCancelTransaction.getBatchID()));
			messageEntity.setOrderID(Long.valueOf(orderCancelTransaction.getOrderID()));
			break;
		case ORDER_CANCEL_REJECT:
			OrderCancelRejectTransaction orderCancelRejectTransaction = mapper.readValue(line, OrderCancelRejectTransaction.class);
			messageEntity.setBatchID(Long.valueOf(orderCancelRejectTransaction.getBatchID()));
			messageEntity.setOrderID(Long.valueOf(orderCancelRejectTransaction.getOrderID()));
			break;
		case ORDER_FILL:
			OrderFillTransaction orderFillTransaction = mapper.readValue(line, OrderFillTransaction.class);
			if (orderFillTransaction.getTradeOpened() != null) {
				tradeID = orderFillTransaction.getTradeOpened().getTradeID();
				messageEntity.setTradeID(Long.valueOf(tradeID));
			}
			if (orderFillTransaction.getTradesClosed() != null) {
				tradeID = orderFillTransaction.getTradesClosed().get(0).getTradeID();
				messageEntity.setTradeID(Long.valueOf(tradeID));
			}
			messageEntity.setBatchID(Long.valueOf(orderFillTransaction.getBatchID()));
			messageEntity.setOrderID(Long.valueOf(orderFillTransaction.getOrderID()));
			break;
		case STOP_LOSS_ORDER:
			TakeProfitOrderTransaction stopLossTransaction = mapper.readValue(line, TakeProfitOrderTransaction.class);
			messageEntity.setTradeID(Long.valueOf(stopLossTransaction.getTradeID()));
			messageEntity.setBatchID(Long.valueOf(stopLossTransaction.getBatchID()));
			break;
		case STOP_LOSS_ORDER_REJECT:
			TakeProfitOrderRejectTransaction stopLossRejectTransaction = mapper.readValue(line, TakeProfitOrderRejectTransaction.class);
			messageEntity.setBatchID(Long.valueOf(stopLossRejectTransaction.getBatchID()));
			messageEntity.setTradeID(Long.valueOf(stopLossRejectTransaction.getTradeID()));

			break;
		case TAKE_PROFIT_ORDER:
			TakeProfitOrderTransaction takeProfitTransaction = mapper.readValue(line, TakeProfitOrderTransaction.class);
			messageEntity.setTradeID(Long.valueOf(takeProfitTransaction.getTradeID()));
			messageEntity.setBatchID(Long.valueOf(takeProfitTransaction.getBatchID()));
			break;
		case TAKE_PROFIT_ORDER_REJECT:
			TakeProfitOrderRejectTransaction takeProfitRejectTransaction = mapper.readValue(line, TakeProfitOrderRejectTransaction.class);
			messageEntity.setBatchID(Long.valueOf(takeProfitRejectTransaction.getBatchID()));
			messageEntity.setTradeID(Long.valueOf(takeProfitRejectTransaction.getTradeID()));
			break;
		default:
			break;
		}
	}

	private void saveMarketOrderMessage(MarketOrderTransaction marketOrderTransaction) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		TransactionMessageEntity messageEntity = new TransactionMessageEntity();
		messageEntity.setTradeID(Long.valueOf(marketOrderTransaction.getId()));
		messageEntity.setMessage(mapper.writeValueAsString(marketOrderTransaction));
		tradingRepository.saveTransactionMessage(messageEntity);
	}

	private void saveOrderFillMessage(OrderFillTransaction orderFillTransaction) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		TransactionMessageEntity messageEntity = new TransactionMessageEntity();
		String tradeID = "";
		if (orderFillTransaction.getTradeOpened() != null) {
			tradeID = orderFillTransaction.getTradeOpened().getTradeID();
		}
		if (orderFillTransaction.getTradeReduced() != null) {
			tradeID = orderFillTransaction.getTradeReduced().getTradeID();
		}

		messageEntity.setTradeID(Long.valueOf(tradeID));

		messageEntity.setMessage(mapper.writeValueAsString(orderFillTransaction));
		tradingRepository.saveTransactionMessage(messageEntity);
	}

	private class OrderServiceRunnable implements Runnable {
		OrderService orderService;
		MarketOrderTransaction marketOrderTransaction;
		OrderFillTransaction orderFillTransaction;

		public OrderServiceRunnable(OrderService orderService, MarketOrderTransaction marketOrderTransaction) {
			this.orderService = orderService;
			this.marketOrderTransaction = marketOrderTransaction;
		}

		public OrderServiceRunnable(OrderService orderService, OrderFillTransaction orderFillTransaction) {
			this.orderService = orderService;
			this.orderFillTransaction = orderFillTransaction;
		}

		@Override
		public void run() {

		}
	}

}
