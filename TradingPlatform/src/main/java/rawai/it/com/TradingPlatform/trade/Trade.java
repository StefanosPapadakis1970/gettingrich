package rawai.it.com.TradingPlatform.trade;

import java.text.SimpleDateFormat;

import rawai.it.com.TradingPlatform.order.Order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trade {

	private String accountName;
	private String currentUnits;
	private String financing;
	private String id;
	private String initialUnits;
	private String instrument;
	private String openTime;
	private String closeTime;
	private String price;
	private String realizedPL;
	private String state;
	private String unrealizedPL;
	private SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD");

	private Order takeProfitOrder;
	private Order stopLossOrder;
	private Order limitOrder;

	public String getCurrentUnits() {
		return currentUnits;
	}

	public void setCurrentUnits(String currentUnits) {
		this.currentUnits = currentUnits;
	}

	public String getFinancing() {
		return financing;
	}

	public void setFinancing(String financing) {
		this.financing = financing;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInitialUnits() {
		return initialUnits;
	}

	public void setInitialUnits(String initialUnits) {
		this.initialUnits = initialUnits;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime.substring(0, 10) + " " + openTime.substring(11, 19);
		;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRealizedPL() {
		return realizedPL;
	}

	public void setRealizedPL(String realizedPL) {
		this.realizedPL = realizedPL;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUnrealizedPL() {
		return unrealizedPL;
	}

	public void setUnrealizedPL(String unrealizedPL) {
		this.unrealizedPL = unrealizedPL;
	}

	public Order getTakeProfitOrder() {
		return takeProfitOrder;
	}

	public void setTakeProfitOrder(Order takeProfitOrder) {
		this.takeProfitOrder = takeProfitOrder;
	}

	public Order getStopLossOrder() {
		return stopLossOrder;
	}

	public void setStopLossOrder(Order stopLossOrder) {
		this.stopLossOrder = stopLossOrder;
	}

	public Order getLimitOrder() {
		return limitOrder;
	}

	public void setLimitOrder(Order limitOrder) {
		this.limitOrder = limitOrder;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getCloseTime() {
		return closeTime;
	}


	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime.substring(0, 10) + " " + closeTime.substring(11, 19);
	}
}
