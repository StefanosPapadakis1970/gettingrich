package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import rawai.it.com.TradingPlatform.engine.signals.RangeEnum;

@Converter
public class RangeConverter implements AttributeConverter<RangeEnum, String> {

	@Override
	public String convertToDatabaseColumn(RangeEnum range) {
		if (range == null) {
			return null;
		}
		return range.getName();
	}

	@Override
	public RangeEnum convertToEntityAttribute(String dbData) {
		return RangeEnum.getValue(dbData);
	}

}
