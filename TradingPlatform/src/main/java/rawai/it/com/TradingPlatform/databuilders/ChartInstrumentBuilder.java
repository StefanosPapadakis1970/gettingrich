package rawai.it.com.TradingPlatform.databuilders;

import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;

public class ChartInstrumentBuilder {

	private String time;
	
	private ChartInstrumentBuilder(){
		
	}
	
	public static ChartInstrumentBuilder aChartInstrument(){
		return new ChartInstrumentBuilder();
	}
	
	public ChartInstrumentBuilder withTime(String time){
		this.time = time;
		return this;
	}
	
	public ChartInstrument build(){
		ChartInstrument chartInstrument = new ChartInstrument();
		chartInstrument.setTime(time);
		return chartInstrument;
	}
}
