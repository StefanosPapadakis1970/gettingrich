package rawai.it.com.TradingPlatform.trade;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeListResponse {

	private String lastTransactionID;
	private List<Trade> trades;

	public String getLastTransactionID() {
		return lastTransactionID;
	}
	public void setLastTransactionID(String lastTransactionID) {
		this.lastTransactionID = lastTransactionID;
	}

	public List<Trade> getTrades() {
		return trades;
	}

	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}
}
