package rawai.it.com.TradingPlatform.candle;

import java.util.List;

public class CandleListResponse {

	private List<Candle> candles;

	public List<Candle> getCandles() {
		return candles;
	}

	public void setCandles(List<Candle> candles) {
		this.candles = candles;
	}

}
