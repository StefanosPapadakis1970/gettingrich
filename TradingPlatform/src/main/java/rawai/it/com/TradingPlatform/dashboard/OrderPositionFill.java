package rawai.it.com.TradingPlatform.dashboard;

public enum OrderPositionFill {
	OPEN_ONLY, REDUCE_FIRST, REDUCE_ONLY, DEFAULT;
}
