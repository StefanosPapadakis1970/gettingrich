package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import rawai.it.com.TradingPlatform.instrument.IntervalEnum;

@Converter
public class IntervalEnumConverter implements AttributeConverter<IntervalEnum, String> {

	@Override
	public String convertToDatabaseColumn(IntervalEnum interval) {
		if (interval == null) {
			return null;
		}
		return interval.getName();
	}

	@Override
	public IntervalEnum convertToEntityAttribute(String dbData) {
		return IntervalEnum.getValue(dbData);
	}

}
