package rawai.it.com.TradingPlatform.engine.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "tradingprofile")
@NamedQueries({ @NamedQuery(name = TradingProfileEntity.FIND_BY_USERID, query = "select profile from TradingProfileEntity profile where profile.userID = :userID"),
		@NamedQuery(name = TradingProfileEntity.FIND_ALL, query = "select profile from TradingProfileEntity profile") })
public class TradingProfileEntity implements Serializable {

	private static final long serialVersionUID = -5953127457080102302L;

	public static final String FIND_BY_USERID = "TradingProfileEntity.findByUserID";
	public static final String FIND_ALL = "TradingProfileEntity.findAll";

	@Id
	@GeneratedValue
	private Long id;

	private String userID;

	@OneToMany(mappedBy = "tradingprofile", orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REMOVE })
	private List<TradingInstrumentEntity> tradingInstruments;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "profile_algorithm", joinColumns = { @JoinColumn(name = "profile_id") }, inverseJoinColumns = { @JoinColumn(name = "algorithm_id") })
	private List<TradingAlgorithmEntity> tradingAlgorithms;


	public List<TradingInstrumentEntity> getTradingInstruments() {
		return tradingInstruments;
	}

	public void setTradingInstruments(List<TradingInstrumentEntity> tradingInstruments) {
		this.tradingInstruments = tradingInstruments;
	}

	public List<TradingAlgorithmEntity> getTradingAlgorithms() {
		return tradingAlgorithms;
	}

	public void setTradingAlgorithms(List<TradingAlgorithmEntity> tradingAlgorithms) {
		this.tradingAlgorithms = tradingAlgorithms;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void initMassTrade() {
		List<TradingInstrumentEntity> delList = new ArrayList<>();
		for (TradingInstrumentEntity instrument : tradingInstruments) {
			if (!instrument.isMassTrade()) {
				delList.add(instrument);
			}
		}
		tradingInstruments.removeAll(delList);

	}
}
