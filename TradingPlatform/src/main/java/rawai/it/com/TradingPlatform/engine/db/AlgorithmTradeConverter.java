package rawai.it.com.TradingPlatform.engine.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.candle.Candle;
import rawai.it.com.TradingPlatform.candle.CandleDataPoint;
import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.databuilders.CandleBuilder;
import rawai.it.com.TradingPlatform.databuilders.CandleEntityBuilder;
import rawai.it.com.TradingPlatform.engine.AlgorithmTrade;
import rawai.it.com.TradingPlatform.engine.TradingAlgorithm;
import rawai.it.com.TradingPlatform.engine.TradingInstrument;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;

public class AlgorithmTradeConverter {

	public static AlgorithmTradeEntity convertToAlgorithmTradeEntity(AlgorithmTrade trade) {
		AlgorithmTradeEntity resultEntity = new AlgorithmTradeEntity(trade.getInstrument().getInstument(), trade.isBuySell());
		resultEntity.setAccountID(trade.getAccount().getId());
		resultEntity.setTradingInterval(trade.getInstrument().getTradingInterval());
		resultEntity.setCandles(convertCandles(trade.getCandles(), resultEntity));
		resultEntity.setAlgorithmID(trade.getTradingAlgorithm().getDbID());
		resultEntity.setTradingCandleIndex(trade.getTradingCandleIndex());
		resultEntity.setUnits(trade.getInstrument().getUnits());
		resultEntity.setCreationDate(trade.getCreationDate());
		resultEntity.setBuySell(trade.isBuySell());
		resultEntity.setTakeProfitPips(trade.getTakeProfitPips());
		resultEntity.setStopLossPips(trade.getStopLossPips());
		resultEntity.setPrice(trade.getPrice());
		resultEntity.setStopLossPrice(trade.getStopLossPrice());
		resultEntity.setTakeProfitPrice(trade.getTakeProfitPrice());
		resultEntity.setSuccess(trade.isSuccess());
		resultEntity.setUnits(trade.getUnits());
		resultEntity.setFinished(trade.isFinished());
		resultEntity.setRealTrade(trade.isRealTrade());
		resultEntity.setPl(trade.getPl());
		resultEntity.setTradeID(trade.getTradeID());

		resultEntity.setTakeProfitCandleIndex(trade.getTakeProfitCandleIndex());
		resultEntity.setStopLossCandleIndex(trade.getStopLossCandleIndex());
		resultEntity.setStopLossTradeID(trade.getStopLossTradeID());
		resultEntity.setTakeProfitID(trade.getTakeProfitID());

		resultEntity.setUuid(trade.getUuid());


		return resultEntity;
	}

	public static AlgorithmTrade convertToAlgorithmTrade(AlgorithmTradeEntity trade) {

		TradingInstrument instrument = new TradingInstrument();
		instrument.setTradingInterval(trade.getTradingInterval());
		instrument.setInstument(trade.getInstrument());
		instrument.setUnits(trade.getUnits());

		AlgorithmTrade result = new AlgorithmTrade(instrument, trade.isBuySell());
		result.setAccount(SessionUtils.findAccountByAccountID(trade.getAccountID()));
		result.setDbID(trade.getId());
		result.setCandles(convertCandlesEntities(trade.getCandles()));
		result.setBuySell(trade.isBuySell());
		result.setCreationDate(trade.getCreationDate());
		if (trade.getStopLossPrice() != null) {
			result.setStopLossPrice(new BigDecimal(trade.getStopLossPrice()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
		}
		if (trade.getTakeProfitPrice() != null) {
			result.setTakeProfitPrice(new BigDecimal(trade.getTakeProfitPrice()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
		}

		result.setPl(trade.getPl() == null ? 0 : new BigDecimal(trade.getPl()).setScale(6,BigDecimal.ROUND_HALF_UP).doubleValue());
		result.setTradeID(trade.getTradeID());

		result.setTakeProfitCandleIndex(trade.getTakeProfitCandleIndex());
		result.setStopLossCandleIndex(trade.getStopLossCandleIndex());

		result.setStopLossTradeID(trade.getStopLossTradeID());
		result.setTakeProfitID(trade.getTakeProfitID());
		// TODO 21.06 change DB-Model
		// result.setTestID(trade.getTest().getId());
		result.setTakeProfitPips(trade.getTakeProfitPips());
		result.setStopLossPips(trade.getStopLossPips());
		result.setPrice(trade.getPrice());
		result.setSuccess(trade.isSuccess());
		result.setUnits(trade.getUnits());
		result.setFinished(trade.isFinished());
		result.setRealTrade(trade.isRealTrade());

		result.setTradingInterVal(trade.getTradingInterval());
		result.setTradingCandleIndex(trade.getTradingCandleIndex());
		result.setUuid(trade.getUuid());
		// TODO 07.08 For the moment
		TradingAlgorithm algorithm = new TradingAlgorithm();
		algorithm.setDbID(trade.getAlgorithmID());
		result.setTradingAlgorithm(algorithm);


		return result;
	}

	public static List<CandleDataPointInterface> convertCandlesEntities(List<CandleEntity> candles) {
		List<CandleDataPointInterface> resultList = new ArrayList<>();
		for (CandleEntity candle : candles) {
			CandleDataPoint convertedCandle = CandleBuilder.aCandle().withFilledMid(candle.getO(), candle.getH(), candle.getL(), candle.getC()).withTime(candle.getTime())
					.buildDataPoint();
			convertedCandle.setCandleIndex(candle.getCandleIndex());
			resultList.add(convertedCandle);
		}
		return resultList;
	}

	public static List<Candle> convertCandlesEntitiesToCandles(List<CandleEntity> candles) {
		List<Candle> resultList = new ArrayList<>();
		for (CandleEntity candle : candles) {
			Candle convertedCandle = CandleBuilder.aCandle().withFilledMid(candle.getO(), candle.getH(), candle.getL(), candle.getC()).withTime(candle.getTime())
					.withCandleIndex(candle.getCandleIndex()).build();
			resultList.add(convertedCandle);
		}
		return resultList;
	}

	public static List<CandleEntity> convertCandles(List<CandleDataPointInterface> candles, AlgorithmTradeEntity algorithm) {
		List<CandleEntity> candleList = new ArrayList<>();
		for (CandleDataPointInterface candle : candles) {
			CandleEntity candleEntity = CandleEntityBuilder.aCandle().withC(candle.getC()).withH(candle.getH()).withL(candle.getL()).withO(candle.getO()).withTrade(algorithm)
					.withTime(candle.getTime()).build();
			candleEntity.setAlgorithmTrade(algorithm);
			candleEntity.setCandleIndex(candle.getCandleIndex());
			candleList.add(candleEntity);

		}
		return candleList;
	}

	public static List<CandleEntity> convertCandles(List<CandleDataPointInterface> candles, TestRunEntity testRun) {
		List<CandleEntity> candleList = new ArrayList<>();
		for (CandleDataPointInterface candle : candles) {
			CandleEntity candleEntity = CandleEntityBuilder.aCandle().withC(candle.getC()).withH(candle.getH()).withL(candle.getL()).withO(candle.getO())
					.withTime(candle.getTime()).build();
			candleEntity.setCandleIndex(candle.getCandleIndex());
			candleEntity.setTestRun(testRun);
			candleList.add(candleEntity);
		}
		return candleList;
	}
}
