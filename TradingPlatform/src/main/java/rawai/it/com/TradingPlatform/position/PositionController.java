package rawai.it.com.TradingPlatform.position;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.AccountController;
import rawai.it.com.TradingPlatform.account.AccountRepository;
import rawai.it.com.TradingPlatform.conditions.PageConditionEnum;
import rawai.it.com.TradingPlatform.instrument.IntervalEnum;
import rawai.it.com.TradingPlatform.order.ResultMessage;
import rawai.it.com.TradingPlatform.price.PriceStreamService;
import rawai.it.com.TradingPlatform.trade.TradeResponse;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.urlutils.SingleInstrument;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class PositionController {

	private static final Logger log = LogManager.getLogger(PositionController.class);
	@Autowired
	private AccountRepository accountRepositiory;
	@Autowired
	private AccountController accountController;
	@Autowired
	private PositionService positionService;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private PriceStreamService priceStreamService;
	@Autowired
	private CurlUtil curlUtil;


	@RequestMapping(value = "position/stopPositions", method = RequestMethod.GET)
	@ResponseBody
	public void stopPositions() {
		SessionUtils.setWatchPositions(false);
	}

	@RequestMapping(value = "position/startPositions", method = RequestMethod.GET)
	@ResponseBody
	public void startPositions() {
		SessionUtils.setWatchPositions(true);
	}


	@RequestMapping(value = "position/positions", method = RequestMethod.GET)
	@ResponseBody
	public List<Position> getOpenPositions(String userID, HttpServletRequest request) {
		RestTemplate tmpl = new RestTemplate();
		String apiKey = SessionUtils.findAccountByUserId(userID).getApiKey();
		String url = curlUtil.createAccountUrl() + "/" + userID + "/openPositions";
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(apiKey);

		ResponseEntity<PositionListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PositionListResponse.class);

		// htttps://api-fxtrade.oanda.com/v3/accounts/<ACCOUNT>/trades/6395"
		for (Position position : resp.getBody().getPositions()) {
			if (position.getShort().getTradeIDs() != null) {
				for (String tradeId : position.getShort().getTradeIDs()) {
					String urlTrade = curlUtil.createAccountUrl() + "/" + userID + "/trades/" + tradeId;
					ResponseEntity<TradeResponse> trade = tmpl.exchange(urlTrade, HttpMethod.GET, apiHeader, TradeResponse.class);
					position.getTrades().add(trade.getBody().getTrade());
				}
			}

			if (position.getLong().getTradeIDs() != null) {
				for (String tradeId : position.getLong().getTradeIDs()) {
					String urlTrade = curlUtil.createAccountUrl() + "/" + userID + "/trades/" + tradeId;
					ResponseEntity<TradeResponse> trade = tmpl.exchange(urlTrade, HttpMethod.GET, apiHeader, TradeResponse.class);
					position.getTrades().add(trade.getBody().getTrade());
				}
			}
		}

		return resp.getBody().getPositions();
	}


	@RequestMapping(value = "position/allpositions", method = RequestMethod.GET)
	@ResponseBody
	public List<Position> getPositionsPositions() {
		return positionService.getAllOpenPositions("openPositions");
	}

	@RequestMapping(value = "position/showSinglePosition", method = RequestMethod.GET)
	public ModelAndView showSinglePosition(@RequestParam("instrument") String instrument, @RequestParam("userID") String userID) {
		RestTemplate tmpl = new RestTemplate();
		SingleInstrument singleInstrument = new SingleInstrument();
		singleInstrument.setInstrument(instrument);
		singleInstrument.setUserID(userID);

		List<String> instruments = new ArrayList<>();
		instruments.add(instrument);

		Account account = SessionUtils.findAccountByUserId(userID);

		String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/positions/" + instrument;
		HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
		ResponseEntity<PositionListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PositionListResponse.class);

		SessionUtils.setSingleInstrument(singleInstrument);
		ModelAndView resultView = new ModelAndView("position/showAllOpenPositions");
		resultView.addObject("singleInstrument", singleInstrument);
		resultView.addObject("instruments", instruments);
		resultView.addObject("positions", resp.getBody());
		return resultView;
	}

	@RequestMapping(value = "position/getAllOpenPositions", method = RequestMethod.GET)
	@ResponseBody
	public List<Position> getAllOpenPositions() {
		RestTemplate tmpl = new RestTemplate();
		if (SessionUtils.getAccounts() == null) {
			SessionUtils.setAccounts(accountRepositiory.findAll());
		}

		List<Position> positions = new ArrayList<>();
		for (Account account : SessionUtils.getAccounts()) {
			String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/openPositions";
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
			ResponseEntity<PositionListResponse> resp = tmpl.exchange(url, HttpMethod.GET, apiHeader, PositionListResponse.class);
			positions.addAll(resp.getBody().getPositions());
		}
		return positions;
	}

	@RequestMapping(value = "position/closePosition", method = RequestMethod.GET)
	@ResponseBody
	public String closeTrade(@RequestParam("instrument") String instrument, @RequestParam("longPosition") boolean longPosition, @RequestParam("userID") String userID,
			HttpServletRequest request) {

		RestTemplate tmpl = new RestTemplate();
		Account account = SessionUtils.findAccountByUserId(userID);
		if (account == null) {
			return "ERROR";
		}
		String apiKey = account.getApiKey();
		String url = curlUtil.createAccountUrl() + "/" + userID + "/positions/" + instrument + "/close";
		String body = "{\"shortUnits\" : \"ALL\"}";
		if (longPosition) {
			body = "{\"longUnits\" : \"ALL\"}";
		}

		HttpEntity<?> apiHeader = curlUtil.createAPIHeaderWithBody(apiKey, body);
		ResultMessage error = null;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> resp = null;
		try {
			resp = tmpl.exchange(url, HttpMethod.PUT, apiHeader, String.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			try {
				error = mapper.readValue(e.getResponseBodyAsString(), ResultMessage.class);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return error.getErrorMessage();
		}

		if (resp.getStatusCode() == HttpStatus.OK && resp.hasBody()) {
			return "OK";
		}
		return "ERROR";
	}

	@RequestMapping(value = "position/showAllOpenPositions", method = RequestMethod.GET)
	public ModelAndView showAllOpenPositionsInitialView() {
		List<Position> positions = positionService.getAllOpenPositions("positions");
		List<String> instruments = new ArrayList<>();
		for (Position position : positions) {
			if (!instruments.contains(position.getInstrument())) {
				instruments.add(position.getInstrument());
			}
		}

		ModelAndView resultView = new ModelAndView("/position/showAllOpenPositions");
		resultView.addObject("instruments", instruments);
		resultView.addObject("positions", positions);
		resultView.addObject("statusConditions", PageConditionEnum.OPENPOSITIONS.getConditions());
		resultView.addObject("intervals", IntervalEnum.values());
		return resultView;
	}

	@RequestMapping(value = "position/startPriceService")
	@ResponseBody
	public String startPriveService(@RequestParam("instruments") List<String> instruments) {
		// SessionUtils.stopPriceRunners(PriceRunner.POSITIONS);
		// SessionUtils.startPriceRunners(PriceRunner.POSITIONS,
		// priceStreamService, instruments);
		return "OK";
	}

	@RequestMapping(value = "position/showAllOpenClosedPositions", method = RequestMethod.GET)
	public ModelAndView showAllOpenClosedPositions() {
		return new ModelAndView("/position/showAllOpenClosedPositions");
	}

	@RequestMapping(value = "position/showAllClosedPositions", method = RequestMethod.GET)
	public ModelAndView showAllClosedPositions(HttpServletRequest request) {
		ModelAndView resultView = new ModelAndView("/position/showAllClosedPositions");
		resultView.addObject("positions", positionService.getAllClosedTrades());
		return resultView;
	}

	@RequestMapping("/socket")
	public ModelAndView getSocket() {
		return new ModelAndView("position/socket");
	}

	public void sendPositionsToClient() {
		log.info("Sending Data to Positionclient");
		template.convertAndSend("/topic/positions", positionService.getAllOpenPositions("openPositions"));
	}

	public void sendSinglePositionToClient() {
		log.info("sending Single Position");
		SingleInstrument singleInstrument = SessionUtils.getSingleInstrument();
		template.convertAndSend("/topic/single", positionService.getSinglePosition(singleInstrument.getInstrument(), singleInstrument.getUserID()));
	}
}
