package rawai.it.com.TradingPlatform.engine.signals;

public class SignalResult {

	boolean success;
	boolean notEnoughCandles = false;
	int candleIndex = -1;
	boolean resetAnyWay = false;

	public SignalResult(boolean success, int candleIndex, boolean notEnoughCandles) {
		super();
		this.success = success;
		this.candleIndex = candleIndex;
		this.notEnoughCandles = notEnoughCandles;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCandleIndex() {
		return candleIndex;
	}

	public void setCandleIndex(int candleIndex) {
		this.candleIndex = candleIndex;
	}

	public boolean isNotEnoughCandles() {
		return notEnoughCandles;
	}

	public void setNotEnoughCandles(boolean notEnoughCandles) {
		this.notEnoughCandles = notEnoughCandles;
	}

	public boolean isResetAnyWay() {
		return resetAnyWay;
	}

	public void setResetAnyWay(boolean resetAnyWay) {
		this.resetAnyWay = resetAnyWay;
	}

}
