package rawai.it.com.TradingPlatform.urlutils;


public class ChartInstrument {

	private String instrument;

	private String interval;

	private String time;

	public ChartInstrument() {
	}

	public ChartInstrument(String instrument, String interval, String time) {
		super();
		this.instrument = instrument;
		this.interval = interval;
		this.time = time;
	}

	public ChartInstrument(String instrument, String interval) {
		this.instrument = instrument;
		this.interval = interval;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instrument == null) ? 0 : instrument.hashCode());
		result = prime * result + ((interval == null) ? 0 : interval.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChartInstrument other = (ChartInstrument) obj;
		if (instrument == null) {
			if (other.instrument != null)
				return false;
		} else if (!instrument.equals(other.instrument))
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		return true;
	}
}
