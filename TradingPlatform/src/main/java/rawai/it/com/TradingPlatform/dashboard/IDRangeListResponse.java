package rawai.it.com.TradingPlatform.dashboard;

import java.util.List;

public class IDRangeListResponse {

	private List<IDRangeResponse> idRanges;

	public List<IDRangeResponse> getIdRanges() {
		return idRanges;
	}

	public void setIdRanges(List<IDRangeResponse> idRanges) {
		this.idRanges = idRanges;
	}
}
