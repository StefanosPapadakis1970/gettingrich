package rawai.it.com.TradingPlatform.engine;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;
import rawai.it.com.TradingPlatform.urlutils.ChartInstrument;

public interface CandleListener {

	public List<ChartInstrument> getChartInstruments();

	public List<AlgorithmTrade> processCandle(CandleDataPointInterface candle, TradingInstrument tradingInstrument);

	public List<AlgorithmTrade> postProcessTrades(List<AlgorithmTrade> trades, CandleDataPointInterface candle);
}
