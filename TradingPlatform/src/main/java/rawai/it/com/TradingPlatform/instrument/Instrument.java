package rawai.it.com.TradingPlatform.instrument;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Instrument {

	private String displayName;
	private String displayPrecision;
	private String marginRate;
	private String maximumOrderUnits;
	private String maximumPositionSize;
	private String maximumTrailingStopDistance;
	private String minimumTradeSize;
	private String minimumTrailingStopDistance;
	private String name;
	private String pipLocation;
	private String tradeUnitsPrecision;
	private String type;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayPrecision() {
		return displayPrecision;
	}

	public void setDisplayPrecision(String displayPrecision) {
		this.displayPrecision = displayPrecision;
	}

	public String getMarginRate() {
		return marginRate;
	}

	public void setMarginRate(String marginRate) {
		this.marginRate = marginRate;
	}

	public String getMaximumOrderUnits() {
		return maximumOrderUnits;
	}

	public void setMaximumOrderUnits(String maximumOrderUnits) {
		this.maximumOrderUnits = maximumOrderUnits;
	}

	public String getMaximumPositionSize() {
		return maximumPositionSize;
	}

	public void setMaximumPositionSize(String maximumPositionSize) {
		this.maximumPositionSize = maximumPositionSize;
	}

	public String getMaximumTrailingStopDistance() {
		return maximumTrailingStopDistance;
	}

	public void setMaximumTrailingStopDistance(String maximumTrailingStopDistance) {
		this.maximumTrailingStopDistance = maximumTrailingStopDistance;
	}

	public String getMinimumTradeSize() {
		return minimumTradeSize;
	}

	public void setMinimumTradeSize(String minimumTradeSize) {
		this.minimumTradeSize = minimumTradeSize;
	}

	public String getMinimumTrailingStopDistance() {
		return minimumTrailingStopDistance;
	}

	public void setMinimumTrailingStopDistance(String minimumTrailingStopDistance) {
		this.minimumTrailingStopDistance = minimumTrailingStopDistance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPipLocation() {
		return pipLocation;
	}

	public void setPipLocation(String pipLocation) {
		this.pipLocation = pipLocation;
	}

	public String getTradeUnitsPrecision() {
		return tradeUnitsPrecision;
	}

	public void setTradeUnitsPrecision(String tradeUnitsPrecision) {
		this.tradeUnitsPrecision = tradeUnitsPrecision;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
