package rawai.it.com.TradingPlatform.engine;

import java.util.ArrayList;
import java.util.List;

import rawai.it.com.TradingPlatform.databuilders.TradingAlgorithmBuilder;
import rawai.it.com.TradingPlatform.engine.signals.TradingSignal;

public class TradingAlgorithmForm {

	private String algorithmName;

	private String dbID;

	private boolean buy;

	private int stopLossPips = 30;

	private int takeProfitPips = 30;

	private List<TradingSignalForm> signals = new ArrayList<>();
	
	private List<PostTradingSignalForm> postTradngSignals = new ArrayList<>();

	public List<TradingSignalForm> getSignals() {
		return signals;
	}

	public void setSignals(List<TradingSignalForm> signals) {
		this.signals = signals;
	}


	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getDbID() {
		return dbID;
	}

	public void setDbID(String dbID) {
		this.dbID = dbID;
	}

	public boolean isBuy() {
		return this.buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}

	public TradingAlgorithm createAlgorithm() {
		List<TradingSignal> tradingSignals = new ArrayList<>();

		return TradingAlgorithmBuilder.aTradingAlgorithmEntityBuilder().withID((dbID == null || dbID.equals("") || dbID.equals("null")) ? null : Long.valueOf(dbID))
				.withName(algorithmName).withTradingSignals(tradingSignals).withTakeProfitPips(takeProfitPips).withStopLossPips(stopLossPips).build();

	}

	public int getStopLossPips() {
		return stopLossPips;
	}

	public void setStopLossPips(int stopLossPips) {
		this.stopLossPips = stopLossPips;
	}

	public int getTakeProfitPips() {
		return takeProfitPips;
	}

	public void setTakeProfitPips(int takeProfitPips) {
		this.takeProfitPips = takeProfitPips;
	}

	public List<PostTradingSignalForm> getPostTradngSignals() {
		return postTradngSignals;
	}

	public void setPostTradngSignals(List<PostTradingSignalForm> postTradngSignals) {
		this.postTradngSignals = postTradngSignals;
	}

}
