package rawai.it.com.TradingPlatform.dashboard;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import rawai.it.com.TradingPlatform.account.Account;
import rawai.it.com.TradingPlatform.account.CurrencyEnum;
import rawai.it.com.TradingPlatform.price.PriceService;
import rawai.it.com.TradingPlatform.urlutils.CurlUtil;
import rawai.it.com.TradingPlatform.urlutils.SessionUtils;
import rawai.it.com.TradingPlatform.utils.FormatingUtils;

@Controller
public class TransactionService {

	private static final Logger log = LogManager.getLogger(TransactionService.class);
	@Autowired
	private CurlUtil curlUtil;
	@Autowired
	private PriceService priceService;

	public double getAllRealizedProfit(Date from, Date to, CurrencyEnum targetCurency) {
		RestTemplate tmpl = new RestTemplate();
		double totalPL = 0;

		Calendar gc = GregorianCalendar.getInstance();
		gc.setTime(from);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		from = gc.getTime();

		gc.setTime(to);
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		to = gc.getTime();

		String fromString = FormatingUtils.createCandleDateString(from);
		String toString = FormatingUtils.createCandleDateString(to);

		List<CalulatorThread> calculatorThreads = new ArrayList<>();

		for (Account account : SessionUtils.getAccounts()) {
			log.info("Account " + account.getUserName());
			log.info("From " + fromString);
			log.info("To " + toString);
			CalculateRealizedProfitRunner runner = new CalculateRealizedProfitRunner(account, fromString, toString, targetCurency);
			calculatorThreads.add(new CalulatorThread(runner));

		}

		for (Thread thread : calculatorThreads) {
			thread.start();
		}

		for (CalulatorThread thread : calculatorThreads) {
			try {
				thread.join();
				totalPL += thread.getResult();
				log.info("Thread Stopped!!");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return totalPL;
	}

	class CalculateRealizedProfitRunner implements Runnable {

		private Account account;
		private String fromString;
		private String toString;
		private double totalPL = 0.0d;
		private CurrencyEnum targetCurrency;

		@Override
		public void run() {
			RestTemplate tmpl = new RestTemplate();

			String url = curlUtil.createAccountUrl() + "/" + account.getUserId() + "/transactions?from=" + fromString + "&to=" + toString + "&type=ORDER_FILL";
			log.info(url);

			Double exchangeRate = 1d;
			if (!account.getCurrency().equals(targetCurrency)) {
				exchangeRate = priceService.fetchExchangeRate(account.getUserId(), account.getCurrency(), targetCurrency);
				exchangeRate = 1d / exchangeRate;
			}
			HttpEntity<Object> apiHeader = curlUtil.createAPIHeader(account.getApiKey());
			ResponseEntity<IDRangeResponse> idRange = tmpl.exchange(url, HttpMethod.GET, apiHeader, IDRangeResponse.class);

			for (String pageURL : idRange.getBody().getPages()) {
				ResponseEntity<OrderFillTransactionListResponse> respList = tmpl.exchange(pageURL, HttpMethod.GET, apiHeader, OrderFillTransactionListResponse.class);

				for (OrderFillTransaction orderFillTransaction : respList.getBody().getTransactions()) {
					totalPL += Double.valueOf(orderFillTransaction.getPl()) * exchangeRate;
				}
			}
			log.info("Account " + account.getUserName());
			log.info("From " + fromString);
			log.info("To " + toString);
			log.info("totalPL " + totalPL);
		}

		public CalculateRealizedProfitRunner(Account account, String fromString, String toString, CurrencyEnum targetCurrency) {
			super();
			this.account = account;
			this.fromString = fromString;
			this.toString = toString;
			this.targetCurrency = targetCurrency;
		}

		public double getTotalPL() {
			return totalPL;
		}

	}

	class CalulatorThread extends Thread {

		private CalculateRealizedProfitRunner runner;

		public CalulatorThread(Runnable target) {
			super(target);
			this.runner = (CalculateRealizedProfitRunner) target;
		}

		public double getResult() {
			return this.runner.getTotalPL();
		}
	}

}
