package rawai.it.com.TradingPlatform.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderCancelTransaction extends BaseOrderCancelTransaction {
	private String replacedByOrderID;

	public String getReplacedByOrderID() {
		return replacedByOrderID;
	}

	public void setReplacedByOrderID(String replacedByOrderID) {
		this.replacedByOrderID = replacedByOrderID;
	}
}
