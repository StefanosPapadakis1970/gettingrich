package rawai.it.com.TradingPlatform.dashboard;

public enum TakeProfitOrderReason {
	CLIENT_ORDER, REPLACEMENT, ON_FILL;
}
