package rawai.it.com.TradingPlatform.chart;

import java.util.List;

import rawai.it.com.TradingPlatform.candle.BasicDataPointInterface;

public class ChartData<T extends BasicDataPointInterface> {

	private String type;
	private String xValueType;
	private List<T> dataPoints;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<T> getDataPoints() {
		return dataPoints;
	}

	public void setDataPoints(List<T> dataPoints) {
		this.dataPoints = dataPoints;
	}

	public String getxValueType() {
		return xValueType;
	}

	public void setxValueType(String xValueType) {
		this.xValueType = xValueType;
	}
}
