package rawai.it.com.TradingPlatform.engine.signals;

import rawai.it.com.TradingPlatform.candle.CandleDataPointInterface;

public class SignalUtility {
	
	public static boolean isPositiveCandleRange(CandleDataPointInterface candle) {
		int close = Integer.valueOf(candle.getC().replace(".", ""));		
		int open = Integer.valueOf(candle.getO().replace(".", ""));
		return close > open;
	}
	
	
	public static int getCandleRange(CandleDataPointInterface candle) {
		int close = Integer.valueOf(candle.getC().replace(".", ""));		
		int open = Integer.valueOf(candle.getO().replace(".", ""));
		return Math.abs(close - open);
	}
}
