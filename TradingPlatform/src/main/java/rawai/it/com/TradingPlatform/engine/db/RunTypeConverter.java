package rawai.it.com.TradingPlatform.engine.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import rawai.it.com.TradingPlatform.tradingprofile.RunType;

@Converter
public class RunTypeConverter implements AttributeConverter<RunType, String> {

	@Override
	public String convertToDatabaseColumn(RunType runType) {
		if (runType == null) {
			return null;
		}
		return runType.name();
	}

	@Override
	public RunType convertToEntityAttribute(String dbData) {
		return RunType.valueOf(dbData);
	}

}
