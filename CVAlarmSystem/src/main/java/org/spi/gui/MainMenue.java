package org.spi.gui;

import com.google.common.eventbus.Subscribe;
import jdk.jfr.Threshold;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.actions.*;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

public class MainMenue extends JMenu {
    private static final Logger logger = LogManager.getLogger(MainMenue.class);
    private final JMenu ex1;
    private final JMenuItem aufgabe2;

    public MainMenue(ResourceBundle bundle) {
        super(bundle.getString("menu.file"));

        JMenuItem menu1_1 = new JMenuItem(new FileOpenAction(bundle.getString("menu.open")));
        JMenuItem menu1_2 = new JMenuItem(new SaveAction(bundle.getString("menu.save")));
        JMenuItem menu1_3 = new JMenuItem(new CVAlarmAction(bundle.getString("menu.exit"), CVAlarmEventEnum.EXIT));

        //ex1 = new JMenu(new SaveAction(bundle.getString("menu.exercise1")));
        ex1 = new JMenu(new CVAlarmAction(bundle.getString("menu.exercise1"), CVAlarmEventEnum.SAVE));
        JMenuItem flip = new JMenuItem(new FlipAction(bundle.getString("menu.flip")));
        JMenuItem replaceColor = new JMenuItem(new ReplaceColorAction(bundle.getString("menu.replaceColor")));
        JMenuItem transparentBackground = new JMenuItem(new TransparentBackgroundAction(bundle.getString("menu.transparentBackground"), CVAlarmEventEnum.MAKE_BACKGROUND_TRANSPARENT));
        JMenuItem startCamera = new JMenuItem(new StartCameraAction(bundle.getString("menu.startCamera"), CVAlarmEventEnum.START_CAMERA));
        JMenuItem threshold = new JMenuItem(new ThresholdAction(bundle.getString("menu.threshold"), CVAlarmEventEnum.THRESHOLD));
        JMenuItem stopCamera = new JMenuItem(new StopCameraAction(bundle.getString("menu.stopCamera"), CVAlarmEventEnum.STOP_CAMERA));
        //JMenuItem detectNumberPlate = new JMenuItem(new DetectNumberPlateAction(bundle.getString("menu.detectNumberPlate"), CVAlarmEventEnum.DETECT_NUMBERPLATE));
        JMenuItem aufgabe1 = new JMenuItem(new Aufgabe1Action(bundle.getString("menu.aufgabe1"), CVAlarmEventEnum.AUFGABE1));
        JCheckBoxMenuItem drawRectangle = new JCheckBoxMenuItem(new CVAlarmAction(bundle.getString("menu.drawRectangle"), CVAlarmEventEnum.DRAW_RECTANGLE));
        JCheckBoxMenuItem drawCircleOnMouseDown = new JCheckBoxMenuItem(bundle.getString("menu.drawCircleOnMouseDown"));
        drawRectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CVAlarmSystemModel.getInstance().setDrawRectangle(((JCheckBoxMenuItem)e.getSource()).getState());
            }
        });

        drawCircleOnMouseDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CVAlarmSystemModel.getInstance().setDrawCircles(((JCheckBoxMenuItem)e.getSource()).getState());
            }
        });

        aufgabe2 = new JMenuItem(new Aufgabe2Action(bundle.getString("menu.aufgabe2"), CVAlarmEventEnum.AUFGABE2));

        ex1.setEnabled(false);
        ex1.add(flip);
        ex1.add(replaceColor);
        ex1.add(ex1.add(transparentBackground));
        ex1.add(new JSeparator());
        ex1.add(aufgabe1);
        this.add(new JSeparator());
        this.add(ex1);
        aufgabe2.setEnabled(false);
        this.add(aufgabe2);
        this.add(threshold);

        this.add(new JSeparator());
        this.add(menu1_1);
        this.add(menu1_2);
        this.add(startCamera);
        this.add(drawRectangle);
        this.add(drawCircleOnMouseDown);
        this.add(stopCamera);
        this.add(new JSeparator());
        this.add(menu1_3);
        EventBusFactory.getEventBus().register(this);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void imageLoaded(CVAlarmEvent event) {
        //logger.debug("Event arrived in Main-menu imageLoaded "+event.getEventEnum());
        if (event.getEventEnum() == CVAlarmEventEnum.IMAGELOADED){
            //logger.debug("Enabling Menu");
            ex1.setEnabled(true);
            aufgabe2.setEnabled(true);
        }
    }
}
