package org.spi.gui;

import com.google.common.eventbus.Subscribe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Point;
import org.spi.gui.actions.CVAlarmAction;
import org.spi.gui.components.ImagePanel;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.imageprocessing.ImageUtils;


import javax.swing.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;


public class MainFrame extends JFrame {
    @SuppressWarnings({"unused"})
    private static final Logger logger = LogManager.getLogger(MainFrame.class);

    private final JLabel imageLabelLeft = new JLabel();
    private final JLabel imageLabelRight = new JLabel();

    private final ImagePanel imagePanel;

    private final ResourceBundle bundle;

    private final List<Component> controllButtons = new ArrayList<>();

    public MainFrame(){
        bundle = ResourceBundle.getBundle("ApplicationMessages");
        imagePanel = new ImagePanel();
        imagePanel.add(imageLabelLeft);
        JScrollPane scrollPane = new JScrollPane(imagePanel);
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        imagePanel.setBorder(BorderFactory.createLineBorder(Color.yellow, 10));
        getContentPane().add(scrollPane);
    }

    private void displayImageLeft(Image image) {
        ImageIcon icon= new ImageIcon(image);
        imageLabelLeft.setIcon(icon);
        imageLabelLeft.setBorder(BorderFactory.createLineBorder(Color.green, 10));
        invalidate();
    }


    @SuppressWarnings({"unused"})
    private void displayImageRight(Image image) {
        ImageIcon icon=new ImageIcon(image);
        imageLabelRight.setIcon(icon);
        invalidate();
    }

    public void drawCirclesOnRightMouseDown(){
        if (imageLabelLeft.getMouseListeners().length > 0){
            return;
        }
        imageLabelLeft.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                logger.debug("drawCirclesOnRightMouseDown "+e.getPoint());
                if (e.getButton() == MouseEvent.BUTTON2){

                }
                ImageUtils.drawCircle(CVAlarmSystemModel.getInstance().getMatrix(),new Point(e.getPoint().getX(),e.getPoint().getY()));
            }
        });
    }

    public void drawCirclesInVideo(){
        if (imageLabelLeft.getMouseListeners().length > 0){
            return;
        }
        imageLabelLeft.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                Point center = new Point(e.getPoint().getX(),e.getPoint().getY());
                logger.debug("drawCirclesInVideo "+e.getPoint()+ " "+center);
                if (e.getButton() == MouseEvent.BUTTON2){

                }
                CVAlarmSystemModel.getInstance().addCircle(center);
            }
        });
    }

    @Subscribe
    @SuppressWarnings({"unused"})
    public void showImage(CVAlarmEvent event) {
        if (event.getEventEnum() == CVAlarmEventEnum.IMAGELOADED || event.getEventEnum() == CVAlarmEventEnum.IMAGE_CHANGED) {
            //logger.debug("Show Image " + event.getEventEnum());
            CVAlarmSystemModel.getInstance().setImageSilent(event.getImage());
            if (event.getMatrix() != null){
                CVAlarmSystemModel.getInstance().setMatrixSilent(event.getMatrix());
            }
            displayImageLeft(CVAlarmSystemModel.getInstance().getImage());
        }
    }

    public void showCameraButtons() {
        logger.debug("Show Camera Buttons");
        if (controllButtons.isEmpty()){

            JToggleButton button = new JToggleButton(new CVAlarmAction(bundle.getString("menu.face"), CVAlarmEventEnum.DETECT_FACE));
            controllButtons.add(button);

            button = new JToggleButton(new CVAlarmAction(bundle.getString("menu.profile"), CVAlarmEventEnum.DETECT_PROFILE));
            controllButtons.add(button);

            button = new JToggleButton(new CVAlarmAction(bundle.getString("menu.body"), CVAlarmEventEnum.DETECT_BODY));
            controllButtons.add(button);


        }

        controllButtons.forEach(imagePanel::add);

        invalidate();
    }

    public void removeCameraButons() {
        logger.debug("Remove Camera Buttons");
        // TODO unteren Ausdruck verstehen
        //controllButtons.forEach(imagePanel::remove);
        controllButtons.forEach(button -> imagePanel.remove(button));
        repaint();
    }
}
