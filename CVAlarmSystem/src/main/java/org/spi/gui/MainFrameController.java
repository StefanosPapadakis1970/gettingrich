package org.spi.gui;

import com.google.common.eventbus.Subscribe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.imageprocessing.ImageUtils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainFrameController {

    private static final Logger logger = LogManager.getLogger(MainFrameController.class);

    private final MainFrame view;

    public MainFrameController(MainFrame view) {
        this.view = view;
    }

    @Subscribe
    @SuppressWarnings({"unused"})
    public void rotateImage(CVAlarmEvent event){
        if (event.getEventEnum() == CVAlarmEventEnum.ROTATE){
            logger.debug("Matrix before Flip"+CVAlarmSystemModel.getInstance().getMatrix());
            Mat tmpMat = CVAlarmSystemModel.getInstance().getMatrix();
            CVAlarmSystemModel.getInstance().setImage(ImageUtils.flipImage(CVAlarmSystemModel.getInstance().getMatrix()));
            logger.debug("Matrix after Flip"+CVAlarmSystemModel.getInstance().getMatrix()+" equal ? "+CVAlarmSystemModel.getInstance().getMatrix().equals(tmpMat));
        }
    }
    @Subscribe
    public void processEvent(CVAlarmEvent event){
        switch (event.getEventEnum()){
            case CHANGE_COLOR -> {
/*                imageCounter++;
                String text = "ImageGaudi "+imageCounter;
                Point position = new Point(170, 280);
                Scalar color = new Scalar(255, 255, 255);
                int font = Imgproc.FONT_HERSHEY_SIMPLEX;
                int scale = 1;
                int thickness = 3; */
                ImageUtils.replacePixelColor(CVAlarmSystemModel.getInstance().getMatrix());
                //Imgproc.putText(matrix, text, position, font, scale, color, thickness);
                CVAlarmSystemModel.getInstance().setImage(ImageUtils.getBufferedImage(CVAlarmSystemModel.getInstance().getMatrix()));
                //displayImageLeft(CVAlarmSystemModel.getInstance().getImage());
                Imgcodecs.imwrite("c:\\development\\result.jpg", CVAlarmSystemModel.getInstance().getMatrix());
            }
            case MAKE_BACKGROUND_TRANSPARENT -> {
                ImageUtils.makeTransparentBackground(CVAlarmSystemModel.getInstance().getMatrix());
                Imgcodecs.imwrite("c:\\development\\result.jpg", CVAlarmSystemModel.getInstance().getMatrix());
                CVAlarmSystemModel.getInstance().setImage(ImageUtils.getBufferedImage(CVAlarmSystemModel.getInstance().getMatrix()));
                //displayImageLeft(CVAlarmSystemModel.getInstance().getImage());
            }
            case START_CAMERA -> {
                Runnable runable = new Runnable() {
                    @Override
                    public void run() {
                        ImageUtils.stopVideo = false;
                        ImageUtils.startVideo();
                    }
                };

                Thread t = new Thread(runable);
                t.start();


            }
            case STOP_CAMERA -> {
                logger.debug("Calling Stop Video");
                ImageUtils.stopVideo();
                view.removeCameraButons();
            }
            case AUFGABE1 -> {
                logger.debug("Calling Aufgabe1");
                ImageUtils.aufgabe1(CVAlarmSystemModel.getInstance().getMatrix(), CVAlarmSystemModel.getInstance().getImage());
            }
            case AUFGABE2 -> {
                logger.debug("Calling Aufgabe2");
                view.drawCirclesOnRightMouseDown();
            }
            case DRAW_CIRCLES -> {
                logger.debug("Calling Draw Circles");
                view.drawCirclesInVideo();
            }
            case DETECT_NUMBERPLATE -> {
                logger.debug("Detect NumberPlate");

            }
            case SHOW_CAMERA_BUTTONS -> {
                logger.debug("Show CameraButtons");
                view.showCameraButtons();
            }
            case DETECT_FACE -> {
                logger.debug("Detect Face");
                CVAlarmSystemModel.getInstance().toggleDetectFace();
            }
            case EXIT -> {
                logger.debug("EXIT");
                System.exit(0);
            }

            case THRESHOLD -> {
                logger.debug("Calling Threshold");
            }
            default -> {

            }
        }
    }
}
