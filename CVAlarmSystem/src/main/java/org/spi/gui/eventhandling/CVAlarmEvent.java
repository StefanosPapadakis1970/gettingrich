package org.spi.gui.eventhandling;

import org.opencv.core.Mat;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.Buffer;

public class CVAlarmEvent {
    private CVAlarmEventEnum eventEnum;

    private BufferedImage image;

    private Mat matrix;

    private Color color;

    private boolean flag;


    public CVAlarmEvent(CVAlarmEventEnum eventEnum, BufferedImage image, Mat matrix) {
        this.eventEnum = eventEnum;
        this.image = image;
        this.matrix = matrix;
    }

    public CVAlarmEvent(CVAlarmEventEnum eventEnum, Color color) {
        this.eventEnum = eventEnum;
        this.color = color;
    }

    public CVAlarmEvent(CVAlarmEventEnum eventEnum, boolean flag) {
        this.eventEnum = eventEnum;
        this.flag = flag;
    }


    public CVAlarmEventEnum getEventEnum() {
        return eventEnum;
    }

    public void setEventEnum(CVAlarmEventEnum eventEnum) {
        this.eventEnum = eventEnum;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Mat getMatrix() {
        return matrix;
    }

    public void setMatrix(Mat matrix) {
        this.matrix = matrix;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
