package org.spi.gui;

import jdk.jfr.Event;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;


import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class CVAlarmSystemModel {

    private Logger logger = LogManager.getLogger(CVAlarmSystemModel.class);
    private BufferedImage image;

    private Mat matrix;

    private Color color = new Color(0,0,0);

    private List<Point> circlesToDraw = new ArrayList<>();

    private static final CVAlarmSystemModel instance;

    private boolean drawRectangle;
    private boolean drawCircles;

    private boolean showCameraButtons;

    private boolean detectFace;

    private boolean detectProfil;

    private boolean detectBody;

    private CVAlarmSystemModel(){}

    // static block initialization for exception handling
    static {
        try {
            instance = new CVAlarmSystemModel();
        } catch (Exception e) {
            throw new RuntimeException("Exception occurred in creating singleton instance");
        }
    }

    public static CVAlarmSystemModel getInstance() {
        return instance;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        EventBusFactory.getEventBus().post(new CVAlarmEvent(CVAlarmEventEnum.IMAGE_CHANGED, image, null));
    }

    public Mat getMatrix() {
        return matrix;
    }

    public void setMatrix(Mat matrix) {
        this.matrix = matrix;
        EventBusFactory.getEventBus().post(new CVAlarmEvent(CVAlarmEventEnum.MATRIX_CHANGED, null, matrix));
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        EventBusFactory.getEventBus().post(new CVAlarmEvent(CVAlarmEventEnum.COLOR_CHANGED, color));
    }

    public boolean isDrawRectangle() {
        return drawRectangle;
    }

    public void setDrawRectangle(boolean drawRectangle) {
        this.drawRectangle = drawRectangle;
    }

    public void setImageSilent(BufferedImage image) {
        this.image = image;
    }

    public void setMatrixSilent(Mat matrix) {
        this.matrix = matrix;
    }

    public void addCircle(Point center){
        if (!circlesToDraw.contains(center)){
            circlesToDraw.add(center);
        }
    }

    public List<Point> getCirclesToDraw() {
        return circlesToDraw;
    }

    public void setDrawCircles(boolean state) {
        drawCircles = state;
        EventBusFactory.getEventBus().post(new CVAlarmEvent(CVAlarmEventEnum.DRAW_CIRCLES, null, null));
    }

    public boolean isDrawCircles() {
        return drawCircles;
    }

    public void setCirclesToDraw(List<Point> circlesToDraw) {
        this.circlesToDraw = circlesToDraw;
    }

    public boolean isShowCameraButtons() {
        return showCameraButtons;
    }

    public void setShowCameraButtons(boolean showCameraButtons) {
        logger.debug("set show camera-buttons  "+showCameraButtons);
        this.showCameraButtons = showCameraButtons;
        EventBusFactory.getEventBus().post(new CVAlarmEvent(CVAlarmEventEnum.SHOW_CAMERA_BUTTONS, null, null));
    }

    public boolean isDetectFace() {
        return detectFace;
    }

    public void setDetectFace(boolean detectFace) {
        this.detectFace = detectFace;
    }

    public boolean isDetectProfil() {
        return detectProfil;
    }

    public void setDetectProfil(boolean detectProfil) {
        this.detectProfil = detectProfil;
    }

    public boolean isDetectBody() {
        return detectBody;
    }

    public void setDetectBody(boolean detectBody) {
        this.detectBody = detectBody;
    }

    public void toggleDetectFace() {
        this.detectFace = !this.detectFace;
    }
}
