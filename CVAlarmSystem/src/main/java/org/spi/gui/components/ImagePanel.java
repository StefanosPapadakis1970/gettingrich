package org.spi.gui.components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.MainFrame;

import javax.swing.JPanel;
import java.awt.Component;

public class ImagePanel extends JPanel {
    private static final Logger logger = LogManager.getLogger(ImagePanel.class);
    @Override
    public void remove(Component comp){
        super.remove(comp);
        logger.debug("Removing component "+comp);
    }
}
