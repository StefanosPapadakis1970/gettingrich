package org.spi.gui.actions;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;

public  class CVAlarmAction extends AbstractAction {


    protected Logger logger;// = LogManager.getLogger(CVAlarmAction.class);


    private final EventBus eventBus;

    private final CVAlarmEventEnum event;

    public CVAlarmAction(String title, CVAlarmEventEnum event) {
        super(title);
        this.event = event;
        this.eventBus = EventBusFactory.getEventBus();
        logger = LogManager.getLogger(title);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug("Action performed -> "+this.event);
        CVAlarmEvent event = new CVAlarmEvent(this.event, null, null);
        eventBus.post(event);
    }
}
