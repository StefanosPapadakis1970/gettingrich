package org.spi.gui.actions;

import org.apache.logging.log4j.LogManager;
import org.spi.gui.CVAlarmSystemModel;
import org.spi.gui.eventhandling.CVAlarmEventEnum;

import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.event.ActionEvent;

public class StartCameraAction extends CVAlarmAction{

    public StartCameraAction(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(StartCameraAction.class);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        CVAlarmSystemModel.getInstance().setShowCameraButtons(true);
    }
}
