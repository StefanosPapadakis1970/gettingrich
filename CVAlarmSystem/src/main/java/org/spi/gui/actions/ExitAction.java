package org.spi.gui.actions;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ExitAction extends AbstractAction {


    public ExitAction(String title) {
        super(title);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
