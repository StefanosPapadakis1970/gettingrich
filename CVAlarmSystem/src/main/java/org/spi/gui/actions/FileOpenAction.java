package org.spi.gui.actions;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;
import org.spi.imageprocessing.ImageUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;

public class FileOpenAction extends AbstractAction {
    private static final Logger logger = LogManager.getLogger(FileOpenAction.class);

    private final EventBus eventBus;

    public FileOpenAction(String title) {
        super(title);
        this.eventBus = EventBusFactory.getEventBus();
    }


    public void actionPerformed(ActionEvent e) {
        System.out.println("action " + e.getSource());
        JFileChooser fileChooser = new JFileChooser("C:\\development\\OpenCV-master_v6_12\\DATA");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JPG & GIF Images", "jpg", "gif");
        fileChooser.setFileFilter(filter);

        int result = fileChooser.showOpenDialog(null);

        // if the user selects a file
        if (result == JFileChooser.APPROVE_OPTION) {
            logger.debug("FilePath: " + fileChooser.getSelectedFile().getAbsolutePath());
        } else {
            return;
        }

        //eventBus.post(ImageUtils.createImageFromPath(fileChooser.getSelectedFile().getAbsolutePath()));
        logger.debug("Image Loaded");
        CVAlarmEvent event = new CVAlarmEvent(CVAlarmEventEnum.IMAGELOADED,ImageUtils.createImageFromPath(fileChooser.getSelectedFile().getAbsolutePath()), ImageUtils.createMatrixFromPath(fileChooser.getSelectedFile().getAbsolutePath()));
        eventBus.post(event);
        logger.debug("Image Loaded send ");
    }
}