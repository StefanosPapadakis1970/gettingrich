package org.spi.gui.actions;

import org.apache.logging.log4j.LogManager;
import org.spi.gui.eventhandling.CVAlarmEventEnum;

public class Aufgabe2Action extends CVAlarmAction {

    public Aufgabe2Action(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(Aufgabe2Action.class);
    }
}
