package org.spi.gui.actions;

import org.apache.logging.log4j.LogManager;
import org.spi.gui.eventhandling.CVAlarmEventEnum;

public class Aufgabe1Action extends CVAlarmAction {

    public Aufgabe1Action(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(Aufgabe1Action.class);
    }
}
