package org.spi.gui.actions;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class FlipAction extends AbstractAction {

    private static final Logger logger = LogManager.getLogger(FlipAction.class);

    private final EventBus eventBus;
    public FlipAction(String title) {
        super(title);
        eventBus = EventBusFactory.getEventBus();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug("FlipAction performed");
        CVAlarmEvent event = new CVAlarmEvent(CVAlarmEventEnum.ROTATE, null, null);
        eventBus.post(event);
    }
}
