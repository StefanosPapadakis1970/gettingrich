package org.spi.gui.actions;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ReplaceColorAction extends AbstractAction {
    private static final Logger logger = LogManager.getLogger(ReplaceColorAction.class);

    private final EventBus eventBus;

    public ReplaceColorAction(String title) {
        super(title);
        this.eventBus = EventBusFactory.getEventBus();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        CVAlarmEvent event = new CVAlarmEvent(CVAlarmEventEnum.CHANGE_COLOR,null,null);
        eventBus.post(event);
    }
}
