package org.spi.gui.actions;

import org.apache.logging.log4j.LogManager;
import org.spi.gui.CVAlarmSystemModel;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ThresholdAction extends CVAlarmAction {

    public ThresholdAction(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(ThresholdAction.class);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        final JColorChooser chooser = new JColorChooser();
        chooser.setColor(Color.BLACK);
        chooser.getSelectionModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                Color color = chooser.getColor();
                CVAlarmSystemModel.getInstance().setColor(color);
            }
        });
        JDialog dialog = JColorChooser.createDialog(null, "Color Chooser",
                true, chooser, null, null);
        dialog.setVisible(true);
    }
}
