package org.spi.gui.actions;

import org.apache.logging.log4j.LogManager;
import org.spi.gui.eventhandling.CVAlarmEventEnum;

public class StopCameraAction extends CVAlarmAction{

    public StopCameraAction(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(StopCameraAction.class);
    }
}
