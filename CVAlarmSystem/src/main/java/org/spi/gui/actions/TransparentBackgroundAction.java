package org.spi.gui.actions;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class TransparentBackgroundAction extends CVAlarmAction {

    public TransparentBackgroundAction(String title, CVAlarmEventEnum event) {
        super(title, event);
        logger = LogManager.getLogger(TransparentBackgroundAction.class);
    }
}
