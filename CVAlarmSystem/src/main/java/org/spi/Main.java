package org.spi;

import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatIntelliJLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.intellijthemes.*;
import com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedDarkIJTheme;
import nu.pattern.OpenCV;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spi.config.PropertiesManager;
import org.spi.gui.MainFrame;
import org.spi.gui.MainFrameController;
import org.spi.gui.MainMenue;
import org.spi.gui.eventhandling.EventBusFactory;

import javax.swing.*;
import java.util.ResourceBundle;




public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        //MainMenue menu = new MainMenue("Fi");
        logger.debug("Starting Application");

        try {
            //UIManager.setLookAndFeel( new FlatLightLaf() );
            //UIManager.setLookAndFeel( new FlatArcOrangeIJTheme() );
            UIManager.setLookAndFeel( new FlatArcDarkIJTheme());

        } catch( Exception ex ) {
            System.err.println( "Failed to initialize LaF" );
        }


        ResourceBundle bundle;

        try {
            OpenCV.loadLocally();
            bundle = ResourceBundle.getBundle("ApplicationMessages");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        PropertiesManager manager  = new PropertiesManager();
        manager.getCVAlarmProperties();
        System.out.println(manager.getCVAlarmProperties());
        System.out.println(manager.getCVAlarmProperties().getProperty("windowTitle"));
        System.out.println(manager.getCVAlarmProperties().getProperty("mailServerName"));


        MainFrame mainFrame = new MainFrame();
        MainFrameController mainFrameController = new MainFrameController(mainFrame);
        mainFrame.setTitle(bundle.getString("windowTitle")+"-"+manager.getCVAlarmProperties().getProperty("env"));
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();

        MainMenue menu = new MainMenue(bundle);
        menuBar.add(menu);

        mainFrame.setJMenuBar(menuBar);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        EventBusFactory.getEventBus().register(mainFrame);
        EventBusFactory.getEventBus().register(mainFrameController);
        mainFrame.invalidate();

    }
}