package org.spi.imageprocessing;

import com.google.common.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;
import org.spi.gui.CVAlarmSystemModel;
import org.spi.gui.eventhandling.CVAlarmEvent;
import org.spi.gui.eventhandling.CVAlarmEventEnum;
import org.spi.gui.eventhandling.EventBusFactory;


import javax.swing.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {

    private static final Logger logger = LogManager.getLogger(ImageUtils.class);

    private static final EventBus eventBus;
    private static VideoCapture capture;
    public static boolean stopVideo = false;

    static {
        eventBus = EventBusFactory.getEventBus();
    }


    public static BufferedImage createImageFromPath(String path){
        Mat matrix = Imgcodecs.imread(path);
        return Mat2BufferedImage(matrix);
    }

    public static Mat createMatrixFromPath(String path){
        Mat matrix = new Mat();
        Mat tmp = new Mat();
        matrix =Imgcodecs.imread(path);
        return matrix;
    }

    public static BufferedImage Mat2BufferedImage(Mat m) {
        // Fastest code
        // output can be assigned either to a BufferedImage or to an Image
        logger.debug("Matrix: "+m);

        logger.debug("Matrix1682: "+m);

        return getBufferedImage(m);
    }

    private static BufferedImage getBufferedImage(Mat m, int type) {
        int bufferSize = m.channels()* m.cols()* m.rows();
        byte [] b = new byte[bufferSize];
        m.get(0,0,b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    public static BufferedImage getBufferedImage(Mat m) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( m.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        if ( m.channels() > 3 ) {
            type = BufferedImage.TYPE_4BYTE_ABGR;
        }
        return getBufferedImage(m,  type);
    }

    @SuppressWarnings("unused")
    public static BufferedImage Mat2BufferedImageGrayScale(Mat m) {
        // Fastest code
        // output can be assigned either to a BufferedImage or to an Image

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( m.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
        Imgproc.cvtColor(m, m, Imgproc.COLOR_BGR2GRAY);
        return getBufferedImage(m,  type);
    }

    @SuppressWarnings("unused")
    public static BufferedImage flipImage(Mat matrix) {
        logger.debug("Flipping image ");
        if (matrix == null){
            return null;
        }
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( matrix.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = matrix.channels()*matrix.cols()*matrix.rows();
        Core.rotate(matrix, matrix, Core.ROTATE_90_CLOCKWISE);
        return getBufferedImage(matrix,  type);
    }

    public static void replacePixelColor(BufferedImage image){

        List<Integer> colorList = new ArrayList<>();

        Mat m = new Mat();

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                //Retrieving contents of a pixel
                int pixel = image.getRGB(x,y);
                //Creating a Color object from pixel value
                Color color = new Color(pixel, true);
                if (!colorList.contains(Integer.valueOf(color.getRGB()))){
                    colorList.add(color.getRGB());
                    logger.debug("Following Collor detected and added "+color+" -> "+color.getRGB());
                }
                //Retrieving the R G B values
                int red = color.getRed();
                int green = color.getGreen();
                int blue = color.getBlue();
                //Modifying the RGB values
                green = 150;
                //Creating new Color object

                //Setting new Color object to the image
                if (blue <= 150 && blue >= 55){
                    blue = 150;
                    color = new Color(red, green, blue);
                    image.setRGB(x, y, color.getRGB());
                }
            }
        }

        logger.debug("Number of different  colors: "+colorList.size());

    }

    public static Mat replacePixelColor(Mat matrix){
        logger.debug("Replace PixelColorCalled");
        Color color= JColorChooser.showDialog(null,"Select a color",null);
        Mat result = new Mat();
        //create a mask based on range
        logger.debug("Color selected "+color+" "+color.getRed()+" "+color.getGreen()+" "+color.getBlue());
        Core.inRange(matrix, new Scalar(0,0,0), new Scalar(color.getRed(),color.getGreen(),color.getBlue()), result);
        //apply the mask with color you are looking for, note here scalar is in hsv
        matrix.setTo(new Scalar(0,0,0),result);
        return matrix;
    }

    public static Mat replacePixelColorFix(Mat matrix){
        logger.debug("Replace PixelColorCalled Fix");
        Mat result = new Mat();
        //create a mask based on range
        //Core.inRange(matrix, new Scalar(0,0,0), new Scalar(color.getRed(),color.getGreen(),color.getBlue()), result);
        Core.inRange(matrix, new Scalar(0,0,0), new Scalar(255,0,0), result);
        //apply the mask with color you are looking for, note here scalar is in hsv
        matrix.setTo(new Scalar(0,0,0),result);
        return matrix;
    }


    public static Mat makeTransparentBackground(Mat matrix){
        logger.debug("Make makeTransparentBackground called");
        Mat tmp = new Mat();
        Imgproc.cvtColor(matrix, tmp, Imgproc.COLOR_BGR2GRAY);
        Mat alpha = new Mat();
        double alphaValue = Imgproc.threshold(tmp,alpha,0,255,Imgproc.THRESH_BINARY);
        List<Mat> resultList = new ArrayList<>();
        Core.split(matrix, resultList);
        resultList.add(alpha);
        Core.merge(resultList, matrix);

        Imgcodecs.imwrite("c:\\development\\result.png", matrix);
        Imgcodecs.imwrite("c:\\development\\alpha.png", alpha);
        Imgcodecs.imwrite("c:\\development\\tmp.png", tmp);
        return matrix;
    }

    public static void startVideo(){
        capture = new VideoCapture(0);
        Mat matrix = new Mat();
        CVAlarmEvent event = new CVAlarmEvent(CVAlarmEventEnum.IMAGELOADED,null,null);

        String xmlProfilesFile = "c:\\development\\OpenCV-master_v6_12\\DATA\\haarcascades\\haarcascade_profileface.xml";
        String xmlFaceFrontal = "c:\\development\\OpenCV-master_v6_12\\DATA\\haarcascades\\haarcascade_frontalface_default.xml";
        String xmlUpperBody = "c:\\development\\OpenCV-master_v6_12\\DATA\\haarcascades\\haarcascade_upperbody.xml";
        String xmlGlasses = "c:\\development\\OpenCV-master_v6_12\\DATA\\haarcascades\\haarcascade_eye_tree_eyeglasses.xml";
        /*String xmlFaceFrontal3 = "c:\\development\\OpenCV-master_v6_12\\DATA\\haarcascades\\haarcascade_frontalface_alt.xml"; */

        CascadeClassifier profileClassifier = new CascadeClassifier(xmlProfilesFile);
        CascadeClassifier faceFrontalClassifier = new CascadeClassifier(xmlFaceFrontal);
        CascadeClassifier upperBody = new CascadeClassifier(xmlUpperBody);
        CascadeClassifier glasses = new CascadeClassifier(xmlGlasses);
        /*CascadeClassifier faceFrontalClassifier2 = new CascadeClassifier(xmlFaceFrontal2);
        CascadeClassifier faceFrontalClassifier3 = new CascadeClassifier(xmlFaceFrontal3); */


        while (capture.isOpened()){
            if (capture.read(matrix)) {

                /*Mat tmp = new Mat();
                Imgproc.cvtColor(matrix, matrix, Imgproc.COLOR_BGR2GRAY);
                Mat alpha = new Mat();
                double alphaValue = Imgproc.threshold(matrix,matrix, CVAlarmSystemModel.getInstance().getColor().getBlue(),255,Imgproc.THRESH_BINARY); */
                Imgproc.cvtColor(matrix, matrix, Imgproc.COLOR_BGR2GRAY);

                if(CVAlarmSystemModel.getInstance().isDrawRectangle()){
                    logger.debug("Drawing Rectangle");
                    Imgproc.rectangle(matrix, new Point(100,100), new Point(400,400), new Scalar(255,0,0));
                }

                if(CVAlarmSystemModel.getInstance().isDrawCircles()){
                    logger.debug("Drawing Rectangle");
                    CVAlarmSystemModel.getInstance().getCirclesToDraw().forEach(center -> Imgproc.circle(matrix, center, 30,new Scalar(255,0,0)));
                }

                MatOfRect hits = new MatOfRect();
                if(CVAlarmSystemModel.getInstance().isDetectProfil()) {
                    profileClassifier.detectMultiScale(matrix, hits);
                    drawRectangles(matrix, hits, new Scalar(255,0,0), "Profil");
                }

                if(CVAlarmSystemModel.getInstance().isDetectFace()) {
                    faceFrontalClassifier.detectMultiScale(matrix, hits, 1.5, 2);
                    drawRectangles(matrix, hits, new Scalar(0, 255, 0), "Face");
                }
                if(CVAlarmSystemModel.getInstance().isDetectBody()) {
                    upperBody.detectMultiScale(matrix,  hits);
                    drawRectangles(matrix,  hits, new Scalar(0,0,255),"Upper");
                }
                //upperBody.detectMultiScale(matrix,  hits);
                //drawRectangles(matrix,  hits, new Scalar(0,0,255),"Upper");
                //glasses.detectMultiScale(matrix,  hits);
                //drawRectangles(matrix,  hits, new Scalar(255,255,255),"Glasses");



                event.setImage(getBufferedImage(matrix));
                event.setMatrix(matrix);
                eventBus.post(event);
            }

            if (stopVideo){
                logger.debug("Stop Camera "+stopVideo);
                capture.release();
                break;
            }
        }
    }

    private static void drawRectangles(Mat matrix, MatOfRect rectangles, Scalar scalar, String text) {
        int font = Imgproc.FONT_HERSHEY_SIMPLEX;
        int scale = 1;
        int thickness = 3;
        rectangles.toList().forEach(rect -> {
            Imgproc.rectangle(matrix, rect.tl(), rect.br(), scalar);
            Imgproc.putText(matrix, text,  rect.tl(), font, scale, scalar, thickness);
            logger.debug("Drawing rectagle arround Profile "+rect.tl()+" " +rect.br());
        });
    }

    public static void stopVideo(){
        stopVideo = true;
        logger.debug("Stop Camera ImageUtil called ->"+stopVideo);
        capture.release();
    }

    public static void aufgabe1(Mat matrix, BufferedImage image) {
        logger.info("Creatint 5x5 Matrix with Value 10");
        Mat mat = Mat.ones(5,5, CvType.CV_8UC1);
        logger.info(mat);
        //mat.setTo(new Scalar(10),mat);
        logger.info(mat.dump());
        Core.setRNGSeed((int)Core.getTickCount());
        Core.randu(mat,0, 100);
        logger.info("Creatint 5x5 Matrix random numbers");
        logger.info(mat.dump());
        logger.info("MinVal "+Core.minMaxLoc(mat).minVal+" MaxVal "+Core.minMaxLoc(mat).maxVal);
        logger.info("Matrix from image "+matrix);
        logger.info("Changeing colorChannel ");
        List<Mat> splitResult = new ArrayList<>();
        Core.split(matrix, splitResult);
        splitResult.get(1).setTo(new Scalar(0));
        splitResult.get(2).setTo(new Scalar(0));
        Core.merge(splitResult, matrix);
        logger.debug("Sending Event ");
        CVAlarmEvent event = new CVAlarmEvent(CVAlarmEventEnum.IMAGELOADED,  getBufferedImage(matrix), matrix);
        eventBus.post(event);
    }


    public static void drawCircle(Mat matrix, Point center) {
        logger.debug("Drawing Circle at "+center);
        Imgproc.circle(matrix, center ,10, new Scalar(0,0,255));
        CVAlarmSystemModel.getInstance().setImage(getBufferedImage(matrix));
    }
}
