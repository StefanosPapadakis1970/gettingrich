package org.spi.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public  class  PropertiesManager {

    public  Properties cvAlarmProperties;

    public  Properties getCVAlarmProperties(){
        if (cvAlarmProperties == null){
            cvAlarmProperties = new Properties();
            try {
                //InputStream in = new FileInputStream("/local.properties");
                InputStream in = this.getClass().getClassLoader().getResourceAsStream("local.properties");
                cvAlarmProperties.load(in);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return cvAlarmProperties;
    }

}
