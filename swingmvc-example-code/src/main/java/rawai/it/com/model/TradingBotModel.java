package rawai.it.com.model;


import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;

public class TradingBotModel extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3914919403184623426L;
	private String maxOrdersAllowed = "";
	private String maximumOrdersProcent = "";
	private String numberOfContracts = "1";
	private String minOrderTicksDistance = "2";	
	private String alarmDistance = "2";
	private String cancelDistance = "1";
	private boolean cancelOrders = true;
	private String url ="";
	private String tradingUrl ="";
	private String apiKey = "";
	private String numberOfOrdersToPlace = "5";
	private String remainingOrders = "remaining Orders";
	
	private boolean createConterOrder = false;
	private boolean emergencyStop = false;
	
	private boolean newOrderAfterFilling;
	
	private String activeSellOrders = "0";
	private String activeBuyOrders = "0";	
	private String nummbeOfFilledOrders = "0";	
	private String uPnl = "0.0";
	
	private String stopBuyPrice = "0";
	private String stopSellPrice = "0";
	
	private boolean tradeNonStop;
	
	
	public String getRemainingOrders() {
		return remainingOrders;
	}

	public void setRemainingOrders(String remainingOrders) {
		String oldValue = this.remainingOrders;
        this.remainingOrders = remainingOrders;		 
        firePropertyChange("remainingOrders", oldValue, this.remainingOrders);
	}

	private boolean absolut = true;



	public TradingBotModel(String apiKeyValue, String platformUrl, String tradingUrl) {
		this.url = platformUrl;
		this.apiKey = apiKeyValue;
		this.tradingUrl = tradingUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		String oldValue = this.url;
        this.url = url;		 
        firePropertyChange("url", oldValue, this.url);
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		String oldValue = this.apiKey;
        this.apiKey = apiKey;		 
        firePropertyChange("apiKey", oldValue, this.apiKey);
	}

	public String getAlarmDistance() {
		return alarmDistance;
	}

	public void setAlarmDistance(String alarmDistance) {
		String oldValue = this.alarmDistance;
        this.alarmDistance = alarmDistance;		 
        firePropertyChange("alarmDistance", oldValue, this.alarmDistance);
	}

	public String getCancelDistance() {
		return cancelDistance;
	}

	public void setCancelDistance(String cancelDistance) {
		String oldValue = this.cancelDistance;
        this.cancelDistance = cancelDistance;		 
        firePropertyChange("cancelDistance", oldValue, this.cancelDistance);
	}

	public String getMinOrderTicksDistance() {
		return minOrderTicksDistance;
	}

	public void setMinOrderTicksDistance(String minOrderTicksDistance) {
		String oldValue = this.minOrderTicksDistance;
        this.minOrderTicksDistance = minOrderTicksDistance;		 
        firePropertyChange("minOrderTicksDistance", oldValue, this.minOrderTicksDistance);
	}

	public String getNumberOfContracts() {
		return numberOfContracts;
	}

	public void setNumberOfContracts(String numberOfContracts) {
		String oldValue = this.numberOfContracts;
        this.numberOfContracts = numberOfContracts;		 
        firePropertyChange("numberOfContracts", oldValue, this.numberOfContracts);
	}

	public String getMaximumOrdersProcent() {
		return maximumOrdersProcent;
	}

	public void setMaximumOrdersProcent(String maximumOrdersProcent) {
		String oldValue = this.maximumOrdersProcent;
        this.maximumOrdersProcent = maximumOrdersProcent;		 
        firePropertyChange("maximumOrdersProcent", oldValue, this.maximumOrdersProcent);
	}

	public String getMaxOrdersAllowed() {		
		return maxOrdersAllowed;
	}

	public void setMaxOrdersAllowed(String maxOrdersAllowed) {
		String oldValue = this.maxOrdersAllowed;
        this.maxOrdersAllowed = maxOrdersAllowed;		 
        firePropertyChange("maxOrdersAllowed", oldValue, this.maxOrdersAllowed);
	}


	public boolean isCancelOrders() {
		return cancelOrders;
	}

	public void setCancelOrders(boolean cancelOrders) {
		boolean oldValue = this.cancelOrders;
        this.cancelOrders = cancelOrders;		 
        firePropertyChange("cancelOrders", oldValue, this.cancelOrders);
	}


	public String getNumberOfOrdersToPlace() {
		return numberOfOrdersToPlace;
	}

	public void setNumberOfOrdersToPlace(String numberOfOrdersToPlace) {
		String oldValue = this.numberOfOrdersToPlace;
        this.numberOfOrdersToPlace = numberOfOrdersToPlace;		 
        firePropertyChange("numberOfOrdersToPlace", oldValue, this.numberOfOrdersToPlace);
	}

	public String getTradingUrl() {
		return tradingUrl;
	}

	public void setTradingUrl(String tradingurl) {		
		String oldValue = this.tradingUrl;
		this.tradingUrl = tradingurl;		 
        firePropertyChange("tradingUrl", oldValue, this.tradingUrl);
	}

	@Override
	public String toString() {
		return "TradingBotModel [maxOrdersAllowed=" + maxOrdersAllowed + ", maximumOrdersProcent="
				+ maximumOrdersProcent + ", numberOfContracts=" + numberOfContracts + ", minOrderTicksDistance="
				+ minOrderTicksDistance + ", alarmDistance=" + alarmDistance + ", cancelDistance=" + cancelDistance
				+ ", cancelOrders=" + cancelOrders + ", url=" + url + ", tradingUrl=" + tradingUrl + ", apiKey="
				+ apiKey + ", numberOfOrdersToPlace=" + numberOfOrdersToPlace + "]";
	}

	public boolean isAbsolut() {
		return absolut;
	}

	public void setAbsolut(boolean absolut) {
		boolean oldValue = this.absolut;
        this.absolut = absolut;		 
        firePropertyChange("absolut", oldValue, this.absolut);
	}

	public boolean isCreateConterOrder() {
		return createConterOrder;
	}

	public void setCreateConterOrder(boolean createConterOrder) {
		boolean oldValue = this.createConterOrder;
        this.createConterOrder = createConterOrder;		 
        firePropertyChange("createConterOrder", oldValue, this.createConterOrder);
	}

	public boolean isEmergencyStop() {
		return emergencyStop;
	}

	public void setEmergencyStop(boolean emergencyStop) {
		boolean oldValue = this.createConterOrder;
        this.emergencyStop = emergencyStop;		 
        firePropertyChange("emergencyStop", oldValue, this.emergencyStop);
	}

	public boolean isNewOrderAfterFilling() {
		return newOrderAfterFilling;
	}

	public void setNewOrderAfterFilling(boolean newOrderAfterFilling) {
		boolean oldValue = this.newOrderAfterFilling;
        this.newOrderAfterFilling = newOrderAfterFilling;		 
        firePropertyChange("newOrderAfterFilling", oldValue, this.newOrderAfterFilling);
	}

	public String getActiveSellOrders() {
		return activeSellOrders;
	}

	public void setActiveSellOrders(String activeSellOrders) {		
		String oldValue = this.activeSellOrders;
        this.activeSellOrders = activeSellOrders;		 
        firePropertyChange("activeSellOrders", oldValue, this.activeSellOrders);
	}

	public String getActiveBuyOrders() {
		return activeBuyOrders;
	}

	public void setActiveBuyOrders(String activeBuyOrders) {		
		String oldValue = this.activeBuyOrders;
		this.activeBuyOrders = activeBuyOrders;		 
        firePropertyChange("activeBuyOrders", oldValue, this.activeBuyOrders);
	}

	public String getNummbeOfFilledOrders() {
		return nummbeOfFilledOrders;
	}

	public void setNummbeOfFilledOrders(String nummbeOfFilledOrders) {		
		String oldValue = this.nummbeOfFilledOrders;
		this.nummbeOfFilledOrders = nummbeOfFilledOrders;		 
        firePropertyChange("nummbeOfFilledOrders", oldValue, this.nummbeOfFilledOrders);
	}

	public String getuPnl() {
		return uPnl;
	}

	public void setuPnl(String uPnl) {		
		String oldValue = this.uPnl;
		this.uPnl = uPnl;		 
        firePropertyChange("uPnl", oldValue, this.uPnl);
	}

	public String getStopBuyPrice() {
		return stopBuyPrice;
	}

	public void setStopBuyPrice(String stopBuyPrice) {
		String oldValue = this.stopBuyPrice;
		this.stopBuyPrice = stopBuyPrice;		 
        firePropertyChange("stopBuyPrice", oldValue, this.stopBuyPrice);
	}

	public String getStopSellPrice() {
		return stopSellPrice;
	}

	public void setStopSellPrice(String stopSellPrice) {
		String oldValue = this.stopSellPrice;
		this.stopSellPrice = stopSellPrice;		 
        firePropertyChange("stopSellPrice", oldValue, this.stopSellPrice);
	}

	public boolean isTradeNonStop() {
		return tradeNonStop;
	}

	public void setTradeNonStop(boolean tradeNonStop) {		
		boolean oldValue = this.tradeNonStop;
		this.tradeNonStop = tradeNonStop;		 
        firePropertyChange("tradeNonStop", oldValue, this.tradeNonStop);
	}	
	
}
