
package rawai.it.com.model.json.trading.closeallconditionalorders;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ts",
    "actionId",
    "oldActionId",
    "pxType",
    "condition",
    "pxValue",
    "clOrdId",
    "ordType",
    "side",
    "timeInForce",
    "px",
    "qty",
    "mayIncrPosition"
})
public class ConditionalOrder {

    @JsonProperty("ts")
    private Integer ts;
    @JsonProperty("actionId")
    private String actionId;
    @JsonProperty("oldActionId")
    private String oldActionId;
    @JsonProperty("pxType")
    private String pxType;
    @JsonProperty("condition")
    private String condition;
    @JsonProperty("pxValue")
    private Integer pxValue;
    @JsonProperty("clOrdId")
    private String clOrdId;
    @JsonProperty("ordType")
    private String ordType;
    @JsonProperty("side")
    private String side;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("mayIncrPosition")
    private Boolean mayIncrPosition;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ts")
    public Integer getTs() {
        return ts;
    }

    @JsonProperty("ts")
    public void setTs(Integer ts) {
        this.ts = ts;
    }

    @JsonProperty("actionId")
    public String getActionId() {
        return actionId;
    }

    @JsonProperty("actionId")
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    @JsonProperty("oldActionId")
    public String getOldActionId() {
        return oldActionId;
    }

    @JsonProperty("oldActionId")
    public void setOldActionId(String oldActionId) {
        this.oldActionId = oldActionId;
    }

    @JsonProperty("pxType")
    public String getPxType() {
        return pxType;
    }

    @JsonProperty("pxType")
    public void setPxType(String pxType) {
        this.pxType = pxType;
    }

    @JsonProperty("condition")
    public String getCondition() {
        return condition;
    }

    @JsonProperty("condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    @JsonProperty("pxValue")
    public Integer getPxValue() {
        return pxValue;
    }

    @JsonProperty("pxValue")
    public void setPxValue(Integer pxValue) {
        this.pxValue = pxValue;
    }

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("ordType")
    public String getOrdType() {
        return ordType;
    }

    @JsonProperty("ordType")
    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @JsonProperty("side")
    public String getSide() {
        return side;
    }

    @JsonProperty("side")
    public void setSide(String side) {
        this.side = side;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("mayIncrPosition")
    public Boolean getMayIncrPosition() {
        return mayIncrPosition;
    }

    @JsonProperty("mayIncrPosition")
    public void setMayIncrPosition(Boolean mayIncrPosition) {
        this.mayIncrPosition = mayIncrPosition;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
