
package rawai.it.com.model.json.trading.order.status;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "timestamp",
    "openTime",
    "orderStatus",
    "orderType",
    "timeInForce",
    "orderSide",
    "px",
    "paidPx",
    "qty",
    "traderBalance",
    "orderMargin",
    "positionMargin",
    "upnl",
    "pnl",
    "markPx",
    "leverage",
    "oldContractId",
    "clOrdId",
    "origClOrdId",
    "origQty"
})
public class Data {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("openTime")
    private Long openTime;
    @JsonProperty("orderStatus")
    private String orderStatus;
    @JsonProperty("orderType")
    private String orderType;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("orderSide")
    private String orderSide;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("paidPx")
    private Integer paidPx;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("traderBalance")
    private Double traderBalance;
    @JsonProperty("orderMargin")
    private Integer orderMargin;
    @JsonProperty("positionMargin")
    private Integer positionMargin;
    @JsonProperty("upnl")
    private Integer upnl;
    @JsonProperty("pnl")
    private Double pnl;
    @JsonProperty("markPx")
    private Double markPx;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("oldContractId")
    private Integer oldContractId;
    @JsonProperty("clOrdId")
    private String clOrdId;
    @JsonProperty("origClOrdId")
    private String origClOrdId;
    @JsonProperty("origQty")
    private Integer origQty;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("openTime")
    public Long getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Long openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("orderStatus")
    public String getOrderStatus() {
        return orderStatus;
    }

    @JsonProperty("orderStatus")
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @JsonProperty("orderType")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("orderType")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("orderSide")
    public String getOrderSide() {
        return orderSide;
    }

    @JsonProperty("orderSide")
    public void setOrderSide(String orderSide) {
        this.orderSide = orderSide;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("paidPx")
    public Integer getPaidPx() {
        return paidPx;
    }

    @JsonProperty("paidPx")
    public void setPaidPx(Integer paidPx) {
        this.paidPx = paidPx;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("traderBalance")
    public Double getTraderBalance() {
        return traderBalance;
    }

    @JsonProperty("traderBalance")
    public void setTraderBalance(Double traderBalance) {
        this.traderBalance = traderBalance;
    }

    @JsonProperty("orderMargin")
    public Integer getOrderMargin() {
        return orderMargin;
    }

    @JsonProperty("orderMargin")
    public void setOrderMargin(Integer orderMargin) {
        this.orderMargin = orderMargin;
    }

    @JsonProperty("positionMargin")
    public Integer getPositionMargin() {
        return positionMargin;
    }

    @JsonProperty("positionMargin")
    public void setPositionMargin(Integer positionMargin) {
        this.positionMargin = positionMargin;
    }

    @JsonProperty("upnl")
    public Integer getUpnl() {
        return upnl;
    }

    @JsonProperty("upnl")
    public void setUpnl(Integer upnl) {
        this.upnl = upnl;
    }

    @JsonProperty("pnl")
    public Double getPnl() {
        return pnl;
    }

    @JsonProperty("pnl")
    public void setPnl(Double pnl) {
        this.pnl = pnl;
    }

    @JsonProperty("markPx")
    public Double getMarkPx() {
        return markPx;
    }

    @JsonProperty("markPx")
    public void setMarkPx(Double markPx) {
        this.markPx = markPx;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("oldContractId")
    public Integer getOldContractId() {
        return oldContractId;
    }

    @JsonProperty("oldContractId")
    public void setOldContractId(Integer oldContractId) {
        this.oldContractId = oldContractId;
    }

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("origClOrdId")
    public String getOrigClOrdId() {
        return origClOrdId;
    }

    @JsonProperty("origClOrdId")
    public void setOrigClOrdId(String origClOrdId) {
        this.origClOrdId = origClOrdId;
    }

    @JsonProperty("origQty")
    public Integer getOrigQty() {
        return origQty;
    }

    @JsonProperty("origQty")
    public void setOrigQty(Integer origQty) {
        this.origQty = origQty;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "Data [symbol=" + symbol + ", timestamp=" + timestamp + ", openTime=" + openTime + ", orderStatus="
				+ orderStatus + ", orderType=" + orderType + ", timeInForce=" + timeInForce + ", orderSide=" + orderSide
				+ ", px=" + px + ", paidPx=" + paidPx + ", qty=" + qty + ", traderBalance=" + traderBalance
				+ ", orderMargin=" + orderMargin + ", positionMargin=" + positionMargin + ", upnl=" + upnl + ", pnl="
				+ pnl + ", markPx=" + markPx + ", leverage=" + leverage + ", oldContractId=" + oldContractId
				+ ", clOrdId=" + clOrdId + ", origClOrdId=" + origClOrdId + ", origQty=" + origQty
				+ ", additionalProperties=" + additionalProperties + "]";
	}

}
