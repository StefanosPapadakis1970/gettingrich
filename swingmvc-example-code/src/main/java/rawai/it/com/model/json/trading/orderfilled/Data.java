
package rawai.it.com.model.json.trading.orderfilled;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "timestamp",
    "clOrdId",
    "orderStatus",
    "newClOrdId",
    "orderType",
    "orderSide",
    "leverage",
    "timeInForce",
    "px",
    "paidPx",
    "qty",
    "origClOrdId",
    "openTime",
    "origQty",
    "droppedQty",
    "traderBalance",
    "orderMargin",
    "positionMargin",
    "upnl",
    "pnl",
    "positionContracts",
    "positionVolume",
    "positionLiquidationVolume",
    "positionBankruptcyVolume",
    "positionType",
    "markPx",
    "contracts",
    "marketTrades"
})
public class Data {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("clOrdId")
    private String clOrdId ="Display";
    @JsonProperty("orderStatus")
    private String orderStatus;
    @JsonProperty("newClOrdId")
    private String newClOrdId ="Display";
    @JsonProperty("orderType")
    private String orderType;
    @JsonProperty("orderSide")
    private String orderSide;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("paidPx")
    private Integer paidPx;
    @JsonProperty("qty")
    private Integer qty  = new Integer(0);
    @JsonProperty("origClOrdId")
    private String origClOrdId="Display";
    @JsonProperty("openTime")
    private Long openTime;
    @JsonProperty("origQty")
    private Integer origQty = new Integer(0);
    @JsonProperty("droppedQty")
    private Integer droppedQty;
    @JsonProperty("traderBalance")
    private Double traderBalance;
    @JsonProperty("orderMargin")
    private Double orderMargin;
    @JsonProperty("positionMargin")
    private Double positionMargin;
    @JsonProperty("upnl")
    private Integer upnl;
    @JsonProperty("pnl")
    private Double pnl;
    @JsonProperty("positionContracts")
    private Integer positionContracts;
    @JsonProperty("positionVolume")
    private Integer positionVolume;
    @JsonProperty("positionLiquidationVolume")
    private Integer positionLiquidationVolume;
    @JsonProperty("positionBankruptcyVolume")
    private Integer positionBankruptcyVolume;
    @JsonProperty("positionType")
    private String positionType;
    @JsonProperty("markPx")
    private Double markPx;
    @JsonProperty("contracts")
    private List<Contract> contracts = null;
    @JsonProperty("marketTrades")
    private List<MarketTrade> marketTrades = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("orderStatus")
    public String getOrderStatus() {
        return orderStatus;
    }

    @JsonProperty("orderStatus")
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @JsonProperty("newClOrdId")
    public String getNewClOrdId() {
        return newClOrdId;
    }

    @JsonProperty("newClOrdId")
    public void setNewClOrdId(String newClOrdId) {
        this.newClOrdId = newClOrdId;
    }

    @JsonProperty("orderType")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("orderType")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("orderSide")
    public String getOrderSide() {
        return orderSide;
    }

    @JsonProperty("orderSide")
    public void setOrderSide(String orderSide) {
        this.orderSide = orderSide;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("paidPx")
    public Integer getPaidPx() {
        return paidPx;
    }

    @JsonProperty("paidPx")
    public void setPaidPx(Integer paidPx) {
        this.paidPx = paidPx;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("origClOrdId")
    public String getOrigClOrdId() {
        return origClOrdId;
    }

    @JsonProperty("origClOrdId")
    public void setOrigClOrdId(String origClOrdId) {
        this.origClOrdId = origClOrdId;
    }

    @JsonProperty("openTime")
    public Long getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Long openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("origQty")
    public Integer getOrigQty() {
        return origQty;
    }

    @JsonProperty("origQty")
    public void setOrigQty(Integer origQty) {
        this.origQty = origQty;
    }

    @JsonProperty("droppedQty")
    public Integer getDroppedQty() {
        return droppedQty;
    }

    @JsonProperty("droppedQty")
    public void setDroppedQty(Integer droppedQty) {
        this.droppedQty = droppedQty;
    }

    @JsonProperty("traderBalance")
    public Double getTraderBalance() {
        return traderBalance;
    }

    @JsonProperty("traderBalance")
    public void setTraderBalance(Double traderBalance) {
        this.traderBalance = traderBalance;
    }

    @JsonProperty("orderMargin")
    public Double getOrderMargin() {
        return orderMargin;
    }

    @JsonProperty("orderMargin")
    public void setOrderMargin(Double orderMargin) {
        this.orderMargin = orderMargin;
    }

    @JsonProperty("positionMargin")
    public Double getPositionMargin() {
        return positionMargin;
    }

    @JsonProperty("positionMargin")
    public void setPositionMargin(Double positionMargin) {
        this.positionMargin = positionMargin;
    }

    @JsonProperty("upnl")
    public Integer getUpnl() {
        return upnl;
    }

    @JsonProperty("upnl")
    public void setUpnl(Integer upnl) {
        this.upnl = upnl;
    }

    @JsonProperty("pnl")
    public Double getPnl() {
        return pnl;
    }

    @JsonProperty("pnl")
    public void setPnl(Double pnl) {
        this.pnl = pnl;
    }

    @JsonProperty("positionContracts")
    public Integer getPositionContracts() {
        return positionContracts;
    }

    @JsonProperty("positionContracts")
    public void setPositionContracts(Integer positionContracts) {
        this.positionContracts = positionContracts;
    }

    @JsonProperty("positionVolume")
    public Integer getPositionVolume() {
        return positionVolume;
    }

    @JsonProperty("positionVolume")
    public void setPositionVolume(Integer positionVolume) {
        this.positionVolume = positionVolume;
    }

    @JsonProperty("positionLiquidationVolume")
    public Integer getPositionLiquidationVolume() {
        return positionLiquidationVolume;
    }

    @JsonProperty("positionLiquidationVolume")
    public void setPositionLiquidationVolume(Integer positionLiquidationVolume) {
        this.positionLiquidationVolume = positionLiquidationVolume;
    }

    @JsonProperty("positionBankruptcyVolume")
    public Integer getPositionBankruptcyVolume() {
        return positionBankruptcyVolume;
    }

    @JsonProperty("positionBankruptcyVolume")
    public void setPositionBankruptcyVolume(Integer positionBankruptcyVolume) {
        this.positionBankruptcyVolume = positionBankruptcyVolume;
    }

    @JsonProperty("positionType")
    public String getPositionType() {
        return positionType;
    }

    @JsonProperty("positionType")
    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    @JsonProperty("markPx")
    public Double getMarkPx() {
        return markPx;
    }

    @JsonProperty("markPx")
    public void setMarkPx(Double markPx) {
        this.markPx = markPx;
    }

    @JsonProperty("contracts")
    public List<Contract> getContracts() {
        return contracts;
    }

    @JsonProperty("contracts")
    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    @JsonProperty("marketTrades")
    public List<MarketTrade> getMarketTrades() {
        return marketTrades;
    }

    @JsonProperty("marketTrades")
    public void setMarketTrades(List<MarketTrade> marketTrades) {
        this.marketTrades = marketTrades;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "Data [symbol=" + symbol + ", timestamp=" + timestamp + ", clOrdId=" + clOrdId + ", orderStatus="
				+ orderStatus + ", newClOrdId=" + newClOrdId + ", orderType=" + orderType + ", orderSide=" + orderSide
				+ ", leverage=" + leverage + ", timeInForce=" + timeInForce + ", px=" + px + ", paidPx=" + paidPx
				+ ", qty=" + qty + ", origClOrdId=" + origClOrdId + ", openTime=" + openTime + ", origQty=" + origQty
				+ ", droppedQty=" + droppedQty + ", traderBalance=" + traderBalance + ", orderMargin=" + orderMargin
				+ ", positionMargin=" + positionMargin + ", upnl=" + upnl + ", pnl=" + pnl + ", positionContracts="
				+ positionContracts + ", positionVolume=" + positionVolume + ", positionLiquidationVolume="
				+ positionLiquidationVolume + ", positionBankruptcyVolume=" + positionBankruptcyVolume
				+ ", positionType=" + positionType + ", markPx=" + markPx + ", contracts=" + contracts
				+ ", marketTrades=" + marketTrades + ", additionalProperties=" + additionalProperties + "]";
	}

}
