
package rawai.it.com.model.json.trading.closeallconditionalorders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "status",
    "conditionalOrders"
})
public class Data {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("status")
    private String status;
    @JsonProperty("conditionalOrders")
    private List<ConditionalOrder> conditionalOrders = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("conditionalOrders")
    public List<ConditionalOrder> getConditionalOrders() {
        return conditionalOrders;
    }

    @JsonProperty("conditionalOrders")
    public void setConditionalOrders(List<ConditionalOrder> conditionalOrders) {
        this.conditionalOrders = conditionalOrders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
