package rawai.it.com.model.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"weight",
"ts",
"px",
"vol"
})
public class Binance {

@JsonProperty("weight")
private Integer weight;
@JsonProperty("ts")
private Long ts;
@JsonProperty("px")
private Integer px;
@JsonProperty("vol")
private Integer vol;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("weight")
public Integer getWeight() {
return weight;
}

@JsonProperty("weight")
public void setWeight(Integer weight) {
this.weight = weight;
}

@JsonProperty("ts")
public Long getTs() {
return ts;
}

@JsonProperty("ts")
public void setTs(Long ts) {
this.ts = ts;
}

@JsonProperty("px")
public Integer getPx() {
return px;
}

@JsonProperty("px")
public void setPx(Integer px) {
this.px = px;
}

@JsonProperty("vol")
public Integer getVol() {
return vol;
}

@JsonProperty("vol")
public void setVol(Integer vol) {
this.vol = vol;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}

