
package rawai.it.com.model.json.trading.conditionalorder.status;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ts",
    "actionId",
    "pxType",
    "condition",
    "pxValue",
    "clOrdId",
    "ordType",
    "side",
    "timeInForce",
    "px",
    "qty",
    "mayIncrPosition"
})
public class ConditionalOrder {

    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("actionId")
    private String actionId;
    @JsonProperty("pxType")
    private String pxType;
    @JsonProperty("condition")
    private String condition;
    @JsonProperty("pxValue")
    private Integer pxValue;
    @JsonProperty("clOrdId")
    private String clOrdId;
    @JsonProperty("ordType")
    private String ordType;
    @JsonProperty("side")
    private String side;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("mayIncrPosition")
    private Boolean mayIncrPosition;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ts")
    public Long getTs() {
        return ts;
    }

    @JsonProperty("ts")
    public void setTs(Long ts) {
        this.ts = ts;
    }

    @JsonProperty("actionId")
    public String getActionId() {
        return actionId;
    }

    @JsonProperty("actionId")
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    @JsonProperty("pxType")
    public String getPxType() {
        return pxType;
    }

    @JsonProperty("pxType")
    public void setPxType(String pxType) {
        this.pxType = pxType;
    }

    @JsonProperty("condition")
    public String getCondition() {
        return condition;
    }

    @JsonProperty("condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    @JsonProperty("pxValue")
    public Integer getPxValue() {
        return pxValue;
    }

    @JsonProperty("pxValue")
    public void setPxValue(Integer pxValue) {
        this.pxValue = pxValue;
    }

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("ordType")
    public String getOrdType() {
        return ordType;
    }

    @JsonProperty("ordType")
    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @JsonProperty("side")
    public String getSide() {
        return side;
    }

    @JsonProperty("side")
    public void setSide(String side) {
        this.side = side;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("mayIncrPosition")
    public Boolean getMayIncrPosition() {
        return mayIncrPosition;
    }

    @JsonProperty("mayIncrPosition")
    public void setMayIncrPosition(Boolean mayIncrPosition) {
        this.mayIncrPosition = mayIncrPosition;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionId == null) ? 0 : actionId.hashCode());
		result = prime * result + ((additionalProperties == null) ? 0 : additionalProperties.hashCode());
		result = prime * result + ((clOrdId == null) ? 0 : clOrdId.hashCode());
		result = prime * result + ((condition == null) ? 0 : condition.hashCode());
		result = prime * result + ((mayIncrPosition == null) ? 0 : mayIncrPosition.hashCode());
		result = prime * result + ((ordType == null) ? 0 : ordType.hashCode());
		result = prime * result + ((px == null) ? 0 : px.hashCode());
		result = prime * result + ((pxType == null) ? 0 : pxType.hashCode());
		result = prime * result + ((pxValue == null) ? 0 : pxValue.hashCode());
		result = prime * result + ((qty == null) ? 0 : qty.hashCode());
		result = prime * result + ((side == null) ? 0 : side.hashCode());
		result = prime * result + ((timeInForce == null) ? 0 : timeInForce.hashCode());
		result = prime * result + ((ts == null) ? 0 : ts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConditionalOrder other = (ConditionalOrder) obj;
		if (actionId == null) {
			if (other.actionId != null)
				return false;
		} else if (!actionId.equals(other.actionId))
			return false;
		if (additionalProperties == null) {
			if (other.additionalProperties != null)
				return false;
		} else if (!additionalProperties.equals(other.additionalProperties))
			return false;
		if (clOrdId == null) {
			if (other.clOrdId != null)
				return false;
		} else if (!clOrdId.equals(other.clOrdId))
			return false;
		if (condition == null) {
			if (other.condition != null)
				return false;
		} else if (!condition.equals(other.condition))
			return false;
		if (mayIncrPosition == null) {
			if (other.mayIncrPosition != null)
				return false;
		} else if (!mayIncrPosition.equals(other.mayIncrPosition))
			return false;
		if (ordType == null) {
			if (other.ordType != null)
				return false;
		} else if (!ordType.equals(other.ordType))
			return false;
		if (px == null) {
			if (other.px != null)
				return false;
		} else if (!px.equals(other.px))
			return false;
		if (pxType == null) {
			if (other.pxType != null)
				return false;
		} else if (!pxType.equals(other.pxType))
			return false;
		if (pxValue == null) {
			if (other.pxValue != null)
				return false;
		} else if (!pxValue.equals(other.pxValue))
			return false;
		if (qty == null) {
			if (other.qty != null)
				return false;
		} else if (!qty.equals(other.qty))
			return false;
		if (side == null) {
			if (other.side != null)
				return false;
		} else if (!side.equals(other.side))
			return false;
		if (timeInForce == null) {
			if (other.timeInForce != null)
				return false;
		} else if (!timeInForce.equals(other.timeInForce))
			return false;
		if (ts == null) {
			if (other.ts != null)
				return false;
		} else if (!ts.equals(other.ts))
			return false;
		return true;
	}

}
