package rawai.it.com.model;

import com.jgoodies.binding.beans.Model;

public class OrderBookModel extends Model{

	private static final long serialVersionUID = 13078959898363073L;
	private String nummbeOfFilledOrders = "FillerOrders0";

	public String getNummbeOfFilledOrders() {
		return nummbeOfFilledOrders;
	}

	public void setNummbeOfFilledOrders(String nummbeOfFilledOrders) {		
		String oldValue = this.nummbeOfFilledOrders;
		this.nummbeOfFilledOrders = nummbeOfFilledOrders;		 
        firePropertyChange("nummbeOfFilledOrders", oldValue, this.nummbeOfFilledOrders);
	}
}
