
package rawai.it.com.model.json.trading.closeposition.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "ordType",
    "px"
})
public class ClosePositionParams {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("ordType")
    private String ordType;
    @JsonProperty("px")
    private Integer px;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("ordType")
    public String getOrdType() {
        return ordType;
    }

    @JsonProperty("ordType")
    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "ClosePositionParams [symbol=" + symbol + ", ordType=" + ordType + ", px=" + px
				+ ", additionalProperties=" + additionalProperties + "]";
	}
    
}
