
package rawai.it.com.model.json.trading.conditionalorder;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "actionId",
    "pxType",
    "condition",
    "pxValue",
    "symbol",
    "clOrdId",
    "ordType",
    "timeInForce",
    "side",
    "px",
    "qty",
    "mayIncrPosition"
})
public class Params {

    @JsonProperty("actionId")
    private String actionId;
    @JsonProperty("pxType")
    private String pxType;
    @JsonProperty("condition")
    private String condition;
    @JsonProperty("pxValue")
    private Integer pxValue;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("clOrdId")
    private String clOrdId;
    @JsonProperty("ordType")
    private String ordType;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("side")
    private String side;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("mayIncrPosition")
    private Boolean mayIncrPosition;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("actionId")
    public String getActionId() {
        return actionId;
    }

    @JsonProperty("actionId")
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    @JsonProperty("pxType")
    public String getPxType() {
        return pxType;
    }

    @JsonProperty("pxType")
    public void setPxType(String pxType) {
        this.pxType = pxType;
    }

    @JsonProperty("condition")
    public String getCondition() {
        return condition;
    }

    @JsonProperty("condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    @JsonProperty("pxValue")
    public Integer getPxValue() {
        return pxValue;
    }

    @JsonProperty("pxValue")
    public void setPxValue(Integer pxValue) {
        this.pxValue = pxValue;
    }

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("ordType")
    public String getOrdType() {
        return ordType;
    }

    @JsonProperty("ordType")
    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("side")
    public String getSide() {
        return side;
    }

    @JsonProperty("side")
    public void setSide(String side) {
        this.side = side;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("mayIncrPosition")
    public Boolean getMayIncrPosition() {
        return mayIncrPosition;
    }

    @JsonProperty("mayIncrPosition")
    public void setMayIncrPosition(Boolean mayIncrPosition) {
        this.mayIncrPosition = mayIncrPosition;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "Params [actionId=" + actionId + ", pxType=" + pxType + ", condition=" + condition + ", pxValue="
				+ pxValue + ", symbol=" + symbol + ", clOrdId=" + clOrdId + ", ordType=" + ordType + ", timeInForce="
				+ timeInForce + ", side=" + side + ", px=" + px + ", qty=" + qty + ", mayIncrPosition="
				+ mayIncrPosition + ", additionalProperties=" + additionalProperties + "]";
	}

}
