
package rawai.it.com.model.json.trading.closecontract;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "contractId",
    "ordType",
    "px",
    "qty"
})
public class CloseContractParams {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("contractId")
    private Integer contractId;
    @JsonProperty("ordType")
    private String ordType;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("qty")
    private Integer qty;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("contractId")
    public Integer getContractId() {
        return contractId;
    }

    @JsonProperty("contractId")
    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @JsonProperty("ordType")
    public String getOrdType() {
        return ordType;
    }

    @JsonProperty("ordType")
    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
