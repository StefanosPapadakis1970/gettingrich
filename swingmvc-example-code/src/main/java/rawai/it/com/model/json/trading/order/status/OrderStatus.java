
package rawai.it.com.model.json.trading.order.status;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import rawai.it.com.controller.Constants;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ch",
    "data"
})
public class OrderStatus implements Comparable<OrderStatus>,Constants {

    @JsonProperty("ch")
    private String ch;
    @JsonProperty("data")
    private Data data;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ch")
    public String getCh() {
        return ch;
    }

    @JsonProperty("ch")
    public void setCh(String ch) {
        this.ch = ch;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public int compareTo(OrderStatus o) {		
		return o.getData().getPx().compareTo(getData().getPx());
	}

	
    public boolean isConterMarketOrder() {
    	return getData().getClOrdId().contains(CNTM) ||  getData().getOrigClOrdId().contains(CNTM);
    }
    
    
    public boolean isConterLimitOrder() {
    	return getData().getClOrdId().contains(CNTO) ||  getData().getOrigClOrdId().contains(CNTO);
    }

    public boolean isConterOrder() {
    	return isConterMarketOrder() || isConterLimitOrder() || isExitLimitOrder();
    }

	public boolean isExitLimitOrder() {
		return getData().getClOrdId().contains(CNTEXO) || getData().getOrigClOrdId().contains(CNTEXO);		
	}

	@Override
	public String toString() {
		return "OrderStatus [ch=" + ch + ", data=" + data + ", additionalProperties=" + additionalProperties + "]";
	}

	public OrderFilled toOrderFilled() {
		OrderFilled resultOrder = new OrderFilled();
		rawai.it.com.model.json.trading.orderfilled.Data data = new rawai.it.com.model.json.trading.orderfilled.Data();
		data.setQty(this.data.getQty());
		data.setOrigQty(this.data.getOrigQty());
		data.setNewClOrdId("toOrder");
		data.setClOrdId(this.data.getClOrdId());
		data.setOrigClOrdId(this.data.getOrigClOrdId());
		resultOrder.setData(data);
		return resultOrder;
	}	

}
