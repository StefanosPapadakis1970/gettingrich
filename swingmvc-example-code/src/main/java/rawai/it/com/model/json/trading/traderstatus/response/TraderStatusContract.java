
package rawai.it.com.model.json.trading.traderstatus.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "timestamp",
    "traderId",
    "positionType",
    "qty",
    "entryPx",
    "paidPx",
    "liquidationPx",
    "bankruptcyPx",
    "exitPx",
    "leverage",
    "contractId",
    "openTime",
    "entryQty",
    "exitQty",
    "exitVolume",
    "fundingPaidPx",
    "fundingQty",
    "fundingVolume",
    "fundingCount",
    "origContractId"
})
public class TraderStatusContract {

    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("traderId")
    private Integer traderId;
    @JsonProperty("positionType")
    private String positionType;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("entryPx")
    private Integer entryPx;
    @JsonProperty("paidPx")
    private Integer paidPx;
    @JsonProperty("liquidationPx")
    private Integer liquidationPx;
    @JsonProperty("bankruptcyPx")
    private Integer bankruptcyPx;
    @JsonProperty("exitPx")
    private Integer exitPx;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("contractId")
    private Integer contractId;
    @JsonProperty("openTime")
    private Long openTime;
    @JsonProperty("entryQty")
    private Integer entryQty;
    @JsonProperty("exitQty")
    private Integer exitQty;
    @JsonProperty("exitVolume")
    private Integer exitVolume;
    @JsonProperty("fundingPaidPx")
    private Integer fundingPaidPx;
    @JsonProperty("fundingQty")
    private Integer fundingQty;
    @JsonProperty("fundingVolume")
    private Integer fundingVolume;
    @JsonProperty("fundingCount")
    private Integer fundingCount;
    @JsonProperty("origContractId")
    private Integer origContractId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("traderId")
    public Integer getTraderId() {
        return traderId;
    }

    @JsonProperty("traderId")
    public void setTraderId(Integer traderId) {
        this.traderId = traderId;
    }

    @JsonProperty("positionType")
    public String getPositionType() {
        return positionType;
    }

    @JsonProperty("positionType")
    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("entryPx")
    public Integer getEntryPx() {
        return entryPx;
    }

    @JsonProperty("entryPx")
    public void setEntryPx(Integer entryPx) {
        this.entryPx = entryPx;
    }

    @JsonProperty("paidPx")
    public Integer getPaidPx() {
        return paidPx;
    }

    @JsonProperty("paidPx")
    public void setPaidPx(Integer paidPx) {
        this.paidPx = paidPx;
    }

    @JsonProperty("liquidationPx")
    public Integer getLiquidationPx() {
        return liquidationPx;
    }

    @JsonProperty("liquidationPx")
    public void setLiquidationPx(Integer liquidationPx) {
        this.liquidationPx = liquidationPx;
    }

    @JsonProperty("bankruptcyPx")
    public Integer getBankruptcyPx() {
        return bankruptcyPx;
    }

    @JsonProperty("bankruptcyPx")
    public void setBankruptcyPx(Integer bankruptcyPx) {
        this.bankruptcyPx = bankruptcyPx;
    }

    @JsonProperty("exitPx")
    public Integer getExitPx() {
        return exitPx;
    }

    @JsonProperty("exitPx")
    public void setExitPx(Integer exitPx) {
        this.exitPx = exitPx;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("contractId")
    public Integer getContractId() {
        return contractId;
    }

    @JsonProperty("contractId")
    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @JsonProperty("openTime")
    public Long getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Long openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("entryQty")
    public Integer getEntryQty() {
        return entryQty;
    }

    @JsonProperty("entryQty")
    public void setEntryQty(Integer entryQty) {
        this.entryQty = entryQty;
    }

    @JsonProperty("exitQty")
    public Integer getExitQty() {
        return exitQty;
    }

    @JsonProperty("exitQty")
    public void setExitQty(Integer exitQty) {
        this.exitQty = exitQty;
    }

    @JsonProperty("exitVolume")
    public Integer getExitVolume() {
        return exitVolume;
    }

    @JsonProperty("exitVolume")
    public void setExitVolume(Integer exitVolume) {
        this.exitVolume = exitVolume;
    }

    @JsonProperty("fundingPaidPx")
    public Integer getFundingPaidPx() {
        return fundingPaidPx;
    }

    @JsonProperty("fundingPaidPx")
    public void setFundingPaidPx(Integer fundingPaidPx) {
        this.fundingPaidPx = fundingPaidPx;
    }

    @JsonProperty("fundingQty")
    public Integer getFundingQty() {
        return fundingQty;
    }

    @JsonProperty("fundingQty")
    public void setFundingQty(Integer fundingQty) {
        this.fundingQty = fundingQty;
    }

    @JsonProperty("fundingVolume")
    public Integer getFundingVolume() {
        return fundingVolume;
    }

    @JsonProperty("fundingVolume")
    public void setFundingVolume(Integer fundingVolume) {
        this.fundingVolume = fundingVolume;
    }

    @JsonProperty("fundingCount")
    public Integer getFundingCount() {
        return fundingCount;
    }

    @JsonProperty("fundingCount")
    public void setFundingCount(Integer fundingCount) {
        this.fundingCount = fundingCount;
    }

    @JsonProperty("origContractId")
    public Integer getOrigContractId() {
        return origContractId;
    }

    @JsonProperty("origContractId")
    public void setOrigContractId(Integer origContractId) {
        this.origContractId = origContractId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
