package rawai.it.com.model;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;

public class TradeModelTableAdapter extends AbstractTableAdapter<TradeModel> {

	private static final long serialVersionUID = -5219846438943150249L;
	
	private boolean longModel = true;

	public TradeModelTableAdapter(ListModel<TradeModel> listModel, String[] columnNames) {
        super(listModel, columnNames);
    }
    
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		TradeModel tradeModel = (TradeModel) getRow(rowIndex);
        if (longModel) {
    		if (columnIndex == 0) {
                return tradeModel.getPrice();
            } else  {
                return tradeModel.getNumberOfcontracts();
            }        	
        } else {
    		if (columnIndex == 0) {
                return tradeModel.getNumberOfcontracts();
            } else  {
                return tradeModel.getPrice();
            }
        }
	}

	public boolean isLongModel() {
		return longModel;
	}

	public void setLongModel(boolean longModel) {
		this.longModel = longModel;
	}
}
