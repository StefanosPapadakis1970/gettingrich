
package rawai.it.com.model.json.trading.conditionalorder.status.cancelled;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "timestamp",
    "orderStatus",
    "prevClOrdId",
    "orders",
    "traderBalance",
    "orderMargin",
    "positionMargin",
    "upnl",
    "pnl",
    "markPx"
})
public class Data {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("orderStatus")
    private String orderStatus;
    @JsonProperty("prevClOrdId")
    private String prevClOrdId;
    @JsonProperty("orders")
    private List<Order> orders = null;
    @JsonProperty("traderBalance")
    private Double traderBalance;
    @JsonProperty("orderMargin")
    private Integer orderMargin;
    @JsonProperty("positionMargin")
    private Integer positionMargin;
    @JsonProperty("upnl")
    private Integer upnl;
    @JsonProperty("pnl")
    private Integer pnl;
    @JsonProperty("markPx")
    private Double markPx;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("orderStatus")
    public String getOrderStatus() {
        return orderStatus;
    }

    @JsonProperty("orderStatus")
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @JsonProperty("prevClOrdId")
    public String getPrevClOrdId() {
        return prevClOrdId;
    }

    @JsonProperty("prevClOrdId")
    public void setPrevClOrdId(String prevClOrdId) {
        this.prevClOrdId = prevClOrdId;
    }

    @JsonProperty("orders")
    public List<Order> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @JsonProperty("traderBalance")
    public Double getTraderBalance() {
        return traderBalance;
    }

    @JsonProperty("traderBalance")
    public void setTraderBalance(Double traderBalance) {
        this.traderBalance = traderBalance;
    }

    @JsonProperty("orderMargin")
    public Integer getOrderMargin() {
        return orderMargin;
    }

    @JsonProperty("orderMargin")
    public void setOrderMargin(Integer orderMargin) {
        this.orderMargin = orderMargin;
    }

    @JsonProperty("positionMargin")
    public Integer getPositionMargin() {
        return positionMargin;
    }

    @JsonProperty("positionMargin")
    public void setPositionMargin(Integer positionMargin) {
        this.positionMargin = positionMargin;
    }

    @JsonProperty("upnl")
    public Integer getUpnl() {
        return upnl;
    }

    @JsonProperty("upnl")
    public void setUpnl(Integer upnl) {
        this.upnl = upnl;
    }

    @JsonProperty("pnl")
    public Integer getPnl() {
        return pnl;
    }

    @JsonProperty("pnl")
    public void setPnl(Integer pnl) {
        this.pnl = pnl;
    }

    @JsonProperty("markPx")
    public Double getMarkPx() {
        return markPx;
    }

    @JsonProperty("markPx")
    public void setMarkPx(Double markPx) {
        this.markPx = markPx;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
