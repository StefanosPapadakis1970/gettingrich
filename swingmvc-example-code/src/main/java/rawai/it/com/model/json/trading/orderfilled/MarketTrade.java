
package rawai.it.com.model.json.trading.orderfilled;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "side",
    "px",
    "paidPx",
    "qty",
    "leverage",
    "isMaker"
})
public class MarketTrade {

    @JsonProperty("side")
    private String side;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("paidPx")
    private Integer paidPx;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("isMaker")
    private Integer isMaker;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("side")
    public String getSide() {
        return side;
    }

    @JsonProperty("side")
    public void setSide(String side) {
        this.side = side;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("paidPx")
    public Integer getPaidPx() {
        return paidPx;
    }

    @JsonProperty("paidPx")
    public void setPaidPx(Integer paidPx) {
        this.paidPx = paidPx;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("isMaker")
    public Integer getIsMaker() {
        return isMaker;
    }

    @JsonProperty("isMaker")
    public void setIsMaker(Integer isMaker) {
        this.isMaker = isMaker;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "MarketTrade [side=" + side + ", px=" + px + ", paidPx=" + paidPx + ", qty=" + qty + ", leverage="
				+ leverage + ", isMaker=" + isMaker + ", additionalProperties=" + additionalProperties + "]";
	}

}
