
package rawai.it.com.model.json.orderbook;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "ts",
    "bids",
    "asks"
})
public class Data {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("bids")
    private List<List<Integer>> bids = null;
    @JsonProperty("asks")
    private List<List<Integer>> asks = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("ts")
    public Long getTs() {
        return ts;
    }

    @JsonProperty("ts")
    public void setTs(Long ts) {
        this.ts = ts;
    }

    @JsonProperty("bids")
    public List<List<Integer>> getBids() {
        return bids;
    }

    @JsonProperty("bids")
    public void setBids(List<List<Integer>> bids) {
        this.bids = bids;
    }

    @JsonProperty("asks")
    public List<List<Integer>> getAsks() {
        return asks;
    }

    @JsonProperty("asks")
    public void setAsks(List<List<Integer>> asks) {
        this.asks = asks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
