package rawai.it.com.model;

public class TradeModel {
	
	private String id;
	private int price;
	private int numberOfcontracts;

	
	public TradeModel(int price, int numberOfcontracts) {
		super();
		this.price = price;
		this.numberOfcontracts = numberOfcontracts;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public int getNumberOfcontracts() {
		return numberOfcontracts;
	}


	public void setNumberOfcontracts(int numberOfcontracts) {
		this.numberOfcontracts = numberOfcontracts;
	}
		
	
}
