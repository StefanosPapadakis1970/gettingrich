
package rawai.it.com.model.json.trading.conditionalorder.status.cancelled;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "clOrdId",
    "timestamp",
    "traderId",
    "orderType",
    "orderSide",
    "leverage",
    "timeInForce",
    "px",
    "qty",
    "paidPx",
    "oldClOrdId",
    "origClOrdId",
    "openTime",
    "origQty"
})
public class Order {

    @JsonProperty("clOrdId")
    private String clOrdId;
    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("traderId")
    private Integer traderId;
    @JsonProperty("orderType")
    private String orderType;
    @JsonProperty("orderSide")
    private String orderSide;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("timeInForce")
    private String timeInForce;
    @JsonProperty("px")
    private Integer px;
    @JsonProperty("qty")
    private Integer qty;
    @JsonProperty("paidPx")
    private Double paidPx;
    @JsonProperty("oldClOrdId")
    private String oldClOrdId;
    @JsonProperty("origClOrdId")
    private String origClOrdId;
    @JsonProperty("openTime")
    private Long openTime;
    @JsonProperty("origQty")
    private Integer origQty;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("clOrdId")
    public String getClOrdId() {
        return clOrdId;
    }

    @JsonProperty("clOrdId")
    public void setClOrdId(String clOrdId) {
        this.clOrdId = clOrdId;
    }

    @JsonProperty("timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("traderId")
    public Integer getTraderId() {
        return traderId;
    }

    @JsonProperty("traderId")
    public void setTraderId(Integer traderId) {
        this.traderId = traderId;
    }

    @JsonProperty("orderType")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("orderType")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("orderSide")
    public String getOrderSide() {
        return orderSide;
    }

    @JsonProperty("orderSide")
    public void setOrderSide(String orderSide) {
        this.orderSide = orderSide;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("timeInForce")
    public String getTimeInForce() {
        return timeInForce;
    }

    @JsonProperty("timeInForce")
    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @JsonProperty("px")
    public Integer getPx() {
        return px;
    }

    @JsonProperty("px")
    public void setPx(Integer px) {
        this.px = px;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonProperty("paidPx")
    public Double getPaidPx() {
        return paidPx;
    }

    @JsonProperty("paidPx")
    public void setPaidPx(Double paidPx) {
        this.paidPx = paidPx;
    }

    @JsonProperty("oldClOrdId")
    public String getOldClOrdId() {
        return oldClOrdId;
    }

    @JsonProperty("oldClOrdId")
    public void setOldClOrdId(String oldClOrdId) {
        this.oldClOrdId = oldClOrdId;
    }

    @JsonProperty("origClOrdId")
    public String getOrigClOrdId() {
        return origClOrdId;
    }

    @JsonProperty("origClOrdId")
    public void setOrigClOrdId(String origClOrdId) {
        this.origClOrdId = origClOrdId;
    }

    @JsonProperty("openTime")
    public Long getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Long openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("origQty")
    public Integer getOrigQty() {
        return origQty;
    }

    @JsonProperty("origQty")
    public void setOrigQty(Integer origQty) {
        this.origQty = origQty;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
