package rawai.it.com.model.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"indexSymbol",
"ts",
"markPx",
"fairPx",
"spotPx",
"components"
})
public class Data {

@JsonProperty("indexSymbol")
private String indexSymbol;
@JsonProperty("ts")
private Long ts;
@JsonProperty("markPx")
private Integer markPx;
@JsonProperty("fairPx")
private Integer fairPx;
@JsonProperty("spotPx")
private Integer spotPx;
@JsonProperty("components")
private Components components;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("indexSymbol")
public String getIndexSymbol() {
return indexSymbol;
}

@JsonProperty("indexSymbol")
public void setIndexSymbol(String indexSymbol) {
this.indexSymbol = indexSymbol;
}

@JsonProperty("ts")
public Long getTs() {
return ts;
}

@JsonProperty("ts")
public void setTs(Long ts) {
this.ts = ts;
}

@JsonProperty("markPx")
public Integer getMarkPx() {
return markPx;
}

@JsonProperty("markPx")
public void setMarkPx(Integer markPx) {
this.markPx = markPx;
}

@JsonProperty("fairPx")
public Integer getFairPx() {
return fairPx;
}

@JsonProperty("fairPx")
public void setFairPx(Integer fairPx) {
this.fairPx = fairPx;
}

@JsonProperty("spotPx")
public Integer getSpotPx() {
return spotPx;
}

@JsonProperty("spotPx")
public void setSpotPx(Integer spotPx) {
this.spotPx = spotPx;
}

@JsonProperty("components")
public Components getComponents() {
return components;
}

@JsonProperty("components")
public void setComponents(Components components) {
this.components = components;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}


