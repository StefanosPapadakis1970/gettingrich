
package rawai.it.com.model.json.trading.closeallconditionalorders;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "actionId",
    "allForTrader"
})
public class CloseAllConditionalOrdersParams {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("actionId")
    private String actionId;
    @JsonProperty("allForTrader")
    private Boolean allForTrader;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("actionId")
    public String getActionId() {
        return actionId;
    }

    @JsonProperty("actionId")
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    @JsonProperty("allForTrader")
    public Boolean getAllForTrader() {
        return allForTrader;
    }

    @JsonProperty("allForTrader")
    public void setAllForTrader(Boolean allForTrader) {
        this.allForTrader = allForTrader;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
