package rawai.it.com.model;

public class Constants {
	
	public static final String tesWebSocketURL = "wss://ws.testnet.digitex.fun";
	
	public static final String testRestURL = "https://rest.tapi.digitexfutures.com";
	
	public static final String testApiKey = "7cfe77e74ec09955dc00c92e8232ab228f0c618e";	
	
}
