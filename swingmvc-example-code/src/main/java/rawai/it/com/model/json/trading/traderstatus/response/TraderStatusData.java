
package rawai.it.com.model.json.trading.traderstatus.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbol",
    "traderBalance",
    "leverage",
    "orderMargin",
    "positionMargin",
    "upnl",
    "pnl",
    "markPx",
    "positionType",
    "positionContracts",
    "positionVolume",
    "positionLiquidationVolume",
    "positionBankruptcyVolume",
    "contracts",
    "activeOrders",
    "conditionalOrders"
})
public class TraderStatusData {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("traderBalance")
    private Double traderBalance;
    @JsonProperty("leverage")
    private Integer leverage;
    @JsonProperty("orderMargin")
    private Integer orderMargin;
    @JsonProperty("positionMargin")
    private Integer positionMargin;
    @JsonProperty("upnl")
    private Double upnl;
    @JsonProperty("pnl")
    private Double pnl;
    @JsonProperty("markPx")
    private Double markPx;
    @JsonProperty("positionType")
    private String positionType;
    @JsonProperty("positionContracts")
    private Integer positionContracts;
    @JsonProperty("positionVolume")
    private Integer positionVolume;
    @JsonProperty("positionLiquidationVolume")
    private Integer positionLiquidationVolume;
    @JsonProperty("positionBankruptcyVolume")
    private Integer positionBankruptcyVolume;
    @JsonProperty("contracts")
    private List<TraderStatusContract> contracts = null;
    @JsonProperty("activeOrders")
    private List<ActiveOrder> activeOrders = null;
    @JsonProperty("conditionalOrders")
    private List<ConditionalOrder> conditionalOrders = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("traderBalance")
    public Double getTraderBalance() {
        return traderBalance;
    }

    @JsonProperty("traderBalance")
    public void setTraderBalance(Double traderBalance) {
        this.traderBalance = traderBalance;
    }

    @JsonProperty("leverage")
    public Integer getLeverage() {
        return leverage;
    }

    @JsonProperty("leverage")
    public void setLeverage(Integer leverage) {
        this.leverage = leverage;
    }

    @JsonProperty("orderMargin")
    public Integer getOrderMargin() {
        return orderMargin;
    }

    @JsonProperty("orderMargin")
    public void setOrderMargin(Integer orderMargin) {
        this.orderMargin = orderMargin;
    }

    @JsonProperty("positionMargin")
    public Integer getPositionMargin() {
        return positionMargin;
    }

    @JsonProperty("positionMargin")
    public void setPositionMargin(Integer positionMargin) {
        this.positionMargin = positionMargin;
    }

    @JsonProperty("upnl")
    public Double getUpnl() {
        return upnl;
    }

    @JsonProperty("upnl")
    public void setUpnl(Double upnl) {
        this.upnl = upnl;
    }

    @JsonProperty("pnl")
    public Double getPnl() {
        return pnl;
    }

    @JsonProperty("pnl")
    public void setPnl(Double pnl) {
        this.pnl = pnl;
    }

    @JsonProperty("markPx")
    public Double getMarkPx() {
        return markPx;
    }

    @JsonProperty("markPx")
    public void setMarkPx(Double markPx) {
        this.markPx = markPx;
    }

    @JsonProperty("positionType")
    public String getPositionType() {
        return positionType;
    }

    @JsonProperty("positionType")
    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    @JsonProperty("positionContracts")
    public Integer getPositionContracts() {
        return positionContracts;
    }

    @JsonProperty("positionContracts")
    public void setPositionContracts(Integer positionContracts) {
        this.positionContracts = positionContracts;
    }

    @JsonProperty("positionVolume")
    public Integer getPositionVolume() {
        return positionVolume;
    }

    @JsonProperty("positionVolume")
    public void setPositionVolume(Integer positionVolume) {
        this.positionVolume = positionVolume;
    }

    @JsonProperty("positionLiquidationVolume")
    public Integer getPositionLiquidationVolume() {
        return positionLiquidationVolume;
    }

    @JsonProperty("positionLiquidationVolume")
    public void setPositionLiquidationVolume(Integer positionLiquidationVolume) {
        this.positionLiquidationVolume = positionLiquidationVolume;
    }

    @JsonProperty("positionBankruptcyVolume")
    public Integer getPositionBankruptcyVolume() {
        return positionBankruptcyVolume;
    }

    @JsonProperty("positionBankruptcyVolume")
    public void setPositionBankruptcyVolume(Integer positionBankruptcyVolume) {
        this.positionBankruptcyVolume = positionBankruptcyVolume;
    }

    @JsonProperty("contracts")
    public List<TraderStatusContract> getContracts() {
        return contracts;
    }

    @JsonProperty("contracts")
    public void setContracts(List<TraderStatusContract> contracts) {
        this.contracts = contracts;
    }

    @JsonProperty("activeOrders")
    public List<ActiveOrder> getActiveOrders() {
        return activeOrders;
    }

    @JsonProperty("activeOrders")
    public void setActiveOrders(List<ActiveOrder> activeOrders) {
        this.activeOrders = activeOrders;
    }

    @JsonProperty("conditionalOrders")
    public List<ConditionalOrder> getConditionalOrders() {
        return conditionalOrders;
    }

    @JsonProperty("conditionalOrders")
    public void setConditionalOrders(List<ConditionalOrder> conditionalOrders) {
        this.conditionalOrders = conditionalOrders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
