package rawai.it.com.model.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"binance",
"bitfinex",
"coinbasepro",
"kraken"
})
public class Components {

@JsonProperty("binance")
private Binance binance;
@JsonProperty("bitfinex")
private Bitfinex bitfinex;
@JsonProperty("coinbasepro")
private Coinbasepro coinbasepro;
@JsonProperty("kraken")
private Kraken kraken;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("binance")
public Binance getBinance() {
return binance;
}

@JsonProperty("binance")
public void setBinance(Binance binance) {
this.binance = binance;
}

@JsonProperty("bitfinex")
public Bitfinex getBitfinex() {
return bitfinex;
}

@JsonProperty("bitfinex")
public void setBitfinex(Bitfinex bitfinex) {
this.bitfinex = bitfinex;
}

@JsonProperty("coinbasepro")
public Coinbasepro getCoinbasepro() {
return coinbasepro;
}

@JsonProperty("coinbasepro")
public void setCoinbasepro(Coinbasepro coinbasepro) {
this.coinbasepro = coinbasepro;
}

@JsonProperty("kraken")
public Kraken getKraken() {
return kraken;
}

@JsonProperty("kraken")
public void setKraken(Kraken kraken) {
this.kraken = kraken;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
