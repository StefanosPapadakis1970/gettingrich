
package rawai.it.com.model.json.trading.orderfilled;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import rawai.it.com.controller.Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ch",
    "data"
})
public class OrderFilled implements Constants {

	
    @JsonProperty("ch")
    private String ch;
    @JsonProperty("data")
    private Data data;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    private boolean alreadyContered = false;
    
    public  OrderFilled() {
      data = new Data();
	}

    @JsonProperty("ch")
    public String getCh() {
        return ch;
    }

    @JsonProperty("ch")
    public void setCh(String ch) {
        this.ch = ch;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "OrderFilled [ch=" + ch + ", data=" + data + ", additionalProperties=" + additionalProperties + "]";
	}

	public boolean isAlreadyContered() {
		return alreadyContered;
	}

	public void setAlreadyContered(boolean alreadyContered) {
		this.alreadyContered = alreadyContered;
	}

    public boolean isConterMarketOrder() {
    	return getData().getClOrdId().contains(CNTM) || getData().getNewClOrdId().contains(CNTM)	|| getData().getOrigClOrdId().contains(CNTM);
    }
    
    
    public boolean isConterLimitOrder() {
    	return getData().getClOrdId().contains(CNTO) || getData().getNewClOrdId().contains(CNTO)	|| getData().getOrigClOrdId().contains(CNTO);
    }

    public boolean isConterOrder() {
    	return isConterMarketOrder() || isConterLimitOrder() || isExitLimitOrder() || isExitMarketOrder();
    }

	public boolean isExitLimitOrder() {
		return getData().getClOrdId().contains(CNTEXO) || getData().getNewClOrdId().contains(CNTEXO)|| getData().getOrigClOrdId().contains(CNTEXO);		
	}

	public boolean isExitMarketOrder() {
		return getData().getClOrdId().contains(CNTMEXO) || getData().getNewClOrdId().contains(CNTMEXO)|| getData().getOrigClOrdId().contains(CNTMEXO);
	}
	
	public boolean isSystemOrder() {
		return getData().getClOrdId().contains(SYSTEMORDER) || getData().getNewClOrdId().contains(SYSTEMORDER)|| getData().getOrigClOrdId().contains(SYSTEMORDER);
	}
	
	public boolean isDisplayOrder() {
		return getData().getClOrdId().contains(DISPLAY) && getData().getNewClOrdId().contains(DISPLAY) && getData().getOrigClOrdId().contains(DISPLAY);
	} 
}
