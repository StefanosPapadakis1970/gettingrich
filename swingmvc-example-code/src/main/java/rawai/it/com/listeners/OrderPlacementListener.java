package rawai.it.com.listeners;

import rawai.it.com.model.json.trading.conditionalorder.status.ConditionalOrderStatus;
import rawai.it.com.model.json.trading.conditionalorder.status.cancelled.ConditionalOrderCancelledStatus;
import rawai.it.com.model.json.trading.contractclosed.ContractClosed;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;

public interface OrderPlacementListener {

	public void onOrderCreation(OrderStatus status);

	public void onOrderCancellation(OrderCancelledStatus cancelledOrderStatus);	
	
	public void onOrderFilled(OrderFilled orderFilled);

	public void onContractClosed(ContractClosed contractClosed);
}
