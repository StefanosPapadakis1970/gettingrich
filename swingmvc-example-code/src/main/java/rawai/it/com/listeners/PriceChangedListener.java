package rawai.it.com.listeners;

public interface PriceChangedListener {

	public void onPriceChanged(String price);

	public void onPriceFlipChanged(int priceFlip);
}
