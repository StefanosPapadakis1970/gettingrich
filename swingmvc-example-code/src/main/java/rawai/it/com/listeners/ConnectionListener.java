package rawai.it.com.listeners;

import rawai.it.com.view.BotSocketEnum;

public interface ConnectionListener {

	public void onConnectionClosed(BotSocketEnum socket);

	public void onConnectionEstablished(BotSocketEnum socket);
}
