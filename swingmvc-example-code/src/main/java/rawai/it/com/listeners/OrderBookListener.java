package rawai.it.com.listeners;

import rawai.it.com.model.json.orderbook.OrderBookData;

public interface OrderBookListener {
	
	public void onOrderBookChanged(OrderBookData orderBook);
	
}
