package rawai.it.com.listeners;

import rawai.it.com.model.json.trading.traderstatus.response.TraderStatusResponse;

public interface TraderStatusListener {

	public void onTraderStatuschanged(TraderStatusResponse traderStatus);

	public void onRequestLimitExceeded();
}
