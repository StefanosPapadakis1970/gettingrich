package rawai.it.com.websockets;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;

import rawai.it.com.listeners.ConnectionListener;
import rawai.it.com.view.BotSocketEnum;

public class BaseSocket {
	
	
	public static final Logger streamLog = LogManager.getLogger("stream");
	public static final Logger log = LogManager.getLogger("ordermanager");
	
	public  WebSocketClient mWs = null;
	private List<ConnectionListener> connectionListeners = new ArrayList<>();

	public void registerConnectionListene(ConnectionListener connectionListener) {
		if(!connectionListeners.contains(connectionListener)) {
			connectionListeners.add(connectionListener);
		}		
	}
	
	public void fireConnetionClosed(BotSocketEnum socket) {
		connectionListeners.stream().forEach(p -> p.onConnectionClosed(socket));
	}
	
	public void fireConnetionEstablished(BotSocketEnum socket) {
		connectionListeners.stream().forEach(p -> p.onConnectionEstablished(socket));
	}
	
	
	
	public synchronized void reconnectIfNecessary() {
	    if (mWs == null) {	    	
	    } else if (mWs.isClosed() || mWs.isClosing()) {	        
	        try {
	        	mWs.reconnectBlocking();
	        } catch (InterruptedException e) {
				log.error(e);
	        }
	    }  
	    
	}
	
	public void closeConnecion() {
		try {
			mWs.closeBlocking();
		} catch (InterruptedException e) {
			log.error(e);
		}
	}
}
