package rawai.it.com.websockets;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rawai.it.com.listeners.ConnectionListener;
import rawai.it.com.listeners.PriceChangedListener;
import rawai.it.com.model.json.orderbook.ChannelSubscriptionRequest;
import rawai.it.com.model.json.trades.Trade;
import rawai.it.com.model.json.trades.Trades;
import rawai.it.com.view.BotSocketEnum;

public class TradesSocket extends BaseSocket{
	
	private static final Logger log = LogManager.getLogger("rolling");
	
	private String currentPrice = "0";
	
	private List<Integer> currentPrices;
	
	private List<PriceChangedListener> priceChangedListeners = new ArrayList<PriceChangedListener>();
	
	private String flipPrice = "0";
	private String currentFlipPrice = "0";
	
	private int priceFlip = -1;
	

	public void start(String url, JLabel label) throws URISyntaxException, InterruptedException {
		    mWs = new WebSocketClient(new URI(url+"?channelname=trades")) {
			@Override
			public void onMessage(String message) {
				Thread.currentThread().setName("TradesSocketThread");
				log.debug(message);
				//streamLog.debug("TradesSocket");
				streamLog.debug(message);
				
				if(message.equals("ping")) {
					//log.info("SENDING PONG");
					send("{ msg: 'pong'}");
					return;
				}
				
				if(message.contentEquals("{\"id\":1,\"status\":\"ok\"}") || message.contains("\"status\":\"error\"}")) {
					return;
				}

				
				ObjectMapper mapper = new ObjectMapper();
				Trades trades = null;
				String px = "";
				try {
					trades = mapper.readValue(message, Trades.class);
					if(trades == null || trades.getData() == null || trades.getData().getTrades() == null) {
						return;
					}
					Trade trade = trades.getData().getTrades().get(trades.getData().getTrades().size()-1);
					label.setText(String.valueOf(trade.getPx()));				
					currentPrice = String.valueOf(trade.getPx());
					currentPrices = new ArrayList<Integer>();
					trades.getData().getTrades().stream().forEach(t ->currentPrices.add(t.getPx()));
					
					if (flipPrice.contentEquals("0")) {
						flipPrice = currentPrice;
					}									
					currentFlipPrice = currentPrice;
					
					int flipPriceIntValue = Integer.valueOf(flipPrice); 
					int currentFlipPriceIntValue = Integer.valueOf(currentFlipPrice);
					
					if (priceFlip < 0) {
						priceFlip = Math.abs((flipPriceIntValue-currentFlipPriceIntValue)/5);	
					}
					
					if (priceFlip != Math.abs((flipPriceIntValue-currentFlipPriceIntValue)/5)) {
						priceFlip = Math.abs((flipPriceIntValue-currentFlipPriceIntValue)/5);
						firePriceFlipChanged(priceFlip);
					}
					
					log.debug(" Price Flip Ticks -> "+flipPriceIntValue+" "+currentFlipPriceIntValue+" "+Math.abs((flipPriceIntValue-currentFlipPriceIntValue)/5));
					
					firePriceChanged(currentPrice);					
				} catch (IOException e) {
					log.error(e);
				}
				
				
				//log.info(px);
				
			}

			@Override
			public void onOpen(ServerHandshake handshake) {				
				log.info("Trades Connection Opened");
				fireConnetionEstablished(BotSocketEnum.TRADES);
				ChannelSubscriptionRequest subsriptionRequest = new ChannelSubscriptionRequest();
				subsriptionRequest.setId(1);
				subsriptionRequest.setMethod("subscribe");		
				List<String> params = new ArrayList<String>();
				params.add("BTCUSD-PERP@trades");
				subsriptionRequest.setParams(params);
				ObjectMapper mapper = new ObjectMapper();		 
				try {
					send(mapper.writeValueAsString(subsriptionRequest));
				} catch (JsonGenerationException e) {
					log.error(e);
				} catch (JsonMappingException e) {
					log.error(e);
				} catch (IOException e) {
					log.error(e);
				}
			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				log.info("closed connection");
				log.debug("Try to reconnect " );
				log.debug("Code "+ code+" Reason "+reason+" remote "+remote);
				//fireConnetionClosed(BotSocketEnum.TRADES);
			}

			@Override
			public void onError(Exception ex) {
				log.error(ex);
			}
			
	   };
	   
	   //mWs.connectBlocking(5, TimeUnit.MINUTES);
	   mWs.connect();
	   //mWs.setConnectionLostTimeout(0);
	}
	
	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}

	public List<Integer> getCurrentPrices() {
		return currentPrices;
	}

	public void setCurrentPrices(List<Integer> currentPrices) {
		this.currentPrices = currentPrices;
	}
	
	
	public void registerPriceChangeListener(PriceChangedListener pricechangeListener) {
		if(!priceChangedListeners.contains(pricechangeListener)) {
			priceChangedListeners.add(pricechangeListener);
		}		
	}	

	
	private void firePriceChanged(String price) {
	   priceChangedListeners.stream().forEach(p -> p.onPriceChanged(price));	
	}
	
	private void firePriceFlipChanged(int priceFlip) {
		   priceChangedListeners.stream().forEach(p -> p.onPriceFlipChanged(priceFlip));	
	}

	public String getFlipPrice() {
		return flipPrice;
	}

	public void setFlipPrice(String flipPrice) {
		this.flipPrice = flipPrice;
	}
}
