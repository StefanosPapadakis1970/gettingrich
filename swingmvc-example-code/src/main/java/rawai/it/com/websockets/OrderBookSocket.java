package rawai.it.com.websockets;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rawai.it.com.listeners.OrderBookListener;
import rawai.it.com.model.json.orderbook.ChannelSubscriptionRequest;
import rawai.it.com.model.json.orderbook.OrderBookData;
import rawai.it.com.view.BotSocketEnum;


public class OrderBookSocket extends BaseSocket{

	private static final Logger log = LogManager.getLogger(OrderBookSocket.class);
	
	private OrderBookData orderBook = null;
	
	private List<OrderBookListener> orderBookListeners = new ArrayList<OrderBookListener>();
	


	public void start(String url, JLabel label) throws URISyntaxException, InterruptedException {
		mWs = new WebSocketClient(new URI(url+"?channelname=orderbook")) {
			@Override
			public void onMessage(String message) {
				Thread.currentThread().setName("OrderBookSocketThread");
				//streamLog.debug("OrderBookSocket");
				streamLog.debug(message);
				//log.info(message);
				if(message.equals("ping")) {
					log.info("SENDING PONG");
					send("{ msg: 'pong'}");
					return;
				}

				
				ObjectMapper mapper = new ObjectMapper();
				
				String px = "";
				try {
					orderBook = mapper.readValue(message, OrderBookData.class);
					fireOrderBookChanged(orderBook);
				} catch (IOException e) {
					log.error(e);
				}			
			}

			@Override
			public void onOpen(ServerHandshake handshake) {
				log.info("OrderBook Connection Opened");
				fireConnetionEstablished(BotSocketEnum.ORDERBOOK);
				ChannelSubscriptionRequest subsriptionRequest = new ChannelSubscriptionRequest();
				subsriptionRequest.setId(1);
				subsriptionRequest.setMethod("subscribe");		
				List<String> params = new ArrayList<String>();
				params.add("BTCUSD-PERP@orderbook_50");
				subsriptionRequest.setParams(params);
				ObjectMapper mapper = new ObjectMapper();		 
				try {
					send(mapper.writeValueAsString(subsriptionRequest));
				} catch (JsonGenerationException e) {
					log.error(e);
				} catch (JsonMappingException e) {
					log.error(e);
				} catch (IOException e) {
					log.error(e);
				}
			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				log.info("closed connection");
				log.debug("Try to reconnect " );
				log.debug("Code "+ code+" Reason "+reason+" remote "+remote);
				//fireConnetionClosed(BotSocketEnum.ORDERBOOK);				
			}

			@Override
			public void onError(Exception ex) {
				log.error(ex);
			}
			
	   };
	   
	   mWs.addHeader("channel", "OrderBook");
	   mWs.connect();
	   //mWs.setConnectionLostTimeout(0);
	   
	}
	
	
	
	public void registerOrderBookListener(OrderBookListener orderBookListener) {
		if (!orderBookListeners.contains(orderBookListener)) {
			orderBookListeners.add(orderBookListener);
		}
	}
	
	private void fireOrderBookChanged(OrderBookData orderBook) {
		orderBookListeners.stream().forEach(orderBookListener -> orderBookListener.onOrderBookChanged(orderBook));
	}
}
