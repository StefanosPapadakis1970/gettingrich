package rawai.it.com.websockets;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.swing.JLabel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rawai.it.com.listeners.OrderPlacementListener;
import rawai.it.com.listeners.TraderStatusListener;
import rawai.it.com.model.json.trading.AuthenticationRequest;
import rawai.it.com.model.json.trading.Params;
import rawai.it.com.model.json.trading.conditionalorder.ConditionalOrderPlacement;
import rawai.it.com.model.json.trading.conditionalorder.status.ConditionalOrderStatus;
import rawai.it.com.model.json.trading.conditionalorder.status.cancelled.ConditionalOrderCancelledStatus;
import rawai.it.com.model.json.trading.contractclosed.ContractClosed;
import rawai.it.com.model.json.trading.error.TradingError;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.model.json.trading.traderstatus.response.TraderStatusResponse;
import rawai.it.com.view.BotSocketEnum;
import rawai.it.com.view.OrderErrorStatus;

@EJB
public class TradingSocket extends BaseSocket {

	private static final Logger log = LogManager.getLogger("rollingFile");

	private static final Logger creationLog = LogManager.getLogger("orderCreation");

	private String apiKey = "";

	private List<OrderPlacementListener> orderPlacmentListeners = new ArrayList<OrderPlacementListener>();

	private List<TraderStatusListener> traderStatusListeners = new ArrayList<TraderStatusListener>();

	public TradingSocket(String apiKey) {
		this.apiKey = apiKey;
	}

	public void start(String url, JLabel label) throws URISyntaxException, InterruptedException {
		mWs = new WebSocketClient(new URI(url + "?channelname=trading")) {
			@Override
			public void onMessage(String message) {
				Thread.currentThread().setName("TradingSocketThread");
				ObjectMapper mapper = new ObjectMapper();
				log.debug(message);
				//streamLog.debug("TradingSocket");
				streamLog.debug(message);

				if (message.contains("\"status\":\"ok\"}")) {
					return;
				}

				if (message.contains("3017")) {
					fireRequestLimitExceeded();
					return;
				}
				
				if (message.equals("ping")) {
					log.info("SENDING PONG");
					send("{ msg: 'pong'}");
					return;
				}

				if (message.contains("tradingStatus")) {
					return;
				}
				
				if (message.contains("error")) {
					return;
				}


				try {
					OrderStatus orderStatus = mapper.readValue(message, OrderStatus.class);

					if (orderStatus.getCh().contentEquals("orderFilled")) {
						OrderFilled orderFilled = mapper.readValue(message, OrderFilled.class);
						fireOrderFilled(orderFilled);
					}

					if (orderStatus.getCh().contentEquals("orderStatus")) {
						fireOrderPlaced(orderStatus);
					}

					if (orderStatus.getCh().contentEquals("contractClosed")) {
						ContractClosed contractClosed = mapper.readValue(message, ContractClosed.class);
						fireContractClosed(contractClosed);
					}

					if (orderStatus.getCh().contentEquals("orderCancelled")) {
						mapper = new ObjectMapper();
						OrderCancelledStatus cancelledOrderStatus = mapper.readValue(message,
								OrderCancelledStatus.class);
						fireOrderCancelled(cancelledOrderStatus);
					}

					if (orderStatus.getCh().contentEquals("traderStatus")) {
						mapper = new ObjectMapper();
						TraderStatusResponse traderStatus = mapper.readValue(message, TraderStatusResponse.class);
						fireTraderStatusChanged(traderStatus);
					}

				} catch (IOException e) {
					log.error("UPS!!", e);
				}
			}

			private void fireOrderFilled(OrderFilled orderFilled) {
				orderPlacmentListeners.stream().forEach(l -> l.onOrderFilled(orderFilled));

			}

			@Override
			public void onOpen(ServerHandshake handshake) {
				log.info("Trading Connection Opened");
				fireConnetionEstablished(BotSocketEnum.TRADING);
				AuthenticationRequest authenticationRequest = new AuthenticationRequest();
				authenticationRequest.setId(1);
				authenticationRequest.setMethod("auth");

				Params params = new Params();
				params.setType("token");
				params.setValue(apiKey);

				authenticationRequest.setParams(params);

				ObjectMapper mapper = new ObjectMapper();
				try {
					send(mapper.writeValueAsString(authenticationRequest));
				} catch (JsonGenerationException e) {
					log.error("UPS!!", e);
					e.printStackTrace();
				} catch (JsonMappingException e) {
					log.error("UPS!!", e);
				} catch (IOException e) {
					log.error("UPS!!", e);
				}
			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				log.info("closed connection");
				log.debug("Try to reconnect ");
				log.debug("Code " + code + " Reason " + reason + " remote " + remote);
				// fireConnetionClosed(BotSocketEnum.TRADING);
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
			}

		};
		// mWs.connectBlocking(5, TimeUnit.MINUTES);

		mWs.connect();
		// mWs.setConnectionLostTimeout(0);
	}

	public void createOrder(ConditionalOrderPlacement orderPlacemant) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			creationLog.debug("####### Creating Order -> " + orderPlacemant.getParams().getClOrdId());
			streamLog.debug("####### Creating Order -> " + orderPlacemant.getParams().getClOrdId());
			mWs.send(mapper.writeValueAsString(orderPlacemant));
		} catch (JsonGenerationException e) {
			log.error(e);
		} catch (JsonMappingException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		streamLog.debug("####### Creating Order Method finish ################");
	}

	public void registerOrderPlacementListener(OrderPlacementListener orderPlacementListener) {
		if (!orderPlacmentListeners.contains(orderPlacementListener)) {
			orderPlacmentListeners.add(orderPlacementListener);
		}
	}

	public void registerTraderStatusListener(TraderStatusListener traderStatusListener) {
		if (!traderStatusListeners.contains(traderStatusListener)) {
			traderStatusListeners.add(traderStatusListener);
		}
	}

	private void fireOrderPlaced(OrderStatus status) {
		orderPlacmentListeners.stream().forEach(listener -> listener.onOrderCreation(status));
	}

	private void fireOrderCancelled(OrderCancelledStatus cancelledOrderStatus) {
		orderPlacmentListeners.stream().forEach(listener -> listener.onOrderCancellation(cancelledOrderStatus));
	}

	protected void fireContractClosed(ContractClosed contractClosed) {
		orderPlacmentListeners.stream().forEach(listener -> listener.onContractClosed(contractClosed));
	}

	private void fireTraderStatusChanged(TraderStatusResponse traderStatus) {
		traderStatusListeners.stream().forEach(l -> l.onTraderStatuschanged(traderStatus));
	}

	protected void fireRequestLimitExceeded() {
		traderStatusListeners.stream().forEach(l -> l.onRequestLimitExceeded());
	}

	public void sendMessage(Object messageObject) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String message = mapper.writeValueAsString(messageObject);
			log.debug("Sending Message " + message);
			if (mWs != null) {
				mWs.send(mapper.writeValueAsString(messageObject));
			}
		} catch (JsonGenerationException e) {
			log.error(e);
		} catch (JsonMappingException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
	}
}
