package rawai.it.com.controller;

public interface Constants {
	public static final String CNTO = "CNTO";
	public static final String CNTM = "CNTM";
	public static final String SYSTEMORDER = "SYS";
	public static final String CNTEXO = "CNTEXO";
	public static final String CNTMEXO = "CNTMEXO";
	public static final String DISPLAY = "Display";
	public static final int    tickValue = 5; 
}
