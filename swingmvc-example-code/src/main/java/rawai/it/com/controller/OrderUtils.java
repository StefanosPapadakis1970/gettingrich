package rawai.it.com.controller;

public class OrderUtils implements Constants{

    public static boolean isConterMarketOrder(String id) {
    	return id.contains(CNTM);
    }
    
    public static boolean isConterLimitOrder(String id) {
    	return id.contains(CNTO);
    }

    public static boolean isConterOrder(String id) {
    	return isConterMarketOrder(id) || isConterLimitOrder(id) || isExitLimitOrder(id) || isExitMarketOrder(id);
    }

	public static boolean isExitLimitOrder(String id) {
		return id.contains(CNTEXO);		
	}

	public static boolean isExitMarketOrder(String id) {
		return id.contains(CNTMEXO);
	}
	
	public static boolean isSystemOrder(String id) {
		return id.contains(SYSTEMORDER);
	}
}
