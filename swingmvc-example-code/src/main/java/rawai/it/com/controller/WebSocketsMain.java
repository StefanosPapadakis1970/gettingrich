package rawai.it.com.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;


import rawai.it.com.model.Constants;
import rawai.it.com.model.json.orderbook.ChannelSubscriptionRequest;
import rawai.it.com.model.json.trades.Trade;
import rawai.it.com.model.json.trades.Trades;
import rawai.it.com.view.TradingBotView;
import rawai.it.com.view.orderbook.OrderBookView;
import rawai.it.com.websockets.TradesSocket;
import rawai.it.com.websockets.TradingSocket;

public class WebSocketsMain {
	
	private static Logger log = LogManager.getLogger("ordermanager");
	

	public static void main(String[] args)  {
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		WebSocketsMain sockets = new WebSocketsMain();
		try {
			sockets.run(args);
		} catch (Exception e) {
			log.error(e);
		}
	}

	// d1b8d5a2ee7d6bf6b8612ac93c0aebcce0de8540
	private void run(String[] args) throws Exception {
		
		TradingBotView view = new TradingBotView(args[0],args[1],args[2]);
		view.createAndShowView();
		OrderBookView orderBook = new OrderBookView(view.getTradingSocket());				
        view.setOrderBookView(orderBook);
	}
}
