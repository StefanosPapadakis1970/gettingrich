package rawai.it.com.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.model.json.orderbook.OrderBookData;
import rawai.it.com.view.OrderSide;

public class OrderBookManager {
	
	private static Logger log = LogManager.getLogger("ordermanager");

	private OrderBookData orderBook;

	public boolean oposingOrdersExist(int price, OrderSide oderSide) {
		switch (oderSide) {
		case BUY:
			return buyOrdersExist(price);
		case SELL:
			return sellOrdersExist(price);
		default:
			break;
		}
		return false;
	}


	private boolean sellOrdersExist(int price) {
		log.debug("Price "+price);
		log.debug(orderBook);		
		if (orderBook.getData().getAsks() == null) {
			return false;
		}
		log.debug(orderBook.getData().getAsks().stream().anyMatch(a -> a.get(0).intValue() == price));
		return orderBook.getData().getAsks().stream().anyMatch(a -> a.get(0).intValue() == price);
	}

	
	
	
	private boolean buyOrdersExist(int price) {
		log.debug("Price "+price);
		log.debug(orderBook);
		if (orderBook.getData().getBids() == null) {
			return false;
		}
		log.debug(orderBook.getData().getBids().stream().anyMatch(b -> b.get(0).intValue() == price));
		return orderBook.getData().getBids().stream().anyMatch(b -> b.get(0).intValue() == price);
	}

	public OrderBookData getOrderBook() {
		return orderBook;
	}

	public void setOrderBook(OrderBookData orderBook) {
		this.orderBook = orderBook;
	}
}
