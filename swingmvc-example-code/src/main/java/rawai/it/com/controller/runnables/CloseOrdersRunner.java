package rawai.it.com.controller.runnables;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrder;
import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrderParams;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.websockets.TradingSocket;

public class CloseOrdersRunner implements Runnable {
	
	private static Logger conterLog = LogManager.getLogger("conterTrade");
	private static Logger log = LogManager.getLogger("ordermanager");
	private TradingSocket tradingSocket;
	private Object lastAction = null;
	public static CountDownLatch conterOrderCancellationLatch;
	private int idCounter;
	
	private final List<OrderStatus> threadLocalOrders;



	public CloseOrdersRunner(TradingSocket tradingSocket, Object lastAction, List<OrderStatus> threadLocalOrders, int idCounter) {
		super();
		this.tradingSocket = tradingSocket;
		this.lastAction = lastAction;
		this.threadLocalOrders = threadLocalOrders;
		this.idCounter = idCounter;
	}



	@Override
	public void run() {
		conterLog.debug("---- Close last100 conterorders ----");
		synchronized (threadLocalOrders) {
//			try {
//				threadLocalOrders.wait();
//			} catch (InterruptedException e) {
//				log.error(e);
//			}
			if (threadLocalOrders.size() > 0) {
				threadLocalOrders.stream().forEach(order -> {
					CancelOrder cancelOrder = new CancelOrder();
					CancelOrderParams params = new CancelOrderParams();
					cancelOrder.setId(idCounter++);
					cancelOrder.setParams(params);
					cancelOrder.setMethod("cancelOrder");
					params.setSymbol(order.getData().getSymbol());
					params.setClOrdId(order.getData().getClOrdId());
					conterLog.debug(" --> Closing Conter Order " + cancelOrder);
					tradingSocket.sendMessage(cancelOrder);
					lastAction = cancelOrder;
					conterOrderCancellationLatch = new CountDownLatch(1);
					try {
						//wait(100);
						conterOrderCancellationLatch.await();
					} catch (InterruptedException e) {
						log.error(e);
					}
				});
			}
			//threadLocalOrders.notifyAll();
		}
	}	

}
