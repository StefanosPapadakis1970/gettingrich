package rawai.it.com.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;
import org.joda.time.DateTime;

import com.jgoodies.common.collect.ArrayListModel;

import rawai.it.com.controller.runnables.CloseOrdersRunner;
import rawai.it.com.listeners.ConnectionListener;
import rawai.it.com.listeners.OrderBookListener;
import rawai.it.com.listeners.OrderPlacementListener;
import rawai.it.com.listeners.PriceChangedListener;
import rawai.it.com.listeners.TraderStatusListener;
import rawai.it.com.model.TradeModel;
import rawai.it.com.model.TradingBotModel;
import rawai.it.com.model.json.orderbook.OrderBookData;
import rawai.it.com.model.json.trading.closeallconditionalorders.CloseAllConditionalOrders;
import rawai.it.com.model.json.trading.closeallconditionalorders.CloseAllConditionalOrdersParams;
import rawai.it.com.model.json.trading.closeallorders.CloseAllLimitOrders;
import rawai.it.com.model.json.trading.closeallorders.CloseAllLimitOrdersParams;
import rawai.it.com.model.json.trading.closeallorders.CloseAllOrders;
import rawai.it.com.model.json.trading.closeallorders.CloseAllOrdersParams;
import rawai.it.com.model.json.trading.closecontract.CloseContract;
import rawai.it.com.model.json.trading.closecontract.CloseContractParams;
import rawai.it.com.model.json.trading.closeposition.request.ClosePositionParams;
import rawai.it.com.model.json.trading.closeposition.request.ClosePositionRequest;
import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrder;
import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrderParams;
import rawai.it.com.model.json.trading.contractclosed.ContractClosed;
import rawai.it.com.model.json.trading.order.PlaceOrder;
import rawai.it.com.model.json.trading.order.PlaceOrderParams;
import rawai.it.com.model.json.trading.order.cancelled.status.Order;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.Contract;
import rawai.it.com.model.json.trading.orderfilled.MarketTrade;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.model.json.trading.traderstatus.StatusParams;
import rawai.it.com.model.json.trading.traderstatus.TraderStatusRequest;
import rawai.it.com.model.json.trading.traderstatus.response.TraderStatusContract;
import rawai.it.com.model.json.trading.traderstatus.response.TraderStatusResponse;
import rawai.it.com.view.BotSocketEnum;
import rawai.it.com.view.OrderSide;
import rawai.it.com.view.OrderStatusEnum;
import rawai.it.com.view.SoundUtils;
import rawai.it.com.websockets.TradesSocket;
import rawai.it.com.websockets.TradingSocket;

public class OrderManager implements OrderBookListener, OrderPlacementListener, PriceChangedListener,
		TraderStatusListener, ConnectionListener, Constants {

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	private static Logger log = LogManager.getLogger("ordermanager");
	private static Logger conterLog = LogManager.getLogger("conterTrade");
	private static Logger creationLog = LogManager.getLogger("ordercreation");
	private static Logger contractsLog = LogManager.getLogger("contracts");
	private static Logger contractsCSVLog = LogManager.getLogger("csvcontracts");

	public static CountDownLatch traderStatusLatch;

	public static CountDownLatch tradeCreationLatch = new CountDownLatch(1);

	public static CountDownLatch emergencyCountDownLatch;

	public static CountDownLatch takeProfitCountdownLatch;

	public static CountDownLatch orderBookCountdownLatch;

	public static CountDownLatch conterOrderCreationLatch;

	public static CountDownLatch conterOrderCancellationLatch;

	private static final String symbol = "BTCUSD-PERP";

	private OrderBookData orderBook;

	private TradingSocket tradingSocket;

	private TradesSocket tradesSocket;

	private ArrayListModel<TradeModel> longtradeListModel;

	private ArrayListModel<TradeModel> shorttradeListModel;

	private TradingBotModel model;

	private OrderFilled currentFilledOrder;

	private OrderFilled currentOrderToObserve;

	private OrderStatus exitLimitOrder;

	private OrderStatus actualConterOrder;

	private String currentObservedOrderId = "";

	private List<OrderFilled> filledOrders = new ArrayList<OrderFilled>();

	private List<OrderStatus> activeConterOrders = new ArrayList<>();

	private List<OrderCancelledStatus> cancelledOrders = new ArrayList<>();

	private List<OrderStatus> buyOrdersTriggered = new ArrayList<>();
	private List<OrderStatus> sellOrdersTriggered = new ArrayList<>();

	private int idCounter = 1;

	private TraderStatusResponse traderStatus;

	private OrderBookManager orderBookManager;

	private int numberOfOrders = 0;

	private int emergencySellPrice = 100000;
	private int emergencyBuyPrice = 0;

	private ScheduledExecutorService executionService = Executors.newScheduledThreadPool(1);
	private boolean creatingOrders;

	private int priceFlip;
	private boolean softEmergency = false;

	private String observeText = "";

	private Object lastAction = null;

	public ArrayListModel<TradeModel> getLongtradeListModel() {
		return longtradeListModel;
	}

	public void setLongtradeListModel(ArrayListModel<TradeModel> longtradeListModel) {
		this.longtradeListModel = longtradeListModel;
	}

	public ArrayListModel<TradeModel> getShorttradeListModel() {
		return shorttradeListModel;
	}

	public void setShorttradeListModel(ArrayListModel<TradeModel> shorttradeListModel) {
		this.shorttradeListModel = shorttradeListModel;
	}

	public OrderManager(TradingSocket tradingSocket, TradesSocket tradesSocket, TradingBotModel model) {
		super();
		this.tradingSocket = tradingSocket;
		this.tradesSocket = tradesSocket;
		this.model = model;
	}

	@Override
	public synchronized void onOrderBookChanged(OrderBookData orderBook) {

		if (creatingOrders) {
			return;
		}

		Runnable orderBookChangedRunner = new Runnable() {

			@Override
			public void run() {

				int remainingAsks = 0;
				int remainingBids = 0;
				int currentPriceIntValue = 0;
				String eatPrice = "FressPreis";
				OrderManager.this.orderBook = orderBook;
				int minOrders = 100;

				if (currentOrderToObserve != null) {
					eatPrice = currentOrderToObserve.getData().getPx().toString();
				}
				if (OrderManager.this.orderBook == null || OrderManager.this.orderBook.getData() == null
						|| OrderManager.this.orderBook.getData().getAsks() == null) {
					return;
				}
				List<List<Integer>> ask = OrderManager.this.orderBook.getData().getAsks().stream()
						.filter(a -> a.get(0).equals(Integer.valueOf(tradesSocket.getCurrentPrice())))
						.collect(Collectors.toList());
				List<List<Integer>> bid = OrderManager.this.orderBook.getData().getBids().stream()
						.filter(b -> b.get(0).equals(Integer.valueOf(tradesSocket.getCurrentPrice())))
						.collect(Collectors.toList());

				if (!ask.isEmpty()) {
					// log.debug("------- Remaining Orders -------------------
					// "+ask.get(0).get(1).toString());
					model.setRemainingOrders(ask.get(0).get(1).toString() + " " + eatPrice);
					remainingAsks = ask.get(0).get(1);
				}

				if (!bid.isEmpty()) {
					// log.debug("------- Remaining Orders -------------------
					// "+bid.get(0).get(1).toString());
					model.setRemainingOrders(bid.get(0).get(1).toString() + " " + eatPrice);
					remainingBids = bid.get(0).get(1);
				}

				try {
					currentPriceIntValue = Integer.valueOf(tradesSocket.getCurrentPrice());
				} catch (NumberFormatException e) {
					log.error(e);
				}

				if (currentOrderToObserve != null) {
					conterLog.debug("Remaining Aks " + remainingAsks + " Remaining Bids " + remainingBids
							+ " orderbookPrice  " + currentPriceIntValue + "----"
							+ currentOrderToObserve.getData().getPx() + " currentOrder " + currentOrderToObserve);
				} else {
					conterLog.debug("Remaining Aks " + remainingAsks + " Remaining Bids " + remainingBids
							+ " orderbookPrice  " + currentPriceIntValue);
				}

//		Runnable orderBookChangedRunner = new Runnable() {

//			@Override
//			public void run() {

				orderBookCountdownLatch = new CountDownLatch(2);

				if (OrderManager.this.orderBook == null || OrderManager.this.orderBook.getData() == null
						|| OrderManager.this.orderBook.getData().getAsks() == null) {
					return;
				}

				if (currentOrderToObserve == null || currentOrderToObserve.isAlreadyContered()) {
					return;
				}

				conterLog.debug("--> Observed Order -> " + currentOrderToObserve);
				// conterLog.debug("Current Order ID
				// "+currentFilledOrder.getData().getClOrdId()+"
				// "+currentFilledOrder.getData().getNewClOrdId()+"
				// "+currentFilledOrder.getData().getOrigClOrdId());
				if (currentOrderToObserve != null && currentOrderToObserve.getData().getClOrdId().contains(CNTO)) {
					// conterLog.debug("Order Filled is conterorder ... nothing to do
					// "+currentFilledOrder.getData().getClOrdId());
					return;
				}

				if (currentOrderToObserve.isAlreadyContered()) {
					// conterLog.debug("Order already contered ... leaving Method");
					return;
				}

				if (OrderManager.this.orderBook.getData() == null) {
					return;
				}

				if (ask.isEmpty() && bid.isEmpty()) {
					model.setRemainingOrders("Verry empty");
				}

				model.setRemainingOrders(remainingAsks + " orderbookPrice  " + currentPriceIntValue + " orderPrice "
						+ currentOrderToObserve.getData().getPx().intValue() + " " + remainingBids);

				if (currentOrderToObserve.isAlreadyContered()) {
					conterLog.debug("Order is already contered ->->->" + currentOrderToObserve);
				}

				if (currentOrderToObserve != null && remainingAsks <= minOrders
						&& currentOrderToObserve.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
					if (currentOrderToObserve != null && !currentOrderToObserve.isAlreadyContered()
							&& currentOrderToObserve.getData().getPx().intValue() == currentPriceIntValue) {

						currentOrderToObserve.setAlreadyContered(true);

						model.setRemainingOrders(remainingBids + "Time to close the Order " + remainingAsks);
						conterLog.debug(
								"################  Creating conter for MARKET SELL - ORDER too much eaten #################");
						conterLog.debug("Filled Order " + currentOrderToObserve.getData().getOrderSide()
								+ " Remaining Bids " + remainingBids + " Remaining Asks " + remainingAsks);

						conterLog.debug(
								"After ask bid check Filled Order " + currentOrderToObserve.getData().getOrderSide()
										+ " Remaining Bids " + remainingBids + " Remaining Asks " + remainingAsks);

						try {
							conterLog.debug("Waitingfor  Conter Order Latch");
							conterOrderCreationLatch.await();
							conterLog.debug("Conter Latch released");
						} catch (InterruptedException e) {
							conterLog.error(conterOrderCreationLatch);
						}

						if (remainingBids > 0) {
							conterLog.debug("--- Creating Conter LIMIT SAME-PRICE ORDER");
							createOppositeOrder(currentOrderToObserve);
						}

						if (remainingAsks <= minOrders && remainingAsks > 0) {
							conterLog.debug("--- Creating Conter MARKET ORDER");
							closeLastConterOrders();
							createConterMarketOrderSamePrice(currentOrderToObserve);
						}

						if (remainingBids == 0 && remainingAsks == 0) {
							conterLog.debug("--- Creating Conter MARKET ORDER");
							closeLastConterOrders();
							createConterMarketOrderSamePrice(currentOrderToObserve);
						}
					}
				}

				if (currentOrderToObserve != null && remainingBids <= minOrders
						&& currentOrderToObserve.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
					if (currentOrderToObserve != null && !currentOrderToObserve.isAlreadyContered()
							&& currentOrderToObserve.getData().getPx().intValue() == currentPriceIntValue) {

						currentOrderToObserve.setAlreadyContered(true);

						model.setRemainingOrders(remainingBids + "Time to close the Order " + remainingAsks);
						conterLog.debug(
								"################  Creating conter for MARKET Buy - ORDER too much eaten #################");
						conterLog.debug("Filled Order " + currentOrderToObserve.getData().getOrderSide()
								+ " Remaining Bids " + remainingBids + " Remaining Asks " + remainingAsks);

						conterLog.debug(
								"After ask bid check Filled Order " + currentOrderToObserve.getData().getOrderSide()
										+ " Remaining Bids " + remainingBids + " Remaining Asks " + remainingAsks);

						try {
							conterLog.debug("Waitingfor  Conter Order Latch");
							conterOrderCreationLatch.await();
							conterLog.debug("Conter Latch released");
						} catch (InterruptedException e) {
							conterLog.error(conterOrderCreationLatch);
						}

						if (remainingAsks > 0) {
							conterLog.debug("--- Creating Conter LIMIT SAME-PRICE ORDER");
							createOppositeOrder(currentOrderToObserve);
						}
						if (remainingBids <= minOrders && remainingBids > 0) {
							conterLog.debug("--- Creating Conter MARKET ORDER");
							closeLastConterOrders();
							createConterMarketOrderSamePrice(currentOrderToObserve);
						}

						if (remainingBids == 0 && remainingAsks == 0) {
							conterLog.debug("--- Creating Conter MARKET ORDER");
							closeLastConterOrders();
							createConterMarketOrderSamePrice(currentOrderToObserve);
						}

					}
				}
				conterLog.debug("#################### ObserverThread finished !! #################");
			}
		};

		Thread orderBookChangedThread = new Thread(orderBookChangedRunner);
		orderBookChangedThread.setName("orderBookChangedThread");
		orderBookChangedThread.start();
	}

	private void cancelPlacedOrders() {
		for (TradeModel longTrade : longtradeListModel) {
			if (tradeHasToBeCancelled(longTrade)) {
				cancelTrade(longTrade);
			}
		}

		for (TradeModel shortTrade : shorttradeListModel) {
			if (tradeHasToBeCancelled(shortTrade)) {
				cancelTrade(shortTrade);
			}
		}
	}

	private void cancelTrade(TradeModel trade) {
		log.debug("##########################CANELING TRADE ->" + trade.getId() + " " + trade.getPrice() + " "
				+ trade.getNumberOfcontracts());
		conterLog.debug("##########################CANELING TRADE ->" + trade.getId() + " " + trade.getPrice() + " "
				+ trade.getNumberOfcontracts());

		if (log.isDebugEnabled()) {
			log.debug("######### Longtrades ######");
			longtradeListModel.stream().forEach(l -> log.debug(l.getId()));
			log.debug("######### Shorttrades ######");
			shorttradeListModel.stream().forEach(l -> log.debug(l.getId()));
		}

		CancelOrder cancelOrder = new CancelOrder();
		cancelOrder.setMethod("cancelOrder");
		CancelOrderParams params = new CancelOrderParams();
		params.setSymbol(symbol);
		params.setClOrdId(trade.getId());
		cancelOrder.setParams(params);
		tradingSocket.sendMessage(cancelOrder);
		lastAction = cancelOrder;
	}

	private boolean tradeHasToBeCancelled(TradeModel trade) {
		for (Integer currentPrice : tradesSocket.getCurrentPrices()) {
			int priceDifference = Math.abs(currentPrice - trade.getPrice());
			int numberOfTicks = priceDifference / tickValue;
			// log.debug("Number of callculated Ticks "+numberOfTicks);
			if (numberOfTicks == 1) {
				return true;
			}
		}
		return false;
	}

	private int calculateTicks(int price) {
		int priceDifference = Math.abs(Integer.valueOf(tradesSocket.getCurrentPrice()) - price);
		int numberOfTicks = priceDifference / tickValue;
		return numberOfTicks;
	}

	public synchronized void createOrders(TradingBotModel model) {
		creatingOrders = true;
		log.debug("CreateOrders");
		creationLog.debug("############ Creating Orders ###################");

		fillUpOrderBookGaps(orderBook.getData().getAsks());
		fillUpOrderBookBidGaps(orderBook.getData().getBids());

		updateNumberOfActiveOrders();
		int activeBuyOrders = Integer.valueOf(model.getActiveBuyOrders());
		int activeSellOrders = Integer.valueOf(model.getActiveSellOrders());

		this.model = model;

		String currentPrice = tradesSocket.getCurrentPrice();

		List<String> sellPrices = fetchSellPrices(currentPrice, model);
		List<String> buyPrices = fetchBuyPrices(currentPrice, model);

		int longOrders = 0;
		int shortOrders = 0;

		if (log.isDebugEnabled()) {
			log.debug("######### fetched SellPrices ###### size " + sellPrices.size());
			sellPrices.stream().forEach(s -> log.debug(s));
		}

		int maximumOfTriggerdOrders = Integer.valueOf(model.getNumberOfContracts())
				* Integer.valueOf(model.getNumberOfOrdersToPlace());

		creationLog.debug("Active Buy orders " + activeBuyOrders + " Active Sell Orders " + activeSellOrders
				+ " Orders to place " + maximumOfTriggerdOrders);

		if (!(activeSellOrders < maximumOfTriggerdOrders)) {
			creationLog.debug("###NO SELLORDER CREATION#################");
		}

		if (activeSellOrders < maximumOfTriggerdOrders) {
			for (String sellPrice : sellPrices) {

				int sellPriceInVal = Integer.valueOf(sellPrice);

				if (shortOrderAlreadyExists(sellPriceInVal)) {
					log.debug("#########################  Cotinue Price exists ##########################->"
							+ sellPriceInVal);
					creationLog.debug("#########################  Cotinue Price exists ##########################->"
							+ sellPriceInVal);
					// shortOrders++;
					continue;
				}

				String orderID = SYSTEMORDER + UUID.randomUUID().toString();

				PlaceOrder orderPlacement = new PlaceOrder();
				PlaceOrderParams orderParams = new PlaceOrderParams();
				orderPlacement.setMethod("placeOrder");
				orderPlacement.setId(idCounter++);
				orderParams.setOrdType("LIMIT");
				orderParams.setTimeInForce("GTC");
				orderParams.setClOrdId(orderID);
				orderParams.setSymbol("BTCUSD-PERP");
				orderParams.setSide("SELL");
				orderParams.setPx(sellPriceInVal);
				orderParams.setQty(Integer.valueOf(model.getNumberOfContracts()));
				orderPlacement.setParams(orderParams);

				log.debug("Sending orderPlacement " + sellPriceInVal + "  " + orderPlacement);
				creationLog.debug("Sending orderPlacement ShortCounter " + shortOrders + " " + sellPriceInVal + "  "
						+ orderPlacement);
				// tradingSocket.createOrder(orderPlacement);
				tradingSocket.sendMessage(orderPlacement);
				lastAction = orderPlacement;

				try {
					creationLog.debug(" ---------  Waiting For Latch -------------");
					tradeCreationLatch.await();
					creationLog.debug(" ---------  Latch Released -------------  Price " + sellPriceInVal);
				} catch (InterruptedException e) {
					log.error(e);
				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("######### fetched BuyPrices ###### size " + buyPrices.size());
			buyPrices.stream().forEach(s -> log.debug(s));
		}

		if (!(activeBuyOrders < maximumOfTriggerdOrders)) {
			creationLog.debug("###NO BUYORDER CREATION#################");
		}

		if (activeBuyOrders < maximumOfTriggerdOrders) {
			for (String buyPrice : buyPrices) {

				int buyPriceInVal = Integer.valueOf(buyPrice);

				if (longOrderAlreadyExists(buyPriceInVal)) {
					log.debug("#########################  Cotinue Price exists ##########################->"
							+ buyPriceInVal);
					creationLog.debug("#########################  Cotinue Price exists ##########################->"
							+ buyPriceInVal);
					// longOrders++;
					continue;
				}

				String orderID = SYSTEMORDER + UUID.randomUUID().toString();

				PlaceOrder orderPlacement = new PlaceOrder();
				PlaceOrderParams orderParams = new PlaceOrderParams();
				orderPlacement.setMethod("placeOrder");
				orderPlacement.setId(idCounter++);
				orderParams.setOrdType("LIMIT");
				orderParams.setTimeInForce("GTC");
				orderParams.setClOrdId(orderID);
				orderParams.setSide("BUY");
				orderParams.setSymbol("BTCUSD-PERP");

				orderParams.setPx(buyPriceInVal);
				orderParams.setQty(Integer.valueOf(model.getNumberOfContracts()));
				orderPlacement.setParams(orderParams);

				log.debug("Sending orderPlacement " + buyPriceInVal + " " + orderPlacement);
				creationLog.debug("Sending orderPlacement LongCounter " + longOrders + " " + buyPriceInVal + " "
						+ orderPlacement);
				longOrders++;
				tradingSocket.sendMessage(orderPlacement);
				lastAction = orderPlacement;

				try {
					creationLog.debug(" ---------  Waiting For Latch -------------");
					tradeCreationLatch.await();
					creationLog.debug(" ---------  Latch Released -------------  Price " + buyPriceInVal);
				} catch (InterruptedException e) {
					log.error(e);
				}

			}
		}

		if (!buyOrdersTriggered.isEmpty()) {
			emergencyBuyPrice = fetchEmergencyBuyPrice();
		}

		if (!sellOrdersTriggered.isEmpty()) {
			emergencySellPrice = fetchEmergencySellPrice();
		}

		model.setStopBuyPrice(String.valueOf(emergencyBuyPrice));
		model.setStopSellPrice(String.valueOf(emergencySellPrice));

		creationLog.debug("############ Finish Creating Orders Finish  ###################");
		creatingOrders = false;
	}

	// This method runs in a Thread
	private void createConterOrder(OrderFilled filledOrder) {

		if (filledOrder.getData().getClOrdId().contains(CNTM) || filledOrder.getData().getClOrdId().contains(CNTO)
				|| filledOrder.getData().getNewClOrdId().contains(CNTM)
				|| filledOrder.getData().getNewClOrdId().contains(CNTO)
				|| filledOrder.getData().getOrigClOrdId().contains(CNTM)
				|| filledOrder.getData().getOrigClOrdId().contains(CNTO)) {
			conterLog.debug(
					"############   Conter Order Not created because of ID relates to contertrade ##################");
			return;
		}

		log.debug("############   Conter Order Creation ##################");
		conterLog.debug("############   Conter Order Creation ##################");

		PlaceOrder orderPlacement = new PlaceOrder();
		PlaceOrderParams orderParams = new PlaceOrderParams();
		orderPlacement.setMethod("placeOrder");
		orderPlacement.setId(idCounter++);
		orderParams.setOrdType("LIMIT");
		orderParams.setTimeInForce("GTC");

		orderParams.setSymbol(filledOrder.getData().getSymbol());

		int qty = 0;

		// Maybe take the highest or lowest price?
		if (filledOrder.getData().getPx() == 0) {
			conterLog.debug("Filled Order PX is 0 multiple order generation ");
			for (MarketTrade trade : filledOrder.getData().getMarketTrades()) {
				String orderID = CNTO + UUID.randomUUID().toString();
				if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
					orderParams.setSide("BUY");
					orderParams.setPx(trade.getPx() - tickValue);
				}

				if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
					orderParams.setSide("SELL");
					orderParams.setPx(trade.getPx() + tickValue);
				}

				orderParams.setClOrdId(orderID);
				orderParams.setQty(trade.getQty());
				orderPlacement.setParams(orderParams);
				conterLog.debug("Sending conterOrderPlacement " + orderPlacement);
				tradingSocket.sendMessage(orderPlacement);
				lastAction = orderPlacement;
				conterOrderCreationLatch = new CountDownLatch(1);
				try {
					conterLog.debug("Waitingfor  Conter Order Latch");
					conterOrderCreationLatch.await();
					conterLog.debug("Conter Latch released");
				} catch (InterruptedException e) {
					conterLog.error(conterOrderCreationLatch);
				}
			}
		} else {
			conterLog.debug("Filled Order PX is not equal 0 like before!! ");
			String orderID = CNTO + UUID.randomUUID().toString();
			if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
				orderParams.setSide("BUY");
				orderParams.setPx(filledOrder.getData().getPx() - tickValue);
			}

			if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
				orderParams.setSide("SELL");
				orderParams.setPx(filledOrder.getData().getPx() + tickValue);
			}
			qty = 0;
			for (MarketTrade trade : filledOrder.getData().getMarketTrades()) {
				qty += trade.getQty();
			}
			orderParams.setQty(qty);
			orderParams.setClOrdId(orderID);
			orderPlacement.setParams(orderParams);
			conterLog.debug("Sending conterOrderPlacement " + orderPlacement);
			tradingSocket.sendMessage(orderPlacement);
			lastAction = orderPlacement;
			conterOrderCreationLatch = new CountDownLatch(1);
			try {
				conterLog.debug("Waitingfor  Conter Order Latch");
				conterOrderCreationLatch.await();
				conterLog.debug("Conter Latch released");
			} catch (InterruptedException e) {
				conterLog.error(conterOrderCreationLatch);
			}
		}
	}

	private void createOppositeOrder(OrderFilled filledOrder) {
		log.debug("############   OPOSITE Order Creation ##################");
		conterLog.debug("############ OPOSITE  Conter Order Creation ##################");

		String orderID = CNTEXO + UUID.randomUUID().toString();

		PlaceOrder orderPlacement = new PlaceOrder();
		PlaceOrderParams orderParams = new PlaceOrderParams();
		orderPlacement.setMethod("placeOrder");
		orderPlacement.setId(idCounter++);
		orderParams.setOrdType("LIMIT");
		orderParams.setTimeInForce("GTC");
		orderParams.setClOrdId(orderID);
		orderParams.setSymbol(filledOrder.getData().getSymbol());

		orderParams.setPx(filledOrder.getData().getPx());
		orderParams.setSide(filledOrder.getData().getOrderSide());

		int qty = 0;
		for (MarketTrade trade : filledOrder.getData().getMarketTrades()) {
			qty += trade.getQty();
		}

		orderParams.setQty(qty);

		orderPlacement.setParams(orderParams);

		if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
			orderParams.setSide("BUY");
		}

		if (filledOrder.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
			orderParams.setSide("SELL");
		}

		conterLog.debug("Sending opositeLimitOrder CNTEXO " + orderPlacement);
		softEmergency = true;
		tradingSocket.sendMessage(orderPlacement);
		lastAction = orderPlacement;
	}

	private List<String> fetchSellPrices(String currentPrice, TradingBotModel model) {
		if (model.isAbsolut()) {
			return fetshSellPricesAbsolut(currentPrice, model);
		}
		return fetshSellPricesPercentual(currentPrice, model);
	}

	private List<String> fetchBuyPrices(String currentPrice, TradingBotModel model) {
		if (model.isAbsolut()) {
			return fetshBuyPricesAbsolut(currentPrice, model);
		}
		return fetshBuyPricesPercentual(currentPrice, model);
	}

	private List<String> fetshSellPricesAbsolut(String currentPrice, TradingBotModel model) {
		List<String> askPrices = new ArrayList<String>();
		int distanceCounter = 0;
		int orderDistance = Integer.valueOf(model.getMinOrderTicksDistance());
		int numberOfContractsPlaced = Integer.valueOf(model.getMaxOrdersAllowed());
		int ordersToBePlaced = Integer.valueOf(model.getNumberOfOrdersToPlace());
		int orderCounter = 0;
		int currentPriceIntVal = Integer.valueOf(currentPrice);

		for (List<Integer> ask : orderBook.getData().getAsks()) {
			if (distanceCounter >= orderDistance && ask.get(1) <= numberOfContractsPlaced
					&& orderCounter < ordersToBePlaced && currentPriceIntVal < ask.get(0)) {
				orderCounter++;
				askPrices.add(String.valueOf(ask.get(0)));
			}
			distanceCounter++;

		}
		return askPrices;
	}

	private List<String> fetshSellPricesPercentual(String currentPrice, TradingBotModel model) {
		List<String> askPrices = new ArrayList<String>();
		int distanceCounter = 0;
		int orderDistance = Integer.valueOf(model.getMinOrderTicksDistance());
		int procent = Integer.valueOf(model.getMaximumOrdersProcent());
		int ordersToBePlaced = Integer.valueOf(model.getNumberOfOrdersToPlace());
		int orderCounter = 0;
		int currentPriceIntVal = Integer.valueOf(currentPrice);
		int totalAskOrders = 0;

		if (orderBook == null || orderBook.getData() == null || orderBook.getData().getAsks() == null) {
			return askPrices;
		}

		for (List<Integer> ask : orderBook.getData().getAsks()) {
			totalAskOrders += ask.get(1);
		}

		int numberOfContractsPlaced = (totalAskOrders / 100) * procent;

		for (List<Integer> ask : orderBook.getData().getAsks()) {
			if (distanceCounter >= orderDistance && ask.get(1) <= numberOfContractsPlaced
					&& orderCounter < ordersToBePlaced && currentPriceIntVal < ask.get(0)) {
				orderCounter++;
				askPrices.add(String.valueOf(ask.get(0)));
			}
			distanceCounter++;

		}

		return askPrices;
	}

	private List<String> fetshBuyPricesAbsolut(String currentPrice, TradingBotModel model) {
		List<String> bidPrices = new ArrayList<String>();
		int distanceCounter = 0;
		int orderDistance = Integer.valueOf(model.getMinOrderTicksDistance());
		int numberOfContractsPlaced = Integer.valueOf(model.getMaxOrdersAllowed());
		int ordersToBePlaced = Integer.valueOf(model.getNumberOfOrdersToPlace());
		int orderCounter = 0;
		int currentPriceIntVal = Integer.valueOf(currentPrice);

		if (orderBook == null || orderBook.getData() == null || orderBook.getData().getBids() == null) {
			return bidPrices;
		}

		for (List<Integer> bid : orderBook.getData().getBids()) {
			if (distanceCounter >= orderDistance && bid.get(1) <= numberOfContractsPlaced
					&& orderCounter < ordersToBePlaced && currentPriceIntVal > bid.get(0)) {
				orderCounter++;
				bidPrices.add(String.valueOf(bid.get(0)));
			}
			distanceCounter++;

		}
		return bidPrices;
	}

	private List<String> fetshBuyPricesPercentual(String currentPrice, TradingBotModel model) {
		List<String> bidPrices = new ArrayList<String>();
		int distanceCounter = 0;
		int orderDistance = Integer.valueOf(model.getMinOrderTicksDistance());
		int procent = Integer.valueOf(model.getMaximumOrdersProcent());
		int ordersToBePlaced = Integer.valueOf(model.getNumberOfOrdersToPlace());
		int orderCounter = 0;
		int currentPriceIntVal = Integer.valueOf(currentPrice);
		int totalBisOrders = 0;

		for (List<Integer> bid : orderBook.getData().getBids()) {
			totalBisOrders += bid.get(1);
		}

		int numberOfContractsPlaced = (totalBisOrders / 100) * procent;

		for (List<Integer> bid : orderBook.getData().getBids()) {
			if (distanceCounter >= orderDistance && bid.get(1) <= numberOfContractsPlaced
					&& orderCounter < ordersToBePlaced && currentPriceIntVal > bid.get(0)) {
				orderCounter++;
				bidPrices.add(String.valueOf(bid.get(0)));
			}
			distanceCounter++;

		}
		return bidPrices;
	}

	private int calculateNumberOfContractsPlaced(String maximumOrdersProcent, String maxOrdersAllowed) {

		return 0;
	}

	private void createOrder(PlaceOrder orderPlacement) {
		log.debug("creating order " + orderPlacement.getParams().getSide() + " " + orderPlacement.getParams().getPx());
		tradingSocket.sendMessage(orderPlacement);
		lastAction = orderPlacement;
	}

	@Override
	public void onOrderCreation(OrderStatus status) {
		contractsCSVLog.debug("", simpleDateFormat.format(new Date(status.getData().getTimestamp())),
				status.getData().getOrderStatus(), status.getData().getClOrdId().replaceAll("\\W", ""), "",
				status.getData().getOrigClOrdId().replaceAll("\\W", ""), status.getData().getOrderType(),
				status.getData().getOrderSide(), status.getData().getOrigQty(), "",
				(status.getData().getOrigQty() - status.getData().getQty()), status.getData().getQty(),
				status.getData().getPx());
		conterLog.debug(" ----OrderCreation-----Orderstatus -> " + status.getData().getClOrdId() + " "
				+ status.getData().getOrderStatus());
		creationLog.debug(
				" ---------Orderstatus -> " + status.getData().getClOrdId() + " " + status.getData().getOrderStatus());
		if (status.getData().getOrderStatus().contentEquals(OrderStatusEnum.ACCEPTED.name())) {
			if (status.isExitLimitOrder()) {
				exitLimitOrder = status;
				conterLog.debug("-> Current Created LimitExitConter  -> " + status.getData().getClOrdId() + " "
						+ status.getData().getOrigClOrdId());
			}

			if (status.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
				sellOrdersTriggered.add(status);
			}

			if (status.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
				buyOrdersTriggered.add(status);
			}

			if (status.getData().getClOrdId().contains(CNTO) || status.getData().getOrigClOrdId().contains(CNTO)) {
				actualConterOrder = status;
				if (!activeConterOrders.contains(status)) {
					conterLog.debug("----ADDING CONTERRORDER TO activeConterOrders ----------------" + status);
					activeConterOrders.add(status);
				}

				conterLog.debug("-> Current Created ConterOrder -> " + status.getData().getClOrdId() + " "
						+ status.getData().getOrigClOrdId());
			}

			updateNumberOfActiveOrders();

			creationLog.debug(" --------- Releasing Latch From onOrderCreation");
			tradeCreationLatch.countDown();
			tradeCreationLatch = new CountDownLatch(1);
			if (takeProfitCountdownLatch != null) {
				creationLog.debug("-----Releasing takeProfitLatch");
				takeProfitCountdownLatch.countDown();
			}
		}

		// Synch actualconterOrder ... can be 0 when orderbook changes
		conterLog.debug("Try to release releasing Conter Order Latch " + this.getClass());
		if (conterOrderCreationLatch != null) {
			conterOrderCreationLatch.countDown();
			conterLog.debug("Releasing Conter Order Latch");
		}
	}

	@Override
	public void onOrderCancellation(OrderCancelledStatus cancelledOrderStatus) {
		conterLog.debug("-----> OnOrderCancellation  " + cancelledOrderStatus);
		if (cancelledOrderStatus != null && cancelledOrderStatus.getData() != null
				&& cancelledOrderStatus.getData().getTimestamp() != null
				&& cancelledOrderStatus.getData().getPrevClOrdId() != null) {
			contractsCSVLog.debug("", simpleDateFormat.format(new Date(cancelledOrderStatus.getData().getTimestamp())),
					cancelledOrderStatus.getData().getOrderStatus(),
					cancelledOrderStatus.getData().getPrevClOrdId().replaceAll("\\W", ""),
					cancelledOrderStatus.getData().getPrevClOrdId().replaceAll("\\W", ""),
					cancelledOrderStatus.getData().getPrevClOrdId().replaceAll("\\W", ""), "", "", "", "", "", "", "");
		} else {
			contractsCSVLog.debug("", "NULL", "NULL", "NULL", "NULL", "NULL", "", "", "", "", "", "", "");
		}

		if (cancelledOrderStatus.getData().getOrderStatus().contentEquals(OrderStatusEnum.CANCELLED.name())) {
			creationLog.debug("-----> Order Cancelled ");
			log.debug("Order Cancelled ");

			updateOrderList(buyOrdersTriggered, cancelledOrderStatus);
			updateOrderList(sellOrdersTriggered, cancelledOrderStatus);
			updateNumberOfActiveOrders();

			if (actualConterOrder != null && actualConterOrder.getData() != null
					&& cancelledOrderStatus.getData().getPrevClOrdId() != null && actualConterOrder.getData()
							.getClOrdId().contentEquals(cancelledOrderStatus.getData().getPrevClOrdId())) {
				actualConterOrder = null;
			}

			if (cancelledOrderStatus.getData().getPrevClOrdId() != null
					&& OrderUtils.isConterOrder(cancelledOrderStatus.getData().getPrevClOrdId())) {
				conterLog.debug("Removing conterorder from List -> " + cancelledOrderStatus.getData().getPrevClOrdId()
						+ " " + activeConterOrders.size());
				removeFromActiveConterOrders(cancelledOrderStatus.getData().getPrevClOrdId());
				if (conterOrderCancellationLatch != null) {
					conterOrderCancellationLatch.countDown();
				}
				conterLog.debug("Removed conterorder from List -> " + cancelledOrderStatus.getData().getPrevClOrdId()
						+ " " + activeConterOrders.size());
			}

			if (model.isCancelOrders()) {
				creationLog.debug("-----> Calling CreateOrders");

				tradeCreationLatch.countDown();
				tradeCreationLatch = new CountDownLatch(1);
				Runnable createOrderRunner = new Runnable() {

					@Override
					public void run() {
						createOrders(model);
					}
				};

				Thread createOrderThread = new Thread(createOrderRunner);
				createOrderThread.start();
				conterLog.debug("-------------- Starting Thread -------- " + createOrderThread.getId());
			}
		}
	}

	private List<OrderStatus> updateOrderList(List<OrderStatus> orders, OrderCancelledStatus cancelledOrderStatus) {
		List<OrderStatus> ordersToRemove = new ArrayList<>();
		for (OrderStatus order : orders) {
			for (Order cancelledOrder : cancelledOrderStatus.getData().getOrders()) {
				if (cancelledOrder.getOldClOrdId().contentEquals(order.getData().getClOrdId())) {
					ordersToRemove.add(order);
				}
			}
		}

		orders.removeAll(ordersToRemove);
		return orders;
	}

	private List<OrderStatus> updateOrderList(List<OrderStatus> orders, OrderFilled orderFilled) {
		List<OrderStatus> ordersToRemove = new ArrayList<>();

		for (OrderStatus order : orders) {
			if (orderFilled.getData().getClOrdId().contentEquals(order.getData().getClOrdId())
					|| orderFilled.getData().getOrigClOrdId().contentEquals(order.getData().getClOrdId())) {
				ordersToRemove.add(order);
			}
		}
		orders.removeAll(ordersToRemove);
		return orders;
	}

	private boolean longOrderAlreadyExists(int price) {
		if (log.isDebugEnabled()) {
			for (TradeModel trade : longtradeListModel) {
				log.debug(trade.getPrice() + " == " + price + "  " + (trade.getPrice() == price));
			}
			log.debug("StreamResult " + longtradeListModel.stream().anyMatch(t -> t.getPrice() == price));
			creationLog.debug("Does LongOrder exist -> " + price + " "
					+ longtradeListModel.stream().anyMatch(t -> t.getPrice() == price));
		}
		return longtradeListModel.stream().anyMatch(t -> t.getPrice() == price);
	}

	private boolean shortOrderAlreadyExists(int price) {
		if (log.isDebugEnabled()) {
			for (TradeModel trade : shorttradeListModel) {
				log.debug(trade.getPrice() + " == " + price + "  " + (trade.getPrice() == price));
			}
			log.debug("StreamResult " + shorttradeListModel.stream().anyMatch(t -> t.getPrice() == price));
			creationLog.debug("Does ShortOrder exist -> " + price + " "
					+ shorttradeListModel.stream().anyMatch(t -> t.getPrice() == price));
		}
		return shorttradeListModel.stream().anyMatch(t -> t.getPrice() == price);
	}

	@Override
	public void onPriceChanged(String price) {
		int priceIntVal = Integer.valueOf(price);
		log.debug("Price changed " + price);

		if (model.isEmergencyStop()) {

			creationLog.debug("Price " + price + " emergencyBuyPrice " + emergencyBuyPrice + " "
					+ (Integer.valueOf(price) == emergencyBuyPrice));
			creationLog.debug("Price " + price + " emergencySellPrice " + emergencySellPrice + " "
					+ (Integer.valueOf(price) == emergencySellPrice));
			if ((Integer.valueOf(price) <= emergencyBuyPrice || Integer.valueOf(price) >= emergencySellPrice)
					&& currentOrderToObserve != null && !currentOrderToObserve.isConterOrder()) {
				creationLog.debug("Calling Emergency Stop !!");
				log.debug("Calling Emergency Stop Execution Thread !!");
				conterLog.debug("Calling Emergency Stop Execution Thread !!");
				emergencyStopAndOrderCreationThreadExecustion();
			}
		}

		// fetchTraderStatus();

		if (model == null) {
			return;
		}

		if (model.isCancelOrders()) {
			cancelPlacedOrders();
		}
	}

//	public void fetchTraderStatus() {
//		creationLog.debug("FETCHING TRADER STATUS");
//		TraderStatusRequest traderStatusRequest = new TraderStatusRequest();
//		traderStatusRequest.setMethod("getTraderStatus");
//		StatusParams statusParams = new StatusParams();
//		statusParams.setSymbol(symbol);
//		traderStatusRequest.setParams(statusParams);
//		log.debug("Sending -> TraderStatusRequest " + traderStatusRequest.toString());
//		creationLog.debug("Sending -> TraderStatusRequest " + traderStatusRequest.toString());
//		tradingSocket.sendMessage(traderStatusRequest);
//	}

	public void closeAllContracts() {

	}

	public void closeAllOrders() {
		CloseAllLimitOrders closeAllLimitOrders = new CloseAllLimitOrders();
		CloseAllLimitOrdersParams allLimitOrdersParams = new CloseAllLimitOrdersParams();
		closeAllLimitOrders.setId(idCounter++);
		closeAllLimitOrders.setMethod("cancelAllOrders");
		closeAllLimitOrders.setParams(allLimitOrdersParams);
		allLimitOrdersParams.setSymbol(symbol);
	}

	public void closeAllConditionalOrders() {
		CloseAllConditionalOrders closeAllConditionalOrders = new CloseAllConditionalOrders();
		CloseAllConditionalOrdersParams closeAllConditionalOrdersParams = new CloseAllConditionalOrdersParams();
		closeAllConditionalOrders.setParams(closeAllConditionalOrdersParams);
		closeAllConditionalOrders.setMethod("cancelCondOrder");
		closeAllConditionalOrdersParams.setAllForTrader(true);
		closeAllConditionalOrdersParams.setSymbol(symbol);
		tradingSocket.sendMessage(closeAllConditionalOrders);
		lastAction = closeAllConditionalOrders;
	}

	private void closeContracts(TraderStatusResponse traderStatus) {
		conterLog.debug("---- Closing all contracts ----");
		String orderID = UUID.randomUUID().toString();
		String orderSide = "";
		if (traderStatus.getData().getPositionType().contentEquals("SHORT")) {
			orderSide = "BUY";
		}

		if (traderStatus.getData().getPositionType().contentEquals("LONG")) {
			orderSide = "SELL";
		}

		PlaceOrder orderPlacement = new PlaceOrder();
		PlaceOrderParams placeOrderParams = new PlaceOrderParams();
		orderPlacement.setMethod("placeOrder");
		placeOrderParams.setOrdType("MARKET");
		placeOrderParams.setTimeInForce("IOC");
		placeOrderParams.setClOrdId(orderID);
		placeOrderParams.setSymbol(traderStatus.getData().getSymbol());
		placeOrderParams.setSide(orderSide);
		placeOrderParams.setQty(traderStatus.getData().getPositionContracts());
		orderPlacement.setParams(placeOrderParams);
		tradingSocket.sendMessage(orderPlacement);
		lastAction = orderPlacement;
	}

	private void closeContracts(OrderFilled filledOrder) {
		conterLog.debug("---- Closing  contracts ----");

		for (Contract contract : filledOrder.getData().getContracts()) {
			CloseContract closeContract = new CloseContract();
			closeContract.setId(Integer.valueOf(idCounter++));
			closeContract.setMethod("closeContract");
			CloseContractParams params = new CloseContractParams();
			closeContract.setParams(params);
			params.setContractId(contract.getContractId());
			params.setSymbol(filledOrder.getData().getSymbol());
			params.setOrdType(filledOrder.getData().getOrderType());
			params.setPx(contract.getEntryPx());
			tradingSocket.sendMessage(closeContract);
			lastAction = closeContract;
		}
	}

	public void closeCurrentOpenContracts() {
		filledOrders.stream().forEach(o -> closeContracts(o));
	}

	// For tomorrow... synch it... somehow
	public void emergencyStop() {
		conterLog.debug("######### Entering Emrgency Stop ###### -> " + priceFlip + " " + softEmergency);
		if (priceFlip < 3 && softEmergency) {
			conterLog.debug("######### Soft Emergency -> no Exit !! -> " + priceFlip + "  " + softEmergency);
			return;
		}

		Runnable emergencyRunner = new Runnable() {

			@Override
			public void run() {
				try {
					emergencyCountDownLatch = new CountDownLatch(1);

					fetchTraderStatus();
					emergencyCountDownLatch.await();

					while (traderStatus.getData().getConditionalOrders().size() > 0) {
						creationLog.debug("Closing All conditional Orders");
						closeAllConditionalOrders();
						emergencyCountDownLatch = new CountDownLatch(1);
						fetchTraderStatus();
						creationLog.debug("Waiting for Emergency Latch closeAllConditionalOrders");
						emergencyCountDownLatch.await();
						creationLog.debug("Going On !!");
					}

					cancelAllOrders();
					emergencyCountDownLatch = new CountDownLatch(1);
					fetchTraderStatus();
					creationLog.debug("Waiting for Emergency Latch cancelAllOrders");
					emergencyCountDownLatch.await();
					creationLog.debug("Going On !!");
					closeCurrentPosition();
					emergencyCountDownLatch = new CountDownLatch(1);
					fetchTraderStatus();
					creationLog.debug("Waiting for Emergency Latch closeCurrentPosition");
					emergencyCountDownLatch.await();
					creationLog.debug("Going On !!");
				} catch (InterruptedException e) {
					log.error(e);
				}

				filledOrders = new ArrayList<>();
				buyOrdersTriggered = new ArrayList<>();
				sellOrdersTriggered = new ArrayList<>();

				if (model.isNewOrderAfterFilling()) {
					createOrders(model);
				}

				emergencyBuyPrice = 0;
				emergencySellPrice = 100000;
				creationLog.debug("Emergency runner Finished ");
				creationLog.debug("Releasing  takeProfitCountdownLatch");
				if (takeProfitCountdownLatch != null) {
					takeProfitCountdownLatch.countDown();
				}
			}
		};

		Thread emergencyThread = new Thread(emergencyRunner);
		emergencyThread.start();
		conterLog.debug(
				"------------------------------------ Starting Thread ----------------" + emergencyThread.getId());

	}

	private void fetchTraderStatus() {
		TraderStatusRequest statusRequest = new TraderStatusRequest();
		StatusParams param = new StatusParams();
		param.setSymbol(symbol);
		statusRequest.setParams(param);
		statusRequest.setId(idCounter++);
		statusRequest.setMethod("getTraderStatus");
		tradingSocket.sendMessage(statusRequest);
		lastAction = statusRequest;
	}

	private void cancelAllOrders() {
		conterLog.debug("------------   Cancelling all Orders --------------");
		CloseAllOrders closeAllOrders = new CloseAllOrders();
		CloseAllOrdersParams closeAllOrdersParams = new CloseAllOrdersParams();
		closeAllOrders.setMethod("cancelAllOrders");
		closeAllOrders.setId(idCounter++);
		closeAllOrders.setParams(closeAllOrdersParams);
		closeAllOrdersParams.setSymbol(symbol);
		tradingSocket.sendMessage(closeAllOrders);
		lastAction = closeAllOrders;
	}

	public void closeCurrentPosition() {

		if (filledOrders.size() == 0) {
			conterLog.debug("###################  ClosePosition left -> filled orders empty ####");
			return;
		}

		orderBookManager = new OrderBookManager();
		orderBookManager.setOrderBook(this.orderBook);
		OrderFilled orderFilled = filledOrders.get(filledOrders.size() - 1);

		log.debug("Filled Orders SIZE" + filledOrders.size());
		log.debug("Position to close " + orderFilled.getData().getPx() + " " + orderFilled.getData().getOrderSide()
				+ " " + orderFilled.getData().getPositionType());
		OrderSide orderSide = null;
		if (orderFilled.getData().getOrderSide() == null) {

			if (orderFilled.getData().getPositionType().contentEquals("SHORT")) {
				orderSide = OrderSide.SELL;
			}

			if (orderFilled.getData().getPositionType().contentEquals("LONG")) {
				orderSide = OrderSide.BUY;
			}

		} else {
			orderSide = OrderSide.valueOf(orderFilled.getData().getOrderSide());
		}

		if (orderBookManager.oposingOrdersExist(orderFilled.getData().getPx(), orderSide)) {
			conterLog.debug("!!!!!!!!! OPOSIN CONTRACTS AVAILABEL  !!!!!!!!!!! ");
			closeCurrentPosition(orderFilled);
		} else {
			conterLog.debug("!!!!!!!!! NO OPOSING CONTRACTS !!!!!!!!!!! " + currentOrderToObserve);
			if (currentOrderToObserve != null) {
				conterLog.debug("######## Emergency-Stop creating conterOrder #############");
				createConterMarketOrder(currentOrderToObserve);
			}
		}

		orderFilled.setAlreadyContered(true);

		currentOrderToObserve = null;
	}

	private void closeCurrentPosition(OrderFilled orderFilled) {
		conterLog.debug("---- Close current Position ----");
		ClosePositionRequest closePositionRquest = new ClosePositionRequest();
		ClosePositionParams params = new ClosePositionParams();
		closePositionRquest.setParams(params);
		closePositionRquest.setId(idCounter++);
		closePositionRquest.setMethod("closePosition");

		params.setOrdType(orderFilled.getData().getOrderType());
		params.setPx(orderFilled.getData().getPx());
		params.setSymbol(orderFilled.getData().getSymbol());
		tradingSocket.sendMessage(closePositionRquest);
		lastAction = closePositionRquest;
		conterLog.debug("---- Close current Position sending ----" + closePositionRquest);
	}

	public void takeProfit() {

		Runnable takeProfitRunner = new Runnable() {

			@Override
			public void run() {
				try {

//				if(currentFilledOrder != null) {
//					takeProfitCountdownLatch = new CountDownLatch(1);
//					createConterMarketOrder(currentFilledOrder);
//					creationLog.debug("Waiting for ConterTrade takeProfitCountdownLatch");
//					takeProfitCountdownLatch.await();					
//				}

					takeProfitCountdownLatch = new CountDownLatch(1);
					emergencyStop();
					creationLog.debug("Waiting for takeProfitCountdownLatch");
					takeProfitCountdownLatch.await();
					creationLog.debug("takeProfitCountdownLatch released");
				} catch (InterruptedException e) {
					log.error(e);
				}
				createOrders(model);
			}
		};

		Thread takeProfitThread = new Thread(takeProfitRunner);
		takeProfitThread.start();
		conterLog.debug(
				"------------------------------------ Starting Thread ----------------" + takeProfitThread.getId());
	}

	public void takeProfitForButton() {

		Runnable takeProfitRunner = new Runnable() {

			@Override
			public void run() {
				try {

//				if(currentFilledOrder != null) {
//					takeProfitCountdownLatch = new CountDownLatch(1);
//					createConterMarketOrder(currentFilledOrder);
//					creationLog.debug("Waiting for ConterTrade takeProfitCountdownLatch");
//					takeProfitCountdownLatch.await();					
//				}

					takeProfitCountdownLatch = new CountDownLatch(1);
					emergencyStop();
					creationLog.debug("Waiting for takeProfitCountdownLatch");
					takeProfitCountdownLatch.await();
					creationLog.debug("takeProfitCountdownLatch released");
				} catch (InterruptedException e) {
					log.error(e);
				}
				createOrders(model);
			}
		};

		Thread takeProfitThread = new Thread(takeProfitRunner);
		takeProfitThread.start();
		conterLog.debug(
				"------------------------------------ Starting Thread ----------------" + takeProfitThread.getId());
	}

	private void emergencyStopAndOrderCreationThreadExecustion() {
		Runnable takeProfitRunner = new Runnable() {

			@Override
			public void run() {
				takeProfitCountdownLatch = new CountDownLatch(1);
				emergencyStop();
				try {
					creationLog.debug("Waiting for takeProfitCountdownLatch");
					takeProfitCountdownLatch.await();
					creationLog.debug("takeProfitCountdownLatch released");
				} catch (InterruptedException e) {
					log.error(e);
				}
				if (model.isTradeNonStop()) {
					createOrders(model);
				}
			}
		};

		Thread takeProfitThread = new Thread(takeProfitRunner);
		takeProfitThread.start();
		conterLog.debug(
				"------------------------------------ Starting Thread ----------------" + takeProfitThread.getId());
	}

	private void createConterMarketOrder(OrderFilled orderFilled) {
		conterLog.debug("####### Creating Market Conter Order Called " + orderFilled);
		if (orderFilled.getData().getClOrdId().contains(CNTM) || orderFilled.getData().getClOrdId().contains(CNTO)
				|| orderFilled.getData().getNewClOrdId().contains(CNTM)
				|| orderFilled.getData().getNewClOrdId().contains(CNTO)
				|| orderFilled.getData().getOrigClOrdId().contains(CNTM)
				|| orderFilled.getData().getOrigClOrdId().contains(CNTO)) {
			return;
		}

		log.debug("####### Market Conter Order " + orderFilled);
		conterLog.debug("####### Creating Market Conter Order " + orderFilled);

		String orderID = CNTM + UUID.randomUUID().toString();
		String orderSide = "";

		if (orderFilled == null || orderFilled.getData() == null || orderFilled.getData().getPositionType() == null) {
			return;
		}

		if (orderFilled.getData().getPositionType().contentEquals("SHORT")) {
			orderSide = "BUY";
		}

		if (orderFilled.getData().getPositionType().contentEquals("LONG")) {
			orderSide = "SELL";
		}

		PlaceOrder orderPlacement = new PlaceOrder();
		PlaceOrderParams placeOrderParams = new PlaceOrderParams();
		orderPlacement.setMethod("placeOrder");
		placeOrderParams.setOrdType("MARKET");
		placeOrderParams.setTimeInForce("IOC");
		placeOrderParams.setClOrdId(orderID);
		placeOrderParams.setSymbol(orderFilled.getData().getSymbol());
		placeOrderParams.setSide(orderSide);
		placeOrderParams.setQty(orderFilled.getData().getPositionContracts());
		orderPlacement.setParams(placeOrderParams);
		tradingSocket.sendMessage(orderPlacement);
		lastAction = orderPlacement;
		conterLog.debug("####### Placed Market Conter Order " + orderPlacement);
	}

	private void createConterMarketOrderSamePrice(OrderFilled orderFilled) {
		conterLog.debug("####### Creating Market Conter Order Called " + orderFilled);
		if (orderFilled.isConterOrder()) {
			return;
		}

		log.debug("####### Market Conter SAME-PRICE Order EXO " + orderFilled);
		conterLog.debug("####### Creating Market SAME-PRICE Conter Order EXO" + orderFilled);

		String orderID = CNTMEXO + UUID.randomUUID().toString();
		String orderSide = "";

		if (orderFilled == null || orderFilled.getData() == null || orderFilled.getData().getPositionType() == null) {
			return;
		}

		if (orderFilled.getData().getPositionType().contentEquals("SHORT")) {
			orderSide = "BUY";
		}

		if (orderFilled.getData().getPositionType().contentEquals("LONG")) {
			orderSide = "SELL";
		}

		PlaceOrder orderPlacement = new PlaceOrder();
		PlaceOrderParams placeOrderParams = new PlaceOrderParams();
		orderPlacement.setMethod("placeOrder");
		placeOrderParams.setOrdType("MARKET");
		placeOrderParams.setTimeInForce("IOC");
		placeOrderParams.setClOrdId(orderID);
		placeOrderParams.setSymbol(orderFilled.getData().getSymbol());
		placeOrderParams.setSide(orderSide);
		placeOrderParams.setQty(orderFilled.getData().getPositionContracts());
		orderPlacement.setParams(placeOrderParams);
		tradingSocket.sendMessage(orderPlacement);
		lastAction = orderPlacement;
		conterLog.debug("####### Placed Market Conter Order " + orderPlacement);
	}

	@Override
	public void onOrderFilled(OrderFilled orderFilled) {

		tradesSocket.setFlipPrice(String.valueOf(orderFilled.getData().getPx()));

		boolean orderExistsInArray = false;
		log.debug("onOderFilled " + orderFilled.getData().getOrderStatus());
		conterLog.debug("OnOrderFilled -> following order Filled " + orderFilled);

		ObjectMessage msg = new ObjectMessage(orderFilled);
		contractsLog.debug(msg);
		contractsCSVLog.debug("", simpleDateFormat.format(new Date(orderFilled.getData().getTimestamp())),
				orderFilled.getData().getOrderStatus(), orderFilled.getData().getClOrdId().replaceAll("\\W", ""),
				orderFilled.getData().getNewClOrdId().replaceAll("\\W", ""),
				orderFilled.getData().getOrigClOrdId().replaceAll("\\W", ""), orderFilled.getData().getOrderType(),
				orderFilled.getData().getOrderSide(), orderFilled.getData().getOrigQty(),
				orderFilled.getData().getMarketTrades().get(0).getQty(),
				(orderFilled.getData().getOrigQty() - orderFilled.getData().getQty()), orderFilled.getData().getQty(),
				orderFilled.getData().getPx());

		if (!orderFilled.getData().getOrderType().contentEquals("MARKET")) {
			filledOrders.add(orderFilled);
		} else {
			if (takeProfitCountdownLatch != null) {
				takeProfitCountdownLatch.countDown();
				creationLog.debug("takeProfitCountdownLatch released from OnOrderFilled");
				conterLog.debug("takeProfitCountdownLatch released from OnOrderFilled");
			}
		}

		if (!orderFilled.isConterOrder()) {
			currentOrderToObserve = orderFilled;
			currentObservedOrderId = currentOrderToObserve.getData().getClOrdId();
			conterLog.debug(" --> Order to for observation ->" + currentOrderToObserve);
			conterLog.debug(" --> Order to for observation ID ->" + currentObservedOrderId);
		} else {
			if (!orderFilled.isConterMarketOrder()
					&& orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.FILLED.name())) {
				conterLog.debug("###### Seting soft emergency mode for conter LIMIT-ORDER ##################");
				conterLog.debug("###### LIMIT-OBSERVE ORDER set to NULL ##################");
				softEmergency = false;
				filledOrders.clear();
				currentOrderToObserve = null;
				currentObservedOrderId = "";
			}
			if ((orderFilled.isExitLimitOrder() || orderFilled.isConterMarketOrder())
					&& orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.FILLED.name())) {
				conterLog.debug("###### EXIT-Limit-Order bzw ConterMarket is filled ##################");
				conterLog.debug(
						"###### Corresponding Limitorder has to be closed ##################->" + actualConterOrder);
				if (actualConterOrder != null) {
					closeOrder(actualConterOrder);
				}
				filledOrders.clear();
				currentOrderToObserve = null;
				currentObservedOrderId = "";
			}
		}

		currentFilledOrder = orderFilled;

		if (orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.FILLED.name())) {
			updateOrderList(buyOrdersTriggered, orderFilled);
			updateOrderList(sellOrdersTriggered, orderFilled);
			// Falls conterorder filled... aus liste entfernen
			if (orderFilled.isConterOrder()) {
				removeFromActiveConterOrders(orderFilled);
			}
		}

		updateNumberOfActiveOrders();

		model.setNummbeOfFilledOrders(orderFilled.getData().getPositionContracts().toString());

		conterLog.debug("Order Filled  " + orderFilled.getData().getClOrdId());

		List<Integer> delPrices = new ArrayList<>();

		delPrices.add(orderFilled.getData().getPx());
		List<TradeModel> delTrades = new ArrayList<>();

		if (!longtradeListModel.isEmpty()) {
			for (TradeModel trade : longtradeListModel) {
				for (Integer delPrice : delPrices) {
					if (trade.getPrice() == delPrice) {
						delTrades.add(trade);
					}
				}
			}
		}

		longtradeListModel.removeAll(delTrades);

		delTrades.clear();

		if (!shorttradeListModel.isEmpty()) {
			for (TradeModel trade : shorttradeListModel) {
				for (Integer delPrice : delPrices) {
					if (trade.getPrice() == delPrice) {
						delTrades.add(trade);
					}
				}
			}
		}

		shorttradeListModel.removeAll(delTrades);

		if (orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.FILLED.name())
				|| orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.PARTIALLY_FILLED.name())) {
			log.debug("Call ConterTradeCreation ");
			if (model.isCreateConterOrder() && !orderFilled.isConterOrder()) {
				conterLog.debug("Regular System Order is Filled creating ConterOrder " + orderFilled);
				Runnable conterOrderRunner = new Runnable() {
					@Override
					public void run() {
						createConterOrder(orderFilled);
					}
				};
				Thread conterOrderThread = new Thread(conterOrderRunner);
				conterOrderThread.setName("conterOrderThread");
				conterOrderThread.start();
			}
		}

		if (model.isNewOrderAfterFilling()) {
			// createOrderThreadExecution();
		} else {
			// fetchTraderStatus();
		}

		if (!model.isCreateConterOrder()) {
			cancelAllOrders();
		}

		// Conterorder wurde ausgeführt -> entweder Gewinn oder verlustfreier
		// Ausstieg...dann neu aufstellen
		if (orderFilled.isConterOrder()
				&& orderFilled.getData().getOrderStatus().contentEquals(OrderStatusEnum.FILLED.name())
				&& model.isTradeNonStop()) {
			conterLog.debug("### Trading nonstop -- Restarting ####");
			takeProfit();
		}

	}

	private void removeFromActiveConterOrders(OrderFilled orderFilled) {
		List<OrderStatus> filteredList = activeConterOrders.stream().filter(new Predicate<OrderStatus>() {
			@Override
			public boolean test(OrderStatus status) {
				return !status.getData().getClOrdId().contentEquals(orderFilled.getData().getClOrdId());
			}
		}).collect(Collectors.toList());
		activeConterOrders = filteredList;
	}

	private void removeFromActiveConterOrders(String id) {
		List<OrderStatus> filteredList = activeConterOrders.stream().filter(new Predicate<OrderStatus>() {
			@Override
			public boolean test(OrderStatus status) {
				return !status.getData().getClOrdId().contentEquals(id);
			}
		}).collect(Collectors.toList());
		activeConterOrders = filteredList;
	}

	private void closeOrder(OrderStatus order) {
		conterLog.debug("---- Close  conterorder ----");
		CancelOrder cancelOrder = new CancelOrder();
		CancelOrderParams params = new CancelOrderParams();
		cancelOrder.setId(idCounter++);
		cancelOrder.setParams(params);
		cancelOrder.setMethod("cancelOrder");
		params.setSymbol(order.getData().getSymbol());
		params.setClOrdId(order.getData().getClOrdId());
		conterLog.debug(" --> Closing Conter Order " + cancelOrder);
		tradingSocket.sendMessage(cancelOrder);
		lastAction = cancelOrder;
	}

	private void cancelOrder(OrderFilled orderFilled) {
		conterLog.debug("---- Close  conterorder ----");
		CancelOrder cancelOrder = new CancelOrder();
		CancelOrderParams params = new CancelOrderParams();
		cancelOrder.setId(idCounter++);
		cancelOrder.setParams(params);
		cancelOrder.setMethod("cancelOrder");
		params.setSymbol(orderFilled.getData().getSymbol());
		params.setClOrdId(orderFilled.getData().getClOrdId());
		conterLog.debug(" --> Closing Conter Order " + cancelOrder);
		tradingSocket.sendMessage(cancelOrder);
		lastAction = cancelOrder;
	}

	@Override
	public void onContractClosed(ContractClosed contractClosed) {
		// fetchTraderStatus();
		// createOrders(model);
	}

	@Override
	public void onTraderStatuschanged(TraderStatusResponse traderStatus) {
		this.traderStatus = traderStatus;

		if (emergencyCountDownLatch != null) {
			emergencyCountDownLatch.countDown();
			creationLog.debug("Releasing Emergency Latch");
		}

	}

	@Override
	public void onConnectionClosed(BotSocketEnum socket) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnectionEstablished(BotSocketEnum socket) {
		// fetchTraderStatus();
	}

	private String fetchActiveOrders(List<OrderStatus> orders) {
		numberOfOrders = 0;
		orders.stream().forEach(o -> numberOfOrders += o.getData().getQty());
		return String.valueOf(numberOfOrders);
	}

	public void updateNumberOfActiveOrders() {
		log.debug("#### Number of Active Buy Orders -> " + fetchActiveOrders(buyOrdersTriggered));
		log.debug("#### Number of Active Sell  Orders -> " + fetchActiveOrders(sellOrdersTriggered));
		model.setActiveSellOrders(fetchActiveOrders(sellOrdersTriggered));
		model.setActiveBuyOrders(fetchActiveOrders(buyOrdersTriggered));
	}

	public int fetchEmergencyBuyPrice() {
		List<OrderStatus> orders = buyOrdersTriggered.stream().sorted().collect(Collectors.toList());
		Collections.reverse(orders);
		creationLog.debug("---->>Emergency Buy Price " + (orders.get(0).getData().getPx() - tickValue) + " BuyPrices "
				+ buyOrdersTriggered.size());
		return orders.get(0).getData().getPx() - tickValue;

	}

	public int fetchEmergencySellPrice() {
		List<OrderStatus> orders = sellOrdersTriggered.stream().sorted().collect(Collectors.toList());
		creationLog.debug("---->>Emergency Sell Price " + (orders.get(0).getData().getPx() + tickValue) + " SellPrices "
				+ sellOrdersTriggered.size());
		return orders.get(0).getData().getPx() + tickValue;
	}

	private int callculateOrdersTickDistance() {
		if (buyOrdersTriggered.isEmpty() || sellOrdersTriggered.isEmpty()) {
			return 100;
		}
		List<OrderStatus> buyOrders = buyOrdersTriggered.stream().sorted().collect(Collectors.toList());
		List<OrderStatus> sellOrders = sellOrdersTriggered.stream().sorted().collect(Collectors.toList());
		// Collections.reverse(sellOrders);
		int highestBuyPrive = buyOrders.get(0).getData().getPx();
		int lowestSellPrice = sellOrders.get(sellOrders.size() - 1).getData().getPx();
		return (lowestSellPrice - highestBuyPrive) / tickValue;
	}

	public void initInfinityTradeTask() {
		InfinityTrader infinityTrader = new InfinityTrader();
		executionService.scheduleAtFixedRate(infinityTrader, 5, 10, TimeUnit.SECONDS);
	}

	public void shutDownInfinityTrade() {
		executionService.shutdown();
	}

	private class InfinityTrader implements Runnable {

		@Override
		public void run() {
			creationLog.debug("############ Infinity runner Scheduled ################### + Callculated TickDistance "
					+ callculateOrdersTickDistance());
			if (currentFilledOrder != null) {
				Timestamp currentTime = new Timestamp(System.currentTimeMillis());
				Date currentDate = new Date(currentTime.getTime());
				Date modificationDate = new Date(currentFilledOrder.getData().getTimestamp());

				DateTime now = DateTime.now();
				DateTime lastModification = new DateTime(currentFilledOrder.getData().getTimestamp());

				long seconds = (now.getMillis() - lastModification.getMillis()) / 1000;

				// if(seconds > 50 && Integer.valueOf(model.getNummbeOfFilledOrders()) == 0) {
				// if(seconds > 120 && callculateOrdersTickDistance() > 2 &&
				// Integer.valueOf(model.getNummbeOfFilledOrders()) == 0) {
				if (seconds > 300 && Integer.valueOf(model.getNummbeOfFilledOrders()) == 0) {
					creationLog.debug("############ Infinity runner Takes Profit and Reruns ###################");
					takeProfit();
					currentFilledOrder.getData().setTimestamp(now.getMillis());
				}
			}
		}
	}

	public void printOrderBook() {
		ObjectMessage msg = new ObjectMessage(orderBook);
		contractsLog.debug(msg);
	}

	public void fillUpOrderBookGaps(List<List<Integer>> orderBookEntries) {
		List<List<Integer>> entries = orderBookEntries;
		List<List<Integer>> gaps = new ArrayList<List<Integer>>();

		if (entries == null) {
			return;
		}
		creationLog.debug(orderBookEntries);

		for (int i = 0; i < entries.size(); i++) {
			if (i + 1 < entries.size()) {
				int askPrice = entries.get(i).get(0);
				int nextAskPrice = entries.get(i + 1).get(0);
				int ticksBetween = (nextAskPrice - askPrice) / tickValue;
				if (ticksBetween > 1) {
					for (int j = 0; j < ticksBetween - 1; j++) {
						List<Integer> gap = new ArrayList<Integer>();
						// gap.add(i+j+1);
						gap.add(i + 1);
						gap.add(entries.get(i).get(0) + ((j + 1) * tickValue));
						gaps.add(gap);
					}
				}
			}
		}

		creationLog.debug(gaps);
		int indexCounter = 1;

		List<List<List<Integer>>> askLists = new ArrayList<List<List<Integer>>>();

		Collections.reverse(gaps);

		for (int i = 0; i < gaps.size(); i++) {
			List<Integer> gap = gaps.get(i);
			List<Integer> insertGap = new ArrayList<>();
			insertGap.add(gap.get(1));
			insertGap.add(0);
			entries.add(gap.get(0), insertGap);
			// askLists.add(asks.subList(fromIndex, toIndex));
			creationLog.debug("Gap Value " + gap.get(1) + " index " + gap.get(0) + " calculated index " + (gap.get(0)));
		}
		creationLog.debug(orderBookEntries);
	}

	public void fillUpOrderBookBidGaps(List<List<Integer>> orderBookEntries) {
		List<List<Integer>> entries = orderBookEntries;
		List<List<Integer>> gaps = new ArrayList<List<Integer>>();

		creationLog.debug(orderBookEntries);

		if (entries == null) {
			return;
		}
		for (int i = entries.size() - 1; i > 0; i--) {
			if (i - 1 > 0) {
				int bidPrice = entries.get(i).get(0);
				int beforeBidPrice = entries.get(i - 1).get(0);
				int ticksBetween = (beforeBidPrice - bidPrice) / tickValue;
				if (ticksBetween > 1) {
					for (int j = 0; j < ticksBetween - 1; j++) {
						List<Integer> gap = new ArrayList<Integer>();
						// gap.add(i+j+1);
						gap.add(i);
						gap.add(entries.get(i).get(0) + ((j + 1) * tickValue));
						gaps.add(gap);
					}
				}
			}
		}

		creationLog.debug(gaps);
		int indexCounter = 1;

		List<List<List<Integer>>> askLists = new ArrayList<List<List<Integer>>>();

		// Collections.reverse(gaps);

		for (int i = 0; i < gaps.size(); i++) {
			List<Integer> gap = gaps.get(i);
			List<Integer> insertGap = new ArrayList<>();
			insertGap.add(gap.get(1));
			insertGap.add(0);
			entries.add(gap.get(0), insertGap);
			// askLists.add(asks.subList(fromIndex, toIndex));
			creationLog.debug("Gap Value " + gap.get(1) + " index " + gap.get(0) + " calculated index " + (gap.get(0)));
		}
		creationLog.debug(orderBookEntries);
	}

	public OrderBookData getOrderBook() {
		return orderBook;
	}

	public void setOrderBook(OrderBookData orderBook) {
		this.orderBook = orderBook;
	}

	public synchronized void closeLastConterOrder() {
		conterLog.debug("---- Close last conterorder ----" + actualConterOrder);
		if (actualConterOrder != null) {
			CancelOrder cancelOrder = new CancelOrder();
			CancelOrderParams params = new CancelOrderParams();
			cancelOrder.setId(idCounter++);
			cancelOrder.setParams(params);
			cancelOrder.setMethod("cancelOrder");
			params.setSymbol(actualConterOrder.getData().getSymbol());
			params.setClOrdId(actualConterOrder.getData().getClOrdId());
			conterLog.debug(" --> Closing Conter Order " + cancelOrder);
			tradingSocket.sendMessage(cancelOrder);
			lastAction = cancelOrder;
		}
	}

	public synchronized void closeLastConterOrders() {
		Runnable closeOrdersRunner = new CloseOrdersRunner(tradingSocket, lastAction, activeConterOrders, idCounter);

		Thread closeConterOrdersThread = new Thread(closeOrdersRunner);
		closeConterOrdersThread.setName("closeConterOrdersThread");
		closeConterOrdersThread.start();
		//activeConterOrders.notifyAll();
	}

	@Override
	public void onPriceFlipChanged(int priceFlip) {
		this.priceFlip = priceFlip;
	}

	@Override
	public void onRequestLimitExceeded() {

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					log.error(e);
				}
				tradingSocket.sendMessage(lastAction);
				conterLog.debug("############ Limit Exceeded -> Last Action repeated -> " + lastAction);
			}
		};

		Thread runner = new Thread(runnable);
		runner.start();
		conterLog.debug("------------------------------------ Starting Thread ----------------" + runner.getId());
	}
}
