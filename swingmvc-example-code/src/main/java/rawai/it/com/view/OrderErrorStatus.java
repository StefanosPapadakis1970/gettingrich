package rawai.it.com.view;

public enum OrderErrorStatus {
	ID_ALREADY_EXISTS(3, "ID already exists"), ID_DOESNOT_EXIST(10, "ID doesn't exist"),
	UNKNOWN_TRADER(14, "Unknown trader"), INVALID_LEVERAGE(18, "Invalid leverage"), INVALID_PRICE(19, "Invalid price"),
	INVALID_QUANTITY(20, "Invalid quantity"), NO_MARKET_PRICE(22, "No market price"),
	NOT_ENOUGH_BALANCE(27, "Not enough balance"), INVALID_CONTRACT_ID(34, "Invalid contract ID"),
	RATE_LIMIT_EXCEEDED(35, "Rate limit exceeded"), NO_CONTRACTS(36, "No contracts"),
	NO_OPPOSING_ORDERS(37, "No opposing orders"),
	PRICE_IS_WORSE_THAN_LIQUIDATION_PRICE(40, "Price is worse than liquidation price"),
	TOURNAMENT_IN_PROGRESS(45, "Tournament in progress"), MAX_QUANTITY_EXCEEDED(53, "Max quantity exceeded"),
	PNL_IS_TOO_NEGATIVE(54, "PnL is too negative"), ORDER_WOULD_BECOME_INVALID(55, "Order would become invalid"),
	TRADING_SUSPENDED(58, "Trading suspended"), CANNOT_BE_FILLED(63, "Can't be filled"),
	TOO_MANY_CONDITIONAL_ORDERS(65, "Too many conditional orders"), TOO_MANY_ORDERS(68, "Too many orders"),
	BAD_REQUEST(3001, "Bad request"), NOT_IMPLEMENTED(3011, "Not implemented"), INTERNAL_ERROR(3012, "Internal error"),
	NOT_AUTHORIZED(3013, "Not authorized"), ALREADY_AUTHORIZED(3014, "Already authorized"),
	TRADING_IS_NOT_AVAILABLE(3015, "Trading is not available"),
	AUTHENTICATION_IN_PROGRESS(3016, "Authentication in progress"),
	REQUEST_LIMIT_EXCEEDED(3017, "Request limit exceeded"), INVALID_CREDENTIALS(10501, "Invalid credentials");

	private int errorCode;
	private String errorMessage;

	OrderErrorStatus(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	OrderErrorStatus error(int errorCode){
	  switch(errorCode){
	     case 3: return ID_ALREADY_EXISTS;
	     case 10: return ID_DOESNOT_EXIST;
	     case 14: return UNKNOWN_TRADER;
	     case 18: return INVALID_LEVERAGE;
	     case 19: return INVALID_PRICE;
	     case 20: return INVALID_QUANTITY;
	     case 22: return NO_MARKET_PRICE;
	     case 27: return NOT_ENOUGH_BALANCE;
	     case 34: return INVALID_CONTRACT_ID;
	     case 35: return RATE_LIMIT_EXCEEDED;
	     case 36: return NO_CONTRACTS;
	     case 37: return NO_OPPOSING_ORDERS;
	     case 40: return PRICE_IS_WORSE_THAN_LIQUIDATION_PRICE;
	     case 45: return TOURNAMENT_IN_PROGRESS;
	     case 53: return MAX_QUANTITY_EXCEEDED;
	     case 54: return PNL_IS_TOO_NEGATIVE;
	     case 55: return ORDER_WOULD_BECOME_INVALID;
	     case 58:   return TRADING_SUSPENDED;
	     case 63:   return CANNOT_BE_FILLED;
	     case 65:   return TOO_MANY_CONDITIONAL_ORDERS;
	     case 68:   return TOO_MANY_ORDERS;
	     case 3001: return BAD_REQUEST;
	     case 3011: return NOT_IMPLEMENTED;
	     case 3012: return INTERNAL_ERROR;
	     case 3013: return NOT_AUTHORIZED; 
	     case 3014: return ALREADY_AUTHORIZED;
	     case 3015: return TRADING_IS_NOT_AVAILABLE;
	     case 3016: return AUTHENTICATION_IN_PROGRESS;
	     case 3017: return REQUEST_LIMIT_EXCEEDED;
	     case 10501: return INVALID_CREDENTIALS;	     
         default: return null;	     
	  }
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
