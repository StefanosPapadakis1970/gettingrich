package rawai.it.com.view.orderbook;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import rawai.it.com.websockets.TradingSocket;

public class NextAction extends BaseSocketAction{

	public NextAction(String name, TradingSocket tradingSocket, String message) {
		super(name, tradingSocket, message);
	}
}
