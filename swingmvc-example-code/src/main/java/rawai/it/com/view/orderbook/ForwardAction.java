package rawai.it.com.view.orderbook;

import rawai.it.com.websockets.TradingSocket;

public class ForwardAction extends BaseSocketAction {

	public ForwardAction(String name, TradingSocket tradingSocket, String message) {
		super(name, tradingSocket, message);
	}
}
