package rawai.it.com.view.orderbook;

import rawai.it.com.model.json.trading.orderfilled.OrderFilled;

public class OrderBookTableEntry {
	
	private OrderFilled buyOrder;
	private OrderFilled sellOrder;
	private String asks;
	private String bids;
	private String price;

	public OrderBookTableEntry() {
		
	}
	
	public OrderBookTableEntry(OrderFilled buyOrder, OrderFilled sellOrder, String aks, String bids, String price) {
		super();
		this.buyOrder = buyOrder;
		this.sellOrder = sellOrder;
		this.asks = aks;
		this.bids = bids;
		this.price = price;
	}
	public OrderFilled getBuyOrder() {
		return buyOrder;
	}
	public void setBuyOrder(OrderFilled buyOrder) {
		this.buyOrder = buyOrder;
	}
	public OrderFilled getSellOrder() {
		return sellOrder;
	}
	public void setSellOrder(OrderFilled sellOrder) {
		this.sellOrder = sellOrder;
	}
	public String getAsks() {
		return asks;
	}
	public void setAsks(String asks) {
		this.asks = asks;
	}
	public String getBids() {
		return bids;
	}
	public void setBids(String bids) {
		this.bids = bids;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

}
