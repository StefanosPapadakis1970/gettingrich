package rawai.it.com.view.orderbook;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;

import rawai.it.com.model.TradeModel;

public class OrderBookAdapter extends AbstractTableAdapter<OrderBookTableEntry>{

	private static final long serialVersionUID = 4122166556796766643L;


	public OrderBookAdapter(ListModel<OrderBookTableEntry> listModel, String[] columnNames) {
        super(listModel, columnNames);
    }

	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		OrderBookTableEntry orderBookEntry = (OrderBookTableEntry) getRow(rowIndex);
		switch(columnIndex) {
		case 0:
			return orderBookEntry.getBuyOrder();
		case 1:
			return orderBookEntry.getBids();
		case 2:
			return orderBookEntry.getPrice();
		case 3:
			return orderBookEntry.getAsks();
		case 4:
			return orderBookEntry.getSellOrder();
		}
		return null;
	}

}
