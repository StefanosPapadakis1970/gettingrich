package rawai.it.com.view.orderbook;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

class OrderBookPriceCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 2341921981643076151L;
	private String price = "";
	private String fillPrice = "";
	private int priceRow = 0;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		JLabel cellRenderer = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
				column);

		if (price.contentEquals(value.toString())) {
			cellRenderer.setBackground(Color.cyan);
		} else {
			cellRenderer.setBackground(Color.YELLOW);
		}
		
		if (fillPrice.contentEquals(value.toString())) {
			cellRenderer.setBorder(BorderFactory.createLineBorder(Color.RED, 4));				
		}		

		return cellRenderer;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getPriceRow() {
		return priceRow;
	}

	public void setPriceRow(int priceRow) {
		this.priceRow = priceRow;
	}

	public String getFillPrice() {
		return fillPrice;
	}

	public void setFillPrice(String fillPrice) {
		this.fillPrice = fillPrice;
	}
}