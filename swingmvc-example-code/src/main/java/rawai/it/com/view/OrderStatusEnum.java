package rawai.it.com.view;

public enum OrderStatusEnum {
	PENDING, ACCEPTED, REJECTED, CANCELLED, FILLED, PARTIALLY_FILLED, TERMINATED, EXPIRED, TRIGGERED;
}
