package rawai.it.com.view.orderbook;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.view.OrderSide;

public class BuySellCellRenderer extends DefaultTableCellRenderer {
	
	private static Logger log = LogManager.getLogger("ordermanager");
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		OrderFilled order = (OrderFilled)value;
				
		if (order.getData() != null && order.getData().getOrigQty() != null) {
				label.setText(order.getData().getOrigQty() +" "+(order.getData().getOrigQty()-order.getData().getQty())+" "+order.getData().getQty());
				if (order.isConterOrder()) {
					label.setBackground(Color.PINK);
				} 
				else if (order.isSystemOrder()) {
					label.setBackground(Color.GREEN);
				} else {
					label.setBackground(Color.MAGENTA);
				}
			}
		
		
		if (order.isDisplayOrder()){
			label.setText("");
			label.setBackground(Color.WHITE);	
		}	
		
		
		
		return label;
	}

}
