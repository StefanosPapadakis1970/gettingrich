package rawai.it.com.view;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.print.attribute.standard.Media;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.table.TableRowSorter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.RadioButtonAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.view.ValidationResultViewFactory;

import rawai.it.com.controller.OrderManager;
import rawai.it.com.listeners.ConnectionListener;
import rawai.it.com.listeners.OrderPlacementListener;
import rawai.it.com.listeners.TraderStatusListener;
import rawai.it.com.model.TradeModel;
import rawai.it.com.model.TradeModelTableAdapter;
import rawai.it.com.model.TradingBotModel;
import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrder;
import rawai.it.com.model.json.trading.conditionalorder.cancelorder.CancelOrderParams;
import rawai.it.com.model.json.trading.conditionalorder.status.ConditionalOrderStatus;
import rawai.it.com.model.json.trading.conditionalorder.status.cancelled.ConditionalOrderCancelledStatus;
import rawai.it.com.model.json.trading.contractclosed.ContractClosed;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.model.json.trading.traderstatus.response.TraderStatusResponse;
import rawai.it.com.view.orderbook.OrderBookView;
import rawai.it.com.websockets.OrderBookSocket;
import rawai.it.com.websockets.TradesSocket;
import rawai.it.com.websockets.TradingSocket;

import java.io.File;


public class TradingBotView extends JPanel implements OrderPlacementListener, TraderStatusListener, ConnectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8526981919733544371L;

	private static Logger log = LogManager.getLogger(TradingBotView.class);
	private static Logger conterLog = LogManager.getLogger("conterTrade");

	private JTextField maximumOrders;
	private JTextField maximumOrdersProcent;
	private JTextField numberOfContracts;
	private JTextField minOrderTicksDistance;
	private JTextField alarmDistance;
	private JTextField cancelDistance;
	private JCheckBox cancelOrders;
	private JTextField url;
	private JTextField tradingUrl;
	private JTextField apiKey;
	private JTextField numberOfOrdersToPlace;
	private JTable shortTradeTable;
	private JTable longTradeTable;

	private JTextField price = new JTextField();

	private TradingBotModel model;
	private ArrayListModel<TradeModel> longtradeListModel;
	private ArrayListModel<TradeModel> shorttradeListModel;

	private JPanel pricePanel;
	private JLabel priceLabel;

	private JPanel remainingOrdersPanel;
	private JLabel remainingOrdersLabel;

	private JRadioButton absolutRadio = new JRadioButton("Absolut");
	private JRadioButton percentRadio = new JRadioButton("Procent");

	private int counter = 0;

	private OrderManager orderManager;

	private TradesSocket tradeSocket;
	private OrderBookSocket orderBookSocket;
	private TradingSocket tradingSocket;	
	
	ValidationResult validationResult = new ValidationResult();

	private final ValidationResultModel validationResultModel = new DefaultValidationResultModel();

	private JCheckBox createConterOrderCheckBox;

	private JCheckBox emergencyStopCheckBox;
	
	private JCheckBox newOrderAfterFilling;	
	
	
	private JLabel activeSellOrdersLabel;
	private JLabel activeBuyOrdersLabel;	
	private JLabel nummbeOfFilledOrdersLabel;	
	private JLabel uPnlLabel; 
	
	private boolean emergencyStop = false;
	
	private JLabel stopSellPriceLabel;
	private JLabel stopBuyPriceLabel;
	
	private JCheckBox tradeNonStopCheckBox;
	
	private OrderBookView orderBookView;
	


	public TradingBotView(String apiKeyValue, String platformUrl, String tradingUrl) {

		model = new TradingBotModel(apiKeyValue, platformUrl, tradingUrl);

		// BeanAdapter beanAdapter = new BeanAdapter(this.model, true);
		ValueModel maximumOrdersAdapter = new PropertyAdapter<>(this.model, "maxOrdersAllowed", true);
		ValueModel maximumOrdersProcentAdapter = new PropertyAdapter<>(this.model, "maximumOrdersProcent", true);
		ValueModel numberOfContractsAdapter = new PropertyAdapter<>(this.model, "numberOfContracts", true);
		
		ValueModel createConterOrder = new PropertyAdapter<>(this.model, "createConterOrder", true);
		ValueModel emergencyStop = new PropertyAdapter<>(this.model, "emergencyStop", true);
		ValueModel newOrderAfterFillingModel = new PropertyAdapter<>(this.model, "newOrderAfterFilling", true);

		ValueModel minOrderTicksDistanceAdapter = new PropertyAdapter<>(this.model, "minOrderTicksDistance", true);
		ValueModel alarmDistanceAdapter = new PropertyAdapter<>(this.model, "alarmDistance", true);

		ValueModel cancelDistanceAdapter = new PropertyAdapter<>(this.model, "cancelDistance", true);
		ValueModel cancelOrdersAdapter = new PropertyAdapter<>(this.model, "cancelOrders", true);

		ValueModel urlAdapter = new PropertyAdapter<>(this.model, "url", true);
		ValueModel tradingUrlAdapter = new PropertyAdapter<>(this.model, "tradingUrl", true);
		ValueModel apiKeyAdapter = new PropertyAdapter<>(this.model, "apiKey", true);
		ValueModel numberOfOrdersToPlaceAdapter = new PropertyAdapter<>(this.model, "numberOfOrdersToPlace", true);
		
		

		ValueModel remainingOrdersAdapter = new PropertyAdapter<>(this.model, "remainingOrders", true);
		                                    
		ValueModel activeSellOrders  = new PropertyAdapter<>(this.model, "activeSellOrders", true);
		ValueModel activeBuyOrders = new PropertyAdapter<>(this.model, "activeBuyOrders", true);	
		ValueModel nummbeOfFilledOrders = new PropertyAdapter<>(this.model, "nummbeOfFilledOrders", true);	
		ValueModel uPnl = new PropertyAdapter<>(this.model, "uPnl", true);
		
		ValueModel stopSellPrice = new PropertyAdapter<>(this.model, "stopSellPrice", true);
		ValueModel stopBuyPrice = new PropertyAdapter<>(this.model, "stopBuyPrice", true);
		
		ValueModel tradeNonStop = new PropertyAdapter<>(this.model, "tradeNonStop", true);
		
		
		


		remainingOrdersLabel = BasicComponentFactory.createLabel(remainingOrdersAdapter);		
		activeSellOrdersLabel  = BasicComponentFactory.createLabel(activeSellOrders);		
		activeBuyOrdersLabel = BasicComponentFactory.createLabel(activeBuyOrders);		
		nummbeOfFilledOrdersLabel = BasicComponentFactory.createLabel(nummbeOfFilledOrders);		
		uPnlLabel = BasicComponentFactory.createLabel(uPnl);
		

		stopSellPriceLabel = BasicComponentFactory.createLabel(stopSellPrice);
		stopBuyPriceLabel = BasicComponentFactory.createLabel(stopBuyPrice);

		
		
		

		maximumOrders = BasicComponentFactory.createTextField(maximumOrdersAdapter, true);
		maximumOrdersProcent = BasicComponentFactory.createTextField(maximumOrdersProcentAdapter, true);
		maximumOrdersProcent.setEnabled(false);

		maximumOrders.setName("maximumOrders");
		maximumOrdersProcent.setName("maximumOrdersProcent");

		numberOfContracts = BasicComponentFactory.createTextField(numberOfContractsAdapter, true);
		minOrderTicksDistance = BasicComponentFactory.createTextField(minOrderTicksDistanceAdapter, true);

		alarmDistance = BasicComponentFactory.createTextField(alarmDistanceAdapter, true);
		cancelDistance = BasicComponentFactory.createTextField(cancelDistanceAdapter, true);
		cancelOrders = BasicComponentFactory.createCheckBox(cancelOrdersAdapter, "");
		newOrderAfterFilling = BasicComponentFactory.createCheckBox(newOrderAfterFillingModel, "Create Orders after Fill");

		apiKey = BasicComponentFactory.createTextField(apiKeyAdapter);
		url = BasicComponentFactory.createTextField(urlAdapter);
		this.tradingUrl = BasicComponentFactory.createTextField(tradingUrlAdapter);
		numberOfOrdersToPlace = BasicComponentFactory.createTextField(numberOfOrdersToPlaceAdapter);

		longtradeListModel = new ArrayListModel<TradeModel>();

		shorttradeListModel = new ArrayListModel<TradeModel>();

		SelectionInList longSelectionInList = new SelectionInList((ListModel) this.longtradeListModel);

		TradeModelTableAdapter longTableModelAdapter = new TradeModelTableAdapter(longSelectionInList,
				new String[] { "Price", "Number of contracts" });

		longTradeTable = new JTable(longTableModelAdapter);

		TableRowSorter<TradeModelTableAdapter> longSorter = new TableRowSorter<TradeModelTableAdapter>(
				longTableModelAdapter);
		longTradeTable.setRowSorter(longSorter);

		SelectionInList shortSelectionInList = new SelectionInList((ListModel) this.shorttradeListModel);

		TradeModelTableAdapter shortTableModelAdapter = new TradeModelTableAdapter(shortSelectionInList,
				new String[] { "Number of contracts", "Price" });
		shortTableModelAdapter.setLongModel(false);

		shortTradeTable = new JTable(shortTableModelAdapter);

		TableRowSorter<TradeModelTableAdapter> shortSorter = new TableRowSorter<TradeModelTableAdapter>(
				shortTableModelAdapter);
		shortTradeTable.setRowSorter(shortSorter);

		ValueHolder selectionModel = new ValueHolder();
		RadioButtonAdapter radioButtonAdapter1 = new RadioButtonAdapter(selectionModel, maximumOrders);
		absolutRadio.setModel(radioButtonAdapter1);

		RadioButtonAdapter radioButtonAdapter2 = new RadioButtonAdapter(selectionModel, maximumOrdersProcent);
		percentRadio.setModel(radioButtonAdapter2);

		selectionModel.setValue(maximumOrders);
		selectionModel.addPropertyChangeListener(new RadioChangeListener());

		this.validationResultModel.addPropertyChangeListener(new ValidationListener());
				
		
		createConterOrderCheckBox =  BasicComponentFactory.createCheckBox(createConterOrder, "Create Conter Order");
		emergencyStopCheckBox = BasicComponentFactory.createCheckBox(emergencyStop, "Emergency Stop");
		
		tradeNonStopCheckBox = BasicComponentFactory.createCheckBox(tradeNonStop, "Trade non Stop");

	}

	private JPanel createView() {

		FormLayout layout = new FormLayout("left:pref, 3dlu, pref, 70dlu, right:pref, 3dlu, pref,pref", // columns
				"p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, p, 10dlu, 10dlu, p, 10dlu ,p,10dlu,p,10dlu"); // rows

		FormLayout tradeLayout = new FormLayout("3dlu, pref, 3dlu,pref,3dlu, pref,3dlu,pref", // columns
				"p,3dlu,pref,p,3dlu,p,p"); // rows

		CellConstraints cc = new CellConstraints();

		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();

		builder.addSeparator("Order settings", cc.xyw(1, 1, 7));
		builder.addLabel("Maximum Orders:", cc.xy(1, 3));
		builder.add(maximumOrders, cc.xyw(3, 3, 5));

		builder.addLabel("Maximum Orders %:", cc.xy(1, 5));
		builder.add(maximumOrdersProcent, cc.xyw(3, 5, 5));

		builder.add(absolutRadio, cc.xy(3, 7));
		builder.add(percentRadio, cc.xy(5, 7));

		builder.addLabel("Number of contracts:", cc.xy(1, 9));
		builder.add(numberOfContracts, cc.xyw(3, 9, 5));

		builder.addLabel("Minimum Ticks Distance:", cc.xy(1, 11));
		builder.add(minOrderTicksDistance, cc.xyw(3, 11, 5));

		builder.addLabel("Alarm Tick Distance:", cc.xy(1, 13));
		builder.add(alarmDistance, cc.xyw(3, 13, 5));

		builder.addLabel("Cancel Ticks Distance:", cc.xy(1, 15));
		builder.add(cancelDistance, cc.xy(3, 15));
		builder.add(createConterOrderCheckBox, cc.xy(5, 15));
		builder.add(emergencyStopCheckBox, cc.xy(7, 15));
		
		
		

		builder.addLabel("Number of Orders to place:", cc.xy(1, 17));
		builder.add(numberOfOrdersToPlace, cc.xyw(3, 17, 5));

		builder.addLabel("Cancel Order:", cc.xy(1, 19));
		builder.add(cancelOrders, cc.xy(3, 19));
		builder.add(tradeNonStopCheckBox, cc.xy(5, 19));
		builder.add(newOrderAfterFilling, cc.xy(7, 19));
		
		
		

		builder.addLabel("Platform URL:", cc.xy(1, 21));
		builder.add(url, cc.xyw(3, 21, 5));

		builder.addLabel("API-Key:", cc.xy(1, 23));
		builder.add(apiKey, cc.xyw(3, 23, 5));

		// PanelBuilder tradeBuilder = new PanelBuilder(tradeLayout, new
		// FormDebugPanel());
		PanelBuilder tradeBuilder = new PanelBuilder(tradeLayout);

		priceLabel = new JLabel("11903", SwingConstants.CENTER);
		pricePanel = new JPanel();
		pricePanel.add(priceLabel);
		pricePanel.setBackground(Color.GREEN);

		remainingOrdersPanel = new JPanel();
		remainingOrdersPanel.add(remainingOrdersLabel);
		remainingOrdersPanel.setBackground(Color.CYAN);

		tradeBuilder.add(remainingOrdersPanel, cc.xy(2, 1));
		tradeBuilder.add(pricePanel, cc.xy(4, 1));
		
		JPanel activeBuyOrdersLabelPanel = new JPanel();
		activeBuyOrdersLabelPanel.add(activeBuyOrdersLabel);
		activeBuyOrdersLabelPanel.setBackground(Color.GREEN);
		
		JPanel activeSellOrdersLabelPanel = new JPanel();
		activeSellOrdersLabelPanel.add(activeSellOrdersLabel);
		activeSellOrdersLabelPanel.setBackground(Color.RED);
		
		JPanel nummbeOfFilledOrdersLabelPanel = new JPanel();
		nummbeOfFilledOrdersLabelPanel.add(nummbeOfFilledOrdersLabel);
		nummbeOfFilledOrdersLabelPanel.setBackground(Color.CYAN);
		
		JPanel uPnlLabelPanel = new JPanel();
		uPnlLabelPanel.add(uPnlLabel);
		uPnlLabelPanel.setBackground(Color.MAGENTA);
		
		JPanel stopBuyPriceLabelPanel = new JPanel();
		stopBuyPriceLabelPanel.add(stopBuyPriceLabel);
		stopBuyPriceLabelPanel.setBackground(Color.GREEN);
		
		JPanel stopSellPriceLabelPanel = new JPanel();
		stopSellPriceLabelPanel.add(stopSellPriceLabel);
		stopSellPriceLabelPanel.setBackground(Color.RED);

		
		tradeBuilder.add(activeBuyOrdersLabelPanel,cc.xy(2,6,CellConstraints.LEFT, CellConstraints.FILL));
		tradeBuilder.add(nummbeOfFilledOrdersLabelPanel,cc.xy(2,6,CellConstraints.CENTER, CellConstraints.FILL));
		tradeBuilder.add(activeSellOrdersLabelPanel,cc.xy(2,6,CellConstraints.RIGHT, CellConstraints.FILL));
		tradeBuilder.add(uPnlLabelPanel,cc.xy(2,7,CellConstraints.CENTER, CellConstraints.FILL));
		tradeBuilder.add(stopBuyPriceLabelPanel,cc.xy(2,7,CellConstraints.LEFT, CellConstraints.FILL));
		tradeBuilder.add(stopSellPriceLabelPanel,cc.xy(2,7,CellConstraints.RIGHT, CellConstraints.FILL));
		
		
		

		JScrollPane longScrollPane = new JScrollPane(longTradeTable);
		Dimension tableSize = longTradeTable.getPreferredScrollableViewportSize();
		tableSize.height = tableSize.height - 200;
		longScrollPane.setPreferredSize(tableSize);
		tradeBuilder.add(longScrollPane, cc.xy(2, 3));

		JScrollPane shortScrollPane = new JScrollPane(shortTradeTable);
		tableSize = shortTradeTable.getPreferredScrollableViewportSize();
		tableSize.height = tableSize.height - 200;
		shortScrollPane.setPreferredSize(tableSize);
		tradeBuilder.add(shortScrollPane, cc.xy(4, 3));

		JPanel tradePanel = tradeBuilder.getPanel();
		//tradePanel.setBorder(BorderFactory.createLineBorder(Color.red));

		// builder.add(new JLabel("Brat"), cc.xywh(8, 1, 1, 11));

		builder.add(tradePanel, cc.xywh(8, 3, 1, 20));

		builder.addLabel("Trading Url:", cc.xy(1, 25));
		builder.add(tradingUrl, cc.xyw(3, 25, 5));

		builder.add(new JButton(new StartAction()), cc.xy(1, 28));
		builder.add(new JButton(new CreateOrderAction()), cc.xy(3, 28));
		builder.add(new JButton(new TakeProfitAction()), cc.xy(4, 28));
		builder.add(new JButton(new CanelOrderAction()), cc.xy(5, 28));
		builder.add(new JButton(new PrintOrderBookAction()), cc.xy(7, 28));

		JComponent validationResultsComponent = ValidationResultViewFactory
				.createReportList(this.validationResultModel);
		
		JScrollPane messages = new JScrollPane(validationResultsComponent);

		builder.appendUnrelatedComponentsGapRow();
		builder.appendRow("fill:50dlu:grow");
		int columnCount = builder.getColumnCount();
		builder.nextLine(2);

		// builder.add(validationResultsComponent, columnCount);
		// builder.append(validationResultsComponent, columnCount);

		builder.add(messages, cc.xyw(1, 31, 8));

		return builder.getPanel();
	}

	public void createAndShowView() {
		// Display it all in a scrolling window and make the window appear
		JFrame frame = new JFrame("DigiText-Trading-Bot");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(createView());
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private static final class ValidationListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
//			String property = evt.getPropertyName();
//			if (ValidationResultModel.PROPERTY_RESULT.equals(property)) {
//				JOptionPane.showMessageDialog(null, "At least one validation result changed");
//			} else if (ValidationResultModel.PROPERTY_MESSAGES.equals(property)) {
//				if (Boolean.TRUE.equals(evt.getNewValue())) {
//					JOptionPane.showMessageDialog(null, "Overall validation changed");
//				}
//			}
		}
	}

	private ValidationResult validateForm() {
		//ValidationResult validationResult = new ValidationResult();

		if (maximumOrders.getText().isEmpty() && absolutRadio.isSelected()) {
			validationResult.addError("Maximum Order has to be set!");
		}

		if (maximumOrdersProcent.getText().isEmpty() && percentRadio.isSelected()) {
			validationResult.addError("Maximum Order Percent has to be set!");
		}

		return validationResult;
	}

    private class PrintOrderBookAction extends AbstractAction{
    	
		public PrintOrderBookAction() {
			super("Show Orderbook");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			orderBookView.getOrderBook().clearTable();
			orderBookView.setTradingSocket(tradingSocket);
			orderBookView.createAndShowView();		
		}    	
    }	
	
	private class StartAction extends AbstractAction {
		public StartAction() {
			super("Init System");
		}

		public void actionPerformed(ActionEvent event) {
			emergencyStop = false;
			validationResult = new ValidationResult();
			validationResult = validateForm();
			validationResultModel.setResult(validationResult);			
			
			if (validationResultModel.hasErrors()) {
				return;
			}
			
			tradeSocket = null;
			orderBookSocket = null;
			tradingSocket = null;

			tradeSocket = new TradesSocket();
			orderBookSocket = new OrderBookSocket();
			tradingSocket = new TradingSocket(model.getApiKey());
			orderManager = new OrderManager(tradingSocket, tradeSocket, model);
			orderManager.setShorttradeListModel(shorttradeListModel);
			orderManager.setLongtradeListModel(longtradeListModel);
			//orderManager.initInfinityTradeTask();
			log.info(model.toString());
			try {
				tradeSocket.registerPriceChangeListener(orderBookView);
				tradeSocket.registerPriceChangeListener(orderManager);
				tradeSocket.start(model.getUrl(), priceLabel);
				orderBookSocket.start(model.getUrl(), priceLabel);
				orderBookSocket.registerOrderBookListener(orderManager);	
				orderBookSocket.registerOrderBookListener(orderBookView);
				
				
				tradingSocket.registerOrderPlacementListener(TradingBotView.this);
				tradingSocket.registerOrderPlacementListener(orderManager);
				tradingSocket.registerTraderStatusListener(orderManager);
				tradingSocket.registerTraderStatusListener(TradingBotView.this);

				tradingSocket.registerConnectionListene(TradingBotView.this);
				tradeSocket.registerConnectionListene(TradingBotView.this);
				orderBookSocket.registerConnectionListene(TradingBotView.this);
				
				tradingSocket.registerOrderPlacementListener(orderBookView);

				tradingSocket.start(model.getTradingUrl(), priceLabel);
			} catch (URISyntaxException e) {
				log.error(e);
			} catch (InterruptedException e) {
				log.error(e);
			}

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				log.error(e);
			}

			//orderManager.createOrders(model);
		}
	}

	private class RadioChangeListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
			((JTextField) evt.getNewValue()).setEnabled(true);
			((JTextField) evt.getOldValue()).setEnabled(false);
			((JTextField) evt.getOldValue()).setText("");

			if (((JTextField) evt.getNewValue()).getName().contentEquals("maximumOrdersProcent")) {
				model.setAbsolut(false);
			} else {
				model.setAbsolut(true);
			}
		}
	}

	@Override
	public void onOrderCreation(OrderStatus status) {
		log.info("######### Order Creation #######################");
		log.info(status.toString());
		log.info("###############################################");

		if (status.getCh() == null) {
			return;
		}
		
		if (!(status.getData() == null || status.getData().getOrderStatus() == null)
				&& status.getData().getOrderStatus().contentEquals(OrderStatusEnum.ACCEPTED.name())) {
			if (status.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {

				TradeModel longTrade = new TradeModel(status.getData().getPx(),
						status.getData().getQty());
				longTrade.setId(status.getData().getClOrdId());

				boolean containsOrderAlready = longtradeListModel.stream()
						.anyMatch(l -> l.getPrice() == status.getData().getPx());
				log.debug("Order would be added to table if true " + containsOrderAlready);
				if (!containsOrderAlready) {
					longtradeListModel.add(longTrade);
				}

			}

			if (status.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {

				TradeModel shortTrade = new TradeModel(status.getData().getPx(),
						status.getData().getQty());
				shortTrade.setId(status.getData().getClOrdId());

				boolean containsOrderAlready = shorttradeListModel.stream()
						.anyMatch(l -> l.getPrice() == status.getData().getPx());
				log.debug("Order would be added to table if true " + containsOrderAlready);
				if (!containsOrderAlready) {
					shorttradeListModel.add(shortTrade);
				}
			}
		}
	}

	@Override
	public void onOrderCancellation(OrderCancelledStatus cancelledOrderStatus) {
		if (cancelledOrderStatus.getData().getOrderStatus().contentEquals(OrderStatusEnum.REJECTED.name())) {
			return;
		}

		List<Integer> delPrices = new ArrayList<>();

		if (cancelledOrderStatus.getData().getOrders().size() > 0) {
			cancelledOrderStatus.getData().getOrders().stream().forEach(o -> delPrices.add(o.getPx()));
		}

		List<TradeModel> delTrades = new ArrayList<>();

		for (TradeModel trade : longtradeListModel) {
			for (Integer delPrice : delPrices) {
				if (trade.getPrice() == delPrice) {
					delTrades.add(trade);
				}
			}
		}

		longtradeListModel.removeAll(delTrades);

		delTrades.clear();

		for (TradeModel trade : shorttradeListModel) {
			for (Integer delPrice : delPrices) {
				if (trade.getPrice() == delPrice) {
					delTrades.add(trade);
				}
			}
		}
		shorttradeListModel.removeAll(delTrades);
	}

	private void cancelTrade(TradeModel trade) {
		log.info("##########################CANELING TRADE ->" + trade.getId());
		CancelOrder cancelOrder = new CancelOrder();
		cancelOrder.setMethod("cancelOrder");
		CancelOrderParams params = new CancelOrderParams();
		params.setSymbol("BTCUSD-PERP");
		params.setClOrdId(trade.getId());
		cancelOrder.setParams(params);
		tradingSocket.sendMessage(cancelOrder);
	}

	private void closeContract(TradeModel trade) {
		log.info("##########################closing Contract ->" + trade.getId());
		CancelOrder cancelOrder = new CancelOrder();
		cancelOrder.setMethod("cancelOrder");
		CancelOrderParams params = new CancelOrderParams();
		params.setSymbol("BTCUSD-PERP");
		params.setClOrdId(trade.getId());
		cancelOrder.setParams(params);
		tradingSocket.sendMessage(cancelOrder);
	}

	private class CanelOrderAction extends AbstractAction {
		public CanelOrderAction() {
			super("CancelOrder");
		}

		public void actionPerformed(ActionEvent event) {
			//orderManager.closeCurrentOpenContracts();
			emergencyStop = true;
			orderManager.emergencyStop();
			orderManager.shutDownInfinityTrade();
		}
	}
	
	private class TakeProfitAction extends AbstractAction {
		public TakeProfitAction() {
			super("Take Profit");
		}

		public void actionPerformed(ActionEvent event) {
			orderManager.takeProfitForButton();
		}
	}

	private class CreateOrderAction extends AbstractAction {
		public CreateOrderAction() {
			super("Create Order");
		}

		public void actionPerformed(ActionEvent event) {

			validationResult = new ValidationResult();
			validationResult = validateForm();
			validationResultModel.setResult(validationResult);			
			
			if (validationResultModel.hasErrors()) {
				return;
			}

			
			orderManager.updateNumberOfActiveOrders();
			Runnable createOrderRunner = new Runnable() {
				
				@Override
				public void run() {
					orderManager.createOrders(model);
					
				}
			};
			
			Thread createOrderThread = new Thread(createOrderRunner);
			createOrderThread.start();
			conterLog.debug("------------------------------------ Starting Thread ----------------"+createOrderThread.getId());
		}
	}

	private class CloseContractAction extends AbstractAction {
		public CloseContractAction() {
			super("Cancel Contract");
		}

		public void actionPerformed(ActionEvent event) {
			
		}
	}

	@Override
	public void onOrderFilled(OrderFilled orderFilled) {

	}

	@Override
	public void onContractClosed(ContractClosed contractClosed) {

	}

	@Override
	public void onTraderStatuschanged(TraderStatusResponse traderStatus) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnectionClosed(BotSocketEnum socket) {	
	
		if (!emergencyStop) {
			validationResult.addError(socket.name()+"Connection closed !!! Connection Closed!!! Connection closed !!! Connection Closed!!!");
			validationResultModel.setResult(validationResult);
			playAlarmSound();	
		} else {
			validationResult.addInfo(socket.name()+"Connection closed !!! Connection Closed!!! Connection closed !!! Connection Closed!!!");
			validationResultModel.setResult(validationResult);
			return;
		}
		
		

		
		
		switch(socket) {
		case ORDERBOOK:reconnectOrderBook();		
		case TRADES:reconnectTrades();
		case TRADING:reconnectTrading();
		default:;
		}
		
		
		
		//tradeSocket.reconnectIfNecessary();
		//tradeSocket.reconnectIfNecessary();
		
//		validationResult = new ValidationResult();
//		validationResult.addInfo("Connections Established");
//		validationResultModel.setResult(validationResult);		

	}

	private void playAlarmSound() {
		AudioInputStream audioInput = null;
		try {
			audioInput = AudioSystem.getAudioInputStream(TradingBotView.class.getResource("/sound/boot.wav"));
		} catch (UnsupportedAudioFileException e2) {
			log.error(e2);
		} catch (IOException e2) {
			log.error(e2);
		}
		Clip clip = null;
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e1) {
			log.error(e1);
		}
		try {
			clip.open(audioInput);
		} catch (LineUnavailableException e1) {
			log.error(e1);
		} catch (IOException e1) {
			log.error(e1);
		}
		clip.start();
	}


	private void reconnectTrading() {
		Runnable reconnectRunnerTrading = new Runnable() {
			
			@Override
			public void run() {
				tradingSocket.reconnectIfNecessary();
			}
		};
		
		

		
		Thread reconnectTrading = new Thread(reconnectRunnerTrading);
		reconnectTrading.start();		

		
	}

	private void reconnectTrades() {
		Runnable reconnectRunnerTradeSocket = new Runnable() {
			
			@Override
			public void run() {
				tradeSocket.reconnectIfNecessary();
			}
		};


		Thread reconnectTrade = new Thread(reconnectRunnerTradeSocket);
		reconnectTrade.start();

	}

	private void reconnectOrderBook() {
		Runnable reconnectRunnerOrderBook = new Runnable() {
			
			@Override
			public void run() {
				orderBookSocket.reconnectIfNecessary();
			}
		};

		Thread reconnectOrderBook = new Thread(reconnectRunnerOrderBook);
		reconnectOrderBook.start();
	}

	@Override
	public synchronized void onConnectionEstablished(BotSocketEnum socket) {
		validationResult.addInfo(socket.name() + " Connections Established");
		validationResultModel.setResult(validationResult);
	}

	
	@Override
	public void onRequestLimitExceeded() {
		// TODO Auto-generated method stub
		
	}
	public OrderBookView getOrderBookView() {
		return orderBookView;
	}

	public void setOrderBookView(OrderBookView orderBookView) {
		this.orderBookView = orderBookView;
	}

	public TradingSocket getTradingSocket() {
		return tradingSocket;
	}

	public void setTradingSocket(TradingSocket tradingSocket) {
		this.tradingSocket = tradingSocket;
	}

}
