package rawai.it.com.view.orderbook;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.table.TableRowSorter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import rawai.it.com.listeners.OrderBookListener;
import rawai.it.com.listeners.OrderPlacementListener;
import rawai.it.com.listeners.PriceChangedListener;
import rawai.it.com.model.OrderBookModel;
import rawai.it.com.model.json.orderbook.OrderBookData;
import rawai.it.com.model.json.trading.contractclosed.ContractClosed;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.view.OrderSide;
import rawai.it.com.view.OrderStatusEnum;
import rawai.it.com.websockets.TradingSocket;

public class OrderBookView extends JPanel implements OrderBookListener, PriceChangedListener, OrderPlacementListener {

	private static final long serialVersionUID = -3857017128971358208L;

	private ArrayListModel<OrderBookTableEntry> orderBookEntries;
	private OrderBook orderBook;
	private JTable orderBookTable;
	private OrderBookPriceCellRenderer bookCellRenderer = new OrderBookPriceCellRenderer();
	private BuySellCellRenderer buySellCellRednerer = new BuySellCellRenderer();
	private OrderBookAdapter orderBookAdapter;

	private JLabel nummbeOfFilledOrdersLabel;
	private TradingSocket tradingSocket;

	private OrderBookModel model = new OrderBookModel();

	public OrderBookView(TradingSocket tradingSocket) {
		this.tradingSocket = tradingSocket;
		this.orderBookEntries = new ArrayListModel<>();

		SelectionInList orderBookList = new SelectionInList((ListModel) this.orderBookEntries);

		orderBookAdapter = new OrderBookAdapter(orderBookList, new String[] { "Buy", "Bids", "Price", "Asks", "Sell" });

		orderBookTable = new JTable(orderBookAdapter);

		orderBook = new OrderBook(this.orderBookEntries);

		ValueModel nummbeOfFilledOrders = new PropertyAdapter<>(this.model, "nummbeOfFilledOrders", true);
		nummbeOfFilledOrdersLabel = BasicComponentFactory.createLabel(nummbeOfFilledOrders);
	}

	public void createAndShowView() {
		// Display it all in a scrolling window and make the window appear
		JFrame frame = new JFrame("OrderBookView");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(createView());
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private Component createView() {
		FormLayout layout = new FormLayout("500dlu", // columns
				"100dlu,200dlu,100dlu"); // rows
		CellConstraints cc = new CellConstraints();

		PanelBuilder builder = new PanelBuilder(layout);

		// PanelBuilder builder = new PanelBuilder(layout, new FormDebugPanel());

		builder.setDefaultDialogBorder();

		JScrollPane orderBookScrollPane = new JScrollPane(orderBookTable);
		Dimension tableSize = orderBookTable.getPreferredScrollableViewportSize();
		orderBookScrollPane.setPreferredSize(tableSize);
		builder.add(orderBookScrollPane, cc.xy(1, 2, CellConstraints.FILL, CellConstraints.TOP));

		FormLayout number = new FormLayout("100dlu", // columns
				"100dlu"); // rows
		CellConstraints numberCC = new CellConstraints();

		PanelBuilder numberbuilder = new PanelBuilder(number);

		JPanel panel = new JPanel();

		panel.add(nummbeOfFilledOrdersLabel);

		nummbeOfFilledOrdersLabel.setFont(new Font("Arial", Font.PLAIN, 30));
		panel.setBorder(BorderFactory.createLineBorder(Color.RED, 5));

		numberbuilder.add(panel, numberCC.xy(1, 1));

		builder.add(numberbuilder.getPanel(), cc.xy(1, 1, CellConstraints.CENTER, CellConstraints.FILL));

		orderBookTable.setFont(new Font("Arial", Font.PLAIN, 30));
		orderBookTable.setRowHeight(30);

		orderBookTable.getColumnModel().getColumn(2).setCellRenderer(bookCellRenderer);
		orderBookTable.getColumnModel().getColumn(0).setCellRenderer(buySellCellRednerer);
		orderBookTable.getColumnModel().getColumn(4).setCellRenderer(buySellCellRednerer);

		FormLayout buttonLayout = new FormLayout("50dlu,10dlu,50dlu,10dlu,50dlu,10dlu,50dlu,10dlu,50dlu,10dlu", // columns
				"50dlu"); // rows
		PanelBuilder buttonBuilder = new PanelBuilder(buttonLayout);
		// CellConstraints numberCC = new CellConstraints();

		buttonBuilder.add(new JButton(new StopAction("Start", tradingSocket, "{\"msg\":\"start\"}")), cc.xy(1, 1));
		buttonBuilder.add(new JButton(new StopAction("Stop", tradingSocket, "{\"msg\":\"stop\"}")), cc.xy(3, 1));
		buttonBuilder.add(new JButton(new NextAction("Next", tradingSocket, "{\"msg\":\"next\"}")), cc.xy(5, 1));
		buttonBuilder.add(new JButton(new PlayAction("Play", tradingSocket, "{\"msg\":\"play\"}")), cc.xy(7, 1));
		buttonBuilder.add(new JButton(new ForwardAction("Forward", tradingSocket, "{\"msg\":\"forward\"}")),
				cc.xy(9, 1));

		builder.add(buttonBuilder.getPanel(), cc.xy(1, 3, CellConstraints.CENTER, CellConstraints.FILL));

		return builder.getPanel();
	}

	@Override
	public void onOrderBookChanged(OrderBookData orderBookData) {

		if (orderBookData.getData() == null || orderBookData.getData().getAsks() == null
				|| orderBookData.getData().getBids() == null) {
			return;
		}

		orderBook.updateOrderBook(orderBookData);
		orderBookAdapter.fireTableDataChanged();
	}

	@Override
	public void onPriceChanged(String price) {
		int rowCounter = 0;
		bookCellRenderer.setPrice(price);
		bookCellRenderer.repaint();

	}

	@Override
	public void onPriceFlipChanged(int priceFlip) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOrderCreation(OrderStatus status) {
		if (status.getData().getOrderStatus().contentEquals(OrderStatusEnum.ACCEPTED.name())) {
			orderBook.updateOrderBook(status);
			orderBookAdapter.fireTableDataChanged();
		}
	}

	@Override
	public void onOrderCancellation(OrderCancelledStatus cancelledOrderStatus) {
		orderBook.updateOrderBook(cancelledOrderStatus);
		orderBookAdapter.fireTableDataChanged();
	}

	@Override
	public void onOrderFilled(OrderFilled orderFilled) {
		OrderSide side = OrderSide.valueOf(orderFilled.getData().getOrderSide());
		if (side.equals(OrderSide.BUY)) {
			model.setNummbeOfFilledOrders("+" + orderFilled.getData().getPositionContracts().toString());
		} else {
			model.setNummbeOfFilledOrders("-" + orderFilled.getData().getPositionContracts().toString());
		}
		if (orderFilled.isSystemOrder()) {
			bookCellRenderer.setFillPrice(orderFilled.getData().getPx().toString());
		}

		orderBook.updateOrderBook(orderFilled);
		orderBookAdapter.fireTableDataChanged();
	}

	@Override
	public void onContractClosed(ContractClosed contractClosed) {
		// TODO Auto-generated method stub

	}

	public TradingSocket getTradingSocket() {
		return tradingSocket;
	}

	public void setTradingSocket(TradingSocket tradingSocket) {
		this.tradingSocket = tradingSocket;
	}

	public OrderBook getOrderBook() {
		return orderBook;
	}

	public void setOrderBook(OrderBook orderBook) {
		this.orderBook = orderBook;
	}

}
