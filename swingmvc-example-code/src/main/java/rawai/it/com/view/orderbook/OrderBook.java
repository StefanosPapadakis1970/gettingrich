package rawai.it.com.view.orderbook;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;

import rawai.it.com.controller.Constants;
import rawai.it.com.model.json.orderbook.OrderBookData;
import rawai.it.com.model.json.trading.order.cancelled.status.Order;
import rawai.it.com.model.json.trading.order.cancelled.status.OrderCancelledStatus;
import rawai.it.com.model.json.trading.order.status.OrderStatus;
import rawai.it.com.model.json.trading.orderfilled.OrderFilled;
import rawai.it.com.view.OrderSide;

public class OrderBook implements Constants {
	private static Logger log = LogManager.getLogger("ordermanager");
	private ArrayListModel<OrderBookTableEntry> orderBookEntries;
	int counter = 0;

	public OrderBook(ArrayListModel<OrderBookTableEntry> orderBookEntries) {
		super();
		this.orderBookEntries = orderBookEntries;
	}

	public void updateOrderBook(OrderCancelledStatus cancelledOrderStatus) {

		for (Order cancelledOrder : cancelledOrderStatus.getData().getOrders()) {

			Stream<OrderBookTableEntry> entries = orderBookEntries.stream()
					.filter(new Predicate<OrderBookTableEntry>() {
						@Override
						public boolean test(OrderBookTableEntry t) {
							return t.getPrice().contentEquals(String.valueOf(cancelledOrder.getPx()));
						}
					});

			Object[] entry = entries.toArray();

			log.debug("Filtered Entries Size -> " + entry.length);

			if (entry.length == 0) {
				return;
			}

			OrderSide side = OrderSide.valueOf(cancelledOrder.getOrderSide());
			switch (side) {
			case BUY:
				((OrderBookTableEntry) entry[0]).getBuyOrder().getData().setOrigQty(((OrderBookTableEntry) entry[0]).getBuyOrder().getData().getOrigQty() - cancelledOrder.getOrigQty());
				((OrderBookTableEntry) entry[0]).getBuyOrder().getData().setQty(((OrderBookTableEntry) entry[0]).getBuyOrder().getData().getQty() - cancelledOrder.getQty());
				break;
			case SELL:
				((OrderBookTableEntry) entry[0]).getSellOrder().getData().setOrigQty(((OrderBookTableEntry) entry[0]).getSellOrder().getData().getOrigQty() - cancelledOrder.getOrigQty());
				((OrderBookTableEntry) entry[0]).getSellOrder().getData().setQty(((OrderBookTableEntry) entry[0]).getSellOrder().getData().getQty() - cancelledOrder.getQty());				
				break;
			default:
				break;
			}

		}
	}

	public void updateOrderBook(OrderStatus status) {

		Stream<OrderBookTableEntry> entries = orderBookEntries.stream().filter(new Predicate<OrderBookTableEntry>() {
			@Override
			public boolean test(OrderBookTableEntry t) {
				return t.getPrice().contentEquals(String.valueOf(status.getData().getPx()));
			}
		});

		Object[] entry = entries.toArray();

		log.debug("Filtered Entries Size -> " + entry.length);

		if (entry.length == 0) {
			return;
		}

		if (status.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
			if (status.isConterOrder()) {
				((OrderBookTableEntry) entry[0]).getSellOrder().getData().setClOrdId(status.getData().getClOrdId());
				((OrderBookTableEntry) entry[0]).getSellOrder().getData()
						.setOrigClOrdId(status.getData().getOrigClOrdId());
				((OrderBookTableEntry) entry[0]).getSellOrder().getData().setOrigQty(status.getData().getOrigQty()
						+ ((OrderBookTableEntry) entry[0]).getSellOrder().getData().getOrigQty());
			} else {
				((OrderBookTableEntry) entry[0]).setSellOrder(status.toOrderFilled());
			}
		}

		if (status.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
			if (status.isConterOrder()) {
				((OrderBookTableEntry) entry[0]).getBuyOrder().getData().setClOrdId(status.getData().getClOrdId());
				((OrderBookTableEntry) entry[0]).getBuyOrder().getData()
						.setOrigClOrdId(status.getData().getOrigClOrdId());
				((OrderBookTableEntry) entry[0]).getBuyOrder().getData().setOrigQty(status.getData().getOrigQty()
						+ ((OrderBookTableEntry) entry[0]).getBuyOrder().getData().getOrigQty());
			} else {
				((OrderBookTableEntry) entry[0]).setBuyOrder(status.toOrderFilled());
			}
		}
	}

	public void updateOrderBook(OrderBookData orderBook) {

		List<List<Integer>> asks = orderBook.getData().getAsks().subList(0, 5);
		Collections.reverse(asks);
		List<List<Integer>> bids = orderBook.getData().getBids().subList(0, 5);

		if (orderBookEntries.size() == 0) {
			if (asks != null) {
				asks.stream().forEach(ask -> {
					orderBookEntries.add(new OrderBookTableEntry(new OrderFilled(), new OrderFilled(),
							ask.get(1).toString(), "", ask.get(0).toString()));
				});
			}

			// auffuellen
			int lowestAsk = asks.get(asks.size() - 1).get(0);
			int higestBid = bids.get(0).get(0);

			if (lowestAsk - higestBid > tickValue) {
				int numberOfMissingPrices = ((lowestAsk - higestBid) / tickValue) - 1;
				for (int i = 1; i <= numberOfMissingPrices; i++) {
					orderBookEntries.add(new OrderBookTableEntry(new OrderFilled(), new OrderFilled(), "", "",
							"" + (lowestAsk - (i * tickValue))));
				}
			}

			if (bids != null) {
				bids.stream().forEach(bid -> {
					orderBookEntries.add(new OrderBookTableEntry(new OrderFilled(), new OrderFilled(), "",
							bid.get(1).toString(), bid.get(0).toString()));
				});
			}
		} else {
			updateOrderBookEntries(orderBook);
		}

	}

	public void updateOrderBook(OrderFilled orderFilled) {
		for (OrderBookTableEntry entry : orderBookEntries) {
			if (entry.getPrice().contentEquals(orderFilled.getData().getPx().toString())) {
				if (orderFilled.getData().getOrderSide().contentEquals(OrderSide.SELL.name())) {
					int diff = orderFilled.getData().getQty();
					// entry.setSellOrder(""+diff+" # "+orderFilled.getData().getOrigQty());
					if (orderFilled.isConterOrder()) {
						entry.getSellOrder().getData().setClOrdId(orderFilled.getData().getClOrdId());
						entry.getSellOrder().getData().setOrigClOrdId(orderFilled.getData().getOrigClOrdId());
						entry.getSellOrder().getData().setNewClOrdId(orderFilled.getData().getNewClOrdId());
						entry.getSellOrder().getData().setOrigQty(
								entry.getSellOrder().getData().getOrigQty() - orderFilled.getData().getOrigQty());
					} else {
						entry.setSellOrder(orderFilled);
					}
				}
				if (orderFilled.getData().getOrderSide().contentEquals(OrderSide.BUY.name())) {
					if (orderFilled.isConterOrder()) {
						entry.getBuyOrder().getData().setClOrdId(orderFilled.getData().getClOrdId());
						entry.getBuyOrder().getData().setOrigClOrdId(orderFilled.getData().getOrigClOrdId());
						entry.getBuyOrder().getData().setOrigQty(
								entry.getSellOrder().getData().getOrigQty() - orderFilled.getData().getOrigQty());
					} else {
						entry.setBuyOrder(orderFilled);
					}
				}
			}
		}
	}

	// Was wenn Preis nicht da ist??
	private void updateOrderBookEntries(OrderBookData orderBook) {
		List<List<Integer>> asks = orderBook.getData().getAsks().subList(0, 5);
		Collections.reverse(asks);
		List<List<Integer>> bids = orderBook.getData().getBids().subList(0, 5);

		for (List<Integer> ask : asks) {
			for (OrderBookTableEntry entry : orderBookEntries) {
				if (entry.getPrice().contentEquals(ask.get(0).toString())) {
					entry.setAsks(ask.get(1).toString());
					entry.setBids("");
				}
			}
		}

		for (List<Integer> bid : bids) {
			for (OrderBookTableEntry entry : orderBookEntries) {
				if (entry.getPrice().contentEquals(bid.get(0).toString())) {
					entry.setBids(bid.get(1).toString());
					entry.setAsks("");
				}
			}
		}

		log.debug("--->>> OrderBookEntries " + orderBookEntries);
		for (OrderBookTableEntry entry : orderBookEntries) {
			log.debug(entry.getBuyOrder() + " " + entry.getBids() + " " + entry.getPrice() + " " + entry.getAsks() + " "
					+ entry.getSellOrder());
		}
	}

	public void clearTable() {
		for (OrderBookTableEntry entry : orderBookEntries) {
			entry.setSellOrder(new OrderFilled());
			entry.setBuyOrder(new OrderFilled());
			entry.setAsks("");
			entry.setBids("");
			entry.setPrice("");
		}
	}
}
