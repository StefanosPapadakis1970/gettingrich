package rawai.it.com.view.orderbook;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import rawai.it.com.websockets.TradingSocket;

public class BaseSocketAction extends AbstractAction {

	protected TradingSocket tradingSocket;	
	protected String message;
	

	public BaseSocketAction(String name, TradingSocket tradingSocket, String message) {
		super(name);
		this.tradingSocket = tradingSocket;
		this.message = message;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		tradingSocket.sendMessage(message);		
	}
}
