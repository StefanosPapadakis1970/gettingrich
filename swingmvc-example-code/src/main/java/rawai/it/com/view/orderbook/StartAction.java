package rawai.it.com.view.orderbook;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import rawai.it.com.websockets.TradingSocket;

public class StartAction extends BaseSocketAction {

	public StartAction(String name, TradingSocket tradingSocket, String message) {
		super(name, tradingSocket, message);
	}
}
