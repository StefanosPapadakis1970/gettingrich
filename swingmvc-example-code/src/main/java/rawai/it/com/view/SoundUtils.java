package rawai.it.com.view;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SoundUtils {

  private static Logger log = LogManager.getLogger("ordermanager");
  public static float SAMPLE_RATE = 8000f;

  public static void tone(int hz, int msecs) 
     throws LineUnavailableException 
  {
     tone(hz, msecs, 1.0);
  }

  public static void tone(int hz, int msecs, double vol)
      throws LineUnavailableException 
  {
    byte[] buf = new byte[1];
    AudioFormat af = 
        new AudioFormat(
            SAMPLE_RATE, // sampleRate
            8,           // sampleSizeInBits
            1,           // channels
            true,        // signed
            false);      // bigEndian
    SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
    sdl.open(af);
    sdl.start();
    for (int i=0; i < msecs*8; i++) {
      double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
      buf[0] = (byte)(Math.sin(angle) * 127.0 * vol);
      sdl.write(buf,0,1);
    }
    sdl.drain();
    sdl.stop();
    sdl.close();
  }

  
	public static void playBootSound() {
		AudioInputStream audioInput = null;
		try {
			audioInput = AudioSystem.getAudioInputStream(TradingBotView.class.getResource("/sound/boot.wav"));
		} catch (UnsupportedAudioFileException e2) {
			log.error(e2);
		} catch (IOException e2) {
			log.error(e2);
		}
		Clip clip = null;
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e1) {
			log.error(e1);
		}
		try {
			clip.open(audioInput);
		} catch (LineUnavailableException e1) {
			log.error(e1);
		} catch (IOException e1) {
			log.error(e1);
		}
		clip.start();
	}
	
	public static void playAlarmSound() {
		AudioInputStream audioInput = null;
		try {
			audioInput = AudioSystem.getAudioInputStream(TradingBotView.class.getResource("/sound/alarm.wav"));
		} catch (UnsupportedAudioFileException e2) {
			log.error(e2);
		} catch (IOException e2) {
			log.error(e2);
		}
		Clip clip = null;
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e1) {
			log.error(e1);
		}
		try {
			clip.open(audioInput);
		} catch (LineUnavailableException e1) {
			log.error(e1);
		} catch (IOException e1) {
			log.error(e1);
		}
		clip.start();
	}

  
  public static void main(String[] args) throws Exception {
    SoundUtils.tone(1000,100);
    Thread.sleep(1000);
    SoundUtils.tone(100,1000);
    Thread.sleep(1000);
    SoundUtils.tone(5000,100);
    Thread.sleep(1000);
    SoundUtils.tone(400,500);
    Thread.sleep(1000);
    SoundUtils.tone(400,500, 0.2);

  }
}