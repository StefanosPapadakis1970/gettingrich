package rawai.it.com.controller;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.TimeToLive;
import org.mockserver.matchers.Times;
import org.mockserver.mock.Expectation;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

public class TestMockServer {
	 
	    private static ClientAndServer mockServer;
	 
	    @BeforeClass
	    public static void startServer() {
	        //mockServer = ClientAndServer.startClientAndServer(1080);
	    }
	 
	    @AfterClass 
	    public static void stopServer() { 
	        //mockServer.stop();
	    	
	    	
	    }

	    @Test
	    public void t1() {
	    	MockServerClient mockServerClient = new MockServerClient("127.0.0.1", 1080);
	    	
	    	 
	    	

	        mockServerClient
	                .when(
	                        HttpRequest.request()
	                                .withMethod("GET")
	                                .withPath("/api/hello"),
	                        Times.unlimited(),
	                        TimeToLive.unlimited())
	                .respond(
	                        HttpResponse.response()
	                                .withBody("Hello")
	                                .withStatusCode(200)
	                );        
	        
	    }
}