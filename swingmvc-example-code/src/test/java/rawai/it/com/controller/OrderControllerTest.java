package rawai.it.com.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import rawai.it.com.model.json.orderbook.Data;
import rawai.it.com.model.json.orderbook.OrderBookData;

public class OrderControllerTest {
	private static Logger creationLog = LogManager.getLogger("ordercreation");

	@Test
	public void fillupAsks() {
		OrderManager orderManager = new OrderManager(null, null, null);
		List<List<Integer>> asks = new ArrayList<List<Integer>>();
		List<List<Integer>> bids = new ArrayList<List<Integer>>();

		asks.add(new ArrayList<Integer>(Arrays.asList(10755, 967)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10760, 992)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10765, 3825)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10770, 5404)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10775, 4242)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10780, 3071)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10785, 1159)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10790, 4541)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10795, 3617)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10800, 4312)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10805, 1776)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10810, 2027)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10815, 1366)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10820, 3897)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10825, 100)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10830, 2396)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10835, 100)));
//		asks.add(new ArrayList<Integer>(Arrays.asList(10840, 245)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10845, 261)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10850, 170)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10855, 200)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10860, 200)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10865, 150)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10875, 150)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10880, 10)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10885, 10)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10890, 60)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10895, 111)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10900, 31)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10905, 10)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10910, 15)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10915, 25)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10920, 400)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10925, 400)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10930, 150)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10940, 3)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10945, 25)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10950, 20)));
		asks.add(new ArrayList<Integer>(Arrays.asList(10955, 4)));

		bids.add(new ArrayList<Integer>(Arrays.asList(10740, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10735, 9)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10730, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10725, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10720, 6)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10715, 5)));
		//bids.add(new ArrayList<Integer>(Arrays.asList(10710, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10705, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10700, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10695, 3)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10690, 1)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10685, 1)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10680, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10675, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10670, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10665, 7)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10660, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10655, 1)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10650, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10640, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10635, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10630, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10625, 1)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10620, 5)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10615, 9)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10610, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10605, 7)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10600, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10595, 4)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10590, 1)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10585, 5)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10580, 2)));
//		bids.add(new ArrayList<Integer>(Arrays.asList(10575, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10570, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10565, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10560, 6)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10555, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10550, 1)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10545, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10540, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10535, 2)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10530, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10525, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10520, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10515, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10510, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10505, 3)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10500, 5)));
		bids.add(new ArrayList<Integer>(Arrays.asList(10495, 2)));

		OrderBookData orderBook = new OrderBookData();
		Data data = new Data();
		orderBook.setData(data);
		data.setAsks(asks);

		orderManager.setOrderBook(orderBook);
		orderManager.fillUpOrderBookGaps(asks);
		orderManager.fillUpOrderBookBidGaps(bids);


	}
}
